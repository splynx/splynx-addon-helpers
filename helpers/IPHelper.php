<?php

namespace splynx\helpers;

use yii\base\BaseObject;
use yii\validators\UrlValidator;

class IPHelper extends BaseObject
{
    /**
     * @return array<array{'ip': string,'mac': string}>
     * @deprecated Deprecated because use `ifconfig`
     */
    private static function getIpsMacsPairs()
    {
        $if = '';
        if (is_file('/bin/ifconfig')) {
            $if = 'export LC_ALL=C LANG=C && /bin/ifconfig';
        } elseif (is_file('/sbin/ifconfig')) {
            $if = 'export LC_ALL=C LANG=C && /sbin/ifconfig';
        }
        $if .= " | grep -v 'inet6'";

        exec($if, $ifConfigOutput);

        $result = [];
        foreach ($ifConfigOutput as $kk => $value) {
            $ip = '';
            if (preg_match('/[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}/', $value, $address)) {
                $mac = $address[0];
                $value2 = isset($ifConfigOutput[$kk + 1]) ? $ifConfigOutput[$kk + 1] : '';
                if (preg_match_all('/inet  ?([^ ]+)/', $value2, $ips)) {
                    $ip = $ips[1][0];
                }
                if (empty($ip)) {
                    $value3 = isset($ifConfigOutput[$kk - 1]) ? $ifConfigOutput[$kk - 1] : '';
                    if (!empty($value3)) {
                        if (preg_match_all('/inet  ?([^ ]+)/', $value3, $ips)) {
                            $ip = $ips[1][0];
                        }
                    }
                }
                if (strpos($ip, ':') !== false) {
                    $vv = explode(':', $ip);
                    $ip = $vv[1];
                }
                if ($mac && $ip) {
                    $result[] = ['ip' => $ip, 'mac' => $mac];
                }
            }
        }

        if (count($result) == 0) {
            exit("No IPs found.\n");
        }

        return $result;
    }

    /**
     * @return array<array<string, string>>
     * @deprecated Method is deprecated because use result of `ifconfig`
     */
    public static function getIpsArray()
    {
        return ArrayHelper::map(self::getIpsMacsPairs(), 'ip', 'mac');
    }

    /**
     * Return all IPs
     * @return array<string>
     */
    public static function getIPs()
    {
        exec('hostname --all-ip-addresses', $output);

        $result = reset($output);
        if (empty($result)) {
            return [];
        }

        $result = array_map('trim', explode(' ', $result));

        return $result;
    }

    /**
     * Return first IP
     * @return string
     */
    public static function getFirstIP()
    {
        $ips = static::getIPs();
        if (empty($ips)) {
            return '';
        }
        return reset($ips);
    }

    /**
     * @return string|null
     */
    public static function getDomain()
    {
        $ip = static::getFirstIP();

        // Check if HTTPS running
        $cmd = 'if [ "$(nc -z -v -w2 ' . $ip . ' 443 2>&1 | grep -c succeeded)" -eq 1 ]; then echo 1; else echo 0; fi';
        exec($cmd, $output);

        if (reset($output) == 0) {
            return null;
        }

        unset($output);
        // Try to get URL from cert
        $cmd = 'echo -n | timeout 5 openssl s_client -showcerts -connect ' . $ip . ':443 </dev/null 2>/dev/null | '
            . 'sed -ne \'/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p\' | openssl x509 -noout -subject | '
            . 'sed -e \'s/^subject.*CN\s*=\s*\([a-zA-Z0-9\.\-]*\).*$/\1/\'';
        exec($cmd, $output);

        $result = reset($output);
        if (false === $result) {
            return null;
        }
        $result = trim($result);

        // Remove '*.' from wildcard record
        if (strpos($result, '*.') !== false) {
            $result = str_replace('*.', '', $result);
        }

        return (new UrlValidator())->validate('https://' . $result) ? $result : null;
    }
}
