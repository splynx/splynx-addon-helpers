<?php

namespace splynx\helpers;

use Exception;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

class SecurityHelper extends BaseObject
{
    public const METHOD = 'aes-256-cbc';

    /**
     * Encrypt string
     *
     * @param string $string
     * @param string $key
     * @return string
     * @throws InvalidConfigException
     */
    public static function encrypt($string, $key)
    {
        $key = hash('sha256', $key, true);

        if (!$string) {
            return '';
        };

        if (mb_strlen($key, '8bit') !== 32) {
            throw new InvalidConfigException('Needs a 256-bit key!');
        }

        $ivSize = openssl_cipher_iv_length(self::METHOD);
        if ($ivSize === false) {
            throw new Exception('Encrypt string failed!');
        }
        $iv = openssl_random_pseudo_bytes($ivSize);

        if ($iv === false) {
            throw new Exception('Encrypt string failed!');
        }

        $cipherText = openssl_encrypt(
            $string,
            self::METHOD,
            $key,
            OPENSSL_RAW_DATA,
            $iv
        );

        return trim(base64_encode($iv . $cipherText));
    }

    /**
     * Decrypt string
     *
     * @param string $string
     * @param string $key
     * @return string
     * @throws InvalidConfigException
     */
    public static function decrypt($string, $key)
    {
        $key = hash('sha256', $key, true);

        if (!$string) {
            return '';
        };

        if (mb_strlen($key, '8bit') !== 32) {
            throw new InvalidConfigException('Needs a 256-bit key!');
        }

        $string = base64_decode($string);

        $ivSize = openssl_cipher_iv_length(self::METHOD);

        if ($ivSize === false) {
            throw new Exception('Decrypt string failed!');
        }

        $iv = mb_substr($string, 0, $ivSize, '8bit');
        $cipherText = mb_substr($string, $ivSize, null, '8bit');

        $result = openssl_decrypt(
            $cipherText,
            self::METHOD,
            $key,
            OPENSSL_RAW_DATA,
            $iv
        );

        if ($result === false) {
            throw new Exception('Decrypt string failed!');
        }

        return trim($result);
    }
}
