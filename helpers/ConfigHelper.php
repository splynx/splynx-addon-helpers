<?php

namespace splynx\helpers;

use Yii;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\redis\Connection;

class ConfigHelper extends BaseObject
{
    public const TYPE_TEXT = 'text';
    public const TYPE_SELECT = 'select';
    public const TYPE_BOOLEAN = 'boolean';
    public const TYPE_MULTIPLE_SELECT = 'multipleSelect';
    public const TYPE_ENCRYPTED = 'encrypted';
    public const ADD_ON_ENCRYPTION_KEY_FIELD = 'add_on_encryption_key';
    public const PARTNERS_SETTINGS_FIELD = 'partners_settings';
    public const DEFAULT_PARTNERS_SETTINGS_FIELD = 'default_partners_settings';
    /** Config file name */
    public const CONFIG_FILE = 'config.ini.php';

    public const ADD_ON_BASE = 'splynx-addon-base-2';

    /** @var string */
    public static $addOnPath;

    /** @var string */
    private static $_encryptionKey;

    /** @var array<string, array<string, mixed>> */
    private static $_splynxConfig;

    /**
     * @return string
     */
    protected static function getParamsFilePath()
    {
        return static::$addOnPath . DIRECTORY_SEPARATOR . 'params.php';
    }

    /**
     * Load params.php file if exist
     * @return false|array<string, mixed>
     */
    protected static function getParamsFile()
    {
        $paramsFilePath = static::getParamsFilePath();
        if (file_exists($paramsFilePath)) {
            return require($paramsFilePath);
        }

        return false;
    }

    /**
     * Full path to Splynx version file
     * @return string
     */
    public static function getSplynxVersionPath()
    {
        return '/var/www/splynx/version';
    }

    /** @var array<array<string, mixed>> */
    private static $_schemes = [];

    /**
     * Load config scheme and convert to array
     * @return false|array<string, mixed>
     */
    public static function getConfigScheme()
    {
        if (isset(self::$_schemes[static::$addOnPath])) {
            return self::$_schemes[static::$addOnPath];
        }

        $schemaFile = static::$addOnPath . DIRECTORY_SEPARATOR . 'config.json';
        if (!file_exists($schemaFile)) {
            return false;
        }
        $schemeContent = file_get_contents($schemaFile);
        if ($schemeContent === false) {
            throw new Exception('Can\'t read the config.json file');
        }
        self::$_schemes[static::$addOnPath] = json_decode($schemeContent, true, 512, JSON_THROW_ON_ERROR);

        return self::$_schemes[static::$addOnPath];
    }

    /**
     * Get field scheme from config.json
     * @param string $keyName
     * @return null|array<string, mixed>
     */
    public static function getKeyScheme($keyName)
    {
        $scheme = static::getConfigScheme();
        if ($scheme === false) {
            return null;
        }

        foreach ($scheme['blocks'] as $block) {
            if (isset($block['items'][$keyName])) {
                return $block['items'][$keyName];
            }
        }

        return null;
    }

    /**
     * Load and prepare config file if exist.
     *
     * @return false|array<string, mixed>
     */
    protected static function getConfigIniFile()
    {
        $file = static::$addOnPath . DIRECTORY_SEPARATOR . self::CONFIG_FILE;
        if (!file_exists($file)) {
            return false;
        }

        $rawParams = parse_ini_file($file, true);
        if (empty($rawParams)) {
            return false;
        }

        return static::prepareRawConfig($rawParams);
    }

    /**
     * Prepare boolean value and convert settings with `key=value,` structure to array
     * @param array<string, mixed> $config
     * @return array<string, mixed>
     */
    protected static function prepareRawConfig($config)
    {
        $result = [];
        foreach ($config as $key => $value) {
            if (is_array($value)) {
                $result[$key] = static::prepareRawConfig($value);
                continue;
            }

            $scheme = static::getKeyScheme($key);
            if ($scheme == null || !isset($scheme['type'])) {
                $type = self::TYPE_TEXT;
            } else {
                $type = $scheme['type'];
            }

            switch ($type) {
                case self::TYPE_BOOLEAN:
                    $value = ($value == 'yes' || $value == '1') ? true : false;
                    break;
                case self::TYPE_MULTIPLE_SELECT:
                    $value = trim($value);
                    $value = empty($value) ? [] : explode(',', $value);
                    break;
            }

            $result[$key] = $value;
        }

        return $result;
    }

    /**
     * Decrypt encrypted values.
     *
     * @param array<string, mixed> $params
     * @return array<string, mixed>
     * @throws InvalidConfigException
     */
    private static function prepareEncryptedFields($params)
    {
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $params[$key] = self::prepareEncryptedFields($value);
                continue;
            }
            $scheme = static::getKeyScheme($key);

            if (isset($scheme['type']) && $scheme['type'] == self::TYPE_ENCRYPTED) {
                $params[$key] = SecurityHelper::decrypt($value, self::$_encryptionKey);
            }
        }

        return $params;
    }

    /**
     * Get params with check partner ID and other expressions
     * @param string|null $addOnDir Path to add-on directory. If not given - will be used from static property
     * @return array<string, mixed>|null
     * @throws InvalidConfigException
     */
    public static function getParams($addOnDir = null)
    {
        if ($addOnDir !== null) {
            static::$addOnPath = $addOnDir;
        }

        $oldParams = static::getParamsFile();
        $iniParams = static::getConfigIniFile();

        if ($iniParams == false) {
            return $oldParams == false ? null : $oldParams;
        }

        if ($oldParams == false) {
            $oldParams = [];
        }

        $resultParams = $iniParams['common'];
        $resultParams[self::DEFAULT_PARTNERS_SETTINGS_FIELD] = $iniParams[self::DEFAULT_PARTNERS_SETTINGS_FIELD];

        foreach ($iniParams as $section => $settings) {
            if ($section == 'common' || $section == self::DEFAULT_PARTNERS_SETTINGS_FIELD) {
                continue;
            }

            $resultParams[self::PARTNERS_SETTINGS_FIELD][$section] = $settings;
        }

        // Remove per partner params from old params
        $oldParams = array_diff_key($oldParams, $resultParams[self::DEFAULT_PARTNERS_SETTINGS_FIELD]);
        // Join settings from params.php
        $resultParams = ArrayHelper::merge($oldParams, $resultParams);

        self::$_encryptionKey = isset($resultParams[self::ADD_ON_ENCRYPTION_KEY_FIELD]) ? $resultParams[self::ADD_ON_ENCRYPTION_KEY_FIELD] : '';
        return self::prepareEncryptedFields($resultParams);
    }

    /**
     * @param string $key
     * @param int $partner
     * @return mixed|null
     */
    public static function get($key, $partner = null)
    {
        if (isset(Yii::$app->params[self::PARTNERS_SETTINGS_FIELD][$partner][$key])) {
            return Yii::$app->params[self::PARTNERS_SETTINGS_FIELD][$partner][$key];
        }

        if (isset(Yii::$app->params[$key])) {
            return Yii::$app->params[$key];
        }

        if (isset(Yii::$app->params[self::DEFAULT_PARTNERS_SETTINGS_FIELD][$key])) {
            return Yii::$app->params[self::DEFAULT_PARTNERS_SETTINGS_FIELD][$key];
        }

        return null;
    }

    /**
     * Use this method to reload add-on config.
     *
     * @return void
     * @throws InvalidConfigException
     */
    public static function reloadConfig()
    {
        Yii::$app->params = static::getParams() ?? [];
    }

    /**
     * Get add-on url rules
     * @param string $baseDir Path to add-on directory
     * @return array<string, string>
     */
    public static function getUrlRules($baseDir)
    {
        $pathToUrlRules = $baseDir . '/config/url_rules.php';
        if (file_exists($pathToUrlRules)) {
            return require($pathToUrlRules);
        } else {
            return [];
        }
    }

    /**
     * Get path to Splynx Redis configuration file
     * @return string
     */
    public static function getPathToRedisConfig()
    {
        return FileHelper::normalizePath('/var/www/splynx/config/redis.php');
    }

    /**
     * Get raw Splynx Redis configuration
     * @return array<mixed>|null
     */
    public static function getRedisConfig()
    {
        $pathToRedisConfig = static::getPathToRedisConfig();
        if (!file_exists($pathToRedisConfig)) {
            return null;
        }
        $config = parse_ini_file($pathToRedisConfig, true);

        if ($config === false) {
            return null;
        }

        return $config;
    }

    /**
     * Get redis configuration for add-ons redis component.
     *
     * @return array<string, mixed>
     */
    public static function getRedisConfigForAddOns()
    {
        $defaultRedisConfig = [
            'hostname' => '127.0.0.1',
            'port' => 6379,
        ];

        $config = static::getRedisConfig();
        if (empty($config['server'])) {
            return $defaultRedisConfig;
        }

        $config = $config['server'];

        if (isset($config['host'])) {
            $hostName = $config['host'];
        } elseif (isset($config['hostname'])) {
            $hostName = $config['hostname'];
        } else {
            $hostName = $defaultRedisConfig['hostname'];
        }

        $resultConfig = [
            'hostname' => $hostName,
            'port' => $config['port'],
        ];
        if (!empty($config['password'])) {
            $resultConfig['password'] = $config['password'];
        }

        return $resultConfig;
    }

    /**
     * @return array<string, mixed>
     */
    public static function getRedisConfigComponent()
    {
        return array_merge([
            'class' => Connection::class,
        ], ConfigHelper::getRedisConfigForAddOns());
    }

    /**
     * Get path to base web config
     * @return string
     */
    public static function getPathToBaseWebConfig()
    {
        return dirname(__DIR__) . '/config/web.php';
    }

    /**
     * Get path to base console config
     * @return string
     */
    public static function getPathToBaseConsoleConfig()
    {
        return dirname(__DIR__) . '/config/console.php';
    }

    /**
     * Get path to base test config
     * @return string
     */
    public static function getPathToBaseTestConfig(): string
    {
        return dirname(__DIR__) . '/config/test.php';
    }

    /**
     * Get add-on name by path
     * @param string $baseDir
     * @return string
     */
    public static function getAddOnNameFromBaseDir($baseDir)
    {
        $parts = explode('/', rtrim($baseDir, '/'));
        return array_pop($parts);
    }

    /**
     * Get session settings.
     * If splynx use redis session then return settings for redis session
     * Otherwise return default Yii session settings
     * @return array<string, mixed>
     */
    public static function getSessionConfig()
    {
        $pathToSplynxRedisConfig = static::getPathToRedisConfig();
        if (file_exists($pathToSplynxRedisConfig) && extension_loaded('redis')) {
            return [
                'class' => 'splynx\base\RedisSession',
                'redis' => ConfigHelper::getRedisConfigForAddOns(),
            ];
        }

        return [
            'class' => 'yii\web\Session',
        ];
    }

    /**
     * Get application config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to app config file
     * @param string $baseConfigPath Path to base app config file
     * @return array<mixed>
     * @throws InvalidConfigException
     */
    private static function loadAppConfig($baseDir, $configPath, $baseConfigPath)
    {
        $params = static::getParams($baseDir . '/config');

        // Set dynamic settings to base config
        $baseConfigFunc = require($baseConfigPath);
        $baseConfig = $baseConfigFunc($params, $baseDir);

        $configFunc = require($configPath);
        $config = $configFunc($params, $baseDir);

        return ArrayHelper::mergeUniqueIntegerKeyed($baseConfig, $config);
    }

    /**
     * Get web config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to web config file
     * @return array<mixed>
     * @throws InvalidConfigException
     */
    public static function getWebConfig($baseDir, $configPath)
    {
        return self::loadAppConfig($baseDir, $configPath, static::getPathToBaseWebConfig());
    }

    /**
     * Get console config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to console config file
     * @return array<mixed>
     * @throws InvalidConfigException
     */
    public static function getConsoleConfig($baseDir, $configPath)
    {
        return self::loadAppConfig($baseDir, $configPath, static::getPathToBaseConsoleConfig());
    }

    /**
     * Get test config merged with base config
     * @param string $baseDir Path to add-on dir
     * @param string $configPath Path to test config file
     * @return array<mixed>
     * @throws InvalidConfigException
     */
    public static function getTestConfig(string $baseDir, string $configPath): array
    {
        return self::loadAppConfig($baseDir, $configPath, static::getPathToBaseTestConfig());
    }

    /**
     * Get path for Splynx config directory
     * @return string
     */
    public static function getSplynxConfigPath()
    {
        return '/var/www/splynx/config';
    }

    /**
     * Get Splynx configs by Splynx config file name
     * @param string $fileName Splynx config file name
     * @return array<string, array<mixed>>
     * @throws Exception
     */
    public static function getSplynxConfig($fileName)
    {
        if (isset(self::$_splynxConfig[$fileName])) {
            return self::$_splynxConfig[$fileName];
        }

        $filePath = static::getSplynxConfigPath() . DIRECTORY_SEPARATOR . $fileName . '.php';
        if (!file_exists($filePath)) {
            throw new Exception($filePath . ' file not found.');
        }
        $config = parse_ini_file($filePath, true);
        return self::$_splynxConfig[$fileName] = $config === false ? [] : $config;
    }

    /**
     * Get Splynx timeZone
     * @return string Splynx timeZone
     * @throws Exception
     */
    public static function getSplynxTimeZone()
    {
        $splynxConfig = static::getSplynxConfig('config');

        if (isset($splynxConfig['splynx']['timezone'])) {
            return $splynxConfig['splynx']['timezone'];
        } elseif (ini_get('date.timezone')) {
            return ini_get('date.timezone');
        }

        return 'UTC';
    }

    /**
     * @return false|string
     */
    public static function getSpynxVersion()
    {
        if (file_exists(static::getSplynxVersionPath())) {
            return file_get_contents(static::getSplynxVersionPath());
        }
        return false;
    }
}
