<?php

namespace splynx\helpers;

use SplynxApi;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * Class ApiHelper provide functionality for working with Splynx API
 *
 * Login as customer and make request
 *
 * ```php
 * $api = ApiHelper::getInstance();
 * $api->setVersion(\SplynxApi::API_VERSION_2);
 * $login = $api->login(\SplynxApi::AUTH_TYPE_CUSTOMER, [
 *      'login' => 'your_bob',
 *      'password' => 'your_password',
 * ]);
 * if ($login) {
 *      $customers = $api->get('admin/customers/customer');
 *      echo "\nAll customers: " . var_export($customers, 1);
 *      $api->logout();
 * } else {
 *      die("\nLogin failed!");
 * }
 * ```
 *
 * Save auth data to session
 *
 * ```php
 * $api = ApiHelper::getInstance();
 * $api->setVersion(\SplynxApi::API_VERSION_2);
 * $api->login(\SplynxApi::AUTH_TYPE_CUSTOMER, [
 *      'login' => 'your_bob',
 *      'password' => 'your_password',
 * ]);
 * $_SESSION['auth_data'] = $api->getAuthData();
 * ```
 *
 * Use saved to session auth data
 *
 * ```
 * $api = ApiHelper::getInstance();
 * $api->setVersion(\SplynxApi::API_VERSION_2);
 * $api->setAuthData($_SESSION['auth_data']);
 * ```
 *
 * @package splynx\helpers
 *
 * @phpstan-type ResultResponse array{'result': bool, 'response': array<string, mixed>|string}
 */
class ApiHelper extends BaseObject
{
    /** @var SplynxApi */
    private $_api;

    /** @var string API base url */
    public $api_domain;

    /** @var string API key. Used in API v1. */
    public $api_key;

    /** @var string API secret. Used in API v1. */
    public $api_secret;

    /** @var self Instance of current class */
    private static $_instance;

    /** Name of header which contains amount of records */
    public const HEADER_X_TOTAL_COUNT = 'X-total-count';

    /**
     * @return boolean|null
     */
    public function getResult()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->result;
        }

        return null;
    }

    /**
     * @return array<mixed>|string|null
     */
    public function getResponse()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->response;
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getResponseCode()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->response_code;
        }

        return null;
    }

    /**
     * @return array<string, string>|null
     */
    public function getResponseHeaders()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->response_headers;
        }

        return null;
    }

    /**
     * Get instance of current helper
     * @return self
     * @throws InvalidConfigException
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            $config = [
                'api_domain' => Yii::$app->params['api_domain'],
                'api_key' => isset(Yii::$app->params['api_key']) ? Yii::$app->params['api_key'] : null,
                'api_secret' => isset(Yii::$app->params['api_secret']) ? Yii::$app->params['api_secret'] : null,
            ];

            if (empty($config['api_domain'])) {
                throw new InvalidConfigException('Invalid API config in params.php');
            }

            $instance = new self($config);
            $instance->_api = new SplynxApi($instance->api_domain, $instance->api_key, $instance->api_secret);
            $instance->_api->debug = false;

            self::$_instance = $instance;
        }

        return self::$_instance;
    }

    /**
     * Set API version
     * @param string $v
     * @return void
     */
    public function setVersion($v)
    {
        $this->_api->setVersion($v);
    }

    /**
     * Make GET request. Get records or record.
     * @param string $url API endpoint
     * @param null|int|string $id Record id
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function get($url, $id = null)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_get($url, $id);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make POST request. Create record.
     * @param string $url API endpoint
     * @param array<mixed> $params Payload
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function post($url, $params = [])
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_post($url, $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make PUT request. Update record by id.
     * @param string $url API endpoint
     * @param int|null $id Record id
     * @param array<mixed> $params Payload
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function put($url, $id, $params = [])
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_put($url, $id, $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make DELETE request
     * @param string $url API endpoint
     * @param int $id Record id
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function delete($url, $id)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_delete($url, $id);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Search records by condition
     * @param string $url API endpoint
     * @param array<mixed> $params Search condition
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function search($url, $params)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_get($url . '?' . http_build_query($params));

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make OPTIONS request
     *
     * Get model properties.
     * Response example:
     *
     * ```php
     * [
     *      'attributes' => [
     *          [
     *              'name' => 'id',
     *              'title' => 'Id',
     *              'type' => 'integer',
     *              'required' => false,
     *          ],
     *      ],
     *      'additional_attributes' => [
     *          [
     *              'module' => 'locations',
     *              'name' => 'loc_ip',
     *              'title' => 'Location IP',
     *              'type' => 'relation',
     *              'required' => false,
     *              // ...
     *          ]
     *      ],
     * ]
     * ```
     * @param string $url API endpoint
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function options($url)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_options($url);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Get amount of records
     *
     * Get amount of active customers:
     *
     * ```php
     * $result = $api->count('admin/customers/customer', [
     *      'main_attributes' => [
     *          'status' => 'active',
     *      ]
     * ]);
     * echo "\nAmount of active customers: " . var_export($result['response'], 1);
     * ```
     *
     * @param string $url API endpoint
     * @param array<string, mixed> $condition Search condition
     * @return array{'result': bool, 'response': int|string|null}
     * @throws InvalidConfigException
     */
    public static function count($url, $condition = [])
    {
        $api = self::getInstance()->_api;
        $condition = empty($condition) ? '' : '?' . http_build_query($condition);
        $result = $api->api_call_head($url . $condition);

        return [
            'result' => $result,
            'response' => isset($api->response_headers[self::HEADER_X_TOTAL_COUNT]) ? $api->response_headers[self::HEADER_X_TOTAL_COUNT] : null,
        ];
    }

    /**
     * Use this to upload file by API.
     *
     * @param string $url
     * @param int|string $id
     * @param array<mixed> $params Array where key - property and value - CURLFile instance of file
     * @return ResultResponse
     */
    public static function upload($url, $id, $params)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_post_file($url . '/' . $id . '--upload', $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make login. Generate JWT tokens pair
     * @param string $authType Possible values: `admin`, `customer`, `api_key`
     * @param array<string, mixed> $data Auth data
     *
     * For login as admin
     *
     * ```php
     * [
     *      'login' => '',
     *      'password' => '',
     *      // 'code' => '', // If 2Fa enabled
     * ]
     * ```
     *
     * For login as customer
     *
     * ```php
     * [
     *      'login' => '',
     *      'password' => '',
     * ]
     * ```
     *
     * For login with using api key
     *
     * ```php
     * [
     *      'key' => '',
     *      'secret' => '',
     * ]
     * ```
     * @return bool
     */
    public function login($authType, $data)
    {
        $result = array_merge([
            'auth_type' => $authType,
        ], $data);
        return $this->_api->login($result);
    }

    /**
     * Logout. Delete JWT token pair
     * @return bool
     */
    public function logout()
    {
        return $this->_api->logout();
    }

    /**
     * Get auth data
     * @return array{
     *     'access_token': string|null,
     *     'access_token_expiration': int|null,
     *     'refresh_token': string|null,
     *     'refresh_token_expiration': int|null,
     *     'permissions': array<string>|null
     * }
     */
    public function getAuthData()
    {
        return $this->_api->getAuthData();
    }

    /**
     * Set auth data. Set your auth data stored in external storage instead of login.
     * @param array{
     *     'access_token': string,
     *     'access_token_expiration': int,
     *     'refresh_token': string,
     *     'refresh_token_expiration': int,
     *     'permissions': array<string>|null
     * } $data
     * @return void
     * ```
     * [
     *      'access_token' => '',
     *      'access_token_expiration' => '',
     *      'refresh_token' => '',
     *      'refresh_token_expiration' => '',
     *      'permissions' => [],
     * ]
     * ```
     */
    public function setAuthData($data)
    {
        $this->_api->setAuthData($data);
    }
}
