<?php

namespace splynx\helpers;

/**
 * Class BaseUrlHelper
 * @package splynx\helpers
 */
class BaseUrlHelper
{
    /**
     * Normalize url:
     *  - remove "/" from the end
     *
     * @param string $url
     * @return string Normalized url
     */
    public static function normalizeUrl($url)
    {
        // Remove slash from the end
        $url = rtrim($url, '/');

        return $url;
    }
}
