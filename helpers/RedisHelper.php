<?php

namespace splynx\helpers;

use Yii;
use yii\base\BaseObject;
use yii\redis\Connection;

/**
 * Class RedisHelper
 * @package splynx\helpers
 */
class RedisHelper extends BaseObject
{
    /** Default prefix for all splynx keys stored in redis */
    public const DEFAULT_SPLYNX_REDIS_KEY_PREFIX = 'spl:';

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public static function set($key, $value)
    {
        return RedisHelper::getRedis()->set(RedisHelper::calculateKey($key), $value);
    }

    /**
     * @return Connection
     */
    public static function getRedis()
    {
        return Yii::$app->redis;//@phpstan-ignore-line
    }

    /**
     * @param string $key
     * @return string
     */
    public static function calculateKey($key)
    {
        return RedisHelper::DEFAULT_SPLYNX_REDIS_KEY_PREFIX . $key;
    }

    /**
     * @param string $key
     * @param number $second
     * @param mixed $value
     * @return void
     */
    public static function setex($key, $second, $value)
    {
        RedisHelper::getRedis()->setex(RedisHelper::calculateKey($key), $second, $value);
    }

    /**
     * @param string $key
     * @return mixed
     */
    public static function get($key)
    {
        return RedisHelper::getRedis()->get(RedisHelper::calculateKey($key));
    }

    /**
     * @param string $key
     * @return bool
     */
    public static function delete($key)
    {
        return (bool)RedisHelper::getRedis()->del(RedisHelper::calculateKey($key));
    }
}
