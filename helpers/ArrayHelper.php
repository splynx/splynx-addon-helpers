<?php

namespace splynx\helpers;

use yii\helpers\ReplaceArrayValue;
use yii\helpers\UnsetArrayValue;

/**
 * Class ArrayHelper provide methods for operate with arrays
 * @package splynx\helpers
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{
    /**
     * Merges two or more arrays into one recursively.
     * If each array has an element with the same string key value, the latter
     * will overwrite the former (different from array_merge_recursive).
     * Recursive merging will be conducted if both arrays have an element of array
     * type and are having the same key.
     * For integer-keyed elements, only unique elements will be returned (different from yii\helpers\ArrayHelper::merge)
     * You can use [[UnsetArrayValue]] object to unset value from previous array or
     * [[ReplaceArrayValue]] to force replace former value instead of recursive merging.
     * @param array<mixed> $a array to be merged to
     * @param array<mixed> $b array to be merged from. You can specify additional
     * arrays via third argument, fourth argument etc.
     * @return array<mixed> the merged array (the original arrays are not changed.)
     */
    public static function mergeUniqueIntegerKeyed($a, $b)
    {
        $args = func_get_args();
        $res = array_shift($args);

        while (!empty($args)) {
            $next = array_shift($args);
            foreach ($next as $k => $v) {
                if ($v instanceof UnsetArrayValue) {
                    unset($res[$k]);
                } elseif ($v instanceof ReplaceArrayValue) {
                    $res[$k] = $v->value;
                } elseif (is_int($k)) {
                    // Added to non-associative array only unique elements
                    if (!in_array($v, $res, true)) {
                        $res[] = $v;
                    }
                } elseif (is_array($v) && isset($res[$k]) && is_array($res[$k])) {
                    $res[$k] = self::mergeUniqueIntegerKeyed($res[$k], $v);
                } else {
                    $res[$k] = $v;
                }
            }
        }

        return $res;
    }
}
