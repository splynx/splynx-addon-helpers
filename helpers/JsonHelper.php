<?php

namespace splynx\helpers;

use JsonException;

class JsonHelper
{
    /**
     * @param string $json
     * @param bool $associative
     * @return mixed
     * @throws JsonException
     */
    public static function decode(string $json, bool $associative = true)
    {
        return json_decode($json, $associative, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param mixed $data
     * @return string
     * @throws JsonException
     */
    public static function encode($data): string
    {
        return json_encode($data, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $json
     * @return bool
     * @throws JsonException
     */
    public static function isJson(string $json): bool
    {
        static::decode($json);
        return json_last_error() === JSON_ERROR_NONE;
    }
}
