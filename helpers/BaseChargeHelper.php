<?php

namespace splynx\helpers;

use yii\base\BaseObject;

/**
 * Class BaseChargeHelper
 * @package splynx\helpers
 */
class BaseChargeHelper extends BaseObject
{
    /** @var int */
    public $id;

    /** @var string */
    public $type;

    public const TYPE_INVOICE = 'invoice';
    public const TYPE_PROFORMA_INVOICE = 'request';

    public const ACTION_UPDATE_CHARGE_PROGRESS = 'update-charge-progress';
    public const ACTION_CHANGE_CHARGE_FILE_EXTENSION = 'change-charge-file-extension';

    /** @var string */
    protected static $baseCommand = '$(which php) /var/www/splynx/system/script/tools ';

    /**
     * @param string $type
     * @param int $id
     * @param array<mixed> $config
     */
    public function __construct($type, $id, array $config = [])
    {
        $this->type = $type;
        $this->id = $id;

        parent::__construct($config);
    }

    /**
     * Exec command.
     *
     * @param string $command
     * @return void
     */
    protected function exec($command)
    {
        exec($command);
    }

    /**
     * Create command.
     *
     * @param string $action
     * @param array<string, string> $params
     * @return string
     */
    protected function createCommand($action, $params = [])
    {
        $cmd = $action . ' --type="' . $this->type . '" --id="' . $this->id . '" ';

        foreach ($params as $key => $param) {
            $cmd .= '--' . $key . '="' . $param . '"';
        }

        return static::$baseCommand . $cmd;
    }

    /**
     * Get command for update charge progress
     * @param int $amount
     * @return string
     */
    protected function getUpdateProgressCommand($amount)
    {
        return $this->createCommand(self::ACTION_UPDATE_CHARGE_PROGRESS, [
            'amount_ready' => (string)$amount,
        ]);
    }

    /**
     * Increase amount charged invoices
     * @param int $amount
     * @return void
     */
    public function updateReady($amount)
    {
        $cmd = $this->getUpdateProgressCommand($amount);

        $this->exec($cmd);
    }

    /**
     * Get command for change extension
     * @param string $extension
     * @return string
     */
    protected function getChangeExtensionCommand($extension = 'txt')
    {
        return $this->createCommand(self::ACTION_CHANGE_CHARGE_FILE_EXTENSION, [
            'extension' => $extension,
        ]);
    }

    /**
     * Change extension for charge
     * @param string $extension
     * @return void
     */
    public function setExtension($extension = 'txt')
    {
        $cmd = $this->getChangeExtensionCommand($extension);

        $this->exec($cmd);
    }
}
