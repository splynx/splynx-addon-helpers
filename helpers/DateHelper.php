<?php

namespace splynx\helpers;

use DateTime;
use splynx\models\console_api\config\ConsoleSplynxConfig;

class DateHelper
{
    /** @var array{'php': string|null, 'js': string|null} */
    protected static $_dateFormats = [
        'php' => null,
        'js' => null,
    ];

    /** @var int */
    protected static $_firstDateOfTheWeek;

    /**
     * @param string $date
     * @return bool
     */
    public static function isNull($date)
    {
        if ($date == '0000-00-00' || $date == '0000-00-00 00:00:00') {
            return true;
        }

        return false;
    }

    /**
     * @param string $date
     * @param string $format
     * @return bool
     */
    public static function validate($date, $format = 'Y-m-d')
    {
        // Check empty date
        if (static::isNull($date)) {
            return true;
        }
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * @param string $date1
     * @param string $date2
     * @param bool $allowSameDay
     * @return bool
     * @throws \Exception
     */
    public static function validatePeriod($date1, $date2, $allowSameDay = false)
    {
        if (static::validate($date1) == false || static::validate($date2) == false) {
            return false;
        }

        // Check empty date
        if (static::isNull($date2)) {
            return true;
        }

        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);

        if ($allowSameDay == true) {
            if ($datetime1 <= $datetime2) {
                return true;
            }
        } else {
            if ($datetime1 < $datetime2) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public static function getJsSplynxDateFormat()
    {
        if (static::$_dateFormats['js'] === null) {
            static::$_dateFormats['js'] = static::getSplynxDateFormat();
            static::$_dateFormats['js'] = static::convertDateTimeFormatToJsStyle(static::$_dateFormats['js']);
        }
        return static::$_dateFormats['js'];
    }

    /**
     * This method converts php dateTimeFormat to js datetimeFormat.
     * @param string $format
     * @return string
     */
    protected static function convertDateTimeFormatToJsStyle($format)
    {
        return str_replace(['H', 'h', 'i', 's', 'd', 'm', 'Y', 'y'], ['HH', 'hh', 'mm', 'ss', 'DD', 'MM', 'YYYY', 'YY'], $format);
    }

    /**
     *  Method load date format from Splynx config.
     * @return string
     */
    public static function getSplynxDateFormat()
    {
        if (static::$_dateFormats['php'] === null) {
            $config = (new ConsoleSplynxConfig())->findOne(['path' => 'system', 'module' => 'main', 'key' => 'date_format']);
            static::$_dateFormats['php'] = !empty($config) ? $config->value : 'Y-m-d';
        }

        return static::$_dateFormats['php'];
    }

    /**
     * Method load first day of the week from Splynx config.
     *
     * @return int
     */
    public static function getSplynxFirstDayOfTheWeek()
    {
        if (static::$_firstDateOfTheWeek === null) {
            $config = (new ConsoleSplynxConfig())->findOne(['path' => 'system', 'module' => 'main', 'key' => 'first_day']);
            static::$_firstDateOfTheWeek = !empty($config) ? (int)$config->value : 1;
        }

        return static::$_firstDateOfTheWeek;
    }

    /**
     * @param string $date
     * @param string $formatTo
     * @param string|null $formatFrom
     * @return string|null
     */
    public static function convertDateFormat($date, $formatTo = 'Y-m-d', $formatFrom = null)
    {
        if (!is_string($date) || self::isNull($date)) {
            return null;
        }

        try {
            if ($formatFrom === null) {
                $object = new DateTime($date);
            } else {
                $object = DateTime::createFromFormat($formatFrom, $date);
                if (!is_object($object)) {
                    return null;
                }
            }
            return $object->format($formatTo);
        } catch (\Exception $e) {
            return null;
        }
    }
}
