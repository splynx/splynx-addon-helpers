<?php

namespace splynx\components\integrations;

use splynx\v2\base\BaseActiveApi;

/**
 * Adds common queries to Splynx api models
 * Trait CommonQueriesTrait
 * @package app\models\splynx
 */
trait CommonQueriesTrait
{
    /**
     * @param array $ids
     * @return BaseActiveApi[]
     */
    public static function findByIds(array $ids)
    {
        $filtered = array_filter($ids);
        return $filtered ? (new static())->findAll(['id' => ['IN', $filtered]]) : [];
    }
}
