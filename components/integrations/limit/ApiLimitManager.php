<?php

namespace splynx\components\integrations\limit;

use yii\base\Model;
use SplObjectStorage;
use Yii;

/**
 * Class ApiLimitManager
 * @package splynx\components\integrations\limit
 *
 * @property string $errorMessage
 */
class ApiLimitManager extends Model implements ApiLimitInterface
{
    /** @var SplObjectStorage<ApiLimitInterface, null> */
    private $_handlers;

    public const ERROR_ATTRIBUTE = 'limit';

    /**
     * @return void
     */
    public function init()
    {
        $this->_handlers = new SplObjectStorage();
        parent::init();
    }

    /**
     * @param ApiLimitInterface $handler
     * @return void
     */
    public function attach(ApiLimitInterface $handler)
    {
        $this->_handlers->attach($handler);
    }

    /**
     * @param ApiLimitInterface $handler
     * @return void
     */
    public function detach(ApiLimitInterface $handler)
    {
        $this->_handlers->detach($handler);
    }

    /**
     * @return bool
     */
    public function apiCallAllowed()
    {
        /** @var ApiLimitInterface $handler */
        foreach ($this->_handlers as $handler) {
            if (!$handler->apiCallAllowed()) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $status CALL_STATUS_SUCCESS|CALL_STATUS_ERROR|CALL_STATUS_LIMIT
     * @param string $message
     *
     * @return bool
     */
    public function addApiCall($status = ApiLimitInterface::CALL_STATUS_SUCCESS, $message = '')
    {
        /** @var ApiLimitInterface $handler */
        foreach ($this->_handlers as $handler) {
            if (!$handler->addApiCall($status, $message)) {
                $this->addError(self::ERROR_ATTRIBUTE, $handler->getErrorMessage());
                return false;
            }
        }

        return true;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        $message = $this->getFirstError(self::ERROR_ATTRIBUTE);
        return  $message ?: Yii::t('app', 'Api calls limit is reached!');
    }
}
