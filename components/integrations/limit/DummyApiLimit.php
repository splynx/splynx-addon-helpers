<?php

namespace splynx\components\integrations\limit;

use Yii;
use yii\base\BaseObject;

/**
 * Class DummyApiLimit
 * @package splynx\components\integrations\limit
 *
 * @property string $errorMessage
 */
class DummyApiLimit extends BaseObject implements ApiLimitInterface
{
    /**
     * @return bool
     */
    public function apiCallAllowed()
    {
        return true;
    }

    /**
     * @param string $status
     * @param string $message
     *
     * @return bool
     */
    public function addApiCall($status = ApiLimitInterface::CALL_STATUS_SUCCESS, $message = '')
    {
        return true;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return Yii::t('app', 'Api calls limit is reached!');
    }
}
