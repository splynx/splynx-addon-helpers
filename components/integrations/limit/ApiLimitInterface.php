<?php

namespace splynx\components\integrations\limit;

/**
 * Interface ApiLimitInterface
 * @package splynx\components\integrations\limit
 */
interface ApiLimitInterface
{
    public const CALL_STATUS_SUCCESS = 'success';
    public const CALL_STATUS_ERROR = 'error';
    public const CALL_STATUS_LIMIT = 'limit';

    /**
     * @return bool
     */
    public function apiCallAllowed();

    /**
     * @param string $status
     * @param string $message
     *
     * @return bool
     */
    public function addApiCall($status = self::CALL_STATUS_SUCCESS, $message = '');

    /**
     * @return string
     */
    public function getErrorMessage();
}
