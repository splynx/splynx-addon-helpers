<?php

namespace splynx\components\integrations;

/**
 * ResettableInterface is the interface that should be implemented in classes which want to support resetting of data.
 * @package splynx\components\integrations
 */
interface ResettableInterface
{
    /**
     * Reset one entry
     * @return bool
     */
    public function reset();

    /**
     * Reset all entries
     * @return int Quantity of affected entries
     */
    public static function resetAll();
}
