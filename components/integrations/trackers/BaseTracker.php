<?php

namespace splynx\components\integrations\trackers;

use splynx\components\integrations\ResettableInterface;
use Throwable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * Class BaseTracker
 * @package splynx\components\integrations\trackers
 *
 * @property integer $id
 * @property string $accounting_id
 * @property integer $created_at
 * @property integer $updated_at
 */
abstract class BaseTracker extends ActiveRecord implements ResettableInterface
{
    /**
     * @inheritdoc
     * @return array<mixed>
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     * @return array<array<mixed>>
     */
    public function rules()
    {
        return [
            [['id', 'accounting_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * @param bool $runValidation
     * @param array<string>|null $attributeNames
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!parent::save($runValidation, $attributeNames)) {
            Yii::error($this->getErrors(), 'error');
            return false;
        }

        return true;
    }

    /**
     * @param array<int> $ids
     * @param string $indexBy
     * @return ActiveRecord[]
     */
    public static function findByIds(array $ids, $indexBy = 'id')
    {
        return static::find()->where(['in', 'id', $ids])->indexBy($indexBy)->all();
    }

    /**
     * @param int $id
     * @return ActiveRecord|null
     */
    public static function findById($id)
    {
        return static::find()->where(['id' => $id])->one();
    }

    /**
     * @return false|int
     * @throws StaleObjectException
     * @throws Throwable
     */
    public function reset()
    {
        return $this->delete();
    }

    /**
     * @return int
     */
    public static function resetAll()
    {
        return static::deleteAll();
    }

    /**
     * Populate model for save.
     * @param object $model Object to track
     * @param string $accountingId Einvoicing id
     * @param array<mixed> $data Other required data
     * @return bool If model invalid for save return false.
     */
    abstract public function loadForSave($model, $accountingId, array $data = []);
}
