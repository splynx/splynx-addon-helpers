<?php

namespace splynx\components\integrations\trackers;

/**
 * Trackable is the interface that should be implemented by classes who want to support tracking of changed fields.
 */
interface Trackable
{
    /**
     * @return mixed
     */
    public function getTracker();
}
