<?php

use yii\db\Migration;

/**
 * Handles the creation of table `customers_manual_pairing`.
 */
class m200330_130004_create_customers_manual_pairing_table extends Migration
{
    const TABLE_NAME = '{{%customers_manual_pairing}}';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        /** @var \yii\db\Connection $db */
        $db = $this->db;
        if ($db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable(static::TABLE_NAME, [
            'id' => $this->string()->unique(),
            'name' => $this->string(),
        ], $tableOptions);
        $this->addPrimaryKey(static::TABLE_NAME, static::TABLE_NAME, 'id');
        return true;
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(static::TABLE_NAME);
        return true;
    }
}
