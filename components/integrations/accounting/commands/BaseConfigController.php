<?php

namespace splynx\components\integrations\accounting\commands;

use Exception;
use splynx\models\console_api\accounting\ConsoleAccountingCreditNotes;
use splynx\models\console_api\accounting\ConsoleAccountingCustomers;
use splynx\models\console_api\accounting\ConsoleAccountingInvoices;
use splynx\models\console_api\accounting\ConsoleAccountingPayments;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Class BaseConfigController
 * @package splynx\components\integrations\accounting\commands
 */
class BaseConfigController extends Controller
{
    /**
     * File to enable accounting module
     */
    public const ACCOUNTING_FLAG_FILE_PATH = '/var/www/splynx/addons/splynx-accounting';

    /**
     * Initializes customers for synchronization from given date or all existing customers, if date is not provided.
     * For already initialized table, can add more records from older dates, but giving more recent date will not delete existing records.
     * @param string|null $startDate Date in Y-m-d format `2019-01-01`
     * @return int
     */
    public function actionSetupAccountingCustomers($startDate = null)
    {
        $this->stdout('Init accounting customers table...' . PHP_EOL, Console::FG_GREEN);
        (new ConsoleAccountingCustomers())->sync($startDate);
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Initializes payments for synchronization from given date or all existing payments, if date is not provided.
     * For already initialized table, can add more records from older dates, but giving more recent date will not delete existing records.
     * @param string|null $startDate Date in Y-m-d format `2019-01-01`
     *  @return int
     */
    public function actionSetupAccountingPayments($startDate = null)
    {
        $this->stdout('Init accounting payments table...' . PHP_EOL, Console::FG_GREEN);
        (new ConsoleAccountingPayments())->sync($startDate);
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Initializes invoices for synchronization from given date or all existing invoices, if date is not provided.
     * For already initialized table, can add more records from older dates, but giving more recent date will not delete existing records.
     * @param string|null $startDate Date in Y-m-d format `2019-01-01`
     * @return int
     */
    public function actionSetupAccountingInvoices($startDate = null)
    {
        $this->stdout('Init accounting invoices table...' . PHP_EOL, Console::FG_GREEN);
        (new ConsoleAccountingInvoices())->sync($startDate);
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Initializes credit notes for synchronization from given date or all existing credit notes, if date is not provided.
     * For already initialized table, can add more records from older dates, but giving more recent date will not delete existing records.
     * @param string|null $startDate Date in Y-m-d format `2019-01-01`
     * @return int
     */
    public function actionSetupAccountingCreditNotes($startDate = null)
    {
        $this->stdout('Init accounting credit notes table...' . PHP_EOL, Console::FG_GREEN);
        (new ConsoleAccountingCreditNotes())->sync($startDate);
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Resets accounting invoices paired data
     * @return int
     */
    public function actionResetAccountingInvoices()
    {
        $this->stdout('Reset accounting invoices data...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->resetAccountingInvoices();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Resets accounting credit notes paired data
     * @return int
     */
    public function actionResetAccountingCreditNotes()
    {
        $this->stdout('Reset accounting credit notes data...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->resetAccountingCreditNotes();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Setup service tax rates for mapping in Splynx
     * @return int
     */
    public function actionSetupAccountingTaxRates()
    {
        $this->stdout('Tax rates mapping setup...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->setupAccountingTaxRates();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Reset service tax rates for mapping in Splynx
     * @return int
     */
    public function actionResetAccountingTaxRates()
    {
        $this->stdout('Tax rates mapping reset...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->resetAccountingTaxRates();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Setup service payment methods for mapping in Splynx
     * @return int
     */
    public function actionSetupAccountingBankAccounts()
    {
        $this->stdout('Payment methods mapping setup...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->setupAccountingBankAccounts();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Reset service payment methods for mapping in Splynx
     * @return int
     */
    public function actionResetAccountingBankAccounts()
    {
        $this->stdout('Payment methods mapping reset...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->resetAccountingBankAccounts();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Setup service categories for mapping in Splynx
     * @return int
     */
    public function actionSetupAccountingCategories()
    {
        $this->stdout('Categories mapping setup...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->setupAccountingCategories();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Reset service categories for mapping in Splynx
     * @return int
     */
    public function actionResetAccountingCategories()
    {
        $this->stdout('Categories mapping reset...' . PHP_EOL, Console::FG_GREEN);
        try {
            $this->resetAccountingCategories();
        } catch (Exception $e) {
            $this->stderr($e->getMessage() . PHP_EOL, Console::FG_RED);

            return ExitCode::SOFTWARE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Adds flag file to enable accounting config entry points
     * @return int
     */
    public function actionEnableAccounting()
    {
        $this->stdout('Enable Accounting module...' . PHP_EOL, Console::FG_GREEN);
        if (!touch(self::ACCOUNTING_FLAG_FILE_PATH)) {
            $this->stderr('Can\'t create flag file: ' . self::ACCOUNTING_FLAG_FILE_PATH . PHP_EOL, Console::FG_RED);

            return ExitCode::OSFILE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Removes flag file to disable accounting config entry points
     * @return int
     */
    public function actionDisableAccounting()
    {
        $this->stdout('Disable accounting module...' . PHP_EOL, Console::FG_GREEN);
        if (file_exists(self::ACCOUNTING_FLAG_FILE_PATH) && unlink(self::ACCOUNTING_FLAG_FILE_PATH) === false) {
            $this->stderr('Error was occurred trying to delete flag file: ' . self::ACCOUNTING_FLAG_FILE_PATH . PHP_EOL);

            return ExitCode::OSFILE;
        }
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * Resets all data related to accounting synchronization
     * @return int
     */
    public function actionResetAll()
    {
        Console::output();
        $this->stdout('Start resetting Splynx accounting data...' . PHP_EOL, Console::FG_GREEN);
        $this->stdout('Reset accounting customers data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingCustomers();
        $this->stdout('Reset accounting invoices data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingInvoices();
        $this->stdout('Reset accounting credit notes data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingCreditNotes();
        $this->stdout('Reset accounting payments data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingPayments();
        $this->stdout('Reset accounting categories data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingCategories();
        $this->stdout('Reset accounting tax rates data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingTaxRates();
        $this->stdout('Reset accounting payments methods data...' . PHP_EOL, Console::FG_GREEN);
        $this->resetAccountingBankAccounts();
        $this->stdout('Done!' . PHP_EOL, Console::FG_GREEN);

        return ExitCode::OK;
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingCustomers()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingInvoices()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingCreditNotes()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingPayments()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingTaxRates()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingBankAccounts()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionResetAll
     * @return mixed
     */
    protected function resetAccountingCategories()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionSetupAccountingTaxRates
     * @return mixed
     */
    protected function setupAccountingTaxRates()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionSetupAccountingBankAccounts
     * @return mixed
     */
    protected function setupAccountingBankAccounts()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }

    /**
     * This method should be implemented in accounting addon
     * @see actionSetupAccountingCategories
     * @return mixed
     */
    protected function setupAccountingCategories()
    {
        return $this->stdout('Not implemented!' . PHP_EOL, Console::FG_YELLOW);
    }
}
