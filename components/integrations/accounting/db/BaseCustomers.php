<?php

namespace splynx\components\integrations\accounting\db;

use splynx\components\integrations\accounting\db\accounting\BaseAccountingCustomers;
use yii\db\ActiveQuery;

/**
 * Class BaseCustomers
 * @package splynx\components\integrations\accounting\db
 *
 * @property integer $id
 * @property string $billing_type
 * @property integer $partner_id
 * @property integer $location_id
 * @property string $added_by
 * @property integer $added_by_id
 * @property string $login
 * @property string $category
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $street_1
 * @property string $zip_code
 * @property string $city
 * @property string $status
 * @property string $date_add
 * @property string $last_online
 * @property string $last_update
 * @property string $daily_prepaid_cost
 * @property string $gps
 * @property ActiveQuery $accountingCustomer
 */
class BaseCustomers extends BaseRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{customers}}';
    }

    /**
     * @return ActiveQuery
     */
    public function getAccountingCustomer()
    {
        return $this->hasOne(BaseAccountingCustomers::class, ['customer_id' => 'id']);
    }
}
