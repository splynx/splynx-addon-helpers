<?php

namespace splynx\components\integrations\accounting\db;

use splynx\components\integrations\accounting\db\accounting\BaseAccountingCategories;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class BaseBillingTransactionsCategories
 * @package splynx\components\integrations\accounting\db
 *
 * @property integer $id
 * @property string $name
 * @property string $deleted
 * @property string $is_base
 * @property integer $accounting_category_id
 * @property integer $einvoicing_category_id
 * @property integer $einvoicing_private_person_category_id
 * @property integer $einvoicing_type_id
 * @property integer $einvoicing_private_person_type_id
 *
 * @property BaseAccountingCategories $accountingCategory
 */
class BaseBillingTransactionsCategories extends BaseRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{billing_transactions_categories}}';
    }

    /**
     * @param string $indexBy
     *
     * @return ActiveRecord[]
     */
    public static function getAll($indexBy = 'id')
    {
        return static::find()->where(['deleted' => '0'])->indexBy($indexBy)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getAccountingCategory()
    {
        return $this->hasOne(BaseAccountingCategories::class, ['id' => 'accounting_category_id']);
    }

    /**
     * @param string $indexBy
     *
     * @return ActiveRecord[]
     */
    public static function getAllNotMappedCategories($indexBy = 'id')
    {
        return static::find()->where(['accounting_category_id' => null, 'deleted' => '0'])->indexBy($indexBy)->all();
    }

    /**
     * Map in format ['splynx transaction category id' => service category|null]
     * @return array<int, BaseAccountingCategories>
     */
    public static function getAccountingCategoriesMap(): array
    {
        $map = [];
        /** @var static[] $models */
        $models = static::getAll();
        foreach ($models as $model) {
            $map[$model->id] = $model->accountingCategory;
        }

        return $map;
    }
}
