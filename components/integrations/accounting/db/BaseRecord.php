<?php

namespace splynx\components\integrations\accounting\db;

use yii\base\NotSupportedException;
use yii\db\ActiveRecord;

/**
 * Class BaseRecord
 * Base class for access to splynx db models.
 * @package splynx\components\integrations\accounting\db;
 */
abstract class BaseRecord extends ActiveRecord
{
    /**
     * @param bool $runValidation
     * @param array<mixed>|null $attributeNames
     * @return bool
     *
     * @throws NotSupportedException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        throw new NotSupportedException('Method not implemented in ' . static::class);
    }

    /**
     * @throws NotSupportedException
     */
    public function delete()
    {
        throw new NotSupportedException('Method not implemented in ' . static::class);
    }

    /**
     * @param null  $condition
     * @param array<mixed> $params
     *
     * @throws NotSupportedException
     */
    public static function deleteAll($condition = null, $params = [])
    {
        throw new NotSupportedException('Method not implemented in ' . static::class);
    }
}
