<?php

namespace splynx\components\integrations\accounting\db;

use splynx\components\integrations\accounting\db\accounting\BaseAccountingTaxRates;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class BaseTax
 * @package splynx\components\integrations\accounting\db
 *
 * @property int $id
 * @property float $rate
 * @property string $name
 * @property string $type
 * @property bool $archived
 * @property int $einvoicing_tax_rates_id
 * @property int $accounting_tax_rates_id
 * @property BaseAccountingTaxRates $accountingTaxRate
 */
class BaseTax extends BaseRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{tax}}';
    }

    /**
     * @return ActiveRecord[]
     */
    public static function getAll()
    {
        return static::find()->all();
    }

    /**
     * @return ActiveRecord[]
     */
    public static function getAllNotMappedTaxes()
    {
        return static::find()->where(['accounting_tax_rates_id' => null])->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getAccountingTaxRate()
    {
        return $this->hasOne(BaseAccountingTaxRates::class, ['id' => 'accounting_tax_rates_id']);
    }

    /**
     * Map in format ['splynx tax id' => accounting tax rate |null]
     * @return array<int, BaseAccountingTaxRates>
     */
    public static function getAccountingTaxMap(): array
    {
        $map = [];
        /** @var static[] $taxes */
        $taxes = static::getAll();
        foreach ($taxes as $tax) {
            $map[$tax->id] = $tax->accountingTaxRate;
        }

        return $map;
    }
}
