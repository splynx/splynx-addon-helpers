<?php

namespace splynx\components\integrations\accounting\db;

/**
 * Class BasePayments
 * Splynx payments table used for relation.
 * @package splynx\components\integrations\accounting\db
 */
class BasePayments extends BaseRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{payments}}';
    }
}
