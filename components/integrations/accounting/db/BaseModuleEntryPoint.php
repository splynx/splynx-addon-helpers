<?php

namespace splynx\components\integrations\accounting\db;

/**
 * Class BaseModuleEntryPoint
 * @package splynx\components\integrations\accounting\db
 *
 * @property int $id
 * @property string $module
 * @property string $place
 * @property string $type
 * @property string $size
 * @property string $title
 * @property string $root
 * @property string $model
 * @property string $icon
 * @property string $background
 * @property string $url
 * @property string $code
 * @property string $partners
 * @property string $name
 * @property string $location
 * @property string $enabled
 * @property string $location_geo
 * @property string $payment_type
 * @property string $payment_account_id
 */
class BaseModuleEntryPoint extends BaseRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{module_entry_point}}';
    }

    /**
     * @inheritdoc
     * @return array<array<array<string>|string>>
     */
    public function rules()
    {
        return [
            [
                [
                    'module',
                    'place',
                    'type',
                    'size',
                    'title',
                    'root',
                    'model',
                    'icon',
                    'background',
                    'url',
                    'code',
                    'partners',
                    'name',
                    'location',
                    'enabled',
                    'location_geo',
                    'payment_type',
                    'payment_account_id',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @param string $module Addon name
     * @param string $name Entry point name
     *
     * @return static|null
     */
    public static function findByModelAndName($module, $name)
    {
        return static::findOne(['module' => $module, 'name' => $name]);
    }
}
