<?php

namespace splynx\components\integrations\accounting\db;

use splynx\components\integrations\accounting\db\accounting\BaseAccountingBankAccounts;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class BasePaymentsTypes
 * @package splynx\components\integrations\accounting\db
 *
 * @property integer $id
 * @property string $name
 * @property string $is_active
 * @property string $name_1
 * @property string $name_2
 * @property string $name_3
 * @property string $name_4
 * @property string $name_5
 * @property integer $accounting_bank_accounts_id
 * @property integer $einvoicing_payment_methods_id
 * @property string $deleted
 * @property BaseAccountingBankAccounts $accountingBankAccount
 */
class BasePaymentsTypes extends BaseRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{payments_types}}';
    }

    /**
     * @param string $indexBy
     *
     * @return ActiveRecord[]
     */
    public static function getAll($indexBy = 'id')
    {
        return static::find()->where(['deleted' => '0'])->indexBy($indexBy)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getAccountingBankAccount()
    {
        return $this->hasOne(BaseAccountingBankAccounts::class, ['id' => 'accounting_bank_accounts_id']);
    }

    /**
     * Map in format ['splynx payment type id' => service id|null]
     * @return array<int, BaseAccountingBankAccounts>
     */
    public static function getAccountingBanksAccountsMap(): array
    {
        $map = [];

        /** @var static[] $models */
        $models = static::getAll();
        foreach ($models as $model) {
            $map[$model->id] = $model->accountingBankAccount;
        }

        return $map;
    }
}
