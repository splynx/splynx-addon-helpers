<?php

namespace splynx\components\integrations\accounting\db\accounting;

use yii\db\ActiveRecord;

/**
 * Class BaseAccountingBankAccounts
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $id
 * @property string $accounting_id
 * @property string $name
 * @property string $additional_1
 * @property string $additional_2
 * @property string $additional_3
 * @property string $deleted
 */
class BaseAccountingBankAccounts extends BaseAccountingMapping
{
    public const DEFAULT_VALUE = 'default';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{accounting_bank_accounts}}';
    }

    /**
     * @param string $indexBy
     *
     * @return ActiveRecord[]
     */
    public static function getAllBankAccounts($indexBy = 'id')
    {
        return static::find()->where(['deleted' => static::ACCOUNTING_NOT_DELETED])->indexBy($indexBy)->all();
    }

    /**
     * @return null|ActiveRecord
     */
    public static function findDefaultAccount()
    {
        return static::find()->where(['deleted' => static::ACCOUNTING_NOT_DELETED, 'additional_2' => self::DEFAULT_VALUE])->one();
    }

    /**
     * @param string $id
     * @return null|ActiveRecord
     */
    public static function findByAccountingId($id)
    {
        return static::find()->where(['deleted' => static::ACCOUNTING_NOT_DELETED, 'accounting_id' => $id])->one();
    }
}
