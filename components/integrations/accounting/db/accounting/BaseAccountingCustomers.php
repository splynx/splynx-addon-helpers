<?php

namespace splynx\components\integrations\accounting\db\accounting;

use splynx\components\integrations\accounting\pairings\BaseCustomersManualPairing;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\BatchQueryResult;

/**
 * Class BaseAccountingCustomers
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $customer_id
 */
class BaseAccountingCustomers extends BaseAccounting
{
    /**
     * @inheritDoc
     * @return string
     */
    public static function tableName()
    {
        return '{{accounting_customers}}';
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getNewRecords($indexBy = 'customer_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getNewRecords($indexBy, $batchSize);
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getModifiedRecords($indexBy = 'customer_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getModifiedRecords($indexBy, $batchSize);
    }

    /**
     * @param array<int> $Ids
     * @param string $indexBy
     * @return array|ActiveRecord[]
     */
    public static function findByCustomerIds(array $Ids, $indexBy = 'customer_id')
    {
        return static::find()->where(['customer_id' => $Ids, 'deleted' => static::ACCOUNTING_NOT_DELETED])->indexBy($indexBy)->all();
    }

    /**
     * @param integer $id
     * @return ActiveRecord|null
     */
    public static function findByCustomerId($id)
    {
        return static::find()
            ->where(['deleted' => static::ACCOUNTING_NOT_DELETED])
            ->andWhere(['customer_id' => $id])
            ->one();
    }

    /**
     * @param array<int> $Ids
     * @param string $indexBy
     * @return array|ActiveRecord[]
     */
    public static function findSyncedByCustomerIds(array $Ids, $indexBy = 'customer_id')
    {
        return static::find()->where(['customer_id' => $Ids, 'deleted' => static::ACCOUNTING_NOT_DELETED])->andWhere(['not', ['accounting_id' => null]])->indexBy($indexBy)->all();
    }

    /**
     * @return ActiveQuery
     */
    public function getAccountingManualPair()
    {
        return $this->hasOne(BaseCustomersManualPairing::class, ['id' => 'accounting_id']);
    }
}
