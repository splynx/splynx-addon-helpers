<?php

namespace splynx\components\integrations\accounting\db\accounting;

use yii\db\ActiveRecord;
use yii\db\BatchQueryResult;

/**
 * Class BaseAccountingInvoices
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $invoice_id
 */
class BaseAccountingInvoices extends BaseAccounting
{
    /**
     * @inheritDoc
     * @return string
     */
    public static function tableName()
    {
        return '{{accounting_invoices}}';
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getNewRecords($indexBy = 'invoice_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getNewRecords($indexBy, $batchSize);
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getModifiedRecords($indexBy = 'invoice_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getModifiedRecords($indexBy, $batchSize);
    }

    /**
     * @param array<int> $Ids
     * @param string $indexBy
     * @return array|ActiveRecord[]
     */
    public static function findByInvoiceIds(array $Ids, $indexBy = 'invoice_id')
    {
        return static::find()->where(['invoice_id' => $Ids, 'deleted' => static::ACCOUNTING_NOT_DELETED])->indexBy($indexBy)->all();
    }

    /**
     * @param array<int> $Ids
     * @param string $indexBy
     * @return array|ActiveRecord[]
     */
    public static function findSyncedByInvoiceIds(array $Ids, $indexBy = 'invoice_id')
    {
        return static::find()->where(['invoice_id' => $Ids, 'deleted' => static::ACCOUNTING_NOT_DELETED])->andWhere(['not', ['accounting_id' => null]])->indexBy($indexBy)->all();
    }
}
