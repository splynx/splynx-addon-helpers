<?php

namespace splynx\components\integrations\accounting\db\accounting;

use yii\db\ActiveRecord;
use yii\db\BatchQueryResult;

/**
 * Class BaseAccountingCreditNotes
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $credit_note_id
 */
class BaseAccountingCreditNotes extends BaseAccounting
{
    /**
     * @inheritDoc
     * @return string
     */
    public static function tableName()
    {
        return '{{accounting_credit_notes}}';
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getNewRecords($indexBy = 'credit_note_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getNewRecords($indexBy, $batchSize);
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getModifiedRecords($indexBy = 'credit_note_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getModifiedRecords($indexBy, $batchSize);
    }

    /**
     * @param array<int> $ids
     * @param string $indexBy
     * @return array|ActiveRecord[]
     */
    public static function findByCreditNotesIds(array $ids, $indexBy = 'credit_note_id')
    {
        return static::find()->where([
            'credit_note_id' => $ids,
            'deleted' => static::ACCOUNTING_NOT_DELETED,
        ])->indexBy($indexBy)->all();
    }

    /**
     * @param array<int> $ids
     * @param string $indexBy
     * @return array|ActiveRecord[]
     */
    public static function findSyncedByCreditNotesIds(array $ids, $indexBy = 'credit_note_id')
    {
        return static::find()
            ->where([
                'credit_note_id' => $ids,
                'deleted' => static::ACCOUNTING_NOT_DELETED,
            ])
            ->andWhere([
                'not', ['accounting_id' => null],
            ])
            ->indexBy($indexBy)
            ->all();
    }
}
