<?php

namespace splynx\components\integrations\accounting\db\accounting;

use yii\db\ActiveRecord;

/**
 * Class BaseAccountingCategories
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $id
 * @property string $accounting_id
 * @property string $name
 * @property string $additional_1
 * @property string $additional_2
 * @property string $additional_3
 * @property string $deleted
 * @property BaseAccountingTaxRates $accountingTaxRate
 */
class BaseAccountingCategories extends BaseAccountingMapping
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{accounting_categories}}';
    }

    /**
     * @param string $indexBy
     *
     * @return ActiveRecord[]
     */
    public static function getAllCategories($indexBy = 'id')
    {
        return static::find()->where(['deleted' => static::ACCOUNTING_NOT_DELETED])->indexBy($indexBy)->all();
    }

    /**
     * @return BaseAccountingTaxRates|null
     */
    public function getAccountingTaxRate()
    {
        return BaseAccountingTaxRates::findOne(['accounting_id' => $this->additional_1]);
    }
}
