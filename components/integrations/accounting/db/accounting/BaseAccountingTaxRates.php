<?php

namespace splynx\components\integrations\accounting\db\accounting;

use yii\db\ActiveRecord;

/**
 * Class BaseAccountingTaxRates
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $id
 * @property string $accounting_id
 * @property string $name
 * @property string $additional_1
 * @property string $additional_2
 * @property string $additional_3
 * @property string $taxType
 * @property string|float|int $taxRate
 * @property string $deleted
 */
class BaseAccountingTaxRates extends BaseAccountingMapping
{
    /**
     * @inheritDoc
     * @return string
     */
    public static function tableName()
    {
        return '{{accounting_tax_rates}}';
    }

    /**
     * @param string $indexBy
     *
     * @return ActiveRecord[]
     */
    public static function getAllTaxRates($indexBy = 'id')
    {
        return static::find()->where(['deleted' => static::ACCOUNTING_NOT_DELETED])->indexBy($indexBy)->all();
    }

    /**
     * @param string $id
     * @return null|ActiveRecord
     */
    public static function findByAccountingId($id)
    {
        return static::find()->where(['deleted' => static::ACCOUNTING_NOT_DELETED, 'accounting_id' => $id])->one();
    }

    /**
     * @return string
     */
    public function getTaxType()
    {
        return $this->accounting_id;
    }

    /**
     * @param string $taxType
     * @return void
     */
    public function setTaxType($taxType)
    {
        $this->accounting_id = $taxType;
    }

    /**
     * @return float
     */
    public function getTaxRate()
    {
        return (float)$this->additional_1;
    }

    /**
     * @param string|int|float $rate
     * @return void
     */
    public function setTaxRate($rate)
    {
        $this->additional_1 = (string)$rate;
    }
}
