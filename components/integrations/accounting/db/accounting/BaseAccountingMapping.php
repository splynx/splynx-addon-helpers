<?php

namespace splynx\components\integrations\accounting\db\accounting;

use splynx\components\integrations\ResettableInterface;
use Throwable;
use yii\db\ActiveRecord;
use yii\db\StaleObjectException;

/**
 * Class BaseAccountingMapping
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $id
 * @property string $accounting_id
 * @property string $name
 * @property string $additional_1
 * @property string $additional_2
 * @property string $additional_3
 * @property string $deleted
 */
abstract class BaseAccountingMapping extends ActiveRecord implements ResettableInterface
{
    public const ACCOUNTING_NOT_DELETED = '0';
    public const ACCOUNTING_DELETED = '1';

    /**
     * @inheritDoc
     * @return false|int
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function reset()
    {
        return $this->delete();
    }

    /**
     * @return int
     */
    public static function resetAll()
    {
        return static::deleteAll();
    }
}
