<?php

namespace splynx\components\integrations\accounting\db\accounting;

use splynx\components\integrations\accounting\db\BaseCustomers;
use splynx\components\integrations\accounting\db\BasePayments;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\BatchQueryResult;

/**
 * Class BaseAccountingPayments
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $payment_id
 * @property ActiveQuery $splynxPayment
 */
class BaseAccountingPayments extends BaseAccounting
{
    /**
     * @inheritDoc
     * @return string
     */
    public static function tableName()
    {
        return '{{accounting_payments}}';
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getNewRecords($indexBy = 'payment_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getNewRecords($indexBy, $batchSize);
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected static function extendNewRecordsQuery(ActiveQuery $query): ActiveQuery
    {
        return $query->joinWith('splynxPayment');
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getModifiedRecords($indexBy = 'payment_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::getModifiedRecords($indexBy, $batchSize);
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected static function extendModifiedRecordsQuery(ActiveQuery $query): ActiveQuery
    {
        return $query->joinWith('splynxPayment');
    }

    /**
     * @return ActiveQuery
     */
    public function getSplynxPayment()
    {
        return $this->hasOne(BasePayments::class, ['id' => 'payment_id']);
    }

    /**
     * @param integer $id
     *
     * @return null|ActiveRecord
     */
    public static function getByPaymentId($id)
    {
        return static::find()
            ->where(['deleted' => static::ACCOUNTING_NOT_DELETED])
            ->andWhere(['payment_id' => $id])
            ->one();
    }

    /**
     * @param string $indexBy
     * @param int $batchSize
     * @return BatchQueryResult
     */
    public static function findRecordsForDelete($indexBy = 'payment_id', $batchSize = self::BATCH_SIZE)
    {
        return parent::findRecordsForDelete($indexBy, $batchSize);
    }

    /**
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected static function extendRecordsForDeleteQuery(ActiveQuery $query): ActiveQuery
    {
        $paymentsTableName = BasePayments::tableName();
        $customersTableName = BaseCustomers::tableName();

        return $query
            ->joinWith('splynxPayment')
            ->leftJoin($customersTableName, "$customersTableName.id = $paymentsTableName.customer_id")
            ->andWhere([
                "$customersTableName.deleted" => static::ACCOUNTING_NOT_DELETED,
            ]);
    }
}
