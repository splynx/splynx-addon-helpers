<?php

namespace splynx\components\integrations\accounting\db\accounting;

use splynx\components\integrations\accounting\db\BaseCustomers;
use splynx\components\integrations\ResettableInterface;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\BatchQueryResult;

/**
 * Class BaseAccounting
 * @package splynx\components\integrations\accounting\db\accounting
 *
 * @property integer $id
 * @property string $modified
 * @property string|null $accounting_id
 * @property string $create_date
 * @property string $last_update
 * @property string|int $accounting_status
 * @property string|null $additional_1
 * @property string|null $additional_2
 * @property string|null $additional_3
 * @property string|null $deleted
 * @property BaseCustomers $splynxCustomer
 */
abstract class BaseAccounting extends ActiveRecord implements ResettableInterface
{
    // Accounting statuses
    public const ACCOUNTING_STATUS_NEW = 0;
    public const ACCOUNTING_STATUS_PENDING = 1;
    public const ACCOUNTING_STATUS_UNKNOWN = 2;
    public const ACCOUNTING_STATUS_ERROR = 3;
    public const ACCOUNTING_STATUS_OK = 4;

    public const ACCOUNTING_NOT_MODIFIED = '0';
    public const ACCOUNTING_MODIFIED = '1';

    public const ACCOUNTING_NOT_DELETED = '0';
    public const ACCOUNTING_DELETED = '1';

    /** @var int Size that match with api recommended batch request size */
    public const BATCH_SIZE = 100;

    /**
     * @param bool $runValidation
     * @param array<string> $attributeNames
     *
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (!parent::save($runValidation, $attributeNames)) {
            Yii::error($this->getErrors(), 'error');
            return false;
        }

        return true;
    }

    /**
     * @throws NotSupportedException
     */
    public function delete()
    {
        throw new NotSupportedException('Method not supported');
    }

    /**
     * @param null $condition
     * @param array<string, mixed> $params
     * @return int
     *
     * @throws NotSupportedException
     */
    public static function deleteAll($condition = null, $params = [])
    {
        throw new NotSupportedException('Method not supported');
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getNewRecords($indexBy = 'id', $batchSize = self::BATCH_SIZE)
    {
        $tableName = static::tableName();
        $query = static::find()
            ->where([
                "$tableName.modified" => static::ACCOUNTING_MODIFIED,
                "$tableName.accounting_id" => null,
                "$tableName.deleted" => static::ACCOUNTING_NOT_DELETED,
            ])
            ->indexBy($indexBy);

        return static::extendNewRecordsQuery($query)->batch($batchSize);
    }

    /**
     * @param string $indexBy
     * @param integer $batchSize
     *
     * @return BatchQueryResult
     */
    public static function getModifiedRecords($indexBy = 'id', $batchSize = self::BATCH_SIZE)
    {
        $tableName = static::tableName();
        $query = static::find()
            ->where([
                "$tableName.modified" => static::ACCOUNTING_MODIFIED,
                "$tableName.deleted" => static::ACCOUNTING_NOT_DELETED,
            ])
            ->andWhere(['not', ["$tableName.accounting_id" => null]])
            ->indexBy($indexBy);

        return static::extendModifiedRecordsQuery($query)->batch($batchSize);
    }

    /**
     * @param string|integer $accountingId
     *
     * @return null|static
     */
    public static function getByAccountingId($accountingId)
    {
        /** @var null|static $model */
        $model = static::find()
            ->where(['deleted' => static::ACCOUNTING_NOT_DELETED])
            ->andWhere(['accounting_id' => $accountingId])
            ->one();

        return $model;
    }

    /**
     * @param string $message
     * @param integer $status
     * @param string $modified
     * @return bool
     */
    public function success($message = '', $status = self::ACCOUNTING_STATUS_OK, $modified = self::ACCOUNTING_NOT_MODIFIED)
    {
        $this->modified = $modified;
        $this->accounting_status = $status;
        $this->additional_1 = $message;

        return $this->save();
    }

    /**
     * @param string $message
     * @param integer $status
     * @return bool
     */
    public function fail($message = '', $status = self::ACCOUNTING_STATUS_ERROR)
    {
        $this->accounting_status = $status;
        $this->additional_1 = $message;

        return $this->save();
    }

    /**
     * @param string $indexBy
     * @param int $batchSize
     * @return BatchQueryResult
     */
    public static function getRecordsForReset($indexBy = 'id', $batchSize = self::BATCH_SIZE)
    {
        return static::find()
            ->where(['deleted' => static::ACCOUNTING_NOT_DELETED])
            ->andWhere(['not', ['accounting_id' => null]])
            ->indexBy($indexBy)
            ->batch($batchSize);
    }

    /**
     * Reset accounting data for all table
     * @return int
     */
    public static function resetAll()
    {
        $count = 0;
        foreach (static::getRecordsForReset() as $batchResult) {
            /** @var BaseAccounting $record */
            foreach ($batchResult as $record) {
                if (!$record->reset()) {
                    Yii::error($record->getErrors());
                } else {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Base method for accounting data record reset.
     * Can be reimplemented in child classes.
     * @return bool
     */
    public function reset()
    {
        $this->accounting_id = null;
        $this->modified = static::ACCOUNTING_MODIFIED;
        $this->accounting_status = static::ACCOUNTING_STATUS_NEW;
        $this->additional_1 = null;
        $this->additional_2 = null;
        $this->additional_3 = null;

        return $this->save();
    }

    /**
     * @param string $indexBy
     * @param int $batchSize
     * @return BatchQueryResult
     */
    public static function findRecordsForDelete($indexBy = 'id', $batchSize = self::BATCH_SIZE)
    {
        $tableName = static::tableName();
        $query = static::find()
            ->where([
                "$tableName.modified" => static::ACCOUNTING_MODIFIED,
                "$tableName.deleted" => static::ACCOUNTING_DELETED,
            ])
            ->andWhere(['not', ["$tableName.accounting_id" => null]])
            ->indexBy($indexBy);

        return static::extendRecordsForDeleteQuery($query)->batch($batchSize);
    }

    /**
     * Checks if accounting id was already assigned to Splynx record.
     * Deleted records not included.
     * To prevent payments double sync bank statements and invoice checking logic also should be used.
     * @param string|integer $accountingId
     *
     * @return bool
     */
    public static function hasSynchronizedRecord($accountingId)
    {
        return static::find()
            ->where(['accounting_id' => $accountingId, 'deleted' => static::ACCOUNTING_NOT_DELETED])
            ->exists();
    }

    /**
     * @return int
     */
    public static function countNewRecords()
    {
        $tableName = static::tableName();
        $query = static::find()
            ->where([
                "$tableName.modified" => static::ACCOUNTING_MODIFIED,
                "$tableName.accounting_id" => null,
                "$tableName.deleted" => static::ACCOUNTING_NOT_DELETED,
            ]);

        return (int)static::extendNewRecordsQuery($query)->count();
    }

    /**
     * @return int
     */
    public static function countModifiedRecords()
    {
        $tableName = static::tableName();
        $query = static::find()
            ->where([
                "$tableName.modified" => static::ACCOUNTING_MODIFIED,
                "$tableName.deleted" => static::ACCOUNTING_NOT_DELETED,
            ])
            ->andWhere(['not', ["$tableName.accounting_id" => null]]);

        return (int)static::extendModifiedRecordsQuery($query)->count();
    }

    /**
     * @return int
     */
    public static function countRecordsForDelete()
    {
        $tableName = static::tableName();
        $query = static::find()
            ->where([
                "$tableName.modified" => static::ACCOUNTING_MODIFIED,
                "$tableName.deleted" => static::ACCOUNTING_DELETED,
            ])
            ->andWhere(['not', ["$tableName.accounting_id" => null]]);

        return (int)static::extendRecordsForDeleteQuery($query)->count();
    }

    /**
     * Extend base query with specific additional params.
     * If customer not deleted.
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected static function extendRecordsForDeleteQuery(ActiveQuery $query): ActiveQuery
    {
        return $query;
    }

    /**
     * Checks if accounting id was already assigned to deleted Splynx record which wasn't deleted in accounting.
     * @param string|int $accountingId
     * @return bool
     */
    public static function hasDeletedSynchronizedRecord($accountingId): bool
    {
        return static::find()
            ->where([
                    'accounting_id' => $accountingId,
                    'deleted' => static::ACCOUNTING_DELETED,
                    'modified' => static::ACCOUNTING_MODIFIED,
            ])
            ->exists();
    }

    /**
     * Extend base query with specific additional params.
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected static function extendNewRecordsQuery(ActiveQuery $query): ActiveQuery
    {
        return $query;
    }

    /**
     * Extend base query with specific additional params.
     * @param ActiveQuery $query
     * @return ActiveQuery
     */
    protected static function extendModifiedRecordsQuery(ActiveQuery $query): ActiveQuery
    {
        return $query;
    }
}
