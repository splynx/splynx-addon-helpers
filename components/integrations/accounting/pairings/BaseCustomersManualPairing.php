<?php

namespace splynx\components\integrations\accounting\pairings;

use yii\db\ActiveRecord;

/**
 * Class BaseCustomersManualPairing
 * @package splynx\components\integrations\accounting\pairings
 *
 * @property string $id
 * @property string $name
 */
class BaseCustomersManualPairing extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customers_manual_pairing}}';
    }

    /**
     * @return array<array<string>>
     */
    public function rules()
    {
        return [
            ['id', 'string'],
            ['id', 'required'],
            ['id', 'unique'],
            ['name', 'string'],
        ];
    }

    /**
     * Search customers by names
     * @param string $name
     * @param integer $limit
     * @return ActiveRecord[]
     */
    public static function findByName($name, $limit = 50)
    {
        return static::find()->where(['like', 'name', $name])->limit($limit)->all();
    }
}
