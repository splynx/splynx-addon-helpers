<?php

namespace splynx\components\integrations;

use splynx\components\tasks\models\BaseWorker;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\helpers\Console;

use const STDERR;
use const STDOUT;

/**
 * Class BaseSyncService
 * @package splynx\components\integrations;
 *
 * @property array $partnersIdsToSkip
 * @property-write int $taskWithErrorsCount
 * @property-write BaseWorker $task
 * @property null|int $taskTotalCount
 */
class BaseSyncService extends Model
{
    /** @var array<int>|null Partners ids to ignore through synchronization */
    private $_partnersIdsToSkip;

    /** @var array<int> Payment Types ids to synchronize */
    private $_paymentTypesIdsFilter;

    /**
     * @var BaseWorker|null $_task Worker for syncs running in background
     */
    private $_task;

    /**
     * @return array<int>
     */
    public function getPartnersIdsToSkip()
    {
        if (!$this->_partnersIdsToSkip) {
            $this->_partnersIdsToSkip = [];
        }

        return $this->_partnersIdsToSkip;
    }

    /**
     * Set partner ids that should be skipped in syncs
     * @param array<int> $ids Black list of partners ids
     * @return void
     */
    public function setPartnersIdsToSkip(array $ids)
    {
        $this->_partnersIdsToSkip = $ids;
    }

    /**
     * Checks if partner should be skipped
     * @param int $id Partner id
     *
     * @return bool true if partner should be skipped
     */
    protected function skipByPartnerId($id)
    {
        return in_array($id, $this->getPartnersIdsToSkip(), true);
    }

    /**
     * @return array<int>
     */
    public function getPaymentTypesIdsFilter()
    {
        return $this->_paymentTypesIdsFilter;
    }

    /**
     * @param array<int> $paymentTypesIdsFilter White list of payments types ids
     * @return void
     */
    public function setPaymentTypesIdsFilter(array $paymentTypesIdsFilter)
    {
        $this->_paymentTypesIdsFilter = $paymentTypesIdsFilter;
    }

    /**
     * Checks if partner should be skipped
     * @param int $id Payment type id
     *
     * @return bool true if partner should be skipped
     */
    protected function skipByPaymentTypeId($id)
    {
        return !in_array($id, $this->getPaymentTypesIdsFilter(), true);
    }

    /**
     * Set worker if running in background
     * @param BaseWorker $worker
     * @return void
     */
    public function setTask(BaseWorker $worker)
    {
        $this->_task = $worker;
    }

    /**
     * Updates task progress
     * @param integer $progress
     *
     * @return bool
     * @throws InvalidConfigException
     */
    protected function addProgress($progress)
    {
        if (!$this->_task) {
            return true;
        }
        return $this->_task->process->updateProgress($progress);
    }

    /**
     * Sets task total count
     * @param integer $total
     * @return void
     */
    protected function setTaskTotalCount($total)
    {
        if (!$this->_task) {
            return;
        }

        $this->_task->process->total_count = $total;
    }

    /**
     * Get task errors count
     * @return int|null
     */
    protected function getTaskTotalCount()
    {
        if (!$this->_task) {
            return null;
        }

        return $this->_task->process->total_count;
    }

    /**
     * Set task errors count
     * @param integer $total
     * @return void
     */
    protected function setTaskWithErrorsCount($total)
    {
        if (!$this->_task) {
            return;
        }

        $this->_task->process->with_errors_count = $total;
    }

    /**
     * Increase task errors count.
     *
     * @return void
     */
    protected function increaseTaskErrorCount()
    {
        if (!$this->_task) {
            return;
        }

        $this->_task->process->with_errors_count++;
    }

    /**
     * Returns a value indicating whether ANSI color is enabled.
     *
     * ANSI color is enabled only if [[color]] is set true or is not set
     * and the terminal supports ANSI color.
     *
     * @param resource $stream the stream to check.
     * @return bool Whether to enable ANSI style in output.
     */
    protected function isColorEnabled($stream = STDOUT)
    {
        return Console::streamSupportsAnsiColors($stream);
    }

    /**
     * Prints a string to STDOUT.
     *
     * You may optionally format the string with ANSI codes by
     * passing additional parameters using the constants defined in [[\yii\helpers\Console]].
     *
     * Example:
     *
     * ```
     * $this->stdout('This will be red and underlined.', Console::FG_RED, Console::UNDERLINE);
     * ```
     *
     * @return int|bool Number of bytes printed or false on error
     */
    protected function stdout(): bool | int
    {
        $args = func_get_args();
        $string = array_shift($args);

        Yii::info($string, 'sync_info');
        if (!defined('STDOUT')) {
            return 0;
        }

        if ($this->isColorEnabled()) {
            $string = Console::ansiFormat($string, $args);
        }

        return Console::stdout($string);
    }

    /**
     * Prints a string to STDERR.
     *
     * You may optionally format the string with ANSI codes by
     * passing additional parameters using the constants defined in [[\yii\helpers\Console]].
     *
     * Example:
     *
     * ```
     * $this->stderr('This will be red and underlined.', Console::FG_RED, Console::UNDERLINE);
     * ```
     *
     * @return int|bool Number of bytes printed or false on error
     */
    protected function stderr(): bool | int
    {
        $args = func_get_args();
        $string = array_shift($args);

        Yii::error($string, 'sync_error');
        if (!defined('STDERR')) {
            return 0;
        }

        if ($this->isColorEnabled(STDERR)) {
            $string = Console::ansiFormat($string, $args);
        }

        return Console::stderr($string);
    }

    /**
     * Prints yii model errors
     * @param array<string, mixed> $errors
     * @return void
     */
    protected function printErrors(array $errors)
    {
        foreach ($errors as $attribute => $attributeErrors) {
            $this->stderr($attribute . PHP_EOL, Console::FG_RED);
            if (is_array($attributeErrors)) {
                foreach ($attributeErrors as $error) {
                    $this->stderr($error . PHP_EOL, Console::FG_RED);
                }
            } else {
                $this->stderr($attributeErrors . PHP_EOL, Console::FG_RED);
            }
        }
    }
}
