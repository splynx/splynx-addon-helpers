<?php

namespace splynx\components\integrations\client\exceptions;

/**
 * Class Exception
 * @package splynx\components\integrations\client\exceptions;
 */
class Exception extends \Exception
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'Exception';
    }
}
