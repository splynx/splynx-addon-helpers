<?php

namespace splynx\components\integrations\client\exceptions;

/**
 * Class ApiAuthException
 * @package splynx\components\integrations\client\exceptions;
 */
class ApiAuthException extends Exception
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'Api authentication exception';
    }
}
