<?php

namespace splynx\components\integrations\client\exceptions;

/**
 * Class ApiLimitException
 * @package splynx\components\integrations\client\exceptions;
 */
class ApiCallLimitException extends Exception
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'Api call limit exception';
    }
}
