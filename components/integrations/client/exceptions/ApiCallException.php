<?php

namespace splynx\components\integrations\client\exceptions;

/**
 * Class ApiCallException
 * @package splynx\components\integrations\client\exceptions;
 */
class ApiCallException extends Exception
{
    /**
     * @return string
     */
    public function getName()
    {
        return 'Api call exception';
    }
}
