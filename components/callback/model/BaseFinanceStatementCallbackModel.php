<?php

namespace splynx\components\callback\model;

use yii\db\ActiveRecord;

/**
 * Class BaseFinanceStatementCallbackModel
 * @package splynx\components\callback
 * @property int $id
 * @property int $bank_statement_id
 * @property string $status
 */
class BaseFinanceStatementCallbackModel extends ActiveRecord
{
    public const CALLBACK_STATUS_NEW = 'new';
    public const CALLBACK_STATUS_PROCESSING = 'processing';
    public const CALLBACK_STATUS_PROCESSED = 'processed';
    public const CALLBACK_STATUS_FAILED = 'failed';

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return '{{%finance_statement_callback}}';
    }

    /**
     * @return array<int, array<int|string, string[]|string>>
     */
    public function rules(): array
    {
        return [
            [['bank_statement_id', 'status'], 'required'],
            [['bank_statement_id'], 'integer'],
            [
                'status',
                'in',
                'range' => [
                    self::CALLBACK_STATUS_FAILED,
                    self::CALLBACK_STATUS_PROCESSED,
                ],
            ],
        ];
    }

    /**
     * @param int $bankStatementId
     * @param string $status
     * @return bool
     */
    public function create(int $bankStatementId, string $status): bool
    {
        $this->bank_statement_id = $bankStatementId;
        $this->status = $status;
        return $this->save();
    }

    /**
     * @param int $bankStatementId
     * @return bool
     */
    public function processed(int $bankStatementId): bool
    {
        return $this->create($bankStatementId, static::CALLBACK_STATUS_PROCESSED);
    }

    /**
     * @param int $bankStatementId
     * @return bool
     */
    public function failed(int $bankStatementId): bool
    {
        return $this->create($bankStatementId, static::CALLBACK_STATUS_FAILED);
    }
}
