<?php

/* @var $this View */

/* @var $content string */

use splynx\widgets\Alert;
use splynx\assets\HelperAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\web\YiiAsset;

$this->registerAssetBundle(YiiAsset::class, $this::POS_HEAD);
$this->registerAssetBundle(BootstrapAsset::class, $this::POS_HEAD);
$this->registerAssetBundle(HelperAsset::class, $this::POS_HEAD);
$this->registerAssetBundle(BootstrapPluginAsset::class, $this::POS_HEAD);
?>
<?php
$this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php
        $this->head() ?>
    </head>
    <body class="bg-grey">
    <?php
    $this->beginBody() ?>
    <div class="wrap-callback">
        <?= Alert::widget() ?>

        <?= $content ?>
    </div>

    <?php
    $this->endBody() ?>
    </body>
    </html>
<?php
$this->endPage() ?>