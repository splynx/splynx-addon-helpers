<?php

namespace splynx\components\callback;

use Exception;
use JsonException;
use splynx\components\callback\model\BaseFinanceStatementCallbackModel;
use splynx\helpers\ArrayHelper;
use splynx\helpers\ConfigHelper;
use splynx\v2\models\finance\BankStatement;
use Yii;
use yii\base\Component;
use yii\web\Response;

class BaseFinanceStatementCallback extends Component
{
    public const STATUS_SUCCESS = 'success';
    public const STATUS_PENDING = 'pending';
    public const STATUS_FAILED = 'failed';

    public const RESULT_URL_FIELD = 'result_url';
    public const ACTION_URL_FIELD = 'action_url';
    public const CALLBACK_AUTHENTICATION_ID_FIELD = 'callback_authentication_id';
    public const CALLBACK_AUTHENTICATION_SECRET_FIELD = 'callback_authentication_secret';
    public const DATA_FIELD = 'data';
    public const STATUS_FIELD = 'status';
    public const BANK_STATEMENT_ID_FIELD = 'bank_statement_id';
    public const SIGNATURE_FIELD = 'signature';
    public const PROCESS_PENDING_FIELD = 'process_pending';
    public const CALLBACK_STATUS_FIELD = 'callback_status';

    /**
     * @return mixed|null
     */
    public static function getCallbackAuthenticationId()
    {
        return ConfigHelper::get(static::CALLBACK_AUTHENTICATION_ID_FIELD);
    }

    /**
     * @return mixed|null
     */
    public static function getCallbackAuthenticationSecret()
    {
        return ConfigHelper::get(static::CALLBACK_AUTHENTICATION_SECRET_FIELD);
    }

    /**
     * @param BankStatement $bankStatement
     * @return string|null
     */
    public static function getStatus(BankStatement $bankStatement): ?string
    {
        if (
            $bankStatement->status === BankStatement::STATUS_PROCESSED
            && $bankStatement->callback_status === BaseFinanceStatementCallbackModel::CALLBACK_STATUS_PROCESSED
        ) {
            return static::STATUS_SUCCESS;
        }

        if (
            $bankStatement->status === BankStatement::STATUS_CANCELED
            || $bankStatement->status === BankStatement::STATUS_ERROR
            || $bankStatement->callback_status === BaseFinanceStatementCallbackModel::CALLBACK_STATUS_FAILED
        ) {
            return static::STATUS_FAILED;
        }

        if (
            $bankStatement->status === BankStatement::STATUS_PENDING
            || $bankStatement->callback_status === BaseFinanceStatementCallbackModel::CALLBACK_STATUS_NEW
            || $bankStatement->callback_status === BaseFinanceStatementCallbackModel::CALLBACK_STATUS_PROCESSING
        ) {
            return static::STATUS_PENDING;
        }

        return null;
    }

    /**
     * @param BankStatement $bankStatement
     * @param array<int|string, mixed> $data
     * @return void
     */
    public static function setStatus(BankStatement $bankStatement, array &$data): void
    {
        $data[static::STATUS_FIELD] = static::getStatus($bankStatement);
    }

    /**
     * @param BankStatement $bankStatement
     * @param array<int|string, mixed> $data
     * @return void
     */
    public static function setBankStatementId(BankStatement $bankStatement, array &$data): void
    {
        $data[static::BANK_STATEMENT_ID_FIELD] = $bankStatement->id;
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function generateSecret(): string
    {
        if (!empty(static::getCallbackAuthenticationSecret())) {
            return '';
        }

        return Yii::$app->getSecurity()->generateRandomString(64);
    }

    /**
     * @param array<int|string, mixed> $data
     * @return bool
     * @throws JsonException
     */
    public static function validateSignature(array $data): bool
    {
        $signature = $data[static::SIGNATURE_FIELD];
        unset($data[static::SIGNATURE_FIELD]);

        return $signature === static::generateSignature($data);
    }

    /**
     * @param array<int|string, mixed> $data
     * @return string
     * @throws JsonException
     */
    public static function generateSignature(array $data): string
    {
        ksort($data);
        return hash_hmac('sha256', json_encode($data, JSON_THROW_ON_ERROR), static::getCallbackAuthenticationSecret());
    }

    /**
     * @param BankStatement $bankStatement
     * @param array<int|string, mixed> $additionalData
     * @return bool
     */
    public static function sendResult(BankStatement $bankStatement, array $additionalData = []): bool
    {
        $url = static::getUrlForSendResult($bankStatement, $additionalData);
        if ($url === null) {
            return false;
        }

        if (Yii::$app->response instanceof Response) {
            Yii::$app->response->redirect($url);
        }

        return true;
    }

    /**
     * @param BankStatement $bankStatement
     * @param array<int|string, mixed> $additionalData
     * @return string|null
     */
    public static function getUrlForSendResult(BankStatement $bankStatement, array $additionalData = []): ?string
    {
        if (!is_array($bankStatement->callback_data) || !$bankStatement->isCallbackData() || static::getStatus($bankStatement) === null) {
            return null;
        }

        $callbackData = $bankStatement->callback_data;

        $additionalData[static::CALLBACK_STATUS_FIELD] = $bankStatement->callback_status;
        $data = ArrayHelper::merge($callbackData[static::DATA_FIELD], $additionalData);

        static::setStatus($bankStatement, $data);
        static::setBankStatementId($bankStatement, $data);

        return $callbackData[static::RESULT_URL_FIELD] . '?' . http_build_query($data);
    }
}
