<?php

namespace splynx\components\callback\controllers;

use splynx\components\callback\model\BaseFinanceStatementCallbackModel;
use splynx\helpers\ConfigHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\Response;

abstract class BaseCallbackController extends Controller
{
    /** @var class-string<ActiveRecord> */
    public string $model = BaseFinanceStatementCallbackModel::class;

    public $layout = '@app/../' . ConfigHelper::ADD_ON_BASE . '/vendor/splynx/splynx-addon-helpers/components/callback/views/layout';

    /**
     * @inheritdoc
     */
    public function getViewPath(): string
    {
        return '/var/www/splynx/addons/' . ConfigHelper::ADD_ON_BASE . '/vendor/splynx/splynx-addon-helpers/components/callback/views';
    }

    /**
     * @inheritdoc
     * @return array<string, string|array<string, mixed>>
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     * @throws BadRequestHttpException
     */
    public function beforeAction($action): bool
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return mixed
     */
    abstract public function actionAction();

    /**
     * @return mixed
     */
    abstract public function actionResult();

    /**
     * @param array<int|string, mixed> $where
     * @return array<string, mixed>
     */
    public function actionCheckCallbackStatus(array $where = []): array
    {
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        return [
            'result' => $this->callbackExists($where),
        ];
    }

    /**
     * @param array<int|string, mixed> $where
     * @param array<int|string, mixed> $data
     * @param string|null $checkUrl
     * @return string
     */
    protected function showPending(array $where = [], array $data = [], ?string $checkUrl = null): string
    {
        return $this->render('pending', ['where' => $where, 'data' => $data, 'checkUrl' => $checkUrl]);
    }

    /**
     * @param array<int|string, mixed> $where
     * @return bool
     */
    protected function callbackExists(array $where = []): bool
    {
        return $this->model::find()->where($where)->exists();
    }
}
