<?php

namespace splynx\components\callback\migrations;

use yii\db\Migration;
use yii\helpers\ArrayHelper;

class BaseMigrationsForFinanceStatementCallbackTable extends Migration
{
    /**
     * @param string $tableName
     * @param array<string, mixed> $columns
     * @return void
     */
    public function create(string $tableName = '{{%finance_statement_callback}}', array $columns = []): void
    {
        $this->createTable(
            $tableName,
            ArrayHelper::merge([
                'id' => $this->primaryKey(),
                'bank_statement_id' => $this->integer(),
                'status' => $this->string(),
            ], $columns)
        );
    }

    /**
     * @param string $tableName
     * @return void
     */
    public function drop(string $tableName = '{{%finance_statement_callback}}'): void
    {
        $this->dropTable($tableName);
    }
}
