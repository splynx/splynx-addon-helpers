<?php

namespace splynx\components;

/**
 * Class BrowserEventEmitter provide server to browser communication interface
 * @package splynx\components
 */
class BrowserEventEmitter
{
    /**
     * Emit event to browser
     * @param string|array<string> $rooms
     * @param string $event
     * @param array<mixed> $data
     * @return bool
     */
    public static function emit($rooms, $event, $data)
    {
        if (empty($rooms)) {
            return true;
        }
        if (!is_array($rooms)) {
            $rooms = [$rooms];
        }
        $ok = false;
        $dataToSend = [
            'command' => 'emit_event',
            'rooms' => $rooms,
            'params' => [
                'event' => $event,
                'data' => $data,
            ],
        ];

        $msg = json_encode($dataToSend) . "\n";

        if (($sock = socket_create(AF_INET, SOCK_STREAM, 0))) {
            if (@socket_connect($sock, '127.0.0.1', 8099)) {
                if (@socket_send($sock, $msg, strlen($msg), 0)) {
                    $ok = true;
                }
            }
            socket_close($sock);
        }

        return $ok;
    }
}
