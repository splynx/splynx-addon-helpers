<?php

namespace splynx\components\log\models;

use yii\base\Model;

/**
 * Class LogProperty
 * @package splynx\components\log\models
 */
class LogProperty extends Model
{
    public const TYPE_ARRAY = 'array';
    public const TYPE_OBJECT = 'object';
    public const TYPE_STRING = 'string';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_DATETIME = 'datetime';

    /**
     * Properties for target config
     */
    public const TYPE = 'type'; // Configure type for property
    public const HIDE_INTO_DETAILS_VIEW = 'in_details'; // Set flag about hide from table and show in details modal window
    public const DETAILS_VIEW_SHOW_IN_ADDITIONAL_PANEL = 'show_in_additional_panel'; // Show value in additional panel in details modal window
    public const OUTPUT_CALLBACK = 'output_callback'; // Callback for show value
    public const INPUT_CALLBACK = 'input_callback'; // Callback for write value

    /** @var array<int, string>|array<string, array<string, mixed>> $property  */
    public $property;

    /** @var array<string> $_propertyNames */
    private $_propertyNames;

    /** @var array<string, array<mixed>>*/
    public $propertyParams;

    /**
     * Prepare value for write to db
     * @param string $property
     * @param mixed $value
     * @return false|string
     */
    public function getValueForWrite($property, $value)
    {
        $params = $this->getPropertyParams($property);
        if (!empty($params)) {
            return $this->prepareValue($params, $value);
        }

        if (is_array($value) || is_object($value)) {
            return json_encode($value);
        }

        return $value;
    }

    /**
     * @return array<string> of property names
     */
    public function getPropertyNames()
    {
        if ($this->_propertyNames === null) {
            $this->_propertyNames = [];
            /** @var array<string|string> $item */
            foreach ($this->property as $key => $item) {
                /** @var string $value */
                $value = is_array($item) ? $key : $item;//@phpstan-ignore-line
                $this->_propertyNames[] = $value;
            }
        }
        return $this->_propertyNames;
    }

    /**
     * @param string $name
     * @return array<string, mixed> property params
     */
    public function getPropertyParams($name)
    {
        return isset($this->property[$name]) ? $this->property[$name] : [];//@phpstan-ignore-line
    }

    /**
     * Prepare value
     * @param array<string, mixed> $params
     * @param mixed $value
     * @return false|string
     */
    public function prepareValue($params, $value)
    {
        if (isset($params[static::INPUT_CALLBACK])) {
            return $params[static::INPUT_CALLBACK]($value);
        }

        if (isset($params[static::TYPE])) {
            return $this->prepareByValueType($params[static::TYPE], $value);
        }

        return $value;
    }

    /**
     * Prepare values by type
     * @param string $type
     * @param mixed $value
     * @return false|string
     */
    public function prepareByValueType($type, $value)
    {
        switch ($type) {
            case static::TYPE_ARRAY:
            case static::TYPE_OBJECT:
                return json_encode($value);
            default:
                return $value;
        }
    }

    /**
     * @return  array<string, array<mixed>> with properties params
     */
    public function getPropertiesParams()
    {
        if ($this->propertyParams === null) {
            $this->propertyParams = [];
            foreach ($this->getPropertyNames() as $name) {
                $this->propertyParams[$name] = $this->getPropertyParams($name);
            }
        }

        return $this->propertyParams;
    }
}
