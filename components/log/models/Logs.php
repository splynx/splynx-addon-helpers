<?php

namespace splynx\components\log\models;

use Exception;
use splynx\components\log\SplynxLogger;
use splynx\components\log\targets\SplynxDbTarget;
use splynx\helpers\DateHelper;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Inflector;

/**
 * Class Logs
 * @package splynx\components\log\models
 */
class Logs extends ActiveRecord
{
    /** @var string */
    public static $tableName = '{{%log}}';

    /** @var array<string, mixed>|null */
    protected static $logRecords;

    /** @var int */
    private $_counterNestedItems = 0;

    /** @var SplynxDbTarget|null */
    protected $_target;

    /** @var array<string, mixed>|null */
    protected $propertiesParams;

    /** @var bool|null */
    private $_hasHideInDetails;

    /** @var bool|null */
    private $_existActions;

    /**
     * @inheritdoc
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->setTarget();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return static::$tableName;
    }

    /**
     * Return base properties.
     *
     * @return array<string>
     */
    public function getDefaultProperties()
    {
        return [
            'id',
            'message',
            'status',
        ];
    }

    /**
     * Return properties from target config if this is splynx target.
     *
     * @return array<string>
     * @throws Exception
     */
    public function getPropertiesFromTarget()
    {
        if (!empty($this->_target)) {
            /** @var LogProperty $properties */
            $properties = $this->getTarget()->properties;
            return $properties->getPropertyNames();
        }

        return [];
    }

    /**
     * @param SplynxDbTarget|null $target
     * @return void
     */
    public function setTarget($target = null)
    {
        if (!empty($target)) {
            $this->_target = $target;
            return;
        }

        $this->_target = $this->findTarget();
    }

    /**
     * @return SplynxDbTarget
     * @throws Exception
     */
    public function getTarget()
    {
        if (empty($this->_target)) {
            $this->setTarget();
            if (empty($this->_target)) {
                throw new Exception(Yii::t('app', 'Target not found.'));
            }
        }

        return $this->_target;
    }

    /**
     * @return SplynxDbTarget|null
     */
    protected function findTarget()
    {
        /** @var SplynxDbTarget[] $targets */
        $targets = \Yii::$app->log->targets;
        foreach ($targets as $target) {
            if (isset($target->logTable) && $target->logTable == static::$tableName && isset($target->properties)) {//@phpstan-ignore-line
                return $target;
            }
        }

        return null;
    }

    /**
     * Return all attributes.
     *
     * @return array<string>
     * @throws Exception
     */
    public function getAttributesForList()
    {
        return ArrayHelper::merge($this->getDefaultProperties(), $this->getPropertiesFromTarget());
    }

    /**
     * Generate and return DataTable header
     * @return string
     * @throws Exception
     */
    public function generateCodeForTableHead()
    {
        $html = '';
        $attributes = $this->getAttributesForList();

        foreach ($attributes as $attribute) {
            if ($this->isHideInDetails($attribute)) {
                continue;
            }

            $html .= Html::tag('th', $this->getAttributeLabel($attribute));
        }

        if ($this->checkExistActions()) {
            $html .= Html::tag('th', Yii::t('app', 'Actions'));
        }

        return $html;
    }

    /**
     * Return all log record from database
     * @return array<string, mixed>
     */
    public function getAllLogRecords()
    {
        if (static::$logRecords === null) {
            static::$logRecords = static::find()->asArray()->all();
        }

        return static::$logRecords;
    }

    /**
     * Convert all log records form object to array
     * @return array<string, mixed>|null
     * @throws Exception
     */
    public function getItemsForDataTable()
    {
        return $this->prepareData();
    }

    /**
     * Return table column names for DataTable
     * @return string
     * @throws Exception
     */
    public function getTableColumnNames()
    {
        $columns = [];

        foreach ($this->getAttributesForList() as $attribute) {
            if (!$this->isHideInDetails($attribute)) {
                $columns[] = ['data' => $attribute, 'name' => $attribute];
            }
        }

        if ($this->checkExistActions()) {
            $columns[] = ['data' => 'logActions'];
        }

        return json_encode($columns, JSON_THROW_ON_ERROR);
    }

    /**
     * Clear log table
     * @return int
     */
    public function clearAll()
    {
        return static::deleteAll();
    }

    /**
     * Prepare data before print
     * @return array<string, mixed>|null
     * @throws Exception
     */
    public function prepareData()
    {
        foreach ($this->getAllLogRecords() as $index => $record) {
            static::$logRecords[$index] = $this->prepareRecord($record);
        }

        return static::$logRecords;
    }

    /**
     * @return array<string, mixed>
     * @throws \Exception
     */
    public function getServerSideData()
    {
        /** @var array<string, mixed> $logRecords */
        $logRecords = $this->getRequestForServerSide()->asArray()->all();
        foreach ($logRecords as $index => $record) {
            $logRecords[$index] = $this->prepareRecord($record);
        }
        return $logRecords;
    }

    /**
     * @return int|string
     */
    public function getServerSideCount()
    {
        return $this->getRequestForServerSide()->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \Exception
     */
    public function getRequestForServerSide()
    {
        /** @var \yii\web\Request $request */
        $request = Yii::$app->getRequest();
        $get = $request->get();
        $query = static::find()
            ->offset($get['start'])
            ->limit($get['length']);

        if (isset($get['order']) && !empty($get['order'])) {
            $columns = json_decode($this->getTableColumnNames(), true);
            $orders = [];
            foreach ($get['order'] as $order) {
                if (isset($columns[$order['column']]) && $columns[$order['column']]['data'] != 'logActions') {
                    $orders[$columns[$order['column']]['data']] = $order['dir'] === 'desc' ? SORT_DESC : SORT_ASC;
                }
            }

            $query->orderBy($orders);
        }

        /** @var LogProperty $properties */
        $properties = $this->getTarget()->properties;
        $params = $properties->getPropertiesParams();
        foreach (($get['columns']) as $column) {
            $value = $column['search']['value'];
            if (!empty($value)) {
                $name = $column['data'];

                if (isset($params[$name]) && $params[$name]['type'] == LogProperty::TYPE_DATETIME) {
                    $value = explode(' - ', $value);
                    $dateFrom = DateHelper::convertDateFormat($value[0], 'Y-m-d', DateHelper::getSplynxDateFormat());
                    $dateTo = DateHelper::convertDateFormat($value[1], 'Y-m-d', DateHelper::getSplynxDateFormat());
                    if (!empty($dateFrom) && !empty($dateTo)) {
                        $whereCondition = [
                            'and',
                        ];
                        $whereCondition[] = ['BETWEEN', $name, $dateFrom . ' 00:00:00', $dateTo . ' 23:59:59'];
                        $query->andWhere($whereCondition);
                    }
                } else {
                    $query->andWhere(['LIKE', $name, '%' . $value . '%', false]);
                }
            }
        }

        if (!empty($get['search']) && !empty($get['search']['value'])) {
            $attributesForList = $this->getAttributesForList();
            $whereCondition = [
                'or',
            ];
            foreach ($attributesForList as $attributes) {
                $whereCondition[] = ['LIKE', $attributes, '%' . $get['search']['value'] . '%', false];
            }
            $query->andWhere($whereCondition);
        }

        return $query;
    }

    /**
     * @param array<string, mixed> $record
     * @param bool $details
     * @return array<string, mixed>
     * @throws Exception
     */
    public function prepareRecord($record, $details = false)
    {
        foreach ($record as $key => $item) {
            if (!$details && $this->isHideInDetails($key)) {
                unset($record[$key]);
                continue;
            }

            $record[$key] = $this->prepareValue($key, $item, $details);
        }

        $actions = $this->getActions($record);
        if (!$details && !empty($actions)) {
            $record['logActions'] = implode(' ', $actions);
        }

        return $record;
    }

    /**
     * Print array
     * @param array<mixed> $array
     * @param string $key
     * @param string $prefix
     * @return string
     */
    public function printArray($array, $key = '', $prefix = '')
    {
        if (count($array) > 5) {
            $array = [
                Inflector::humanize($key) => $array,
            ];
        }

        $html = "<ul class='sidebar-nav' style='padding-left: 10px'>";
        $html .= $this->getArrayView($array, $prefix);
        $html .= '</ul>';
        return '<div>' . $html . '</div>';
    }

    /**
     * Recursive build list
     * @param string $prefix
     * @param array<mixed> $array
     * @return string
     */
    protected function getArrayView($array, $prefix = '')
    {
        $arrayHtml = '';
        foreach ($array as $key => $value) {
            $keyValue = is_numeric($key) ? ($key + 1) : $key;
            if (is_array($value)) {
                $value = $this->getArrayView($value, $prefix);
                $this->_counterNestedItems++;
                $arrayHtml .= "
                <li style='list-style-type: none;'>
                    <a href='#{$prefix}-submenu-{$this->_counterNestedItems}' class='accordion-toggle collapsed toggle-switch' data-toggle='collapse'>
                        <span><strong>$keyValue: </strong></span>
                        <b class='caret'></b>
                    </a>
                    <ul id='{$prefix}-submenu-{$this->_counterNestedItems}' class='panel-collapse collapse panel-switch' role='menu'>
                        $value
                    </ul>
                </li>
            ";
            } else {
                $arrayHtml .= "<li style='list-style-type: none;'><strong>$keyValue:</strong>  $value</li>";
            }
        }
        return $arrayHtml;
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param bool $details
     * @return string
     * @throws Exception
     */
    public function prepareValue($attribute, $value, $details = false)
    {
        /** @var LogProperty $properties */
        $properties = $this->getTarget()->properties;
        $params = $properties->getPropertyParams($attribute);

        if ($attribute == 'status' && $this->getTarget()->prettyStatus) {
            return $this->getStatusLabel($value);
        }

        if (empty($params)) {
            $result = json_decode($value, true);
            if ($result != null && is_array($result)) {
                if ($details) {
                    return $this->printArray($result, $attribute, 'details');
                }

                return $this->printArray($result, $attribute);
            }
            return $value;
        }

        if (isset($params[LogProperty::OUTPUT_CALLBACK])) {
            return $params[LogProperty::OUTPUT_CALLBACK]($value);
        }

        if (!empty($params[LogProperty::TYPE])) {
            return $this->prepareValueByType($params[LogProperty::TYPE], $value, $attribute, $details);
        }

        return $value;
    }

    /**
     * Prepare value for output by type
     * @param string $type
     * @param string $value
     * @param string $attribute
     * @param bool $details
     * @return mixed|string
     */
    public function prepareValueByType($type, $value, $attribute, $details = false)
    {
        switch ($type) {
            case LogProperty::TYPE_ARRAY:
            case LogProperty::TYPE_OBJECT:
                $value = json_decode($value, true);
                if (!empty($value)) {
                    if (is_array($value)) {
                        if ($details) {
                            return $this->printArray($value, $attribute, 'details');
                        }

                        return $this->printArray($value, $attribute);
                    }

                    return Yii::t('app', 'Error while preparing data to display.');
                }

                return '';
            default:
                return $value;
        }
    }

    /**
     * @return bool if true exist actions (used for add column actions into datatable)
     * @throws Exception
     */
    public function checkExistActions()
    {
        if ($this->_existActions === null) {
            $this->_existActions = false;
            if ($this->hasHideInDetails() || $this->getTarget()->enableActions) {
                $this->_existActions = true;
            }
        }

        return $this->_existActions;
    }

    /**
     * @param array<string, mixed> $model
     * @return array<string, mixed>
     * @throws Exception
     */
    public function getActions($model)
    {
        return ArrayHelper::merge($this->logActions($model), $this->getDefaultLogActions($model));
    }

    /**
     * Override this for add custom actions
     *
     * Example: return [
     *      'test_action' => Html::a("Test action", 'example/link'),
     * ];
     * @param array<string, mixed> $model
     * @return array<string, mixed>
     */
    public function logActions($model)
    {
        return [];
    }

    /**
     * @param array<string, mixed> $model
     * @return array<string, mixed> array of default actions
     * @throws Exception
     */
    protected function getDefaultLogActions($model)
    {
        $actions = [];
        if ($this->hasHideInDetails()) {
            $actions['details_action'] = Html::label(
                Html::a("<i class='icon-ic_fluent_open_24_regular'></i>", '#', [
                    'onclick' => "showDetailsModal({$model['id']})",
                ]),
                null,
                [
                    'class' => 'action-cell',
                ]
            );
        }
        return $actions;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function hasHideInDetails()
    {
        if ($this->_hasHideInDetails === null) {
            $this->_hasHideInDetails = false;
            /** @var LogProperty $properties */
            $properties = $this->getTarget()->properties;
            foreach ($properties->getPropertyNames() as $propertyName) {
                if ($this->isHideInDetails($propertyName)) {
                    $this->_hasHideInDetails = true;
                }
            }
        }

        return $this->_hasHideInDetails;
    }

    /**
     * @return  array<string, array<mixed>> properties params
     * @throws Exception
     */
    public function getPropertyParams()
    {
        if ($this->propertiesParams === null) {
            /** @var LogProperty $properties */
            $properties = $this->getTarget()->properties;
            $this->propertiesParams = $properties->getPropertiesParams();
        }

        return $this->propertiesParams;
    }

    /**
     * @param string $propertyName
     * @param string $parameterName
     * @return mixed|null
     * @throws Exception
     */
    public function getPropertyParameter($propertyName, $parameterName)
    {
        $parameters = $this->getPropertyParams();
        return isset($parameters[$propertyName][$parameterName]) ? $parameters[$propertyName][$parameterName] : null;
    }

    /**
     * check if in property config set HIDE_INTO_DETAILS_VIEW
     * @param string $propertyName
     * @return bool|null
     * @throws Exception
     */
    public function isHideInDetails($propertyName)
    {
        return $this->getPropertyParameter($propertyName, LogProperty::HIDE_INTO_DETAILS_VIEW);
    }

    /**
     * Get and prepare record by id
     * @param integer $id
     * @return array<string, mixed>|null
     */
    public static function getSingleRecord($id)
    {
        /** @var array<string, mixed>|null $record */
        $record = static::find()->where(['id' => $id])->asArray()->one();

        if (empty($record)) {
            return $record;
        }

        return (new static())->prepareRecord($record, true);//@phpstan-ignore-line
    }

    /**
     * Need override for add custom status labels
     * @return array<string, string>
     */
    public function statusesLabels()
    {
        return [];
    }

    /** @var array<string, string> */
    public static $labelsCache;

    /**
     * @param string $value
     * @return string
     */
    public function getStatusLabel($value)
    {
        // Ask if clear cash after update add-on
        if (static::$labelsCache === null) {
            static::$labelsCache = ArrayHelper::merge([
                SplynxLogger::STATUS_SUCCESS => '<span class="label label-success">'
                    . Yii::t('app', 'Success') . '</span>',
                SplynxLogger::STATUS_FAILED => '<span class="label label-danger">'
                    . Yii::t('app', 'Failed') . '</span>',
            ], $this->statusesLabels());
        }

        return isset(static::$labelsCache[$value]) ? static::$labelsCache[$value] : $value;
    }
}
