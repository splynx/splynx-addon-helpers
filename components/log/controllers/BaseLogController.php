<?php

namespace splynx\components\log\controllers;

use Exception;
use splynx\components\log\models\LogProperty;
use splynx\components\log\models\Logs;
use splynx\helpers\ConfigHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class BaseLogController
 *
 * You may extends from this controller your log controller if you want use default views
 * @package splynx\components\log\controllers
 */
class BaseLogController extends Controller
{
    /** @var string namespace to Logs model */
    public static $logModel = 'splynx\components\log\models\Logs';

    public $layout = '@app/../' . ConfigHelper::ADD_ON_BASE . '/vendor/splynx/splynx-addon-helpers/components/log/views/layout';

    /** @var int */
    public $serverSideLimit = 1000;

    /**
     * @inheritdoc
     */
    public function getViewPath()
    {
        return '/var/www/splynx/addons/' . ConfigHelper::ADD_ON_BASE . '/vendor/splynx/splynx-addon-helpers/components/log/views';
    }

    /**
     * @inheritdoc
     * @return array<string, string|array<string, mixed>>
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return array<string, array<string, mixed>|string>
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Action index
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex()
    {
        /** @var Logs $model */
        $model = Yii::createObject(static::$logModel);
        $dates = [];
        /** @var LogProperty $properties */
        $properties = $model->getTarget()->properties;
        foreach ($properties->getPropertiesParams() as $name => $params) {
            if (isset($params['type']) && $params['type'] == LogProperty::TYPE_DATETIME) {
                $dates[$name] = Inflector::humanize($name);
            }
        }

        return $this->render('log', [
            'dates' => $dates,
            'model' => $model,
        ]);
    }

    /**
     * Action get-logs-records
     * @return array<string, mixed>
     * @throws InvalidConfigException
     */
    public function actionGetLogsRecords()
    {
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        /** @var Logs $model */
        $model = Yii::createObject(static::$logModel);
        $count = $model::find()->count();

        if ($count <= $this->serverSideLimit) {
            return [
                'data' => $model->getItemsForDataTable(),
            ];
        }

        return [
            'data' => $model->getServerSideData(),
            'draw' => 0,
            'recordsTotal' => $count,
            'recordsFiltered' => $model->getServerSideCount(),
            'serverSideEnabled' => true,
        ];
    }

    /**
     * Dummy for action clear-log
     * @return array{'response': true, 'message': string}
     */
    public function actionClearLog()
    {
        /** @var Response $response */
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        return [
            'response' => true,
            'message' => Yii::t('app', 'Old log records will be cleared automatically'),
        ];
    }

    /**
     * Action get-record
     * @param integer $id
     * @return string
     * @throws Exception
     */
    public function actionGetLogRecord($id)
    {
        /** @var Logs $logModel */
        $logModel = new static::$logModel();

        return $this->render('modal', [
            'record' => $logModel::getSingleRecord($id),
            'propertyParams' => $logModel->getPropertyParams(),
            'model' => $logModel,
        ]);
    }
}
