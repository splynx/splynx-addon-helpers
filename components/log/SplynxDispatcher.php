<?php

namespace splynx\components\log;

use Exception;
use splynx\components\log\targets\SplynxDbTarget;
use yii\base\ErrorHandler;
use yii\log\Dispatcher;
use Yii;
use yii\log\Logger;

/**
 * Class SplynxDispatcher
 * @package splynx\components\log
 */
class SplynxDispatcher extends Dispatcher
{
    /**
     * @param \splynx\components\log\SplynxLogger $logger
     * @param array<mixed> $config
     */
    public function __construct(SplynxLogger $logger, array $config = [])
    {
        Yii::setLogger($logger);
        parent::__construct($config);
    }
    /**
     * @inheritDoc
     * @param array<mixed> $messages
     * @return void
     */
    public function dispatch($messages, $final)
    {
        $targetErrors = [];
        foreach ($this->targets as $target) {
            if ($target->enabled) {
                try {
                    // Fix for other targets
                    $messagesForTarget = $messages;
                    if (!($target instanceof SplynxDbTarget)) {
                        $messagesForTarget = array_filter($messages, function ($message) {
                            return isset($message[1]);
                        });
                    }

                    $target->collect($messagesForTarget, $final);
                } catch (Exception $e) {
                    $target->enabled = false;
                    $targetErrors[] = [
                        'Unable to send log via ' . get_class($target) . ': ' . ErrorHandler::convertExceptionToString($e),
                        Logger::LEVEL_WARNING,
                        __METHOD__,
                        microtime(true),
                        [],
                    ];
                }
            }
        }

        if (!empty($targetErrors)) {
            $this->dispatch($targetErrors, true);
        }
    }

    /**
     * @return void
     *
     * @throws \yii\db\Exception
     */
    public function rotate()
    {
        foreach ($this->targets as $target) {
            if ($target->enabled && $target instanceof SplynxDbTarget) {
                $target->rotate();
            }
        }
    }
}
