<?php

namespace splynx\components\log\migrations;

use yii\db\Migration;
use yii\helpers\ArrayHelper;

/**
 * Class BaseMigrationForLogTable
 *
 * If you want use custom field in you log, you mast create migration and extends her from this class.
 * For data type array you mast create field type string. SplynxDbTarget convert array to json.
 *
 * @package splynx\components\log\migrations
 */
class BaseMigrationForLogTable extends Migration
{
    /**
     * @param string $tableName
     * @param array<string, mixed> $columns
     * @return null
     */
    public function create($tableName = "{{%log}}", $columns = [])
    {
        return $this->createTable($tableName, ArrayHelper::merge([
            'id' => $this->primaryKey(),
            'message' => $this->string(),
            'status' => $this->string(),
        ], $columns));
    }
}