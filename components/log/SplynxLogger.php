<?php

namespace splynx\components\log;

use yii\db\Exception;

/**
 * Class Logger
 * @package splynx\components\log
 */
class SplynxLogger extends \yii\log\Logger
{
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILED = 'failed';

    /**
     * @param string $message log message
     * @param array<string, mixed> $columns array with your colymns where key this is name column in your database
     * @param string $status
     * @param string $category
     * @return void
     */
    public function writeToLog($message, $columns, $status, $category = 'application')
    {
        $this->messages[] = [
            'message' => $message,
            'columns' => $columns,
            'status' => $status,
            'category' => $category,
        ];

        if ($this->flushInterval > 0 && count($this->messages) >= $this->flushInterval) {
            $this->flush();
        }
    }

    /**
     * Write logs with success status
     *
     * @param string $message
     * @param array<string, mixed> $columns array where key this is name column in your table and value it is value your field
     *          example: [
     *              'user_id' => 6,
     *              'error' => 'Error message'
     *          ]
     * @param string $category
     * @return void
     */
    public function success($message, $columns = [], $category = 'application')
    {
        $this->writeToLog($message, $columns, static::STATUS_SUCCESS, $category);
    }

    /**
     * Write logs with failed status
     *
     * @param string $message
     * @param array<string, mixed> $columns array where key this is name column in your table and value it is value your field
     *          example: [
     *              'user_id' => 6,
     *              'error' => 'Error message'
     *          ]
     * @param string $category
     * @return void
     */
    public function failed($message, $columns = [], $category = 'application')
    {
        $this->writeToLog($message, $columns, static::STATUS_FAILED, $category);
    }

    /**
     * @param bool $final
     * @return void
     */
    public function flush($final = false)
    {
        try {
            parent::flush($final);
            if ($final && method_exists($this->dispatcher, 'rotate')) {
                $this->dispatcher->rotate();
            }
        } catch (Exception $e) {
            // We get an error if there are problems with the database, and we do nothing
            return;
        }
    }
}
