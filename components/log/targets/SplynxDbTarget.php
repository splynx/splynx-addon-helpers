<?php

namespace splynx\components\log\targets;

use splynx\components\log\models\LogProperty;
use yii\helpers\ArrayHelper;
use yii\log\DbTarget;

/**
 * Class SplynxDbTarget
 *
 * For use this target add this code to web.php/console.php
 *
 * 'components' => [
 *      'log' => [
 *          'targets' => [
 *              [
 *                  'class' => 'splynx\components\log\targets\SplynxDbTarget',
 *                  'logTable' => 'log',
 *                  'properties' => [
 *                      'testProperty' => [
 *                          'type' => LogProperty::TYPE_STRING
 *                       ],
 *                  ]
 *              ],
 *          ]
 *      ]
 * ],
 *
 * @package splynx\components\log\targets
 */
class SplynxDbTarget extends DbTarget
{
    /** @var bool */
    public $prettyStatus = false;

    /** @var bool */
    public $enableActions = false;

    /** @var int */
    public $timeToStoreLogs = 60 * 60 * 24 * 30 * 6; // 6 month

    /** @var LogProperty */
    public $properties;

    /** @var string */
    private $_tableColumns;

    /** @var string */
    private $_tableValues;

    /**
     * @inheritdoc
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->properties = new LogProperty([
            'property' => $this->properties,
        ]);
    }

    /**
     * Method for export records to database.
     *
     * @return void
     *
     * @throws \yii\db\Exception
     */
    public function export()
    {
        /** @var \yii\db\Connection $db */
        $db = $this->db;
        if ($db->getTransaction()) {
            $db = clone $db;
        }

        $tableName = $db->quoteTableName($this->logTable);

        $this->generateValuesAndColumnsList();

        $sql = "INSERT INTO $tableName ($this->_tableColumns)
                VALUES ($this->_tableValues)";

        $command = $db->createCommand($sql);
        foreach ($this->messages as $message) {
            if (!isset($message['message'])) {
                continue;
            }

            $bindValues = [
                ':message' => $message['message'],
                ':status' => $message['status'],
            ];

            if ($this->properties instanceof LogProperty) {
                foreach ($this->properties->getPropertyNames() as $column) {
                    $bindValues[":$column"] = $this->getColumnValue($message, $column);
                }
            }

            $command->bindValues($bindValues)->execute();
        }
    }

    /**
     * Get list all columns.
     *
     * @return array<string> list all columns
     */
    public function getColumns()
    {
        return ArrayHelper::merge([
            'message',
            'status',
        ], $this->properties->getPropertyNames());
    }

    /**
     * Generate templates for request by column list.
     *
     * @return void
     */
    public function generateValuesAndColumnsList()
    {
        $this->_tableColumns = '';
        $this->_tableValues = '';
        foreach ($this->getColumns() as $column) {
            if (empty($this->_tableColumns) || empty($this->_tableValues)) {
                $this->_tableColumns = "[[$column]]";
                $this->_tableValues = ":$column";
                continue;
            }

            $this->_tableColumns .= ", [[$column]]";
            $this->_tableValues .= ", :$column";
        }
    }

    /**
     * Filter and export send messages to export
     * @param array<mixed> $messages
     * @param bool $final
     * @return void
     *
     * @throws \yii\db\Exception
     */
    public function collect($messages, $final = true)
    {
        $this->messages = ArrayHelper::merge($this->messages, $this->filteringMessages($messages));
        $count = count($this->messages);
        if ($count > 0 && ($final || ($this->exportInterval > 0 && $count >= $this->exportInterval))) {
            // Set exportInterval to 0 to avoid triggering export again while exporting
            $oldExportInterval = $this->exportInterval;
            $this->exportInterval = 0;
            $this->export();
            $this->exportInterval = $oldExportInterval;
            $this->messages = [];
        }
    }

    /**
     * Check is set column in message and convert array or object to json.
     *
     * @param array<mixed> $message
     * @param string $columnName
     * @return false|null|string
     */
    public function getColumnValue($message, $columnName)
    {
        if (!isset($message['columns'][$columnName])) {
            return null;
        }

        return $this->properties->getValueForWrite($columnName, $message['columns'][$columnName]);
    }

    /**
     * @param array<mixed> $messages
     * @return array<mixed>
     */
    public function filteringMessages($messages)
    {
        foreach ($messages as $key => $message) {
            if (!isset($message['category']) || !(in_array($message['category'], $this->categories) || empty($this->categories))) {
                unset($messages[$key]);
            }
        }

        return $messages;
    }

    /**
     * @return void
     *
     * @throws \yii\db\Exception
     */
    public function rotate()
    {
        // Check if date_time field is present
        if (!in_array('date_time', $this->getColumns())) {
            return;
        }
        /** @var \yii\db\Connection $db */
        $db = $this->db;
        $tableName = $db->quoteTableName($this->logTable);
        $sql = "DELETE FROM $tableName WHERE date_time <= :date_time";
        $bindValues = [
            ':date_time' => date('Y-m-d H:i:s', time() - $this->timeToStoreLogs),
        ];
        $command = $db->createCommand($sql);
        $command->bindValues($bindValues)->execute();
    }
}
