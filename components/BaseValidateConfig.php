<?php

namespace splynx\components;

use splynx\helpers\ConfigHelper;
use splynx\v2\helpers\ApiHelper;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\UserException;

/**
 * Class BaseValidateConfig
 * @package splynx\components
 */
class BaseValidateConfig extends Component
{
    /**
     * @return void
     * @throws InvalidConfigException
     * @throws UserException
     */
    public static function checkAll(): void
    {
        self::checkApi();

        // A method some add-ons have to check configuration.
        static::checkCustomConfigs();
    }

    /**
     * @return void
     * @throws InvalidConfigException
     * @throws UserException
     */
    public static function checkApi(): void
    {
        if (!ConfigHelper::get('api_key')) {
            throw new UserException('Error: api_key is not set. Please check your addon config!');
        }

        if (!ConfigHelper::get('api_secret')) {
            throw new UserException('Error: api_secret is not set. Please check your addon config!');
        }

        $apiDomain = ConfigHelper::get('api_domain');
        if (!$apiDomain) {
            throw new UserException('Error: api_domain is not set. Please check your addon config!');
        }

        if (strpos($apiDomain, 'http://') !== 0 && strpos($apiDomain, 'https://') !== 0) {
            throw new UserException(
                'Error: api_domain must start with `http://` or `https://`. Please check your addon config!'
            );
        }

        if (!ApiHelper::checkApi()) {
            throw new UserException('Error: Api call error. Please check your addon config!');
        }
    }

    /**
     * A stub method, it need to avoid errors when inherited add-on doesn't have own method.
     * @return void
     */
    public static function checkCustomConfigs(): void
    {
    }
}
