<?php

namespace splynx\components\twig;

use Twig\TwigFunction;
use yii\twig\Extension;
use yii\widgets\Pjax;

class TwigPjax extends Extension
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'Pjax';
    }

    /**
     * {@inheritdoc}
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('pjax_begin', [$this, 'begin']),
            new TwigFunction('pjax_end', [$this, 'end']),
        ];
    }

    /**
     * @param array<string, mixed> $args
     * @return \yii\base\Widget|\yii\widgets\Pjax
     */
    public function begin($args = [])
    {
        return Pjax::begin($args);
    }

    /**
     * @return void
     */
    public function end()
    {
        Pjax::end();
    }
}
