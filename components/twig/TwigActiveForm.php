<?php

namespace splynx\components\twig;

use Twig\TwigFunction;
use yii\bootstrap\ActiveForm;
use yii\twig\Extension;

class TwigActiveForm extends Extension
{
    /**
     * {@inheritdoc}
     * @return string
     */
    public function getName()
    {
        return 'ActiveForm';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('form_begin', [$this, 'begin']),
            new TwigFunction('form_end', [$this, 'end']),
        ];
    }

    /**
     * @param array<string, mixed> $args
     * @return \yii\base\Widget|\yii\bootstrap\ActiveForm
     */
    public function begin($args)
    {
        return ActiveForm::begin($args);
    }

    /**
     * @return void
     */
    public function end()
    {
        ActiveForm::end();
    }
}
