<?php

namespace splynx\components\tasks\commands;

use splynx\components\tasks\models\BaseBackgroundTask;
use Yii;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class ProcessController
 * @package splynx\components\tasks\controllers
 */
class BaseProcessController extends Controller
{
    /** @var int */
    public $processId;

    /**
     * Override if you use custom model
     *
     * Example: protected $model = Tasks::class
     *
     * @var class-string<BaseBackgroundTask>
     */
    protected $model = BaseBackgroundTask::class;

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        return ['processId'];
    }

    /**
     * Action for run background work.
     *
     * @return void
     *
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRunInBackground()
    {
        if ($this->processId === null) {
            $this->stdout($this->getFormattedMessage(Yii::t('app', 'Error starting background process. Error: processId is empty')), Console::FG_RED);
            return;
        }

        $this->stdout($this->getFormattedMessage(Yii::t('app', 'Start background process with id: {id}', [
            'id' => $this->processId,
        ])), Console::FG_GREEN);

        /** @var BaseBackgroundTask $model */
        $model = Yii::createObject($this->model);
        $result = $model::runProcess($this->processId);

        if ($result['result']) {
            $this->stdout($this->getFormattedMessage(Yii::t('app', 'Background process with id: {id} successfully ended', [
                'id' => $this->processId,
            ])), Console::FG_GREEN);
            return;
        }

        $this->stdout($this->getFormattedMessage(Yii::t('app', "The background process with id: {id} failed. Errors: \n{errors}", [
            'id' => $this->processId,
            'errors' => print_r($result['message'], true),
        ])), Console::FG_RED);
    }

    /**
     * @param string $message
     * @return string formatted message
     */
    private function getFormattedMessage($message)
    {
        return date('Y-m-d H:i:s') . ' ' . $message . "\n";
    }
}
