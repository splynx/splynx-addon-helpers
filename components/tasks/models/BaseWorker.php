<?php

namespace splynx\components\tasks\models;

use yii\base\Model;

/**
 * Class BaseWorker processing long work
 *
 * @package splynx\components\tasks\models
 */
abstract class BaseWorker extends Model
{
    /** @var array<mixed> */
    public $arguments;

    /** @var BaseBackgroundTask */
    public $process;

    /** @var double */
    private $_progress;//@phpstan-ignore-line

    /** @var mixed */
    private $_result;

    /**
     * @return mixed
     */
    abstract public function run();

    /**
     * Set result.
     *
     * @param mixed $result
     * @return void
     */
    public function setResult($result)
    {
        $this->_result = $result;
    }

    /**
     * Get result
     * @return mixed
     */
    public function getResult()
    {
        return $this->_result;
    }

    /**
     * Callback for success result working process.
     *
     * @return void
     */
    public function onSuccess()
    {
    }

    /**
     * Callback for error result working process.
     *
     * @return void
     */
    public function onError()
    {
    }

    /**
     * Callback for processing.
     *
     * @param double $progress
     * @return void
     */
    public function onProgress($progress)
    {
        $this->_progress = $progress;
    }

    /**
     * Callback for done with errors result working process.
     *
     * @return void
     */
    public function onDoneWithErrors()
    {
    }

    /**
     * Callback for down status.
     *
     * @return void
     */
    public function onDown()
    {
    }
}
