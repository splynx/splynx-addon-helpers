<?php

namespace splynx\v2\base;

use splynx\base\ApiResponseException;
use splynx\base\BaseApiModel;
use splynx\base\BaseBatchApiResult;
use splynx\base\CheckResponseTrait;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;

/**
 * Class BaseActiveApi
 *
 * @property bool $isNewRecord Whether the record is new and should be inserted when calling [[save()]].
 * @package splynx\v2\base
 */
class BaseActiveApi extends BaseApiModel
{
    use CheckResponseTrait;

    /**
     * In some cases after saving model Splynx are change some model properties values like total in invoice.
     * So if you have such model you must set this property to true - to get updated data from Splynx.
     * @var bool
     */
    protected $reloadAttributesAfterSave = false;

    /**
     * @var array<string, string|array<string, mixed>>|null old attribute values indexed by attribute names.
     * This is `null` if the record [[isNewRecord|is new]].
     */
    private $_oldAttributes;

    /** @var string */
    protected static $apiUrl;

    /**
     * @param static $model
     * @param array<string, mixed> $data
     * @return void
     */
    public static function populate($model, $data)
    {
        parent::populate($model, $data);
        $model->_oldAttributes = $model->attributes;
    }

    /**
     * @param null|int|string $id
     * @param array<string, mixed> $conditions
     * @return string
     */
    protected function getApiUrl($id = null, $conditions = [])
    {
        $result = static::$apiUrl;

        // Set id
        if ($id !== null) {
            $result .= '/' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            // Normalize conditions, change all null values to empty string, because http_build_query doesn't know how to work with null
            $conditions = $this->normalizeCondition($conditions);
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * @return string|null
     */
    public function getPrimaryKey()
    {
        return 'id';
    }

    /**
     * @param array<string, mixed> $conditions
     * @param bool $one
     * @return array<static>|static|null
     * @throws ApiResponseException
     */
    protected function find(array $conditions, $one)
    {
        if ($one) {
            $conditions['limit'] = 1;
        }

        try {
            $result = ApiHelper::getInstance()->get($this->getApiUrl(null, $conditions));
        } catch (InvalidConfigException $e) {
            return null;
        }

        if ($result['result'] == false) {
            $this->checkResponseForError($result['response']);
        }

        /** @var array{'result': bool, 'response': array<string, mixed>} $result */

        if (empty($result['response'])) {
            return $one ? null : [];
        }

        /** @var non-empty-array<static> $models */
        $models = [];

        foreach ($result['response'] as $item) {
            $model = new static();//@phpstan-ignore-line
            static::populate($model, $item);
            $models[] = $model;
        }

        return $one ? reset($models) : $models;
    }

    /**
     * @param int|string|null $id
     * @return null|static
     * @throws ApiResponseException
     */
    public function findById($id)
    {
        if ($id === null) {
            return null;
        }

        try {
            $result = ApiHelper::getInstance()->get($this->getApiUrl($id));
        } catch (InvalidConfigException $e) {
            return null;
        }

        if ($result['result'] == false) {
            $this->checkResponseForError($result['response']);
        }

        /** @var array{'result': bool, 'response': array<string, mixed>} $result */

        if (empty($result['response'])) {
            return null;
        }

        $model = new static();//@phpstan-ignore-line
        static::populate($model, $result['response']);

        return $model;
    }

    /**
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $limit
     * @param int $offset
     * @return array<string, mixed>
     */
    protected function combineSearchConditions(
        array $mainAttributes = [],
        array $additionalAttributes = [],
        array $order = [],
        $limit = null,
        $offset = null
    ) {
        $result = [];
        if (!empty($mainAttributes)) {
            $result['main_attributes'] = $mainAttributes;
        }
        if (!empty($additionalAttributes)) {
            $result['additional_attributes'] = $additionalAttributes;
        }
        if (!empty($order)) {
            $result['order'] = $order;
        }
        if ($limit !== null) {
            if (!is_int($limit)) {
                throw new InvalidParamException('limit must be integer');
            }
            $result['limit'] = $limit;
        }

        if ($offset !== null) {
            if (!is_int($offset)) {
                throw new InvalidParamException('offset must be integer');
            }
            $result['offset'] = $offset;
        }

        return $result;
    }

    /**
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @return static|null
     */
    public function findOne(array $mainAttributes, array $additionalAttributes = [], array $order = [])
    {
        /** @var static|null $result */
        $result = $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order), true);

        return $result;
    }

    /**
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $limit
     * @param int $offset
     * @return static[]
     */
    public function findAll(
        array $mainAttributes,
        array $additionalAttributes = [],
        array $order = [],
        $limit = null,
        $offset = null
    ) {
        /** @var array<static> $result */
        $result = $this->find(
            $this->combineSearchConditions($mainAttributes, $additionalAttributes, $order, $limit, $offset),
            false
        );

        return $result;
    }

    /**
     * @return $this[]
     */
    public static function getListAll()
    {
        return (new static())->findAll([]);//@phpstan-ignore-line
    }

    /**
     * Returns a value indicating whether the current record is new.
     * @return bool whether the record is new and should be inserted when calling [[save()]].
     */
    public function getIsNewRecord()
    {
        return $this->_oldAttributes === null;
    }

    /**
     * Save model
     *
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        if ($this->isNewRecord) {
            return $this->create($runValidation);
        }

        return $this->update($runValidation);
    }

    /**
     * @param bool $runValidation
     * @return bool
     */
    public function create($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $primaryKey = $this->getPrimaryKey();

        $attributesForSend = $this->getAttributesForSend();
        try {
            $result = ApiHelper::getInstance()->post($this->getApiUrl(), $attributesForSend);
        } catch (InvalidConfigException $e) {
            return false;
        }

        // Set primary key value after create record
        if ($result['result'] === true && $primaryKey !== null && isset($result['response'][$primaryKey])) {
            $this->$primaryKey = $result['response'][$primaryKey];
        }

        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * @param bool $runValidation
     * @return bool
     */
    public function update($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }
        $attributesForSend = $this->getAttributesForSend();
        try {
            $result = ApiHelper::getInstance()->put(
                $this->getApiUrl($this->getPrimaryKeyValue()),
                null,
                $attributesForSend
            );
        } catch (InvalidConfigException $e) {
            return false;
        }

        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * Return list of attributes for save or update.
     *
     * @return array<string>
     */
    public function getAttributesForSendArray()
    {
        return $this->attributes();
    }

    /**
     * @return array<string, mixed>
     */
    public function getAttributesForSend()
    {
        $attributes = $this->getAttributes($this->getAttributesForSendArray());

        $additional_attributes = null;
        if (array_key_exists('additional_attributes', $attributes)) {
            $additional_attributes = $attributes['additional_attributes'];
            unset($attributes['additional_attributes']);
        }

        $attributesForSend = [];
        if ($this->isNewRecord) {
            // Add not empty attributes for send
            foreach ($attributes as $name => $value) {
                if (isset($value)) {
                    $attributesForSend[$name] = $value;
                }
            }
        } else {
            // Add changed attributes for send
            foreach ($attributes as $name => $value) {
                if (
                    is_array($this->_oldAttributes) && (array_key_exists(
                        $name,
                        $this->_oldAttributes
                    ) && $value !== $this->_oldAttributes[$name])
                ) {
                    $attributesForSend[$name] = $value;
                }
            }
        }

        if ($additional_attributes !== null) {
            foreach ($additional_attributes as $name => $value) {
                if (
                    $this->isNewRecord || (is_array($this->_oldAttributes) && is_array(
                        $this->_oldAttributes['additional_attributes']
                    ) && array_key_exists(
                        $name,
                        $this->_oldAttributes['additional_attributes']
                    ) && $value !== $this->_oldAttributes['additional_attributes'][$name])
                ) {
                    $attributesForSend['additional_attributes'][$name] = $value;
                }
            }
        }

        return $attributesForSend;
    }

    /**
     * @param array{'result': bool, 'response': array<string, mixed>|string|null} $result
     * @return void
     */
    private function checkResult($result)
    {
        if ($result['result'] == true) {
            // Reload attributes
            if ($this->reloadAttributesAfterSave) {
                $this->reloadAttributes();
            } else {
                $this->_oldAttributes = $this->attributes;
            }
        } elseif (is_array($result['response'])) {
            if (isset($result['response']['error'])) {
                $error = $result['response']['error'];
                $this->addError('api_error', 'Code: ' . $error['code'] . ' Message: ' . $error['message']);

                if ($error['code'] === 401) {
                    static::clearAuthDataCache();
                }

                return;
            }

            foreach ($result['response'] as $item) {
                $this->addError(
                    $item['field'] ?? ($item['attribute'] ?? 'error'),
                    $item['message'] ?? ($item['error'] ?? 'Undefined error')
                );
            }
        }
    }

    /**
     * Call this method to reload all models attributes by AP
     *
     * @return void
     */
    public function reloadAttributes()
    {
        if ($this->isNewRecord) {
            throw new InvalidParamException('Can\'t get primary key');
        }
        $primaryKey = $this->getPrimaryKeyValue();
        if ($primaryKey === null) {
            throw new InvalidParamException('Can\'t get primary key');
        }
        $result = ApiHelper::getInstance()->get($this->getApiUrl($primaryKey));
        if ($result['result'] == true && !empty($result['response'])) {
            /** @var array{'result': bool, 'response': array<string, mixed>} $result */
            static::populate($this, $result['response']);
        } else {
            throw new InvalidParamException('Cant get data for reload attributes!');
        }
    }

    /**
     * @return null|mixed
     */
    private function getPrimaryKeyValue()
    {
        $attributes = $this->getAttributes();
        $primaryKey = $this->getPrimaryKey();
        if (isset($attributes[$primaryKey]) && $attributes[$primaryKey] !== null) {
            return $attributes[$primaryKey];
        }

        return null;
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function delete()
    {
        $id = $this->getPrimaryKeyValue();
        if ($id === null) {
            return false;
        }

        $result = ApiHelper::getInstance()->delete($this->getApiUrl(), $id);
        if ($result['result'] === true) {
            $this->_oldAttributes = null;
        }

        return $result['result'];
    }

    /**
     * Get all models batch on $batchSize
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $batchSize default 100
     * @return BaseBatchApiResult<array<static>>
     */
    public function batch(array $mainAttributes, array $additionalAttributes = [], array $order = [], $batchSize = 100)
    {
        return new BaseBatchApiResult([
            'model' => new static(),//@phpstan-ignore-line
            'mainAttributesCondition' => $mainAttributes,
            'additionalAttributesCondition' => $additionalAttributes,
            'order' => $order,
            'batchSize' => $batchSize,
        ]);
    }

    /**
     * Get all models by one and load batch on $batchSize
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $batchSize default 100
     * @return BaseBatchApiResult<static>
     */
    public function each(array $mainAttributes, array $additionalAttributes = [], array $order = [], $batchSize = 100)
    {
        return new BaseBatchApiResult([
            'model' => new static(),//@phpstan-ignore-line
            'mainAttributesCondition' => $mainAttributes,
            'additionalAttributesCondition' => $additionalAttributes,
            'order' => $order,
            'batchSize' => $batchSize,
            'each' => true,
        ]);
    }

    /**
     * Clean old attributes.
     *
     * @return void
     */
    protected function cleanOldAttributes()
    {
        $this->_oldAttributes = null;
    }

    /**
     * Change all null values to empty string. Need for correct working of http_build_query
     * @param array<string, mixed> $conditions
     * @return array<string, mixed>
     */
    public function normalizeCondition(array $conditions): array
    {
        return array_map(function ($v) {
            if (is_array($v)) {
                return $this->normalizeCondition($v);
            }

            return is_null($v) ? '' : $v;
        }, $conditions);
    }

    /**
     * @return void
     */
    public static function clearAuthDataCache(): void
    {
        ApiHelper::clearAuthDataCache();
    }
}
