<?php

namespace splynx\v2\models\crm;

use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\NotSupportedException;

/**
 * Class BaseLeadsNote
 * @package splynx\v2\models\crm
 */
class BaseLeadsNote extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $datetime;

    /** @var int */
    public $administrator_id;

    /** @var string */
    public $type;

    /** @var string */
    public $class;

    /** @var string */
    public $title;

    /** @var string */
    public $name;

    /** @var string */
    public $comment;

    /** @var string */
    public $values;

    public const TYPE_COMMENT = 'comment';
    public const TYPE_CUSTOMER = 'customer';
    public const TYPE_FINANCE = 'finance';
    public const TYPE_SERVICE = 'service';
    public const TYPE_MESSAGE = 'message';
    public const TYPE_CALL = 'call';

    public static $apiUrl = 'admin/crm/leads-notes';

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['customer_id', 'comment'], 'required'],
            ['type', 'in', 'range' => static::getTypesArray()],
            ['comment', 'validateComment'],
        ];
    }

    /**
     * @return array<string>
     */
    public static function getTypesArray()
    {
        return [
            static::TYPE_COMMENT,
            static::TYPE_CUSTOMER,
            static::TYPE_FINANCE,
            static::TYPE_SERVICE,
            static::TYPE_MESSAGE,
            static::TYPE_CALL,
        ];
    }

    /**
     * Validate comment
     * @param string $attribute
     * @param array<mixed> $params
     * @return bool
     */
    public function validateComment($attribute, $params)
    {
        if (in_array(trim($this->{$attribute}), ['', '<br>'])) {
            return false;
        }
        return true;
    }

    /**
     * @inheritDoc
     * @throws NotSupportedException
     */
    public function update($runValidation = true)
    {
        throw new NotSupportedException(\Yii::t('app', 'Update action is not supported'));
    }

    /**
     * @return bool
     * @throws InvalidConfigException
     */
    public function delete()
    {
        if (empty($this->customer_id)) {
            throw new InvalidParamException(\Yii::t('app', 'You must set customer_id value'));
        }

        if ($this->id === null) {
            return false;
        }
        $id = $this->customer_id . '--' . $this->id;

        $result = ApiHelper::getInstance()->delete($this->getApiUrl(), $id);
        if ($result['result'] === true) {
            $this->cleanOldAttributes();
        }
        return $result['result'];
    }
}
