<?php

namespace splynx\v2\models\crm;

use splynx\v2\models\finance\BaseFinanceClass;

/**
 * Class BaseQuote
 * @package splynx\v2\models\crm
 */
class BaseQuote extends BaseFinanceClass
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $number;

    /** @var string */
    public $date_created;

    /** @var string */
    public $real_create_datetime;

    /** @var string */
    public $date_updated;

    // Date when quote was accepted or denied
    /** @var string */
    public $date_of_decision;

    /** @var string */
    public $date_till;

    /** @var number */
    public $total;

    /** @var int */
    public $invoice_id;

    /** @var int */
    public $request_id;

    /** @var string */
    public $status;

    /** @var bool */
    public $is_sent;

    /** @var string */
    public $note;

    /** @var string */
    public $memo;

    /** @var bool|int<0,1> */
    public $is_customers = 0;

    /** @var int */
    public $deal_id;

    /** @var string */
    public $added_by;

    /** @var int */
    public $added_by_id;

    /** @var array<mixed> */
    public $additional_attributes = [];

    // Quotes statuses
    public const STATUS_NEW = 'new';
    public const STATUS_SENT = 'sent';
    public const STATUS_ON_REVIEW = 'on_review';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_DENIED = 'denied';

    public const ADDED_BY_SYSTEM = 'system';
    public const ADDED_BY_API = 'api';
    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_WIDGET = 'widget';

    public static $apiUrl = 'admin/crm/quotes';

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['customer_id', 'status'], 'required'],
            ['status', 'in', 'range' => static::getQuotesStatusArray()],
            ['added_by', 'in', 'range' => static::getAddedByArray()],
        ];
    }

    /**
     * @return array<string>
     */
    public static function getQuotesStatusArray()
    {
        return [
            static::STATUS_NEW,
            static::STATUS_SENT,
            static::STATUS_ON_REVIEW,
            static::STATUS_ACCEPTED,
            static::STATUS_DENIED,
        ];
    }

    /**
     * @return array<string>
     */
    public static function getAddedByArray()
    {
        return [
            static::ADDED_BY_SYSTEM,
            static::ADDED_BY_API,
            static::ADDED_BY_ADMIN,
            static::ADDED_BY_WIDGET,
        ];
    }

    /**
     * @return string
     */
    public function getItemsClassName(): string
    {
        return BaseQuotesItem::class;
    }
}
