<?php

namespace splynx\v2\models\crm;

use splynx\v2\base\BaseActiveApi;
use yii\base\InvalidCallException;
use yii\base\NotSupportedException;

/**
 * Class BaseLeadsInfo
 * @package splynx\v2\models\crm
 */
class BaseLeadsInfo extends BaseActiveApi
{
    /** @var int */
    public $customer_id;

    /** @var int */
    public $score;

    /** @var string */
    public $last_contacted;
    /**
     * ID of crm status from splynx config
     * @var integer $crm_status
     */
    public $crm_status;
    /**
     * ID of admin
     * @var integer $owner
     */
    public $owner;

    /** @var string */
    public $source;

    public static $apiUrl = 'admin/crm/leads-info';

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return 'customer_id';
    }

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
        ];
    }

    /**
     * @return bool|void
     * @throws NotSupportedException
     */
    public function delete()
    {
        throw new NotSupportedException(\Yii::t('app', 'Delete action is not supported'));
    }

    /**
     * @inheritDoc
     * @throws NotSupportedException
     */
    public function create($runValidation = true)
    {
        throw new NotSupportedException(\Yii::t('app', 'Create action is not supported'));
    }
}
