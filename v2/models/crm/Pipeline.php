<?php

namespace splynx\v2\models\crm;

use splynx\v2\base\BaseActiveApi;

/**
 * Class Pipeline
 * @package splynx\v2\models\crm
 */
class Pipeline extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var bool */
    public $is_base;

    public static $apiUrl = 'admin/config/crm-pipeline';

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
        ];
    }
}
