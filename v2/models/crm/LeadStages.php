<?php

namespace splynx\v2\models\crm;

use splynx\v2\base\BaseActiveApi;

/**
 * Class LeadStages
 * @package splynx\v2\models\crm
 * @deprecated
 */
class LeadStages extends BaseActiveApi
{
    public $id;
    public $name;
    public $is_base;

    public static $apiUrl = 'admin/config/crm-lead-stages';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
        ];
    }
}
