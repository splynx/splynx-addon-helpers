<?php

namespace splynx\v2\models\crm;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseDeal
 * Removed from system in 3.1
 * @package splynx\v2\models\crm
 * @deprecated
 */
class BaseDeal extends BaseActiveApi
{
    public $id;
    public $name;
    public $owner;
    public $customer_id;
    public $total;
    public $status;
    public $date_add;
    public $expected_close_date;
    public $last_updated_at;
    public $last_updated_by;
    public $type;
    public $source;
    public $quote_id;
    public $is_customers = 0;
    public $additional_attributes = [];

    public const TYPE_NEW_BUSINESS = 'new_business';
    public const TYPE_EXISTING_BUSINESS = 'existing_business';

    public const UPDATED_BY_ADMIN = 'admin';
    public const UPDATED_BY_API = 'api';

    public const STATUS_OPEN = 'open';
    public const STATUS_WON = 'won';
    public const STATUS_LOST = 'lost';

    public static $apiUrl = 'admin/crm/deals';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'owner'], 'required'],
            ['last_updated_by', 'in', 'range' => static::getUpdatedByArray()],
            ['status', 'in', 'range' => static::getDealsStatusArray()],
            ['type', 'in', 'range' => static::getTypesArray()],
        ];
    }

    /**
     * @return array
     */
    public static function getDealsStatusArray()
    {
        return [
            static::STATUS_OPEN,
            static::STATUS_WON,
            static::STATUS_LOST,
        ];
    }

    /**
     * @return array
     */
    public static function getUpdatedByArray()
    {
        return [
            static::UPDATED_BY_ADMIN,
            static::UPDATED_BY_API,
        ];
    }
    /**
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            static::TYPE_NEW_BUSINESS,
            static::TYPE_EXISTING_BUSINESS,
        ];
    }
}
