<?php

namespace splynx\v2\models\crm;

use exceptions\Exception;
use splynx\v2\models\common\BaseCustomerModel;
use yii\helpers\ArrayHelper;

/**
 * Class Leads
 * @package splynx\v2\models\crm
 */
class BaseLead extends BaseCustomerModel
{
    public static $apiUrl = 'admin/crm/leads';

    /**
     * @return array|mixed[]
     * @throws Exception
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'partner_id', 'location_id'], 'required'],
        ]);
    }
}
