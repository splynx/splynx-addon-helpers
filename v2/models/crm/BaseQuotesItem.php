<?php

namespace splynx\v2\models\crm;

use splynx\v2\models\finance\item\BaseItem;
use yii\base\Model;

/**
 * Class BaseQuoteItem
 * @package splynx\v2\models\crm
 */
class BaseQuotesItem extends BaseItem
{
    /**
     * Set here tariff in format {tariff_category}--{tariff_id}
     * Example internet--1
     * @var string $tariff
     */
    public $tariff;
    /** @var  int */
    public $pos;

    // Tariffs categories for items
    public const TARIFF_INTERNET = 'internet';
    public const TARIFF_VOICE = 'voice';
    public const TARIFF_CUSTOM = 'custom';
    public const TARIFF_BUNDLE = 'bundle';
    public const TARIFF_ONE_TIME = 'one_time';
}
