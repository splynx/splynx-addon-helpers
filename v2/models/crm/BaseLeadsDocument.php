<?php

namespace splynx\v2\models\crm;

use splynx\v2\models\customer\BaseCustomerDocument;

/**
 * Class BaseLeadsDocument
 * @package splynx\v2\models\customer
 */
class BaseLeadsDocument extends BaseCustomerDocument
{
    public static $apiUrl = 'admin/crm/leads-documents';
}
