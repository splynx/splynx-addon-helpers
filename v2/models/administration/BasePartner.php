<?php

namespace splynx\v2\models\administration;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BasePartner
 * @package splynx\models\administration
 */
class BasePartner extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var array<string, mixed>*/
    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/partners';

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
