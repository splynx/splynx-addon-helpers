<?php

namespace splynx\v2\models\administration;

use splynx\v2\base\BaseActiveApi;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Class BaseAdministrator
 * @package splynx\models\administration
 */
class BaseAdministrator extends BaseActiveApi implements IdentityInterface
{
    /** @var int */
    public $id;

    /** @var string */
    public $login;

    /** @var string */
    public $password;

    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $phone;

    /** @var int */
    public $timeout;

    /** @var string */
    public $role_name;

    /** @var string */
    public $router_access;

    /** @var int */
    public $partner_id;

    /** @var string */
    public $otp_secret;

    /** @var string */
    public $last_ip;

    /** @var string */
    public $last_dt;

    /** @var string */
    public $avatar_api_url;

    /** @var string */
    public $download_avatar_link;

    /** @var array<string, mixed> */
    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/administrators';

    /**
     * @inheritdoc
     * @param int $id
     */
    public static function findIdentity($id)
    {
        return (new static())->findById($id);//@phpstan-ignore-line
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
