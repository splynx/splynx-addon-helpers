<?php

namespace splynx\v2\models\tariffs;

use splynx\v2\base\BaseActiveApi;
use splynx\v2\models\customer\BaseCustomer;
use Yii;

/**
 * @see \models\admin\tariffs\Bundle Splynx model
 *
 * @property integer $id
 * @property string $title
 * @property string $service_description
 * @property double $price
 * @property integer $customers
 * @property integer $services
 * @property boolean $with_vat
 * @property boolean $available_for_services
 * @property double $vat_percent
 * @property array $partner_ids
 * @property array $billing_types
 * @property double $activation_fee
 * @property string $get_activation_fee_when
 * @property boolean $issue_invoice_while_service_creation
 * @property integer $contract_duration
 * @property boolean $automatic_renewal
 * @property boolean $auto_reactivate
 * @property double $cancellation_fee
 * @property double $prior_cancellation_fee
 * @property double $change_to_other_bundle_fee
 * @property integer $discount_period
 * @property double $discount_percent
 * @property double $discount_value
 * @property string $discount_type
 * @property array $internet_tariffs
 * @property array $voice_tariffs
 * @property array $custom_tariffs
 * @property array $additional_attributes
 * @property string $apiUrl
 * @property int $tax_id
 */
class BaseBundleTariff extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $service_description;

    /** @var double */
    public $price;

    /** @var int */
    public $customers;

    /** @var int */
    public $services;

    /** @var bool */
    public $with_vat;

    /** @var bool */
    public $available_for_services;

    /** @var double */
    public $vat_percent;

    /** @var array<int> */
    public $partner_ids = [];

    /** @var array<string> */
    public $billing_types = [];

    /** @var double */
    public $activation_fee;

    /** @var string */
    public $get_activation_fee_when;

    /** @var bool */
    public $issue_invoice_while_service_creation;

    /** @var int */
    public $contract_duration;

    /** @var bool */
    public $automatic_renewal;

    /** @var bool */
    public $auto_reactivate;

    /** @var double */
    public $cancellation_fee;

    /** @var double */
    public $prior_cancellation_fee;

    /** @var double */
    public $change_to_other_bundle_fee;

    /** @var int */
    public $discount_period;

    /**
     * @deprecated
     * @var float
     */
    public $discount_percent;

    /** @var float */
    public $discount_value;

    /** @var string */
    public $discount_type;

    /** @var array<int> */
    public $internet_tariffs = [];

    /** @var array<int> */
    public $voice_tariffs = [];

    /** @var array<int> */
    public $custom_tariffs = [];

    /** @var array<mixed> */
    public $additional_attributes = [];

    /** @var int */
    public $tax_id;

    public const ACTIVATION_FEE_WHEN_FIRST_SERVICE_BILLING = 'first_service_billing';
    public const ACTIVATION_FEE_WHEN_CREATE_SERVICE = 'create_service';

    /** @var string */
    public const DISCOUNT_TYPE_PERCENT = 'percent';

    /** @var string */
    public const DISCOUNT_TYPE_FIXED = 'fixed';

    public static $apiUrl = 'admin/tariffs/bundle';

    /**
     * @inheritdoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['title', 'partner_ids', 'billing_types', 'billing_types'], 'required'],
            [['title', 'service_description', 'discount_type'], 'string'],
            [['price', 'activation_fee', 'cancellation_fee', 'prior_cancellation_fee', 'change_to_other_bundle_fee'], 'number'],
            [['with_vat', 'available_for_services', 'issue_invoice_while_service_creation', 'automatic_renewal', 'auto_reactivate'], 'boolean'],
            [['vat_percent', 'discount_percent', 'discount_value'], 'number', 'min' => 0],
            ['billing_types', 'in', 'range' => BaseCustomer::getAllBillingTypes(), 'allowArray' => true],
            ['get_activation_fee_when', 'in', 'range' => array_keys(self::getActivationFeeWhenArray())],
            [['contract_duration', 'discount_period', 'customers', 'services', 'tax_id'], 'integer', 'min' => 0],
            [['partner_ids', 'internet_tariffs', 'voice_tariffs', 'custom_tariffs'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * Get all activation fee conditions
     * @return array<string, string>
     */
    public static function getActivationFeeWhenArray()
    {
        return [
            self::ACTIVATION_FEE_WHEN_FIRST_SERVICE_BILLING => Yii::t('app', 'first service billing'),
            self::ACTIVATION_FEE_WHEN_CREATE_SERVICE => Yii::t('app', 'create service'),
        ];
    }
}
