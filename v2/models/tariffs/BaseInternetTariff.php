<?php

namespace splynx\v2\models\tariffs;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BaseInternetTariff
 * @package splynx\models\tariffs
 */
class BaseInternetTariff extends BaseTariff
{
    /** @var int */
    public $speed_download;

    /** @var int */
    public $speed_upload;

    /** @var int */
    public $speed_limit_at = 10;

    /** @var int */
    public $aggregation = 1;

    /** @var int */
    public $burst_limit = 0;

    /** @var int */
    public $burst_threshold = 0;

    /** @var int */
    public $burst_time = 0;

    /** @var int */
    public $services_online;

    /** @var static::PRIORITY_LOW|static::PRIORITY_NORMAL|static::PRIORITY_HIGH */
    public $priority = self::PRIORITY_NORMAL;

    public const PRIORITY_LOW = 'low';
    public const PRIORITY_NORMAL = 'normal';
    public const PRIORITY_HIGH = 'high';

    public static $apiUrl = 'admin/tariffs/internet';

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['speed_limit_at', 'default', 'value' => 10],
            ['aggregation', 'default', 'value' => 1],
            ['burst_limit', 'default', 'value' => 0],
            ['burst_threshold', 'default', 'value' => 0],
            ['burst_time', 'default', 'value' => 0],
            [['priority'], 'string'],
            ['priority', 'in', 'range' => array_keys(static::getPrioritiesArray())],
            [['speed_download', 'speed_upload', 'burst_limit', 'burst_threshold', 'burst_time'], 'integer'],
            [['speed_download', 'speed_upload'], 'required'],
        ], parent::rules());
    }

    /**
     * Return priorities array.
     *
     * @return array<string, string>
     */
    public static function getPrioritiesArray()
    {
        return [
            self::PRIORITY_LOW => Yii::t('app', 'Low'),
            self::PRIORITY_NORMAL => Yii::t('app', 'Normal'),
            self::PRIORITY_HIGH => Yii::t('app', 'High'),
        ];
    }
}
