<?php

namespace splynx\v2\models\tariffs;

use splynx\v2\base\BaseActiveApi;
use splynx\models\customer\BaseCustomer;
use Yii;

/**
 * Class BaseTariff
 * @package splynx\models\tariffs
 */
abstract class BaseTariff extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var double */
    public $price;

    /** @var string */
    public $title;

    /** @var bool */
    public $with_vat;

    /** @var bool */
    public $available_for_services;

    /** @var double */
    public $vat_percent;

    /** @var int */
    public $services;

    /** @var string */
    public $service_name;

    /** @var bool */
    public $update_service_price;

    /** @var bool */
    public $update_service_name;

    /** @var bool */
    public $force_update_service_price;

    /** @var bool */
    public $force_update_service_name;

    /** @var int */
    public $transaction_category_id = 0;

    /** @var array<int> */
    public $tariffs_for_change = [];

    /** @var array<int> */
    public $partners_ids = [];

    /** @var array<mixed> */
    public $additional_attributes = [];

    /** @var array<BaseCustomer::BILLING_PREPAID|BaseCustomer::BILLING_PREPAID_MONTHLY|BaseCustomer::BILLING_RECURRING> */
    public $billing_types = [];

    /** @var int<1, 365> */
    public $billing_days_count;

    /** @var bool|int<0, 1> */
    public $custom_period;

    /** @var int */
    public $tax_id;

    /**
     * @inheritdoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['title', 'service_name'], 'string'],
            [['price', 'billing_days_count'], 'number'],
            ['with_vat', 'boolean'],
            ['with_vat', 'default', 'value' => true],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['available_for_services', 'update_service_price', 'force_update_service_price', 'update_service_name', 'force_update_service_name'], 'boolean'],
            [['title', 'with_vat', 'partners_ids', 'billing_types'], 'required'],
            [['partners_ids'], 'each', 'rule' => ['required']],
            ['billing_types', 'validateBillingTypes'],
            ['custom_period', 'in', 'range' => [0, 1]],
            [['tax_id'], 'integer'],
        ];
    }

    /**
     * @param string $attribute
     * @return void
     */
    public function validateBillingTypes($attribute)
    {
        foreach ($this->{$attribute} as $type) {
            if (!in_array($type, BaseCustomer::getAllBillingTypes())) {
                $this->addError($attribute, Yii::t('customers', 'Invalid billing type {type}!', ['type' => $type]));
                break;
            }
        }
    }
}
