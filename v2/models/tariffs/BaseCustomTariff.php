<?php

namespace splynx\v2\models\tariffs;

/**
 * Class BaseCustomTariff
 * @package splynx\models\tariffs
 */
class BaseCustomTariff extends BaseTariff
{
    public static $apiUrl = 'admin/tariffs/recurring';
}
