<?php

namespace splynx\v2\models\tariffs;

use splynx\v2\base\BaseActiveApi;

class BaseOneTimeTariff extends BaseActiveApi
{
    /** @var int */
    public $id;
    /** @var string */
    public $title;
    /** @var string */
    public $service_description;
    /** @var float */
    public $price;
    /** @var bool */
    public $with_vat;
    /** @var float */
    public $vat_percent;
    /** @var bool */
    public $enabled;
    /** @var int */
    public $transaction_category_id = 0;
    /** @var int[] */
    public $partners_ids = [];
    /** @var array<string, mixed> */
    public $additional_attributes = [];

    /** @var int */
    public $tax_id;

    /** @var string */
    public static $apiUrl = 'admin/tariffs/one-time';
}
