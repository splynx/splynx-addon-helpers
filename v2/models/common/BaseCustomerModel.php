<?php

namespace splynx\v2\models\common;

use splynx\v2\base\BaseActiveApi;
use yii\validators\EmailValidator;

/**
 * Class BaseCustomerModel
 * Base model for same attributes in lead and customer
 * @package splynx\v2\models\common
 */
abstract class BaseCustomerModel extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $login;

    /** @var int */
    public $partner_id;

    /** @var int */
    public $location_id;

    /** @var string */
    public $password;

    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $phone;

    /** @var string */
    public $street_1;

    /** @var string */
    public $zip_code;

    /** @var string */
    public $city;

    /** @var string */
    public $added_by = self::ADDED_BY_API;

    /** @var int */
    public $added_by_id;

    /** @var string */
    public $billing_type = self::BILLING_RECURRING;

    /** @var string */
    public $date_add;

    /** @var array<mixed> */
    public $additional_attributes = [];

    // Billing types
    public const BILLING_RECURRING = 'recurring';
    public const BILLING_PREPAID = 'prepaid';
    public const BILLING_PREPAID_MONTHLY = 'prepaid_monthly';

    // Added by
    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_API = 'api';
    public const ADDED_BY_WIDGET = 'widget';

    /**
     * @return array|mixed[]
     */
    public function rules()
    {
        return [
            ['email', 'validateEmail'],
            ['added_by', 'in', 'range' => static::getAddedByArray()],
            ['billing_type', 'in', 'range' => static::getAllBillingTypes()],
        ];
    }

    /**
     * Validate email.
     *
     * @param string $attribute
     * @param array<mixed> $params
     * @return void
     */
    public function validateEmail($attribute, $params)
    {
        if (strpos($this->$attribute, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->$attribute));
        } else {
            $emails = [$this->$attribute];
        }

        $validator = new EmailValidator();

        foreach ($emails as $email) {
            if (!$validator->validate($email)) {
                $this->addError($attribute, "$email is not a valid email.");
            }
        }
    }

    /**
     * Get all emails.
     *
     * @return array<string>
     */
    public function getEmailsArray()
    {
        if (empty($this->email)) {
            return [];
        }

        if (strpos($this->email, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->email));
        } else {
            $emails = [$this->email];
        }

        return $emails;
    }

    /**
     * Get all billing types.
     *
     * @return array<string>
     */
    public static function getAllBillingTypes()
    {
        return [
            self::BILLING_PREPAID,
            self::BILLING_PREPAID_MONTHLY,
            self::BILLING_RECURRING,
        ];
    }

    /**
     * @return array<int, string>
     */
    public static function getAddedByArray(): array
    {
        return [
            static::ADDED_BY_ADMIN,
            static::ADDED_BY_API,
            static::ADDED_BY_WIDGET,
        ];
    }
}
