<?php

namespace splynx\v2\models\services;

/**
 * Class BaseCustomService
 * @package splynx\models\services
 */
class BaseCustomService extends BaseService
{
    /**
     * @inheritdoc
     */
    public function getServiceApiUrl()
    {
        return 'custom-services';
    }
}
