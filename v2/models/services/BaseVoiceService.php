<?php

namespace splynx\v2\models\services;

/**
 * Class BaseVoiceService
 * @package splynx\models\services
 */
class BaseVoiceService extends BaseService
{
    /** @var int */
    public $voice_device_id;

    /** @var string */
    public $phone;

    /**
     * @inheritDoc
     */
    public function getServiceApiUrl()
    {
        return 'voice-services';
    }
}
