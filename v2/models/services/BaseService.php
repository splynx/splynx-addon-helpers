<?php

namespace splynx\v2\models\services;

use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;

/**
 * Class BaseService
 * @package splynx\models\services
 */
abstract class BaseService extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int */
    public $parent_id;

    /** @var int */
    public $customer_id = 0;

    /** @var int */
    public $tariff_id;

    /** @var self::STATUS_ACTIVE|self::STATUS_DISABLED|self::STATUS_STOPPED|self::STATUS_PENDING */
    public $status = self::STATUS_ACTIVE;

    /** @var self::STATUS_ACTIVE|self::STATUS_DISABLED|self::STATUS_STOPPED|self::STATUS_PENDING */
    public $status_new;

    /** @var string */
    public $description;

    /** @var int */
    public $quantity = 1;

    /** @var string */
    public $unit;

    /** @var float */
    public $unit_price;

    /** @var string */
    public $start_date;

    /** @var string */
    public $end_date;

    /** @var bool|int<0, 1> */
    public $discount;

    /**
     * @deprecated
     * @var float
     */
    public $discount_percent;

    /** @var float */
    public $discount_value;

    /** @var string */
    public $discount_type;

    /** @var string */
    public $discount_start_date;

    /** @var string */
    public $discount_end_date;

    /** @var string */
    public $discount_text;

    /** @var int */
    public $bundle_service_id;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_STOPPED = 'stopped';
    public const STATUS_PENDING = 'pending';

    /** @var string */
    public const DISCOUNT_TYPE_PERCENT = 'percent';

    /** @var string */
    public const DISCOUNT_TYPE_FIXED = 'fixed';

    public static $apiUrl = 'admin/customers/customer';

    /**
     * @param int|string|null $id
     * @param array<string, mixed> $conditions
     * @return string
     */
    protected function getApiUrl($id = null, $conditions = [])
    {
        $result = self::$apiUrl . '/' . $this->customer_id . '/' . $this->getServiceApiUrl();

        // Set id
        if ($id !== null) {
            $result .= '--' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * Method is overwritten because services has own logic (SPL-1117)
     * @param $id
     * @return null|BaseActiveApi
     */
    public function findById($id)
    {
        return parent::findOne([
            'id' => $id,
        ]);
    }

    /**
     * @return string
     */
    abstract protected function getServiceApiUrl();

    /**
     * Delete service. Method is overwritten because in services we use custom URLs
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function delete()
    {
        $result = ApiHelper::getInstance()->delete($this->getApiUrl($this->id), null);
        return $result['result'];
    }
}
