<?php

namespace splynx\v2\models\services;

/**
 * Class BaseInternetService
 * @package splynx\models\services
 */
class BaseInternetService extends BaseService
{
    /** @var int */
    public $router_id;

    /** @var int */
    public $sector_id;

    /** @var string */
    public $login;

    /** @var string */
    public $password;

    /** @var int<0, 2> */
    public $taking_ipv4;

    /** @var string */
    public $ipv4;

    /** @var string */
    public $ipv4_route;

    /** @var int */
    public $ipv4_pool_id;

    /** @var string */
    public $mac;

    /** @var string */
    public $port_id;

    /** @var array{'address': string, 'marker': string} */
    public $geo;

    public const STATUS_ONLINE = 'online';

    /**
     * @inheritDoc
     */
    public function getServiceApiUrl()
    {
        return 'internet-services';
    }
}
