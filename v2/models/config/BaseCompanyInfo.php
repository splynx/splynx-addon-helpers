<?php

namespace splynx\v2\models\config;

use RuntimeException;
use splynx\base\ApiResponseException;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\models\customer\BaseCustomer;
use Yii;
use yii\web\Application;

/**
 * Class CompanyInfo
 * @package app\models
 */
class BaseCompanyInfo extends BaseActiveApi
{
    /** @var string */
    public static $apiUrl = 'admin/config/company-info';

    /** @var array<int, static> */
    public static $info;

    /** @var int */
    public $id;
    /** @var int */
    public $invoice_template_id;
    /** @var int */
    public $request_template_id;
    /** @var int */
    public $quote_template_id;
    /** @var int */
    public $receipt_template_id;

    /**
     * This value will now be in the Config
     * finance.settings.reminder_1_email_template
     * finance.settings.reminder_2_email_template
     * finance.settings.reminder_3_email_template
     *
     * @var int
     * @deprecated
     */
    public $reminder_mail_template_id;

    /**
     * This value will now be in the Config
     * finance.settings.reminder_1_sms_template
     * finance.settings.reminder_2_sms_template
     * finance.settings.reminder_3_sms_template
     *
     * @var int
     * @deprecated
     */
    public $reminder_sms_template_id;
    /** @var int */
    public $report_statements_template_id;
    /** @var string */
    public $company_name;
    /** @var string */
    public $street_1;
    /** @var string */
    public $street_2;
    /** @var string */
    public $zip;
    /** @var string */
    public $city;
    /** @var string */
    public $country;
    /** @var string */
    public $iso_country;
    /** @var string */
    public $email;
    /** @var string */
    public $phone;
    /** @var int */
    public $company_id;
    /** @var float */
    public $company_vat;
    /** @var float */
    public $vat_percent;
    /** @var string */
    public $bank_account;
    /** @var string */
    public $bank_name;
    /** @var int */
    public $bank_id;
    /** @var string */
    public $bank_address;
    /** @var string */
    public $splynx_url;
    /** @var float */
    public $partner_percent;
    /** @var string */
    public $file_logo;
    /** @var int */
    public $credit_note_template_id;

    /** @var int */
    public $tax_id;

    /**
     * Get info from cache by partner id
     * @param int $partnerId
     * @return static
     * @throws ApiResponseException
     */
    public static function getInfoByPartnerId(int $partnerId): BaseCompanyInfo
    {
        if (!isset(static::$info[$partnerId])) {
            $model = (new static())->findById($partnerId);//@phpstan-ignore-line
            if ($model === null) {
                throw new RuntimeException('Can\'t find company info');
            }
            static::$info[$partnerId] = $model;
        }

        return static::$info[$partnerId];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $companyName = $this->company_name;
        return !empty($companyName) ? $companyName : Yii::t('app', 'System');
    }

    /**
     * @return string|null
     */
    public function getLogo(): ?string
    {
        $logo = $this->file_logo;
        return !empty($logo) ? '/portal/images--view-logo-partner?id=' . $logo : null;
    }

    /**
     * Get company info by customer id
     * @param int|null $customerID
     * @return BaseCompanyInfo
     * @throws ApiResponseException
     */
    public static function getInfoByCustomer(?int $customerID = null): BaseCompanyInfo
    {
        if ($customerID) {
            $customer = BaseCustomer::findIdentity($customerID);
        } elseif (Yii::$app instanceof Application) {
            $customer = Yii::$app->user->identity;
        }
        /**
         * @var BaseCustomer $customer
         */
        return static::getInfoByPartnerId(!empty($customer) ? $customer->partner_id : 0);
    }
}
