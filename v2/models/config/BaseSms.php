<?php

namespace splynx\v2\models\config;

use splynx\v2\base\BaseActiveApi;

class BaseSms extends BaseActiveApi
{
    /** @var string */
    public static $apiUrl = 'admin/config/sms';

    /** @var int */
    public $id;
    /** @var string */
    public $type;
    /** @var int */
    public $message_id;
    /** @var int */
    public $customer_id;
    /** @var string */
    public $status;
    /** @var string */
    public $datetime_added;
    /** @var string */
    public $datetime_start_sending;
    /** @var string */
    public $datetime_sent;
    /** @var string */
    public $recipient;
    /** @var string */
    public $message;

    public const TYPE_MESSAGE = 'message';
    public const TYPE_TEST = 'test';
    public const TYPE_MONITORING = 'monitoring';
    public const TYPE_ADDON = 'add-on';
    public const TYPE_TICKET = 'ticket';

    public const STATUS_NEW = 'new';
    public const STATUS_SENDING = 'sending';
    public const STATUS_SENT = 'sent';
    public const STATUS_ERROR = 'error';
    public const STATUS_EXPIRED = 'expired';
}
