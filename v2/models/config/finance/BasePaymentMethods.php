<?php

namespace splynx\v2\models\config\finance;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BasePaymentMethods
 * @package splynx\models\config\finance
 */
class BasePaymentMethods extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var bool */
    public $is_active;

    /** @var string */
    public $name_1;

    /** @var string */
    public $name_2;

    /** @var string */
    public $name_3;

    /** @var string */
    public $name_4;

    /** @var string */
    public $name_5;

    public static $apiUrl = 'admin/finance/payment-methods';

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['name', 'is_active'], 'required'],
            [['is_active'], 'boolean'],
            [['name', 'name_1', 'name_2', 'name_3', 'name_4', 'name_5'], 'string'],
        ];
    }
}
