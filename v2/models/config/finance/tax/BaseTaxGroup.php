<?php

namespace splynx\v2\models\config\finance\tax;

use db\Expression;
use yii\base\InvalidParamException;

class BaseTaxGroup extends BaseTax
{
    /**
     * @inheridoc
     */
    public function init(): void
    {
        $this->type = static::TYPE_GROUP;
        parent::init();
    }

    /**
     * @inheridoc
     */
    protected function combineSearchConditions(
        array $mainAttributes = [],
        array $additionalAttributes = [],
        array $order = [],
        $limit = null,
        $offset = null
    ): array {
        $mainAttributes['type'] = BaseTax::TYPE_GROUP;

        return parent::combineSearchConditions($mainAttributes, $additionalAttributes, $order, $limit, $offset);
    }
}
