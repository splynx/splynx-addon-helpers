<?php

namespace splynx\v2\models\config\finance\tax;

use splynx\v2\base\BaseActiveApi;
use splynx\v2\models\finance\item\ItemsObject;
use yii\web\MethodNotAllowedHttpException;

/**
 * Class BaseTax
 *
 * @property array<BaseTaxGroupItem>|array<string, mixed> $items
 */
class BaseTax extends BaseActiveApi
{
    /** @var int */
    public int $id = 0;

    /** @var string */
    public string $name = '';

    /** @var float|int */
    public $rate = 0;

    /** @var string */
    public string $type = self::TYPE_SINGLE;

    /** @var bool */
    public bool $archived = false;

    /** @var int|null */
    public ?int $accounting_tax_rates_id = null;

    /** @var int|null */
    public ?int $einvoicing_tax_rates_id = null;

    public const TYPE_SINGLE = 'single';
    public const TYPE_GROUP = 'group';

    public static $apiUrl = 'admin/config/taxes';

    /** @var ItemsObject<BaseTaxGroupItem> */
    protected ItemsObject $_items;

    /**
     * @inheridoc
     */
    public function init(): void
    {
        $this->clearItems();
        parent::init();
    }

    /**
     * @return BaseTaxGroupItem[]|ItemsObject<BaseTaxGroupItem>
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * @param array<mixed>|BaseTaxGroupItem[] $items
     * @param bool $clearItems
     * @return void
     */
    public function setItems(?array $items = null, bool $clearItems = true): void
    {
        if (is_array($items)) {
            foreach ($items as $item) {
                $this->items[] = $item;
            }
        }
    }

    /**
     * Inits empty documetns items
     * @return void
     */
    public function clearItems(): void
    {
        $this->_items = new ItemsObject(
            [],
            0,
            'ArrayIterator',
            BaseTaxGroupItem::class
        );
    }

    /**
     * @inheritdoc
     */
    public static function populate($model, $data): void
    {
        $items = [];
        if (array_key_exists('items', $data)) {
            $items = $data['items'];
            unset($data['items']);
        }

        parent::populate($model, $data);
        if (!$model->isGroup()) {
            // For a simple Tax no need to load items
            return;
        }

        // Deleting old items
        $model->clearItems();
        $model->items = $items;
    }

    /**
     * @return bool
     */
    public function isGroup(): bool
    {
        return $this->type === static::TYPE_GROUP;
    }

    /**
     * @return null|BaseTax
     */
    public static function getZeroTax(): ?BaseTax
    {
        // Get the tax even if it is archived
        // Tax 0.0000 is always, it was the default until 4.2 version
        return (new BaseTax())->findOne([
            'rate' => '0.0000',
        ]);
    }

    /**
     * @inheridoc
     * @throws MethodNotAllowedHttpException
     */
    public function create($runValidation = true)
    {
        throw new MethodNotAllowedHttpException('Method "POST" is disabled!');
    }

    /**
     * @inheridoc
     * @throws MethodNotAllowedHttpException
     */
    public function update($runValidation = true)
    {
        throw new MethodNotAllowedHttpException('Method "PUT" is disabled!');
    }

    /**
     * @inheridoc
     * @throws MethodNotAllowedHttpException
     */
    public function delete()
    {
        throw new MethodNotAllowedHttpException('Method "DELETE" is disabled!');
    }
}
