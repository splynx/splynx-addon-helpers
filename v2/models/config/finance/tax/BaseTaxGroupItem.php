<?php

namespace splynx\v2\models\config\finance\tax;

use yii\base\Model;

class BaseTaxGroupItem extends Model
{
    /** @var int */
    public $id;

    /** @var string */
    public string $type;

    /** @var int */
    public int $apply_to_all_previous = 0;

    /** @var int */
    public int $position = 0;
}
