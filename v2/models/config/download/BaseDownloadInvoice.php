<?php

namespace splynx\v2\models\config\download;

class BaseDownloadInvoice extends BaseDownload
{
    /** @var string */
    public $module = 'invoices';
}
