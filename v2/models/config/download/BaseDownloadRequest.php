<?php

namespace splynx\v2\models\config\download;

class BaseDownloadRequest extends BaseDownload
{
    /** @var string */
    public $module = 'requests';
}
