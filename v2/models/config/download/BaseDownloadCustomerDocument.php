<?php

namespace splynx\v2\models\config\download;

class BaseDownloadCustomerDocument extends BaseDownload
{
    /** @var string */
    public $module = 'customer_documents';
}
