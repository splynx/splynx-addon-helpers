<?php

namespace splynx\v2\models\config\download;

use Exception;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidCallException;
use yii\base\InvalidValueException;

abstract class BaseDownload extends BaseActiveApi
{
    /** @var string */
    public $module;

    /** @var int */
    public $document_id;

    /** @var string */
    public $filename;

    /** @var string */
    public $content_type;

    /** @var string */
    public $content;

    public const TYPE_MODULE_CUSTOMER_DOCUMENT = 'customer_documents';
    public const TYPE_MODULE_CUSTOMER_INVOICE = 'invoices';
    public const TYPE_MODULE_CUSTOMER_REQUEST = 'requests';
    public const TYPE_MODULE_CUSTOMER_PAYMENT = 'payments';

    protected static $apiUrl = 'admin/config/download';

    /**
     * @param int $document_id
     * @param array<mixed> $config
     */
    public function __construct($document_id, array $config = [])
    {
        $this->document_id = $document_id;
        parent::__construct($config);
    }

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['module', 'document_id'], 'required'],
            [['document_id'], 'integer'],
            [['module'], 'in', 'range' => self::getDocumentTypes()],
        ];
    }

    /**
     * Download document.
     * @return string
     * @throws \yii\base\InvalidConfigException|InvalidCallException
     */
    public function download()
    {
        if ($this->validate()) {
            $result = ApiHelper::getInstance()->get('admin/config/download', $this->module . '--' . $this->document_id);
            if ($result['result'] && !empty($result['response'])) {
                /** @var array{'result': bool, 'response': array<string, mixed>} $result */
                $this->content = $this->decodeContent($result['response']['content']);
                $this->filename = $result['response']['name'];
                $this->content_type = $result['response']['content_type'];
                return $this->content;
            }
            throw new InvalidCallException('Error in Api call');
        }
        throw new InvalidValueException('Parameters module or document_id not set correctly ' . PHP_EOL . implode(PHP_EOL, array_values($this->getFirstErrors())));
    }

    /**
     * get document type
     * @return array<string>
     */
    public function getDocumentTypes()
    {
        return [
            self::TYPE_MODULE_CUSTOMER_DOCUMENT,
            self::TYPE_MODULE_CUSTOMER_INVOICE,
            self::TYPE_MODULE_CUSTOMER_REQUEST,
            self::TYPE_MODULE_CUSTOMER_PAYMENT,
        ];
    }

    /**
     * @param string $content
     * @return string
     */
    protected function decodeContent($content)
    {
        /** @var string|false $decodedContent */
        $decodedContent =  base64_decode($content);
        if ($decodedContent === false) {
            throw new Exception('Can\'t decode content!');
        }
        return $decodedContent;
    }
}
