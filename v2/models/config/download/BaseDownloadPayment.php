<?php

namespace splynx\v2\models\config\download;

class BaseDownloadPayment extends BaseDownload
{
    /** @var string */
    public $module = 'payments';
}
