<?php

namespace splynx\v2\models\transport;

use splynx\helpers\ArrayHelper;

/**
 * Class BaseSms
 * @package splynx\v2\models\transport
 */
class BaseSms extends BaseMessage
{
    protected static $apiUrl = 'admin/config/sms';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['recipient'], 'string', 'max' => 64],
        ], parent::rules());
    }
}
