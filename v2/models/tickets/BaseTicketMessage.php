<?php

namespace splynx\v2\models\tickets;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseTicketMessage
 * @package splynx\v2\models\tickets
 */
class BaseTicketMessage extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int */
    public $ticket_id;

    /** @var int */
    public $customer_id;

    /** @var int */
    public $incoming_customer_id;

    /** @var int */
    public $admin_id;

    /** @var int */
    public $api_id;

    /** @var string */
    public $date;

    /** @var string */
    public $time;

    /** @var string */
    public $message;

    /** @var mixed */
    public $files;

    /** @var int<0, 1>|bool */
    public $is_merged;

    /** @var int<0, 1>|bool */
    public $can_be_deleted;

    /** @var int<0, 1>|bool */
    public $hide_for_customer;

    /** @var string */
    public $mail_to;

    /** @var string */
    public $mail_cc;

    /** @var string */
    public $mail_bcc;

    /** @var self::MESSAGE_TYPE_MESSAGE|self::MESSAGE_TYPE_NOTE|self::MESSAGE_TYPE_CHANGE_TICKET */
    public $message_type;

    protected static $apiUrl = 'admin/support/ticket-messages';

    public const MESSAGE_TYPE_MESSAGE = 'message';
    public const MESSAGE_TYPE_NOTE = 'note';
    public const MESSAGE_TYPE_CHANGE_TICKET = 'change_ticket';
}
