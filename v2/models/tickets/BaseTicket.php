<?php

namespace splynx\v2\models\tickets;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseTicket
 * @package splynx\v2\models\tickets
 */
class BaseTicket extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var int */
    public $incoming_customer_id;

    /** @var int<0, 1> */
    public $hidden;

    /** @var int */
    public $assign_to;

    /** @var int */
    public $status_id;

    /** @var int */
    public $group_id;

    /** @var int */
    public $type_id;

    /** @var string */
    public $subject;

    /** @var self::PRIORITY_LOW|self::PRIORITY_MEDIUM|self::PRIORITY_HIGH|self::PRIORITY_URGENT */
    public $priority;

    /** @var int<0, 1> */
    public $star;

    /** @var int<0, 1> */
    public $unread_by_customer;

    /** @var int<0, 1> */
    public $unread_by_admin;

    /** @var int<0, 1> */
    public $closed;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;

    /** @var int<0, 1> */
    public $trash;

    /** @var int */
    public $lead_id;

    /** @var mixed */
    public $author;

    /** @var int<0, 1> */
    public $deleted;

    /** @var array<mixed> */
    public $additional_attributes;

    protected static $apiUrl = 'admin/support/tickets';

    public const PRIORITY_LOW = 'low';
    public const PRIORITY_MEDIUM = 'medium';
    public const PRIORITY_HIGH = 'high';
    public const PRIORITY_URGENT = 'urgent';

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'customer_id',
                    'incoming_customer_id',
                    'assign_to',
                    'status_id',
                    'group_id',
                    'star',
                    'unread_by_customer',
                    'unread_by_admin',
                    'closed',
                    'trash',
                    'deleted',
                ],
                'integer',
            ],
            [['subject', 'priority'], 'string'],
            [['created_at', 'updated_at'], 'string'],
        ];
    }
}
