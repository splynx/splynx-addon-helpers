<?php

namespace splynx\v2\models\finance;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseTransactions
 * @package splynx\models\finance
 */
class BaseTransactions extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $type;

    /** @var int */
    public $quantity = 1;

    /** @var string */
    public $unit;

    /** @var float */
    public $price;

    /** @var float|null */
    public $tax_percent;

    /** @var float */
    public $total;

    /** @var string */
    public $date;

    /** @var string */
    public $category;

    /** @var string */
    public $description;

    /** @var string */
    public $period_from;

    /** @var string */
    public $period_to;

    /** @var int */
    public $service_id;

    /** @var int */
    public $payment_id;

    /** @var int */
    public $invoice_id;

    /** @var int|null */
    public $invoiced_by_id;

    /** @var string */
    public $comment;

    /** @var bool */
    public $to_invoice;

    /** @var string */
    public $service_type;

    /** @var string */
    public $source;

    /** @var int */
    public $tax_id;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public const SOURCE_MANUAL = 'manual';
    public const SOURCE_AUTO = 'auto';
    public const SOURCE_CDR = 'cdr';

    public const TYPE_DEBIT = 'debit';
    public const TYPE_CREDIT = 'credit';

    public const SERVICE_TYPE_INTERNET = 'internet';
    public const SERVICE_TYPE_VOICE = 'voice';
    public const SERVICE_TYPE_CUSTOM = 'custom';

    public static $apiUrl = 'admin/finance/transactions';
}
