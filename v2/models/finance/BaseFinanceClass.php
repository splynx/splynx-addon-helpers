<?php

namespace splynx\v2\models\finance;

use ArrayObject;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\models\finance\item\BaseItem;
use splynx\v2\models\finance\item\ItemsObject;
use yii\helpers\ArrayHelper;

/**
 * Class BaseFinanceClass
 *
 * adding property and methods for use Items in the Finance classes.
 *
 * usage:
 *
 * create item
 *
 * ```php
 * $newItem = ['id'=>1,'description'=>'Desc'...]
 * ```
 *
 * or
 *
 * ```php
 * $newItem = new Item(['id'=>1,'description'=>'Desc'...]);
 * ```
 * or
 *
 * ```php
 * $newItem = new Item();
 * $newItem->id = 1;
 * $newItem->description = 'Desc';
 * $newItem->...
 * ```
 *
 * Add Item
 *
 * ```php
 * $model->items[] = $newItem;
 * ```
 *
 * Change Item
 *
 * ```php
 * $item = $model->items[0];
 * $item->description = 'New description'
 * ```
 *
 * @property array<BaseItem>|array<string, mixed> $items
 * */
class BaseFinanceClass extends BaseActiveApi implements FinanceDocumentItemsInterface
{
    /**
     * @var ItemsObject<BaseItem>
     */
    protected $_items;

    /**
     * Sets items object.
     * @inheritDoc
     * @return void
     */
    public function init()
    {
        $this->clearItems();
        parent::init();
    }

    /**
     * @return BaseItem[]|ItemsObject<BaseItem>
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * @param array<mixed>|BaseItem[] $items
     * @param bool $clearItems
     * @return void
     */
    public function setItems(array $items = null, $clearItems = true)
    {
        // Fix for version 3.1 api response for empty items
        if (is_array($items)) {
            foreach ($items as $item) {
                $this->items[] = $item;
            }
        }
    }

    /**
     * Inits empty documetns items
     * @return void
     */
    public function clearItems()
    {
        $this->_items = new ItemsObject(
            [],
            0,
            'ArrayIterator',
            $this->getItemsClassName()
        );
    }

    /**
     * @param static $model
     * @inheritdoc
     */
    public static function populate($model, $data)
    {
        // Populate Items to model
        $items = null;
        if (array_key_exists('items', $data)) {
            $items = $data['items'];
            unset($data['items']);
        }
        parent::populate($model, $data);
        // Deleting old items
        $model->clearItems();
        $model->items = $items;
    }

    /**
     * @inheritdoc
     * and validate items before save
     */
    public function save($runValidation = true)
    {
        if ($runValidation) {
            // Validate Items Before save
            foreach ($this->items as $item) {
                if (!$item->validate()) {
                    $this->addError('items', json_encode($item->getErrors(), JSON_THROW_ON_ERROR));
                }
            }
            if (!empty($this->errors)) {
                return false;
            }
        }
        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     *
     * Add Items to attributesForSend
     *
     * @see BaseActiveApi::getAttributesForSend()
     */
    public function getAttributesForSend()
    {
        // Add Items to attributesForSend
        $attributesForSend = parent::getAttributesForSend();
        $attributesForSend['items'] = [];
        foreach ($this->items as $item) {
            $attributesForSend['items'][] = ArrayHelper::toArray($item);
        }
        return $attributesForSend;
    }

    /**
     * @param integer $number
     * @return static|null
     */
    public static function findByNumber($number)
    {
        $invoice = new static();//@phpstan-ignore-line
        return $invoice->findOne(['number' => $number]);
    }

    /**
     * Update Invoice and reload attributes.
     * @inheritdoc
     */
    public function update($runValidation = true)
    {
        $this->reloadAttributesAfterSave = true;
        return parent::update($runValidation);
    }

    /**
     * @return class-string<BaseItem>
     */
    public function getItemsClassName(): string
    {
        return BaseItem::class;
    }
}
