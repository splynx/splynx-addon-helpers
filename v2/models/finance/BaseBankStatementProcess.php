<?php

namespace splynx\v2\models\finance;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseBankStatementProcess
 * @package splynx\models\finance
 */
class BaseBankStatementProcess extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $date_time;

    /** @var string */
    public $title;

    /** @var string */
    public $handler;

    /** @var string */
    public $status;

    /** @var string */
    public $filename_original;

    /** @var string */
    public $filename_uploaded;

    /** @var string */
    public $filename_parsed;

    /** @var int */
    public $records;

    /** @var int */
    public $processed;

    /** @var int */
    public $errors;

    /** @var array<mixed> */
    public $selected_columns;

    /** @var array<mixed> */
    public $dataFromApi;

    /** @var int */
    public $payment_type;

    protected static $apiUrl = 'admin/finance/bank-statements';
}
