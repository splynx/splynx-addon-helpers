<?php

namespace splynx\v2\models\finance;

use splynx\v2\models\finance\item\BaseProformaInvoiceItem;

/**
 * Class BaseProformaInvoice
 * @package splynx\models\finance
 */
class BaseProformaInvoice extends BaseFinanceClass
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $date_created;

    /** @var string */
    public $real_create_datetime;

    /** @var string */
    public $date_updated;

    /** @var string */
    public $date_payment;

    /** @var string */
    public $date_till;

    /** @var string */
    public $number;

    /** @var float */
    public $total;

    /** @var int */
    public $payment_id;

    /** @var string */
    public $status;

    /** @var bool */
    public $is_sent;

    /** @var string */
    public $note;

    /** @var string */
    public $memo;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/proforma-invoices';

    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';
    public const STATUS_PENDING = 'pending';

    /**
     * @return string
     */
    public function getItemsClassName(): string
    {
        return BaseProformaInvoiceItem::class;
    }
}
