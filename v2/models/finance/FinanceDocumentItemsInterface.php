<?php

namespace splynx\v2\models\finance;

/**
 * Interface FinanceDocumentItemsInterface
 * @package splynx\v2\models\finance
 */
interface FinanceDocumentItemsInterface
{
    /**
     * Should return classname for finance document item
     * @return string
     */
    public function getItemsClassName(): string;
}
