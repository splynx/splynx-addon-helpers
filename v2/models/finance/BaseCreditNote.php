<?php

namespace splynx\v2\models\finance;

use splynx\v2\models\finance\item\BaseCreditNoteItem;

/**
 * Class BaseCreditNote
 * @package splynx\models\finance
 */
class BaseCreditNote extends BaseFinanceClass
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $date_created;

    /** @var string */
    public $date_payment;

    /** @var string */
    public $date_updated;

    /** @var bool */
    public $disable_cache;

    /** @var string */
    public $note;

    /** @var string */
    public $number;

    /** @var int */
    public $payment_id;

    /** @var string */
    public $status;

    /** @var float */
    public $total;

    /** @var bool */
    public $is_sent;

    /** @var int */
    public $invoicesId;

    /** @var string */
    public $added_by;

    /** @var int */
    public $added_by_id;

    /** @var string */
    public $real_create_datetime;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/credit-notes';

    public const STATUS_REFUNDED = 'refunded';
    public const STATUS_NOT_REFUNDED = 'not_refunded';
    public const STATUS_DELETED = 'deleted';

    /**
     * @return string
     */
    public function getItemsClassName(): string
    {
        return BaseCreditNoteItem::class;
    }
}
