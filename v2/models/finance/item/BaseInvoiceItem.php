<?php

namespace splynx\v2\models\finance\item;

/**
 * Class BaseInvoiceItem
 * @package splynx\models\finance\item
 */
class BaseInvoiceItem extends BaseItem
{
    /** @var  int */
    public $transaction_id;
    /** @var int */
    public $categoryIdForTransaction;
    /** @var  int */
    public $pos;
    /** @var  string */
    public $period_from;
    /** @var  string */
    public $period_to;
}
