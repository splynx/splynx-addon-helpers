<?php

namespace splynx\v2\models\finance\item;

use yii\base\Model;

/**
 * Class BaseItemModel
 *
 * @package splynx\models\finance
 */
class BaseItem extends Model
{
    /** @var  int */
    public $id;
    /** @var  int */
    public $tax_amount;

    /** @var  string */
    public $description;

    /** @var  int */
    public $quantity;

    /** @var  number */
    public $unit;

    /** @var  double */
    public $price;

    /** @var  double|null */
    public $tax;

    /** @var int */
    public $tax_id;

    /**
     * Item constructor.
     *
     * @param array<mixed> $config
     */
    public function __construct($config = [])
    {
        foreach ($config as $key => $prop) {
            if (!property_exists($this, $key)) {
                unset($config[$key]);
            }
        }
        parent::__construct($config);
    }

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['description', 'quantity', 'price'], 'required'],
            [['price', 'tax'], 'double'],
            ['quantity', 'default', 'value' => 1],
            ['quantity', 'integer', 'min' => 1],
            ['tax_id', 'integer'],
        ];
    }
}
