<?php

namespace splynx\v2\models\finance\item;

use ArrayObject;
use InvalidArgumentException;

/**
 * Class ItemsObject
 *
 * Convert array item to Item object
 * @package splynx\models\finance\item
 *
 * @template T
 * @extends ArrayObject<int, object<T>>
 */
class ItemsObject extends ArrayObject
{
    /** @var class-string Item class name */
    public $model;

    /**
     * @inheridoc
     * @param array<mixed>|object $array The input parameter accepts an array or an Object.
     * @param int $flags Flags to control the behaviour of the ArrayObject object.
     * @param class-string $iteratorClass Specify the class that will be used for iteration of the ArrayObject object. ArrayIterator is the default class used.
     * @param class-string<T> $model Item class name
     */
    public function __construct(
        $array = [],
        $flags = 0,
        $iteratorClass = 'ArrayIterator',
        $model = BaseItem::class
    ) {
        $this->model = $model;
        parent::__construct($array, $flags, $iteratorClass);
    }

    /**
     * Check and write new Item
     *
     * @param int $index
     * @param array<mixed>|T $newItem
     * @throws InvalidArgumentException
     */
    public function offsetSet($index, $newItem): void
    {
        $item = null;
        if (is_array($newItem)) {
            if (!class_exists($this->model)) {
                throw new InvalidArgumentException('Invalid item class value: ' . $this->model);
            }
            $item = new $this->model($newItem);
        } elseif ($newItem instanceof BaseItem) {
            $item = $newItem;
        } else {
            throw new InvalidArgumentException('Invalid item value');
        }
        parent::offsetSet($index, $item);
    }
}
