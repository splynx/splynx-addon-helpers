<?php

namespace splynx\v2\models\finance\item;

/**
 * Class BaseProformaInvoiceItem
 * @package splynx\models\finance\item
 */
class BaseProformaInvoiceItem extends BaseItem
{
    /** @var  int */
    public $pos;
    /** @var  string */
    public $period_from;
    /** @var  string */
    public $period_to;
}
