<?php

namespace splynx\v2\models\finance\item;

/**
 * Class BaseCreditNoteItem
 * @package splynx\models\finance\item
 */
class BaseCreditNoteItem extends BaseItem
{
    /** @var  int */
    public $transaction_id;
    /** @var int */
    public $categoryIdForTransaction;
    /** @var int */
    public $position;
    /** @var int */
    public $invoice_item_id;
    /** @var bool */
    public $isCorrection;
    /** @var bool */
    public $deleteTransaction;
}
