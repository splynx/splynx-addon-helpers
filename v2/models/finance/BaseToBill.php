<?php

namespace splynx\v2\models\finance;

use splynx\v2\helpers\ApiHelper;
use yii\base\BaseObject;

/**
 * Class BaseToBill
 * @package splynx\models\finance
 */
class BaseToBill extends BaseObject
{
    /** @var int */
    public $customer_id;

    /** @var 'preview'|'generate' */
    public $action;

    /** @var string */
    public $toBillDate;

    /** @var string */
    public $transactionDate;

    /** @var int */
    public $period;

    /** @var array<mixed>|false */
    public $result;

    /** @var array<mixed>|false */
    public $invoice;

    public const ACTION_PREVIEW = 'preview';
    public const ACTION_GENERATE = 'generate';

    /** @var string */
    public static $apiUrl = 'admin/finance/to-bill';

    /**
     * @return string
     */
    protected function getApiUrl()
    {
        return static::$apiUrl . '/' . $this->customer_id . '--' . $this->action;
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     */
    public function call()
    {
        $attributes = [
            'toBillDate' => $this->toBillDate,
            'transactionDate' => $this->transactionDate,
            'period' => $this->period,
        ];

        $result = ApiHelper::getInstance()->post($this->getApiUrl(), $attributes);

        if ($result['result'] == true) {
            /** @var array{'result': bool, 'response': array<string, mixed>} $result */
            $this->result = $result['response']['result'];
            $this->invoice = $result['response']['invoice'];
        }

        return $result['result'];
    }
}
