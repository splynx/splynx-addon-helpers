<?php

namespace splynx\v2\models\finance;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BasePayment
 * @package splynx\models\finance
 */
class BasePayment extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var int */
    public $invoice_id;

    /** @var int */
    public $request_id;

    /** @var int */
    public $credit_note_id;

    /** @var int */
    public $transaction_id;

    /** @var string */
    public $payment_type;

    /** @var string */
    public $receipt_number;

    /** @var string */
    public $date;

    /** @var float */
    public $amount;

    /** @var string */
    public $comment;

    /** @var string */
    public $field_1;

    /** @var string */
    public $field_2;

    /** @var string */
    public $field_3;

    /** @var string */
    public $field_4;

    /** @var string */
    public $field_5;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/payments';

    /**
     * @return \splynx\v2\models\finance\BaseProformaInvoice|null
     * @throws \splynx\base\ApiResponseException
     */
    public function getProformaInvoice()
    {
        return (new BaseProformaInvoice())->findById($this->request_id);
    }
}
