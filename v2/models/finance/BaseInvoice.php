<?php

namespace splynx\v2\models\finance;

use splynx\v2\models\finance\item\BaseInvoiceItem;

/**
 * Class BaseInvoice
 * @package splynx\models\finance
 */
class BaseInvoice extends BaseFinanceClass
{
    /** @var int */
    public $id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $date_created;

    /** @var string */
    public $date_payment;

    /** @var string */
    public $date_till;

    /** @var string */
    public $date_updated;

    /** @var bool|null */
    public $disable_cache;

    /** @var string */
    public $memo;

    /** @var string */
    public $note;

    /** @var string */
    public $number;

    /** @var int */
    public $payment_id;

    /** @var string */
    public $status;

    /** @var float */
    public $total;

    /** @var float */
    public $due;

    /** @var '0'|'1' */
    public $use_transactions;

    /** @var int<0, 1> */
    public $payd_from_deposit;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/invoices';

    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';
    public const STATUS_DELETED = 'deleted';
    public const STATUS_PENDING = 'pending';

    /**
     * @return string
     */
    public function getItemsClassName(): string
    {
        return BaseInvoiceItem::class;
    }
}
