<?php

namespace splynx\v2\models\finance;

use JsonException;
use splynx\helpers\JsonHelper;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;
use splynx\v2\helpers\PaymentStatementSourceEnum;
use Yii;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;

/**
 * Class BankStatement
 * @package splynx\models\finance
 */
class BankStatement extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var int|null */
    public $bank_statements_process_id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $payment_date;

    /** @var float */
    public $amount;

    /** @var string */
    public $status = self::STATUS_NEW;

    /** @var int */
    public $payment_id;

    /** @var int */
    public $invoice_id;

    /** @var int */
    public $credit_note_id;

    /** @var int */
    public $request_id;

    /** @var int */
    public $transaction_id;

    /** @var string|null */
    public $comment;

    /** @var string */
    public $additional_1;

    /** @var string */
    public $additional_2;

    /** @var string */
    public $additional_3;
    /** @var array<int|string, mixed>|string|null */
    public $callback_data;
    /** @var string */
    public string $callback_status = self::CALLBACK_STATUS_NEW;

    public string $source = '';

    /** @var string */
    public $addonTitle;

    public const STATUS_PENDING = 'pending';
    public const STATUS_NEW = 'new';
    public const STATUS_PROCESSED = 'processed';
    public const STATUS_ERROR = 'error';
    public const STATUS_CANCELED = 'canceled';

    public const CALLBACK_STATUS_NEW = 'new';
    public const CALLBACK_STATUS_PROCESSING = 'processing';
    public const CALLBACK_STATUS_FAILED = 'failed';
    public const CALLBACK_STATUS_PROCESSED = 'processed';

    /** @var string */
    protected static $statementsApiCall = 'admin/finance/bank-statements';

    protected static $apiUrl = 'admin/finance/bank-statements-records';

    /**
     * @inheritdoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['customer_id', 'amount'], 'required'],
            [
                [
                    'id',
                    'customer_id',
                    'payment_id',
                    'bank_statements_process_id',
                    'invoice_id',
                    'credit_note_id',
                    'request_id',
                    'transaction_id',
                ],
                'integer',
            ],
            [['comment', 'source'], 'string'],
            [
                'status',
                'in',
                'range' => [
                    self::STATUS_PENDING,
                    self::STATUS_NEW,
                    self::STATUS_PROCESSED,
                    self::STATUS_CANCELED,
                    self::STATUS_ERROR,
                ],
            ],
            [
                'callback_status',
                'in',
                'range' => [
                    self::CALLBACK_STATUS_NEW,
                    self::CALLBACK_STATUS_PROCESSING,
                    self::CALLBACK_STATUS_FAILED,
                    self::CALLBACK_STATUS_PROCESSED,
                ],
            ],
        ];
    }

    /**
     * @param static $model
     * @param array<string, mixed> $data
     * @return void
     * @throws JsonException
     */
    public static function populate($model, $data): void
    {
        foreach ($data as $name => &$value) {
            if (property_exists($model, $name) && $name === 'callback_data' && !is_array($value) && $value !== null) {
                $value = json_decode($value, true, 512, JSON_THROW_ON_ERROR);
            }
        }

        parent::populate($model, $data);
    }

    /**
     * @inheritdoc
     * @throws JsonException
     */
    public function save($runValidation = true)
    {
        if (is_null($this->bank_statements_process_id)) {
            $this->bank_statements_process_id = $this->getStatementProcessId();
        }
        if (is_null($this->comment)) {
            $this->comment = $this->addonTitle;
        }
        if (($this->callback_data !== null && !is_string($this->callback_data)) || (is_string($this->callback_data) && !JsonHelper::isJson($this->callback_data))) {
            $this->callback_data = JsonHelper::encode($this->callback_data);
        }

        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     */
    public function getAttributesForSendArray()
    {
        return [
            'bank_statements_process_id',
            'customer_id',
            'payment_date',
            'amount',
            'status',
            'comment',
            'payment_id',
            'invoice_id',
            'credit_note_id',
            'request_id',
            'transaction_id',
            'additional_1',
            'additional_2',
            'additional_3',
            'callback_data',
            'callback_status',
            'source',
        ];
    }

    /**
     * @return static|null
     */
    public function createBankStatement()
    {
        $this->source = PaymentStatementSourceEnum::getPaymentStatementSource()->value;
        if ($this->save()) {
            return $this;
        }
        return null;
    }

    /**
     * @return string
     */
    protected function getStatementProcessTitle()
    {
        $period = Yii::$app->params['bank_statements_group'];

        if ($period == 'day') {
            $comment = date('d/m/Y');
        } else {
            $comment = date('m/Y');
        }

        return $this->addonTitle . ' ' . $comment;
    }

    /**
     * @return mixed
     * @throws InvalidConfigException
     */
    protected function getStatementProcessId()
    {
        $title = $this->getStatementProcessTitle();

        $params = [
            'main_attributes' => [
                'title' => $title,
            ],
        ];

        $result = ApiHelper::getInstance()->search(self::$statementsApiCall, $params);

        if ($result['result'] == false) {
            throw new InvalidCallException(Yii::t('app', 'Error in API Call!'));
        }
        /** @var array{'result': bool, 'response': array<string, mixed>} $result */
        if (!empty($result['response'])) {
            $item = reset($result['response']);
            return $item['id'];
        }

        // Create new
        $params = [
            'title' => $title,
            'status' => 'success',
        ];

        $result = ApiHelper::getInstance()->post(self::$statementsApiCall, $params);
        if ($result['result'] == false) {
            throw new InvalidCallException(Yii::t('app', 'Error in API Call!'));
        }
        /** @var array{'result': bool, 'response': array<string, mixed>} $result */
        return $result['response']['id'];
    }

    /**
     * Find BankStatement by id.
     * @param integer $id
     * @return static|null
     */
    public function getBankStatementById($id)
    {
        return $this->findOne(['id' => $id]);
    }

    /**
     * @return bool
     */
    public function isCallbackData(): bool
    {
        return $this->callback_data !== null;
    }

    /**
     * @return BankStatement
     */
    public function callbackFailed(): BankStatement
    {
        $this->callback_status = static::CALLBACK_STATUS_FAILED;

        return $this;
    }

    /**
     * @return BankStatement
     */
    public function callbackProcessed(): BankStatement
    {
        $this->callback_status = static::CALLBACK_STATUS_PROCESSED;

        return $this;
    }
}
