<?php

namespace splynx\v2\models\finance\notifications;

use splynx\base\ApiResponseException;
use splynx\helpers\ConfigHelper;
use splynx\models\console_api\administration\ConsoleApiKey;
use splynx\v2\helpers\ApiHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;

class FailedPaymentNotifications extends Model
{
    protected const SEND_NOTIFICATIONS_NAME = 'send_failed_payment_notifications';
    protected static string $_apiUrl = 'admin/finance/failed-payment-notifications';

    public int $customerId;
    public string $paymentGateway;
    public ?int $paymentStatementId = null;
    /** @var array<mixed> */
    public array $errors = [];
    /** @var array<mixed> */
    public array $additionalData = [];
    private string $_addonTitle = '';

    /**
     * @inheritdoc
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->findAddonTitle();
    }

    /**
     * @inheritdoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['customerId', 'paymentGateway'], 'required'],
            [['paymentGateway'], 'string', 'max' => 255],
            [['customerId'], 'integer'],
            [['errors', 'additionalData'], 'safe'],
        ];
    }

    /**
     * @return bool
     */
    protected function isEnabledNotification(): bool
    {
        return (bool)ConfigHelper::get(static::SEND_NOTIFICATIONS_NAME);
    }

    /**
     * @return bool
     * @throws ApiResponseException
     * @throws InvalidConfigException
     */
    public function send(): bool
    {
        if (!$this->isEnabledNotification()) {
            $message = Yii::t('app', 'Send failed payment notification is disabled in addon config!');
            $this->addError('config', $message);
            Yii::error($message);
            return false;
        }

        if (!$this->validate()) {
            Yii::error(var_export($this->errors, true));
            return false;
        }

        $result = ApiHelper::getInstance()->post(self::$_apiUrl, $this->fields());
        if (empty($result['result'])) {
            Yii::error(var_export($result, true));
        }
        return !empty($result['result']);
    }

    /**
     * @return array<string, mixed>
     */
    public function fields()
    {
        return [
            'customer_id' => $this->customerId,
            'payment_gateway' => $this->_addonTitle . $this->paymentGateway,
            'payment_statement_id' => $this->paymentStatementId,
            'errors' => $this->errors,
            'additional_data' => $this->additionalData,
        ];
    }

    /**
     * @return void
     */
    private function findAddonTitle(): void
    {
        $apiKey = Yii::$app->params['api_key'];
        if ($model = (new ConsoleApiKey())->findOne(['api_key' => $apiKey])) {
            $this->_addonTitle = $model->title . ': ';
        }
    }
}
