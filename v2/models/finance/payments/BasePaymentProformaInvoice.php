<?php

namespace splynx\v2\models\finance\payments;

use splynx\v2\models\finance\BaseProformaInvoice;
use yii\helpers\ArrayHelper;

/**
 * Class BasePaymentProformaInvoice
 * @package splynx\models\finance\payments
 * @property BaseProformaInvoice $invoice
 */
abstract class BasePaymentProformaInvoice extends BasePaymentModel
{
    /** @var BaseProformaInvoice|null */
    protected $_invoice;

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['invoice'], 'required'],
        ], parent::rules());
    }

    /**
     * @return BaseProformaInvoice|null
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * Set Proforma Invoice, amount and customer ID
     * @param BaseProformaInvoice $request
     * @return void
     */
    public function setInvoice($request)
    {
        $this->_invoice = $request;
        $this->amount = $request->total;
        $this->customer_id = $request->customer_id;
    }
}
