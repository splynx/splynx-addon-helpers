<?php

namespace splynx\v2\models\finance\payments;

use splynx\v2\models\finance\BaseInvoice;
use yii\helpers\ArrayHelper;

/**
 * Class BasePaymentInvoice
 * @package splynx\models\finance\payments
 * @property BaseInvoice $invoice
 */
abstract class BasePaymentInvoice extends BasePaymentModel
{
    /** @var BaseInvoice|null */
    protected $_invoice;

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['invoice'], 'required'],
        ], parent::rules());
    }

    /**
     * @return BaseInvoice|null
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * Set Invoice, amount and customer ID
     * @param BaseInvoice $invoice
     * @return void
     */
    public function setInvoice($invoice)
    {
        $this->_invoice = $invoice;
        $this->amount = $invoice->total;
        $this->customer_id = $invoice->customer_id;
    }
}
