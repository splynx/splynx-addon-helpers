<?php

namespace splynx\v2\models\customer;

use splynx\v2\base\BaseActiveApi;
use yii\base\InvalidCallException;

/**
 * Class BasePaymentAccount
 * @package splynx\models\customer
 */
class BasePaymentAccount extends BaseActiveApi
{
    /** @var int */
    public $account_id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $field_1;

    /** @var string */
    public $field_2;

    /** @var string */
    public $field_3;

    /** @var string */
    public $field_4;

    /** @var string */
    public $field_5;

    /** @var string */
    public $field_6;

    /** @var string */
    public $field_7;

    /** @var string */
    public $field_8;

    /** @var string */
    public $field_9;

    /** @var string */
    public $field_10;

    public static $apiUrl = 'admin/customers/customer-payment-accounts';

    protected function getApiUrl($id = null, $conditions = [])
    {
        return static::$apiUrl . '/' . $this->customer_id . '--' . $this->account_id;
    }

    public function create($runValidation = true)
    {
        throw new InvalidCallException('Payment accounts does not has create call!');
    }

    /**
     * Because payment accounts a little bit different than ither API models
     * use this method to get customer payment account data.
     * @param integer $customerId Customer id
     * @param integer $accountId Payment account id
     * @return static|null
     */
    public static function get($customerId, $accountId)
    {
        $model = new static();//@phpstan-ignore-line
        $model->customer_id = $customerId;
        $model->account_id = $accountId;
        return $model->findById($customerId);
    }
}
