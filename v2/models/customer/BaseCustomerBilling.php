<?php

namespace splynx\v2\models\customer;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseCustomerBilling
 * @package splynx\models\customer
 */
class BaseCustomerBilling extends BaseActiveApi
{
    /** @var int */
    public $customer_id;

    /** @var int<-1, 99> */
    public $blocking_period;

    /** @var string */
    public $billing_city;

    /** @var int<0, static::MAX_BILLING_DAY> */
    public $billing_date;

    /** @var int<0, 99> */
    public $billing_due;

    /** @var string */
    public $billing_person;

    /** @var string */
    public $billing_street_1;

    /** @var string */
    public $billing_zip_code;

    /** @var float */
    public $deposit;

    /** @var boolean */
    public $enabled;

    /** @var int<0, 99> */
    public $grace_period;

    /** @var boolean */
    public $make_invoices;

    /** @var float */
    public $min_balance;

    /** @var int */
    public $payment_method;

    /** @var int<0, 99> */
    public $reminder_day_1;

    /** @var int<0, 99> */
    public $reminder_day_2;

    /** @var int<0, 99> */
    public $reminder_day_3;

    /** @var bool */
    public $reminder_enable;

    /** @var bool */
    public $reminder_payment;

    /** @var string */
    public $reminder_payment_comment;

    /** @var float */
    public $reminder_payment_value;

    /** @var int<0, 2> */
    public $reminder_type;

    /** @var int<0, static::MAX_BILLING_DAY> */
    public $request_auto_day;

    /** @var bool */
    public $request_auto_enable;

    /** @var string */
    public $request_auto_next;

    /** @var int<0, 1> */
    public $request_auto_period;

    /** @var int */
    public $request_auto_type;

    /** @var int */
    public $type;

    public const MAX_BILLING_DAY = 28;

    public static $apiUrl = 'admin/customers/customer-billing';

    public function getPrimaryKey()
    {
        return 'customer_id';
    }
}
