<?php

namespace splynx\v2\models\customer;

use exceptions\Exception;
use splynx\v2\models\common\BaseCustomerModel;
use yii\base\NotSupportedException;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * Class BaseCustomer
 * @package splynx\models\customer
 */
class BaseCustomer extends BaseCustomerModel implements IdentityInterface
{
    /** @var string */
    public $category = self::CATEGORY_PERSON;

    /** @var string */
    public $status;

    /** @var string */
    public $last_online;

    /** @var string */
    public $last_update;

    public static $apiUrl = 'admin/customers/customer';

    // Categories
    public const CATEGORY_PERSON = 'person';
    public const CATEGORY_COMPANY = 'company';

    // Statuses
    public const STATUS_NEW = 'new';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_BLOCKED = 'blocked';
    public const STATUS_DOESNT_USE_SERVICES = 'disabled';

    /**
     * @return array|mixed[]
     * @throws Exception
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['name', 'partner_id', 'location_id', 'category'], 'required'],
        ]);
    }

    /**
     * @inheritdoc
     * @param int $id
     */
    public static function findIdentity($id)
    {
        return (new static())->findById($id);//@phpstan-ignore-line
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }
}
