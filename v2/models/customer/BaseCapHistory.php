<?php

namespace splynx\v2\models\customer;

use splynx\v2\base\BaseActiveApi;
use InvalidArgumentException;
use yii\web\MethodNotAllowedHttpException;

/**
 * Class BaseCapHistory
 * @package splynx\models\customer
 */
class BaseCapHistory extends BaseActiveApi
{
    public const ACTION_AUTO_TOP_UP = 'auto_top_up';
    public const ACTION_MANUAL_TOP_UP = 'manual_top_up';
    public const ACTION_PORTAL_TOP_UP = 'portal_top_up';
    public const ACTION_API_TOP_UP = 'api_top_up';
    public const ACTION_ROLLOVER_TO_NEXT_MONTH = 'rollover_to_next_month';
    public const ACTION_ROLLOVER_TO_NEW_SERVICE = 'rollover_to_new_service';
    public const ACTION_END_OF_PERIOD = 'end_of_period';
    public const ACTION_END_OF_VALIDITY = 'end_of_validity';
    public const ACTION_DISABLE_TOP_UPS = 'disable_top_ups';
    public const ACTION_ENDED_QUANTITY = 'ended_quantity';
    public const ACTION_DELETE = 'delete';
    public const ACTION_REMOVE_CAP = 'remove_cap';

    /** @var int */
    public $service_id;
    /** @var string */
    public $date_time;
    /** @var string */
    public $action;
    /** @var int|string */
    public $amount;
    /** @var int|string */
    public $total_amount;
    /** @var int|null */
    private $_customer_id;

    protected static $apiUrl = 'admin/customers/cap-history';

    protected function getApiUrl($id = null, $conditions = [])
    {
        return parent::getApiUrl($this->_customer_id, $conditions);
    }

    /**
     * @param int $customerId
     * @return static
     */
    public static function getModelForSearch($customerId)
    {
        $model = new static();//@phpstan-ignore-line
        $model->_customer_id = $customerId;
        return $model;
    }

    /**
     * @param array<string, mixed> $conditions
     * @param bool $one
     * @return static|null
     * @throws \splynx\base\ApiResponseException
     * @throws InvalidArgumentException
     */
    public function find(array $conditions, $one)
    {
        if (empty($this->_customer_id)) {
            throw new InvalidArgumentException('Customer ID is empty. Set customer ID first. Use static method "getModelForSearch".');
        }
        /** @var static|null $result */
        $result =  parent::find($conditions, $one);
        return $result;
    }

    /**
     * @param bool $runValidation
     * @throws MethodNotAllowedHttpException
     */
    public function create($runValidation = true)
    {
        throw new MethodNotAllowedHttpException('Method "POST" is disabled!');
    }

    /**
     * @param bool $runValidation
     * @throws MethodNotAllowedHttpException
     */
    public function update($runValidation = true)
    {
        throw new MethodNotAllowedHttpException('Method "PUT" is disabled!');
    }

    /**
     * @throws MethodNotAllowedHttpException
     */
    public function delete()
    {
        throw new MethodNotAllowedHttpException('Method "DELETE" is disabled!');
    }

    /**
     * @throws MethodNotAllowedHttpException
     */
    public function findById($id)
    {
        throw new MethodNotAllowedHttpException('Method "findById" is disabled!');
    }
}
