<?php

namespace splynx\v2\models\customer\vouchers;

use splynx\v2\base\BaseActiveApi;

class BaseVouchers extends BaseActiveApi
{
    /** @var string */
    public static $apiUrl = 'admin/customers/prepaid-cards';

    /** @var int */
    public $id;
    /** @var string */
    public $type;
    /** @var string */
    public $status;
    /** @var float */
    public $price;
    /** @var int */
    public $pool_id;
    /** @var int */
    public $partner_id;
    /** @var int */
    public $location_id;
    /** @var int */
    public $speed_download;
    /** @var int */
    public $speed_upload;
    /** @var int */
    public $traffic_in_total;
    /** @var int */
    public $traffic_in_used;
    /** @var int */
    public $traffic_in_used_mb;
    /** @var int */
    public $traffic_out_total;
    /** @var int */
    public $traffic_out_used;
    /** @var int */
    public $traffic_out_used_mb;
    /** @var int */
    public $traffic_all_total;
    /** @var int */
    public $traffic_all_used;
    /** @var string */
    public $prefix;
    /** @var string */
    public $login_short;
    /** @var string */
    public $login;
    /** @var string */
    public $password;
    /** @var string */
    public $seller_name;
    /** @var string */
    public $full_name;
    /** @var string */
    public $email;
    /** @var string */
    public $phone;
    /** @var string */
    public $remark;
    /** @var string */
    public $activated_at;
    /** @var int */
    public $time_online;
    /** @var string */
    public $time_online_selector;
    /** @var int */
    public $time_online_used;
    /** @var string */
    public $valid_till;
    /** @var int */
    public $time_expired;
    /** @var string */
    public $expired_by_selector;
    /** @var string */
    public $time_expired_selector;
    /** @var string */
    public $serie;
    /** @var string */
    public $created_at;
    /** @var string */
    public $expired_by;
    /** @var string */
    public $mac;
    /** @var int */
    public $customer_id;
    /** @var int */
    public $quantity;
    /** @var int */
    public $tariff_id;
    /** @var int */
    public $invoice_id;
    /** @var array<mixed> */
    public $additional_attributes = [];

    public const MINUTES = 'minutes';
    public const HOURS = 'hours';
    public const DAYS = 'days';

    public const EXPIRED_BY_VALID_TILL = 'valid_date';
    public const EXPIRED_BY_ACTIVATION_TIME = 'activation_time';
}
