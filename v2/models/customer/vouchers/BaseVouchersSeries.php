<?php

namespace splynx\v2\models\customer\vouchers;

use splynx\v2\base\BaseActiveApi;

class BaseVouchersSeries extends BaseActiveApi
{
    /** @var string */
    public static $apiUrl = 'admin/customers/prepaid-cards-series';

    /** @var string */
    public $serie;
    /** @var string */
    public $prefix;
    /** @var int */
    public $quantity;
    /** @var float */
    public $price;
    /** @var string */
    public $valid_till;
    /** @var int */
    public $pool_id;
    /** @var int */
    public $partner_id;
    /** @var int */
    public $location_id;
    /** @var int */
    public $speed_download;
    /** @var int */
    public $speed_upload;
    /** @var int */
    public $traffic_in_total;
    /** @var int */
    public $traffic_out_total;
    /** @var int */
    public $traffic_all_total;
    /** @var int */
    public $time_online;
    /** @var string */
    public $time_online_selector;
    /** @var string */
    public $expired_by;
    /** @var int */
    public $time_expired;
    /** @var string */
    public $time_expired_selector;

    public const STATUS_ACTIVE = 'active';
    public const STATUS_USED = 'used';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_EXPIRED = 'expired';
    public const STATUS_NEW = 'new';
    public const STATUS_ONLINE = 'online';

    public const MINUTES = 'minutes';
    public const HOURS = 'hours';
    public const DAYS = 'days';

    /**
     * @return string
     */
    public function getPrimaryKey(): string
    {
        return 'serie';
    }
}
