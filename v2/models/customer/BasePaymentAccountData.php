<?php

namespace splynx\v2\models\customer;

use splynx\v2\base\BaseActiveApi;
use yii\base\InvalidCallException;

/**
 * Class BasePaymentAccountData
 * @package splynx\models\customer
 */
class BasePaymentAccountData extends BaseActiveApi
{
    /** @var int */
    public $account_id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $field_1;

    /** @var string */
    public $field_2;

    /** @var string */
    public $field_3;

    /** @var string */
    public $field_4;

    /** @var string */
    public $field_5;

    /** @var string */
    public $field_6;

    /** @var string */
    public $field_7;

    /** @var string */
    public $field_8;

    /** @var string */
    public $field_9;

    /** @var string */
    public $field_10;

    public static $apiUrl = 'admin/customers/customer-payment-account-data';

    public function create($runValidation = true)
    {
        throw new InvalidCallException('Payment accounts does not has create call!');
    }

    public function delete()
    {
        throw new InvalidCallException('Payment accounts does not has delete call!');
    }

    public function update($runValidation = true)
    {
        throw new InvalidCallException('Payment accounts does not has update call!');
    }
}
