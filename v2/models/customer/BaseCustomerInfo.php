<?php

namespace splynx\v2\models\customer;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseCustomerInfo
 * @package splynx\v2\models\customer
 * @property string $primaryKey
 */
class BaseCustomerInfo extends BaseActiveApi
{
    /** @var int */
    public $customer_id;

    /** @var string */
    public $birthday;

    /** @var string */
    public $passport;

    /** @var string */
    public $company_id;

    /** @var string */
    public $vat_id;

    protected static $apiUrl = 'admin/customers/customer-info';

    /**
     * @inheritdoc
     */
    public function getPrimaryKey()
    {
        return 'customer_id';
    }
}
