<?php

namespace splynx\v2\models\customer;

use CURLFile;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidCallException;
use yii\web\BadRequestHttpException;

class BaseCustomerDocument extends BaseActiveApi
{
    /** @var int */
    public $id;

    /** @var string */
    public $added_by = self::ADDED_BY_API;

    /** @var int */
    public $added_by_id;

    /** @var int */
    public $customer_id;

    /** @var string */
    public $type;

    /** @var string */
    public $title;

    /** @var string */
    public $created_at;

    /** @var string */
    public $updated_at;

    /** @var string */
    public $description;

    /** @var int */
    public $visible_by_customer;

    /** @var mixed */
    public $file;

    /** @var string */
    public $filename_original;

    /** @var string */
    public $filename_uploaded;

    /** @var string */
    public $code;

    /** @var array<mixed> */
    public $additional_attributes = [];

    public static $apiUrl = 'admin/customers/customer-documents';

    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_API = 'api';
    public const ADDED_BY_CUSTOMER = 'customer';
    public const ADDED_BY_SYSTEM = 'system';

    public const TYPE_UPLOADED = 'uploaded';
    public const TYPE_GENERATED = 'generated';

    /**
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['customer_id', 'type', 'title'], 'required'],
            [['title'], 'string', 'max' => 64],
            [['customer_id', 'added_by_id', 'visible_by_customer'], 'integer'],
            [['visible_by_customer'], 'default', 'value' => 0],
            [['visible_by_customer'], 'in', 'range' => [1, 0]],
            [['added_by'], 'in', 'range' => self::getRolesList()],
            [['type'], 'in', 'range' => self::getTypesList()],
        ];
    }

    /**
     * return array of adding types
     * @return array<string>
     */
    public static function getRolesList()
    {
        return [
            self::ADDED_BY_ADMIN,
            self::ADDED_BY_API,
            self::ADDED_BY_CUSTOMER,
            self::ADDED_BY_SYSTEM,
        ];
    }

    /**
     * return array of types
     * @return array<string>
     */
    public static function getTypesList()
    {
        return [
            self::TYPE_UPLOADED,
            self::TYPE_GENERATED,
        ];
    }

    /**
     * upload file
     * @param string $filename
     * @return bool
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function upload($filename)
    {
        if (!class_exists('CURLFile')) {
            throw new InvalidCallException('Method CURLFile not found! It exists in PHP 5.5 and higher!');
        }
        if (!$this->save()) {
            throw new BadRequestHttpException('Error while saving customer document by API!');
        }
        $result = ApiHelper::getInstance()->upload(self::$apiUrl, $this->id, [
            'file' => new CURLFile($filename),
        ]);

        return (bool)$result['result'];
    }
}
