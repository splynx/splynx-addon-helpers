<?php

namespace splynx\v2\models\inventory;

use splynx\v2\base\BaseActiveApi;

/**
 * Class BaseItem
 * @package splynx\v2\models\inventory
 */
class BaseItem extends BaseActiveApi
{
    /**  @var int */
    public $id;

    /**  @var int */
    public $supplier_invoices_item_id;

    /**  @var string */
    public $barcode;

    /**  @var string */
    public $status;

    /**  @var self::MARK_NEW|self::MARK_USED|self::MARK_BROKEN */
    public $mark;

    /**  @var int */
    public $customer_id;

    /**  @var int */
    public $admin_id;

    /**  @var int */
    public $service_id;

    /**  @var int */
    public $invoice_id;

    /**  @var int */
    public $product_id;

    /**  @var string */
    public $notes;

    /**  @var string */
    public $filename_original;

    /**  @var string */
    public $filename;

    /**  @var int */
    public $stock_id;

    /**  @var string */
    public $file_link;

    /**  @var array<mixed> */
    public $additional_attributes = [];

    public const STATUS_STOCK = 'stock';
    public const STATUS_INTERNAL_USAGE = 'internal_usage';
    public const STATUS_SOLD = 'sold';
    public const STATUS_RENT = 'rent';
    public const STATUS_RETURNED = 'returned';
    public const STATUS_ASSIGNED = 'assigned';

    public const MARK_NEW = 'new';
    public const MARK_USED = 'used';
    public const MARK_BROKEN = 'broken';

    public static $apiUrl = 'admin/inventory/items';

    /**
     * @inheritDoc
     * @return array<mixed>
     */
    public function rules()
    {
        return [
            [['id', 'supplier_invoices_item_id', 'customer_id', 'admin_id', 'service_id', 'invoice_id', 'product_id', 'stock_id'], 'integer'],
            [['barcode', 'status', 'mark', 'notes', 'filename_original', 'filename', 'file_link'], 'string'],
            ['status', 'in', 'range' => $this->getStatusArray()],
            ['mark', 'in', 'range' => $this->getMarksArray()],
        ];
    }

    /**
     * @return array<string> of item statuses
     */
    public function getStatusArray()
    {
        return [
            static::STATUS_STOCK,
            static::STATUS_INTERNAL_USAGE,
            static::STATUS_SOLD,
            static::STATUS_RENT,
            static::STATUS_RETURNED,
            static::STATUS_ASSIGNED,
        ];
    }

    /**
     * @return array<string> of item marks
     */
    public function getMarksArray()
    {
        return [
            static::MARK_BROKEN,
            static::MARK_NEW,
            static::MARK_USED,
        ];
    }
}
