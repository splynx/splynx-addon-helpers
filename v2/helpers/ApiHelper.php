<?php

namespace splynx\v2\helpers;

use Exception;
use splynx\base\ApiResponseException;
use splynx\base\CheckResponseTrait;
use splynx\helpers\RedisHelper;
use SplynxApi;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * Class ApiHelper provide functionality for working with Splynx API
 *
 * Save auth data to session
 *
 * ```php
 * $api = ApiHelper::getInstance();
 * $_SESSION['auth_data'] = $api->getAuthData();
 * ```
 *
 * Use saved to session auth data
 *
 * ```
 * $api = ApiHelper::getInstance();
 * $api->setAuthData($_SESSION['auth_data']);
 * ```
 *
 * @package splynx\helpers
 *
 * @phpstan-type ResultResponse array{'result': bool, 'response': array<string, mixed>|string}
 */
class ApiHelper extends BaseObject
{
    use CheckResponseTrait;

    /** @var SplynxApi */
    private $_api;

    /** @var string API base url */
    public $api_domain;

    /** @var self|null Instance of current class */
    private static $_instance;

    /** @var bool */
    private $_disableLogout = false;

    /** @var bool */
    protected bool $_canLoginUsingDataFromCache = false;

    /** Name of header which contains amount of records */
    public const HEADER_X_TOTAL_COUNT = 'X-total-count';

    protected const SECOND_TO_RENEW_TOKEN = 5;

    /**
     * @param string $url API endpoint
     * @param bool $isSameServer
     * @return array{'result': bool, 'response': array<mixed>|string, 'response_code': int}
     * @throws InvalidConfigException
     */
    public static function checkApiSameServer($url = 'admin/api/check', $isSameServer = false)
    {
        $url .= '?checkAuth=true';
        if ($isSameServer) {
            $code = md5(microtime() . mt_rand());
            RedisHelper::setex('server_validating_codes_' . $code, 60, $code);
            $url .= '&code=' . $code;
        }

        $api = self::getInstance()->_api;
        $api->api_call_get($url);

        return [
            'result' => $api->result,
            'response' => $api->response,
            'response_code' => $api->response_code,
        ];
    }

    /**
     * @param string $url API endpoint
     * @return bool
     * @throws InvalidConfigException
     */
    public static function checkApi($url = 'admin/api/check')
    {
        $response = ApiHelper::checkApiSameServer($url);
        // Check of success codes, method can get response code from request that has been before it and code can be not 200
        return $response['result'] && $response['response_code'] >= 200 && $response['response_code'] < 300;
    }

    /**
     * @return boolean|null
     */
    public function getResult()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->result;
        }

        return null;
    }

    /**
     * @return array<mixed>|string|null
     */
    public function getResponse()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->response;
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getResponseCode(): ?int
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->response_code;
        }

        return null;
    }

    /**
     * @return array<string, string>|null
     */
    public function getResponseHeaders()
    {
        if (self::$_instance !== null && self::$_instance->_api !== null) {
            return self::$_instance->_api->response_headers;
        }

        return null;
    }

    /**
     * Get instance of current helper
     * @return self
     * @throws InvalidConfigException
     * @throws ApiResponseException
     * @throws Exception
     */
    public static function getInstance()
    {
        if (!isset(Yii::$app->api['version']) || Yii::$app->api['version'] !== SplynxApi::API_VERSION_2) {
            throw new InvalidConfigException('Invalid api version');
        }

        if (self::$_instance === null) {
            $config = [
                'api_domain' => Yii::$app->params['api_domain'],
            ];

            if (empty($config['api_domain'])) {
                throw new InvalidConfigException('Invalid API config');
            }

            $instance = new self($config);
            $instance->_api = new SplynxApi($instance->api_domain);
            $instance->_api->debug = false;
            $instance->_api->setVersion(SplynxApi::API_VERSION_2);

            $authType = Yii::$app->api['auth_type'] ?? SplynxApi::AUTH_TYPE_API_KEY;

            if (isset(Yii::$app->api['disable_logout'])) {
                $instance->_disableLogout = Yii::$app->api['disable_logout'];
            }

            $instance->setCanLoginUsingDataFromCache(isset(Yii::$app->api['auth_data_cache_key']));

            if ($instance->canLoginUsingDataFromCache() && $instance->authDataFromCacheValid()) {
                $instance->setAuthData($instance->getAuthDataFromCache());
            } elseif ($instance->login($authType, $instance->getAuthCredentialsByType($authType))) {
                if ($instance->canLoginUsingDataFromCache()) {
                    $instance->setAuthDataToCache($instance->getAuthData());
                }
            } else {
                $instance->checkResponseForError($instance->_api->response);
                // If the error is not received from the system
                throw new Exception('Error: Please check your system API settings');
            }

            self::$_instance = $instance;
        }

        return self::$_instance;
    }

    /**
     * Make GET request. Get records or record.
     * @param string $url API endpoint
     * @param null|int|string $id Record id
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function get($url, $id = null)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_get($url, $id);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make POST request. Create record.
     * @param string $url API endpoint
     * @param array<mixed> $params Payload
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function post($url, $params = [])
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_post($url, $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make PUT request. Update record by id.
     * @param string $url API endpoint
     * @param int|string|null $id Record id
     * @param array<mixed> $params Payload
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function put($url, $id, $params = [])
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_put($url, $id, $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make DELETE request
     * @param string $url API endpoint
     * @param int|string|null $id Record id
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function delete($url, $id)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_delete($url, $id);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Search records by condition
     * @param string $url API endpoint
     * @param array<mixed> $params Search condition
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function search($url, $params)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_get($url . '?' . http_build_query($params));

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make OPTIONS request
     *
     * Get model properties.
     * Response example:
     *
     * ```php
     * [
     *      'attributes' => [
     *          [
     *              'name' => 'id',
     *              'title' => 'Id',
     *              'type' => 'integer',
     *              'required' => false,
     *          ],
     *      ],
     *      'additional_attributes' => [
     *          [
     *              'module' => 'locations',
     *              'name' => 'loc_ip',
     *              'title' => 'Location IP',
     *              'type' => 'relation',
     *              'required' => false,
     *              // ...
     *          ]
     *      ],
     * ]
     * ```
     * @param string $url API endpoint
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function options($url)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_options($url);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Get amount of records
     *
     * Get amount of active customers:
     *
     * ```php
     * $result = $api->count('admin/customers/customer', [
     *      'main_attributes' => [
     *          'status' => 'active',
     *      ]
     * ]);
     * echo "\nAmount of active customers: " . var_export($result['response'], 1);
     * ```
     *
     * @param string $url API endpoint
     * @param array<string, mixed> $condition Search condition
     * @return array{'result': bool, 'response': int|string|null}
     * @throws InvalidConfigException
     */
    public static function count($url, $condition = [])
    {
        $api = self::getInstance()->_api;
        $condition = empty($condition) ? '' : '?' . http_build_query($condition);
        $result = $api->api_call_head($url . $condition);

        return [
            'result' => $result,
            'response' => isset($api->response_headers[self::HEADER_X_TOTAL_COUNT]) ? $api->response_headers[self::HEADER_X_TOTAL_COUNT] : null,
        ];
    }

    /**
     * Use this to upload file by API.
     *
     * @param string $url
     * @param int|string $id
     * @param array<mixed> $params Array where key - property and value - CURLFile instance of file
     * @return ResultResponse
     * @throws InvalidConfigException
     */
    public static function upload($url, $id, $params)
    {
        $api = self::getInstance()->_api;

        $result = $api->api_call_post_file($url . '/' . $id . '--upload', $params);

        return [
            'result' => $result,
            'response' => $api->response,
        ];
    }

    /**
     * Make login. Generate JWT tokens pair
     * @param string $authType Possible values: `admin`, `customer`, `api_key`
     * @param array<string, mixed> $data Auth data
     *
     * For login as admin
     *
     * ```php
     * [
     *      'login' => '',
     *      'password' => '',
     *      // 'code' => '', // If 2Fa enabled
     * ]
     * ```
     *
     * For login as customer
     *
     * ```php
     * [
     *      'login' => '',
     *      'password' => '',
     * ]
     * ```
     *
     * For login with using api key
     *
     * ```php
     * [
     *      'key' => '',
     *      'secret' => '',
     * ]
     * ```
     * @return bool
     * @throws Exception
     */
    public function login($authType, $data)
    {
        $result = array_merge([
            'auth_type' => $authType,
        ], $data);
        return $this->_api->login($result);
    }

    /**
     * Logout. Delete JWT token pair
     * @return bool
     */
    public function logout(): bool
    {
        if ($this->_api->logout()) {
            static::clearAuthDataCache();
            return true;
        }

        return false;
    }

    /**
     * Get auth data
     * @return array{
     *     'access_token': string|null,
     *     'access_token_expiration': int|null,
     *     'refresh_token': string|null,
     *     'refresh_token_expiration': int|null,
     *     'permissions': array<string>|null
     * }
     */
    public function getAuthData()
    {
        return $this->_api->getAuthData();
    }

    /**
     * Set auth data. Set your auth data stored in external storage instead of login.
     * @param array{
     *      'access_token': string|null,
     *      'access_token_expiration': int|null,
     *      'refresh_token': string|null,
     *      'refresh_token_expiration': int|null,
     *      'permissions': array<string>|null
     *  } $data
     * ```
     * [
     *      'access_token' => '',
     *      'access_token_expiration' => '',
     *      'refresh_token' => '',
     *      'refresh_token_expiration' => '',
     *      'permissions' => [],
     * ]
     * ```
     *
     * @return void
     */
    public function setAuthData(array $data): void
    {
        $this->_api->setAuthData($data);
    }

    /**
     * Method for logout before die
     *
     * If it is API v2 need logout before application die.
     *
     * @param bool $force
     * @return void
     */
    public static function logoutBeforeDie(bool $force = false): void
    {
        try {
            if (empty(self::$_instance)) {
                return;
            }

            if (!$force && self::$_instance->canLoginUsingDataFromCache()) {
                return;
            }

            if (self::$_instance->_api->getVersion() !== SplynxApi::API_VERSION_2) {
                return;
            }

            if (self::$_instance->_disableLogout) {
                return;
            }

            $authData = self::$_instance->_api->getAuthData();
            if (!empty($authData['refresh_token'])) {
                self::$_instance->_api->logout();
            }

            self::$_instance = null;
        } catch (Exception $exception) {
            Yii::error('Error while logout Error:' . $exception->getMessage() . ' file: ' . $exception->getFile() . ':' . $exception->getLine());
        }
    }

    /**
     * @param string $authType
     * @return array<string, mixed>
     */
    protected function getAuthCredentialsByType(string $authType): array
    {
        switch ($authType) {
            case SplynxApi::AUTH_TYPE_API_KEY:
                return [
                    'key' => Yii::$app->params['api_key'] ?? null,
                    'secret' => Yii::$app->params['api_secret'] ?? null,
                ];
            // TODO think about auth with login and pass may be use Identity interface
            /*case SplynxApi::AUTH_TYPE_ADMIN:
            case SplynxApi::AUTH_TYPE_CUSTOMER:
                $authData = [
                    'login' => isset(Yii::$app->params['api_login']) ? Yii::$app->params['api_login'] : null,
                    'password' => isset(Yii::$app->params['api_password']) ? Yii::$app->params['api_password'] : null,
                ];
                break;
            case SplynxApi::AUTH_TYPE_SESSION:
                $authData = [
                    'session_id' => isset(Yii::$app->params['api_session_id']) ? Yii::$app->params['api_session_id'] : null,
                ];
                break;*/
        }

        return [];
    }

    /**
     * @return bool
     */
    protected function canLoginUsingDataFromCache(): bool
    {
        return $this->_canLoginUsingDataFromCache;
    }

    /**
     * @param bool $canLoginUsingDataFromCache
     * @return void
     */
    protected function setCanLoginUsingDataFromCache(bool $canLoginUsingDataFromCache): void
    {
        $this->_canLoginUsingDataFromCache = $canLoginUsingDataFromCache;
    }

    /**
     * @return string
     */
    protected static function getAuthDataCacheKey(): string
    {
        return Yii::$app->api['auth_data_cache_key'] ?? '_auth_data';
    }

    /**
     * @return array{
     *      'access_token': string|null,
     *      'access_token_expiration': int|null,
     *      'refresh_token': string|null,
     *      'refresh_token_expiration': int|null,
     *      'permissions': array<string>|null
     *  }
     */
    protected function getAuthDataFromCache(): array
    {
        if (($data = RedisHelper::get(static::getAuthDataCacheKey())) === null) {
            return [
                'access_token' => null,
                'access_token_expiration' => null,
                'refresh_token' => null,
                'refresh_token_expiration' => null,
                'permissions' => null,
            ];
        }
        return unserialize($data, ['allowed_classes' => false]);
    }

    /**
     * @param array{
     *      'access_token': string|null,
     *      'access_token_expiration': int|null,
     *      'refresh_token': string|null,
     *      'refresh_token_expiration': int|null,
     *      'permissions': array<string>|null
     *  } $data
     */
    protected function setAuthDataToCache(array $data): void
    {
        RedisHelper::setex(
            static::getAuthDataCacheKey(),
            ($data['access_token_expiration'] ?? time()) - time() - static::SECOND_TO_RENEW_TOKEN,
            serialize($data)
        );
    }

    /**
     * @return bool
     */
    protected function authDataFromCacheValid(): bool
    {
        return (($this->getAuthDataFromCache()['access_token_expiration'] ?? 0) - static::SECOND_TO_RENEW_TOKEN) >= time();
    }

    /**
     * @return void
     */
    public static function clearAuthDataCache(): void
    {
        RedisHelper::delete(static::getAuthDataCacheKey());
    }
}
