<?php

namespace splynx\v2\helpers;

use Yii;
use yii\web\Application;

enum PaymentStatementSourceEnum: string
{
    case CUSTOMER = 'customer';
    case ADMIN = 'admin';
    case SYSTEM = 'system';
    case DIRECT = 'direct';

    /** @var string */
    protected const SESS_CUSTOMER_ID_KEY = 'splynx_customer_id';
    /** @var string */
    protected const SESS_ADMIN_ID_KEY = 'splynx_admin_id';

    /**
     * Determines the source of the payment statement based on the type of request and session data.
     *
     * @return self The source of the payment statement, which could be SYSTEM, CUSTOMER, ADMIN, or DIRECT.
     */
    public static function getPaymentStatementSource(): self
    {
        if (self::isConsoleRequest()) {
            return self::SYSTEM;
        }

        /** @var Application $app */
        $app = Yii::$app;
        $session = $app->getSession();
        $customerId = $session->get(self::SESS_CUSTOMER_ID_KEY);
        $adminId = $session->get(self::SESS_ADMIN_ID_KEY);

        if (self::isCustomer($customerId, $adminId)) {
            return self::CUSTOMER;
        }

        if (self::isAdmin($adminId)) {
            return self::ADMIN;
        }

        return self::DIRECT;
    }

    /**
     * @return bool
     */
    private static function isConsoleRequest(): bool
    {
        return Yii::$app->getRequest()->isConsoleRequest;
    }

    /**
     * @param mixed $customerId
     * @param mixed $adminId
     * @return bool
     */
    private static function isCustomer(mixed $customerId, mixed $adminId): bool
    {
        return !empty($customerId) && empty($adminId);
    }

    /**
     * @param mixed $adminId
     * @return bool
     */
    private static function isAdmin($adminId): bool
    {
        return !empty($adminId);
    }
}
