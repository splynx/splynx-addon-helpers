<?php

namespace splynx\widgets;

class Alert extends \yii\bootstrap\Widget
{
    /** @var array{'error': string,'danger': string,'success': string,'info': string,'warning': string}  */
    public $alertTypes = [
        'error' => 'alert-danger',
        'danger' => 'alert-danger',
        'success' => 'alert-success',
        'info' => 'alert-info',
        'warning' => 'alert-warning',
    ];

    /** @var array<string, string>|false */
    public $closeButton = [];

    /**
     * @inheritDoc
     * @return void
     */
    public function init()
    {
        parent::init();

        /** @var \yii\web\Application $app */
        $app = \Yii::$app;

        $session = $app->getSession();
        $flashes = $session->getAllFlashes();
        $appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        foreach ($flashes as $type => $message) {
            if (isset($this->alertTypes[$type])) {
                /* initialize css class for each alert box */
                $this->options['class'] = $this->alertTypes[$type] . $appendCss;

                /* assign unique id to each alert box */
                $this->options['id'] = $this->getId() . '-' . $type;

                echo \yii\bootstrap\Alert::widget([
                    'body' => $message,
                    'closeButton' => $this->closeButton,
                    'options' => $this->options,
                ]);

                $session->removeFlash($type);
            }
        }
    }
}
