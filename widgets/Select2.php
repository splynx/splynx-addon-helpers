<?php

namespace splynx\widgets;

use splynx\assets\Select2Asset;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Class Select2
 * @package splynx\widgets
 *
 * Widget for using Select2 jQuery plugin. Usage example:
 *
 * Select2::widget([
 *      'name' => "nameSelectTag",
 *      'id' => 'select2Id',
 *      'value' => 'value',
 *      'placeholder' => 'Select ...',
 *      'selectAttributes' => [
 *          'data-some-id' => 'some_value',
 *      ],
 *      'selectOptions' => [
 *          'value' => 'Title',
 *          'value1' => 'Title Next',
 *      ],
 *      'pluginOptions' => [
 *          'allowClear' => true,
 *      ],
 *      'callback' => 'function callback() {
 *          console.log("123");
 *      }'
 * ])
 */
class Select2 extends InputWidget
{
    public const DEFAULT_INPUT_CLASSES = 'form-control input-sm';

    /**
     * Template for HTML <select> tag.
     *
     * @var string
     */
    public $templateSelect = '<select id=":id" name=":name" class=":class" :attributes>:options</select>';

    /**
     * Template for HTML <option> tag in HTML <select> tag.
     *
     * @var string
     */
    public $templateOptions = '<option value=":value" :attributes>:title</option>';

    /**
     * Array for rendering renderOptions();
     * Using for rendering select options.
     * This must be array where key is option value and array value is option title. Example:
     * [
     *  'value' => 'Title',
     *  'value1' => 'Title Next',
     * ]
     * @var array<string, mixed>
     */
    public $selectOptions = [];

    /**
     * Additional class names for HTML <select> tag
     * @var string Example: 'form-control input-sm'
     */
    public $class = '';

    /**
     * Id for Html <select> tag, needs for init Select2 Plugin by this id, will be used in function initSelect2()
     * @var string
     */
    public $id = 'select2';

    /**
     * Value for HTML <select> tag. Needs if only use ajax in Select2 jQuery Plugin/
     * Will be created HTML <option> tag and selected this option in function initSelect2();
     * @var string
     */
    public $value;

    /**
     * Placeholder, needs in allowClear option allowed in jQuery Select2 Plugin, fixes js error.
     * Also as placeholder for HTML <select> tag.
     * @var string
     */
    public $placeholder = 'Select ...';

    /**
     * Array of options for jQuery select2 Plugin,
     * Can be used any api options from @url https://select2.org/configuration/options-api
     *
     * @var array<string, mixed>
     */
    public $pluginOptions = [];

    /**
     * JavaScript named function, will be used in function initSelect2(). Example:
     *
     *   'callback' => 'function callback() {
     *      console.log("123");
     *   }'
     *
     * @var bool|string
     */
    public $callback = false;

    /**
     * Array of attributes for html <select> tag. Example:
     * [
     *  'data-select-some' => 'Some value'
     * ]
     *
     * @var array<string, string>
     */
    public $selectAttributes = [];

    /**
     * Array of attributes for html <option> tag. Example:
     * [
     *  'value' => 'Some title value'
     * ]
     *
     * @var array<string, string>
     */
    public $optionAttributes = [];

    /**
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        Select2Asset::register($this->getView());
        parent::init();
    }

    /**
     * @return string|void
     */
    public function run()
    {
        $options = json_encode([
            'id' => $this->id,
            'value' => $this->value,
            'placeholder' => $this->placeholder,
            'pluginOptions' => $this->pluginOptions,
            'callback' => $this->callback,
        ]);
        echo $this->renderSelect();
        $this->getView()->registerJs('initSelect2(' . $options . ')');
    }

    public function renderSelect(): string
    {
        $search = [
            ':id',
            ':name',
            ':class',
            ':placeholder',
            ':attributes',
            ':options',
        ];
        $replace = [
            $this->id,
            $this->name,
            static::DEFAULT_INPUT_CLASSES . $this->class,
            $this->placeholder,
            Html::renderTagAttributes($this->selectAttributes),
            $this->renderOptions(),
        ];
        return str_replace($search, $replace, $this->templateSelect);
    }

    /**
     * @return string
     */
    public function renderOptions(): string
    {
        $options = '';

        foreach ($this->selectOptions as $value => $option) {
            if (is_array($option)) {
                $options .= $this->renderOptGroup($value, $option);
            } else {
                $options .= $this->renderOption($option, $value);
            }
        }

        return $options;
    }

    /**
     * @param string $label
     * @param array<string, mixed> $options
     * @return string
     */
    public function renderOptGroup($label, array $options): string
    {
        $optgroup = '<optgroup label="' . $label . '">';

        foreach ($options as $value => $title) {
            if (is_array($title)) {
                $optgroup .= $this->renderOptGroup($value, $title);
            } else {
                $optgroup .= $this->renderOption($title, $value);
            }
        }

        $optgroup .= '</optgroup>';

        return $optgroup;
    }

    /**
     * @param string $title
     * @param string $value
     * @return string
     */
    public function renderOption($title, $value)
    {
        return str_replace(
            [
                ':title',
                ':value',
                ':attributes',
            ],
            [
                $title,
                $value,
                Html::renderTagAttributes($this->optionAttributes),
            ],
            $this->templateOptions
        );
    }
}
