<?php

namespace splynx\base;

use Exception;
use splynx\helpers\ConfigHelper;
use splynx\helpers\IPHelper;
use splynx\models\console_api\config\integration\ConsoleModuleConfig;
use splynx\v2\helpers\ApiHelper;
use Yii;
use Yii\base\ErrorException;
use yii\base\InvalidConfigException;
use yii\base\InvalidRouteException;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * This class could help for create install controllers in add-ons.
 * For use need extend this class in your InstallController class and redefine needs methods.
 * You need redefine base methods (See abstract methods) in your install controller class.
 * This class required Splynx with version >= 2.0.
 * @package splynx\base
 *
 * @phpstan-type EventType array{'model': string, 'actions': array<string>}
 * @phpstan-type HookType array{'title': string, 'enabled'?: string, 'type'?: string, 'path'?: string, 'url'?: string,'content_type'?: string, 'events'?: array<EventType>}
 *
 */
abstract class BaseInstallController extends Controller
{
    /**
     * Minimum Splynx version needs for this add-on. If empty then version will not check.
     * @var string
     */
    public static $minimumSplynxVersion = '4.3';

    /**
     * Full path to Splynx version file
     * @deprecated moved to ConfigHelper::getSplynxVersionPath
     */
    public const VERSION_FILE_PATH = '/var/www/splynx/version';

    /** Name of add-on encryption key field in params file */
    public const ENCRYPTION_KEY_FIELD = 'add_on_encryption_key';

    /** Name of cookie validation key field in params file */
    public const COOKIE_VALIDATION_KEY_FIELD = 'cookieValidationKey';

    /** Name of api domain field in params file */
    public const API_DOMAIN_FIELD = 'api_domain';

    /** Name of apikey key field in params file */
    public const API_KEY_FIELD = 'api_key';

    /** Name of apikey secret field in params file */
    public const API_SECRET_FIELD = 'api_secret';

    /** Default rule for apikey permission rule */
    public const DEFAULT_PERMISSION_RULE = 'allow';

    public const INSTALL_PROCESS = 'install';
    public const UPDATE_PROCESS = 'update';

    // Handlers
    public const FINANCE_BANK_STATEMENS_HANDLERS = 'bank-statements';
    public const FINANCE_CHARGE_HANDLERS = 'charge';
    public const FINANCE_CHARGE_BALANCES_HANDLERS = 'charge-balances';
    public const FINANCE_EXPORT_HANDLERS = 'export';

    public const MODULE_STATUS_ENABLED = 1;
    public const MODULE_STATUS_DISABLED = 0;

    public const CALLBACK_AUTHENTICATION_ID_FIELD = 'callback_authentication_id';
    public const CALLBACK_AUTHENTICATION_SECRET_FIELD = 'callback_authentication_secret';

    /**
     * @var string $env if you want to install dev environment it should be `dev`. Default `prod'.
     * Usage: ./yii install -env=dev
     */
    public $env;

    /**
     * By default module is disabled, if you want to enable module after install set $module_status = self::MODULE_STATUS_ENABLED
     * @var string|int $module_status '0' '1' default '0'
     */
    public $module_status;

    /**
     * Show errors in the console during addon installation
     * @var bool
     */
    public bool $outputErrorsToConsole = true;

    /**
     * Property to check the type of process (install or update process)
     * @var string
     */
    protected $process;

    /**
     * Full install add-on to Splynx.
     * Install add-on in step by step:
     * - Check Splynx version.
     * - Create Splynx apikey.
     * - Set apikey permissions.
     * - Create Splynx module.
     * - Create entry points.
     * - Create additional fields.
     * - Create hooks with events.
     * - Create and prepare params.php file with default settings and extended params.example.php file.
     * - Chmod files and directories.
     * - Restart server process (nginx or apache).
     * - Run migration if directory migrations is exist.
     *
     * @return void
     * @throws ErrorException
     * @throws Yii\base\Exception
     * @throws InvalidConfigException
     * @throws InvalidRouteException
     * @throws Exception
     */
    public function actionIndex()
    {
        echo "Start install..\n";

        $apiKeyId = $this->createApiKey();
        if (!$apiKeyId) {
            exit('Create API key failed!' . "\n");
        }

        // Add it here because maybe we will need to add permissions for the module
        if (!$this->createModules()) {
            exit('Create modules has failed!' . "\n");
        }

        $this->setApiPermissions($apiKeyId);

        $apiData = static::getApiKeyAndSecretById($apiKeyId);
        if ($apiData === false) {
            exit("Get API key anf secret failed!\n");
        }

        if (!$this->createModule()) {
            exit('Error adding ' . $this->getModuleName());
        }

        if (!$this->createPaymentAccounts()) {
            exit('Create payment accounts has failed!' . "\n");
        }

        if (!$this->createEntryPoints()) {
            exit('Add entry points failed!' . "\n");
        }

        if (!$this->createAdditionalFields()) {
            exit('Create additional fields failed!' . "\n");
        }

        if (!$this->createHooks()) {
            exit('Create hooks failed!' . "\n");
        }

        if (!$this->removeFinancialHandlers()) {
            exit('Remove financial handlers has failed!' . "\n");
        }

        if (!$this->copyFinancialHandlers()) {
            exit('Copying financial handlers has failed!' . "\n");
        }

        $this->prepareParamsFile($apiData['key'], $apiData['secret']);

        if (!$this->createCallbackAuthentication()) {
            exit('Create callback authentication failed!' . PHP_EOL);
        }

        ConfigHelper::reloadConfig();

        ApiHelper::clearAuthDataCache();

        if (is_dir(Yii::$app->getBasePath() . '/migrations')) {
            Yii::$app->runAction('migrate/up', [
                'interactive' => '0',
            ]);
        }

        // Set permissions
        static::chmodFiles($this->getFilesPermission());

        // Reload nginx
        if (file_exists('/etc/init.d/nginx')) {
            exec('/etc/init.d/nginx restart  > /dev/null 2>&1 3>/dev/null');
        }

        // Reload apache
        if (file_exists('/etc/init.d/apache2')) {
            exec('/etc/init.d/apache2 restart  > /dev/null 2>&1 3>/dev/null');
        }

        echo "Done!\n";
    }

    /**
     * Set options
     *
     * @param string $actionID
     * @return array<string>
     */
    public function options($actionID)
    {
        return ['env'];
    }

    /**
     * Set alias for options
     *
     * @return array<string, string>
     */
    public function optionAliases()
    {
        return ['env' => 'env', 'e' => 'env'];
    }

    /**
     * If environment is `dev` - generate `dev.php` file in `config` folder with necessary params.
     * If environment is `prod` - delete `dev.php` file if exists.
     * Exit from action if something went wrong.
     *
     * @param string $env if `dev` - dev env., in other cases - `prod`.
     * @return bool `true` - if everything is OK.
     * @deprecated See https://jira-splynx.atlassian.net/browse/SAHELPER-281
     */
    protected function createEnvironment($env)
    {
        $filePath = Yii::$app->getBasePath() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'dev.php';

        if ($env != 'dev') {
            if (file_exists($filePath) && unlink($filePath) === false) {
                $this->stderr('Error was occurred trying to delete ' . $filePath . ' file.' . PHP_EOL);
            }
            return true;
        }

        $file = fopen($filePath, 'w');
        if ($file === false) {
            $this->stderr('Error was occurred trying to create ' . $filePath . ' for dev environment.' . PHP_EOL);
            exit(ExitCode::OSFILE);
        }

        $content = '<?php' . PHP_EOL;
        $content .= "defined('YII_DEBUG') || define('YII_DEBUG', true);" . PHP_EOL;
        $content .= "defined('YII_ENV') || define('YII_ENV', 'dev');" . PHP_EOL;
        if (fwrite($file, $content) === false) {
            $this->stderr(
                'Error was occurred trying to write content in  ' . $filePath . ' for dev environment.' . PHP_EOL
            );
            exit(ExitCode::OSFILE);
        }

        fclose($file);

        return true;
    }

    /**
     * Generate and save encryption key to current add-on. Return null if error.
     * @return null|string - Encryption key.
     * @throws Yii\base\Exception
     */
    public function actionGenerateKey()
    {
        $paramsFile = static::getParamsFilePath();
        if (!file_exists($paramsFile)) {
            echo 'params.php is not exist! Please run `./yii install`' . "\n";
            return null;
        }

        $params = require($paramsFile);
        if (isset($params[static::ENCRYPTION_KEY_FIELD]) && !empty($params[static::ENCRYPTION_KEY_FIELD])) {
            echo 'Encryption key already set!' . "\n";
            return null;
        }

        $encryptionKey = static::generateEncryptionKey();
        $comment = 'Required param for encrypting add-on settings with type \'encrypted\', PLEASE DON\'T DELETE';

        $insertResult = static::insertParamToFile($paramsFile, static::ENCRYPTION_KEY_FIELD, $encryptionKey, $comment);
        if ($insertResult === false) {
            echo 'Error writing encryption key to params.php!' . "\n";
            return null;
        }

        echo 'Encryption key was generated!' . "\n";

        // Reload add-on configs
        exec(static::getSplynxDir() . 'system/setup/setup reload-configs --moduleName=' . $this->getModuleName());

        return $encryptionKey;
    }

    /**
     * Get add-on title.
     * @return string
     */
    abstract public function getAddOnTitle();

    /**
     * Get name of add-on module
     * @return string
     */
    abstract public function getModuleName();

    /**
     * Get api permissions for add-on.
     * Example return data:
     *
     *```php
     *[
     *  [
     *      'controller' => 'api\admin\customers\Customer',
     *      'actions' => ['index', 'add', 'delete'],
     *      'rule' => 'deny'
     *  ]
     *]
     * ```
     * Where 'api\admin\customers\Customer' is controller name and 'index' is action of controller.
     * @return array<array{'controller': string , 'actions'?: array<string>, 'rule'?: 'allow'|'deny'}>
     */
    abstract public function getApiPermissions();

    /**
     * Get add-on entry points.
     * @return array<array<string, string>>
     */
    public function getEntryPoints()
    {
        return [];
    }

    /**
     * Get files permissions.
     * Use this method for set custom permissions for your files or directories.
     * @return array<string, string>
     */
    public function getFilesPermission()
    {
        return [
            'runtime' => '0777',
            'runtime' . DIRECTORY_SEPARATOR . 'logs' => '0777',
            'web' . DIRECTORY_SEPARATOR . 'assets' => '0777',
            'yii' => '0755',
            'config/config.ini.php' => '0666',
        ];
    }

    /**
     * Get list of additional fields. Use this method for add your additional fields.
     * Example method return:
     * ```
     * [
     *     [
     *         'main_module' => 'customers',
     *         'module' => 'name_of_your_add_on',
     *         'name' => 'additional_filed_name',
     *         'required' => 'false',
     *         'addon_uri' => '/url_to_process_this_afs',
     *     ]
     * ]
     * ```
     * @return array<array<string, string>> List of additional fields
     */
    public function getAdditionalFields()
    {
        return [];
    }

    /**
     * Get list of hooks. Use this method for add your hooks to splynx.
     * For example the return array:
     * ```
     * [
     *     [
     *         'title' => 'My hook',
     *         'enabled' => 'true',
     *         'type' => 'cli', // `cli` or `queue`
     *         'path' => '/var/www/test.php',
     *         'url' => '/test/url/',
     *         'content_type'=> 'application/json', // 'application/x-www-form-urlencoded'
     *         'events' => [
     *             [
     *                 'model' => 'models\common\customers\Customers',
     *                 'actions' => ['edit']
     *             ],
     *             [
     *                 'model' => 'models\admin\administration\Locations',
     *                 'actions' => ['create', 'delete']
     *             ],
     *         ]
     *     ]
     * ]
     * ```
     * @return array<HookType>|array{} List of hooks
     */
    public function getHooks()
    {
        return [];
    }

    /**
     * Get list of callback authentication
     * For example the return array:
     * ```
     *  'd04b98f48e8f8bcc15c6ae5ac050801cd6dcfd428fb5f9e65c4e16e7807340fa'
     * ```
     * @return string
     */
    public function getCallbackAuthenticationSecret(): string
    {
        return '';
    }

    /** @var string $_baseIp - Used for save getBaseIp method result */
    protected static $_baseIp;

    /**
     * Get IP of current server.
     * @return string
     */
    protected static function getBaseIp()
    {
        if (empty(static::$_baseIp)) {
            static::$_baseIp = IPHelper::getFirstIP();
        }

        return static::$_baseIp;
    }

    /** @var string|null */
    protected static $_domain;

    /**
     * @return string|null
     */
    protected static function getDomain()
    {
        if (empty(static::$_domain)) {
            static::$_domain = IPHelper::getDomain();
        }

        return static::$_domain;
    }

    /**
     * Get path to param.php file.
     * @return string
     */
    protected static function getParamsFilePath()
    {
        return static::getBaseDir() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.php';
    }

    /**
     * Get path to params.example.php file.
     * @return string
     */
    protected static function getBaseParamsFilePath()
    {
        return static::getBaseDir() . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'params.example.php';
    }

    /**
     * Create encryption points.
     * @return bool
     */
    protected function createEntryPoints()
    {
        $points = $this->getEntryPoints();
        if (empty($points)) {
            return true;
        }

        $splynxDir = static::getSplynxDir();
        $showErrors = $this->outputErrorsToConsole ? ' --showErrors=1' : '';

        foreach ($points as $point) {
            $command = "{$splynxDir}system/script/addon add-or-get-entry-point";
            $properties = ArrayHelper::merge($this->getBaseEntryPointProperties(), $point);

            foreach ($properties as $key => $value) {
                if ($key == 'url' || $key == 'code') {
                    /**@see  https://jira.splynx.com/browse/SAHELPER-25 */
                    $testEncodedValue = urlencode(urldecode($value));
                    if ($testEncodedValue !== $value) {
                        // String is NOT urlencoded and need to urlencode
                        $value = urlencode($value);
                    }
                }
                $command .= " --$key=\"$value\"";
            }
            $command .= $showErrors;

            $result = exec($command);
            if (!$result) {
                return false;
            }
        }

        return true;
    }

    /**
     * Properties used in all entry points.
     *
     * @return array<string, string>
     */
    protected function getBaseEntryPointProperties()
    {
        return [
            'module' => $this->getModuleName(),
        ];
    }

    /**
     * Create add-on module by name.
     * @return bool
     */
    protected function createModule()
    {
        $splynxDir = static::getSplynxDir();
        $moduleName = $this->getModuleName();
        $title = $this->getAddOnTitle();
        $baseDir = static::getBaseDir();

        $moduleStatus = $this->module_status ? "--status=\"{$this->module_status}\"" : '';
        $showErrors = $this->outputErrorsToConsole ? ' --showErrors=1' : '';

        $result = exec(
            "{$splynxDir}system/script/addon add-or-get-module --module=\"$moduleName\" --title=\"$title\" --path=\"$baseDir\" {$moduleStatus} {$showErrors}"
        );

        if (trim($result) != $moduleName) {
            return false;
        }

        return true;
    }

    /**
     * Add current IPs to API key white list.
     *
     * @param int $apiKeyId
     * @return void
     */
    public function actionAddIpsToWhiteList($apiKeyId)
    {
        // Add IPs to white list
        exec(
            static::getSplynxDir() . 'system/script/addon api-key-white-list --id=' . $apiKeyId . ' --list="' . implode(
                ',',
                IPHelper::getIPs()
            ) . '"'
        );
    }

    /**
     * Get api key data by ID. Get false when error.
     * @param string|int $id - Api key ID.
     * @return array{'key': string, 'secret': string}|false
     */
    protected static function getApiKeyAndSecretById($id)
    {
        $result = exec(static::getSplynxDir() . 'system/script/addon get-api-key-and-secret --id=' . $id);
        if (!$result) {
            return false;
        }

        list($key, $secret) = explode(',', $result);

        return [
            'key' => $key,
            'secret' => $secret,
        ];
    }

    /**
     * Set api key permissions by id.
     *
     * @param string|int $apiKeyId
     * @return void
     */
    protected function setApiPermissions($apiKeyId)
    {
        $permissions = $this->getApiPermissions();
        if (!empty($permissions)) {
            $splynxDir = static::getSplynxDir();

            foreach ($permissions as $permission) {
                $controller = $permission['controller'];
                $actions = isset($permission['actions']) ? $permission['actions'] : ['index'];
                $rule = isset($permission['rule']) ? $permission['rule'] : static::DEFAULT_PERMISSION_RULE;

                if (!is_array($actions)) {
                    $actions = [$actions];
                }

                foreach ($actions as $action) {
                    exec(
                        "{$splynxDir}system/script/addon set-api-key-permission --id=\"$apiKeyId\" --controller=\"$controller\" --action=\"$action\" --rule=\"$rule\""
                    );
                }
            }
        }
    }

    /**
     * Create api key in Splynx for current add-on.
     * Add-on title gets from static::getAddOnTitle() method.
     * @return int - Api key ID.
     */
    protected function createApiKey()
    {
        $command = static::getSplynxDir()
            . 'system/script/addon add-or-get-api-key --title="'
            . $this->getAddOnTitle() . '"';
        return (int)exec($command);
    }

    /**
     * Get Splynx global path
     * @return string - Global path to Splynx
     */
    protected static function getSplynxDir()
    {
        return '/var/www/splynx/';
    }

    /**
     * Get add-on base dir
     * @return string
     */
    protected static function getBaseDir()
    {
        return Yii::$app->getBasePath();
    }

    /**
     * Check Splynx version
     * @return bool
     */
    protected static function checkSplynxVersion(): bool
    {
        if (empty(static::$minimumSplynxVersion)) {
            return true;
        }

        if ($splynxVersion = ConfigHelper::getSpynxVersion()) {
            if (version_compare($splynxVersion, static::$minimumSplynxVersion) === -1) {
                return false;
            }
        } else {
            echo 'Please ensure you have the latest version of Splynx. Version ' . static::$minimumSplynxVersion . " is required for the addon to function correctly.\n";
        }

        return true;
    }

    /**
     * Generate random encryption key.
     * @return string
     * @throws Yii\base\Exception
     */
    protected static function generateEncryptionKey()
    {
        return Yii::$app->getSecurity()->generateRandomString(rand(20, 30));
    }

    /**
     * Set permissions to files or directories.
     *
     * @param array<string> $paths - Key of array item is relative path to file or directory. Value of array item is permission.
     * @return void
     */
    protected static function chmodFiles($paths)
    {
        if (empty($paths)) {
            return;
        }

        $baseDir = static::getBaseDir();

        foreach ($paths as $path => $permission) {
            $absolutePath = $baseDir . DIRECTORY_SEPARATOR . $path;

            echo "chmod('$absolutePath', $permission)...";

            if (is_dir($absolutePath) || is_file($absolutePath)) {
                try {
                    if (chmod($absolutePath, (int)octdec($permission))) {
                        echo "done.\n";
                    }
                } catch (Exception $e) {
                    echo $e->getMessage() . "\n";
                }
            } else {
                echo "file not found.\n";
            }
        }
    }

    /**
     * Set initial params to Splynx config and create params.php file with default params.
     * By default these setting will be added to params.php file:
     * 1. add_on_encryption_key - Used for encrypted settings.
     * 2. cookieValidationKey - Need for identify our cookies.
     * 3. api_domain
     * 4. api_key
     * 5. api_secret
     * @param string $apiKeyKey
     * @param string $apiKeySecret
     * @return void
     * @throws Yii\base\Exception
     */
    protected function prepareParamsFile($apiKeyKey, $apiKeySecret)
    {
        $splynxDir = static::getSplynxDir();
        $moduleName = $this->getModuleName();
        if (($domain = static::getDomain()) !== null) {
            $url = 'https://' . $domain . '/';
        } else {
            $url = 'http://' . static::getBaseIp() . '/';
        }

        if (file_exists(static::getParamsFilePath())) {
            $this->actionGenerateKey();

            // Move new settings from params.example.php to params.php
            $this->mergeDefaultParams();

            // Reload add-on configs
            exec($splynxDir . 'system/setup/setup reload-configs --moduleName=' . $moduleName);

            // Try save params to splynx config
            // Check if API domain is already set
            if (empty(ConfigHelper::get('api_domain'))) {
                // Set API IP
                exec(
                    $splynxDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_domain" --value="' . $url . '"'
                );
            }
            exec(
                $splynxDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_key" --value="' . $apiKeyKey . '"'
            );
            exec(
                $splynxDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_secret" --value="' . $apiKeySecret . '"'
            );

            echo 'Params file already exists..' . "\n";
            return;
        }

        /** @var array<string, string> $defaultRequiredParams */
        $defaultRequiredParams = [
            static::ENCRYPTION_KEY_FIELD => static::generateEncryptionKey(),
            static::COOKIE_VALIDATION_KEY_FIELD => Yii::$app->getSecurity()->generateRandomString(),
            static::API_DOMAIN_FIELD => $url,
            static::API_KEY_FIELD => $apiKeyKey,
            static::API_SECRET_FIELD => $apiKeySecret,
        ];

        $paramsFilePath = static::getParamsFilePath();
        // Create params.php
        copy(static::getBaseParamsFilePath(), $paramsFilePath);

        // Set default params
        foreach ($defaultRequiredParams as $key => $value) {
            $comment = 'It is required default param';
            $this->insertParamToFile($paramsFilePath, $key, $value, $comment);
        }

        // Reload add-on configs
        exec($splynxDir . 'system/setup/setup reload-configs --moduleName=' . $moduleName);

        // Try save params to splynx config
        exec(
            $splynxDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_domain" --value="' . $url . '"'
        );
        exec(
            $splynxDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_key" --value="' . $apiKeyKey . '"'
        );
        exec(
            $splynxDir . 'system/script/addon config-set --addon="' . $moduleName . '" --key="api_secret" --value="' . $apiKeySecret . '"'
        );
    }

    /**
     * Set param to php array file
     * @param string $paramsFilePath Path to params file
     * @param string $key
     * @param string|int $value
     * @param string $itemComment Comment which inserted before param string (Only for new params)
     * @return bool
     */
    protected function insertParamToFile($paramsFilePath, $key, $value, $itemComment = '')
    {
        if (!file_exists($paramsFilePath)) {
            return false;
        }

        $paramsArray = require($paramsFilePath);
        $fileContent = file_get_contents($paramsFilePath);

        if ($fileContent === false) {
            return false;
        }

        $valueAsPhpCode = var_export($value, true);
        if (isset($paramsArray[$key])) {
            $fileContent = preg_replace(
                '/(("|\')' . $key . '("|\')\s*=>\s*)(""|\'\')/',
                '${1}' . $valueAsPhpCode,
                $fileContent
            );
        } else {
            if (!empty($itemComment)) {
                $itemComment = '// ' . $itemComment;
            }

            $insertingText = <<<TEXT
\$0
    $itemComment
    '$key' => $valueAsPhpCode,
TEXT;
            $fileContent = preg_replace('/return[\n\s\t]*(\s*\[|array\s*\()/im', $insertingText, $fileContent);
        }
        file_put_contents($paramsFilePath, $fileContent);

        return true;
    }

    /**
     * Move new params from params.example.php to params.php.
     *
     * @return void
     */
    protected function mergeDefaultParams()
    {
        $defaultParamsFile = static::getBaseParamsFilePath();
        $paramsFile = static::getParamsFilePath();
        $defaultParamsArray = require($defaultParamsFile);
        $paramsArray = require($paramsFile);

        foreach ($defaultParamsArray as $key => $value) {
            if (!array_key_exists($key, $paramsArray)) {
                $this->insertParamToFile($paramsFile, $key, $value);
            }
        }
    }

    /**
     * Create additional fields from getAdditionalFields method.
     *
     * @return bool false if failure
     */
    protected function createAdditionalFields()
    {
        $fields = $this->getAdditionalFields();

        if (empty($fields)) {
            return true;
        }

        $splynxDir = static::getSplynxDir();

        foreach ($fields as $fieldData) {
            $command = "{$splynxDir}system/script/addon add-or-get-additional-field ";

            foreach ($fieldData as $key => $value) {
                $command .= " --$key=\"$value\"";
            }

            if (!exec($command)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create hooks with events from getHooks method.
     * @return bool False on failure
     */
    protected function createHooks()
    {
        $hooks = $this->getHooks();
        if (empty($hooks)) {
            return true;
        }

        $splynxDir = static::getSplynxDir();

        $showErrors = $this->outputErrorsToConsole ? ' --showErrors=1' : '';

        foreach ($hooks as $hook) {
            $command = "{$splynxDir}system/script/addon add-or-get-hook ";

            $events = isset($hook['events']) ? $hook['events'] : [];
            unset($hook['events']);

            foreach ($hook as $key => $value) {
                $command .= " --$key=\"$value\"";
            }
            $command .= $showErrors;

            $hookId = exec($command);
            if ($hookId == '0') {
                return false;
            }
            if (!$this->createHookEvents((int)$hookId, $events)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Create callback authentication.
     * @return bool
     */
    protected function createCallbackAuthentication(): bool
    {
        $callbackAuthenticationSecret = $this->getCallbackAuthenticationSecret();

        if (empty($callbackAuthenticationSecret)) {
            return true;
        }

        $command = static::getSplynxDir()
            . 'system/script/addon add-or-get-callback-authentication '
            . '--title="' . static::getAddOnTitle() . '" '
            . '--secret="' . $callbackAuthenticationSecret . '"';

        if (($callbackAuthenticationId = (int)exec($command)) === 0) {
            return false;
        }

        $this->insertParamToFile(
            static::getParamsFilePath(),
            static::CALLBACK_AUTHENTICATION_ID_FIELD,
            $callbackAuthenticationId,
            'ID callback authentication secret key'
        );

        $this->insertParamToFile(
            static::getParamsFilePath(),
            static::CALLBACK_AUTHENTICATION_SECRET_FIELD,
            $callbackAuthenticationSecret,
            'Callback authentication secret key'
        );

        return true;
    }

    /**
     * Create hook events.
     * Example $events list:
     * [
     *      [
     *          'model' => 'models\common\customers\Customers',
     *          'actions' => ['create', 'edit']
     *      ]
     * ]
     * @param int $hookId Hook id
     * @param array<EventType>|array{} $events Events list
     * @return bool False on failure
     */
    protected function createHookEvents($hookId, $events)
    {
        if (empty($hookId) || empty($events)) {
            return true;
        }

        $splynxDir = static::getSplynxDir();
        $showErrors = $this->outputErrorsToConsole ? ' --showErrors=1' : '';

        $createEventBaseCommand = "{$splynxDir}system/script/addon add-or-get-hook-event --id=\"$hookId\"";

        foreach ($events as $event) {
            if (!isset($event['model']) || !isset($event['actions'])) {//@phpstan-ignore-line
                return false;
            }

            foreach ($event['actions'] as $action) {
                $command = "$createEventBaseCommand --model=\"{$event['model']}\" --action=\"$action\" {$showErrors}";

                if (exec($command) == '0') {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        // Check Splynx version
        if (!static::checkSplynxVersion()) {
            exit("Error: Your Splynx version is too old!\nMinimum required Splynx version: " . static::$minimumSplynxVersion . "!\n");
        }

        $process = exec(
            static::getSplynxDir() . 'system/script/addon check-module --name="' . $this->getModuleName() . '"'
        );
        $this->process = $process == '1' ? static::UPDATE_PROCESS : static::INSTALL_PROCESS;

        return parent::beforeAction($action);
    }

    /**
     * @param string $module
     * @return bool
     */
    protected static function checkIfModuleInstalled($module)
    {
        return (bool)exec(self::getAddOnScriptPath(' check-module --name="' . $module . '"'));
    }

    /**
     * @return bool
     */
    protected function checkIfThisModuleInstalled()
    {
        return static::checkIfModuleInstalled($this->getModuleName());
    }

    /**
     * Check or it is install process
     * @return bool
     */
    public function isInstallProcess()
    {
        return $this->process == static::INSTALL_PROCESS;
    }

    /**
     * Check or it is update process
     * @return bool
     */
    public function isUpdateProcess()
    {
        return $this->process == static::UPDATE_PROCESS;
    }

    /**
     * Get path to add-on script
     * @param string $param String to add
     * @return string
     */
    private static function getAddOnScriptPath($param = '')
    {
        return static::getSplynxDir() . 'system/script/addon' . $param;
    }

    /**
     * @var array<int>
     */
    protected $_created_payment_accounts = [];

    /**
     * Create payment accounts
     * @return bool
     */
    public function createPaymentAccounts()
    {
        $accounts = $this->getPaymentAccounts();

        if (empty($accounts)) {
            return true;
        }

        foreach ($accounts as $account) {
            $command = self::getAddOnScriptPath(' add-or-get-payment-account ');

            foreach ($account as $key => $value) {
                $command .= ' --' . $key . '="' . $value . '"';
            }

            $result = exec($command);
            if (!$result) {
                return false;
            }

            $this->_created_payment_accounts[] = (int)$result;
        }

        return true;
    }

    /**
     * Config data for payment accounts.
     * Example:
     *  return [
     *      [
     *          'title' => 'SEPA',
     *          'field_1' => 'IBAN',
     *      ]
     *  ];
     * @return array<array<string, string|int>>
     */
    public function getPaymentAccounts()
    {
        return [];
    }

    /**
     * @param string $key
     * @param string $value
     * @param string|int|null $partner_id
     * @return void
     */
    protected function configSet($key, $value, $partner_id = null)
    {
        // Escapes special chars that breaks command syntax
        // Saved value should be equal to input value
        // There is some special cases for \ char, when it should be escaped more times to save it
        $escapedValue = str_replace(["'", '"'], ["'\''", '\"'], $value);
        $script = ' config-set --addon="' . $this->getModuleName()
            . '" --key="' . $key . '" --value=\'' . $escapedValue . '\'';
        if ($partner_id) {
            $script .= ' --partner_id="' . $partner_id . '"';
        }

        exec(self::getAddOnScriptPath($script));
    }

    /**
     * Array with default financial handler types
     *      return [
     *          'export' => [],
     *          'charge' => [],
     *      ];
     *
     * @return array<string, array<string>>
     */
    public function getAllowedFinancialHandlerTypes(): array
    {
        return [
            self::FINANCE_CHARGE_HANDLERS => [],
            self::FINANCE_CHARGE_BALANCES_HANDLERS => [],
            self::FINANCE_EXPORT_HANDLERS => [],
            self::FINANCE_BANK_STATEMENS_HANDLERS => [],
        ];
    }

    /**
     * @param string $handlerType
     * @return bool
     */
    public function isFinancialHandlerTypeAllowed(string $handlerType): bool
    {
        $handlerTypes = $this->getAllowedFinancialHandlerTypes();
        return isset($handlerTypes[$handlerType]);
    }

    /**
     * Array with financial handlers to remove
     *
     * Then this method must return array like this:
     *      return [
     *          'export' => ['export-handler'],
     *          'charge' => ['charge-handler'],
     *      ];
     *
     * @see https://jira-splynx.atlassian.net/browse/SAHELPER-250?focusedCommentId=33148
     *
     * @return array<string, array<string>>
     */
    public function getFinancialHandlersForRemove()
    {
        return $this->getAllowedFinancialHandlerTypes();
    }

    /**
     * Remove financial handlers
     * @return bool
     * @throws ErrorException
     */
    private function removeFinancialHandlers()
    {
        $handlersAll = $this->getFinancialHandlersForRemove();

        if (!is_array($handlersAll)) {
            return true;
        }

        foreach ($handlersAll as $type => $handlers) {
            if (!$this->isFinancialHandlerTypeAllowed($type) || empty($handlers)) {
                continue;
            }

            foreach ($handlers as $handler) {
                FileHelper::removeDirectory(
                    static::getSplynxDir() . 'system/external_handlers/finance/' . $type . '/' . $handler
                );
            }
        }

        return true;
    }

    /**
     * Array with financial handlers to copy
     *
     * In add-on you must have directory `handler` with directory (or a few) that contains handlers files.
     * Fox example two handlers: `splynx-addon/handler/export-handler` and `splynx-addon/handler/charge-handler`.
     * Each contains all files that it needs.
     *
     * Then this method must return array like this:
     *      return [
     *          'export' => ['export-handler'],
     *          'charge' => ['charge-handler'],
     *      ];
     *
     * @see https://jira-splynx.atlassian.net/browse/SAHELPER-250?focusedCommentId=33148
     *
     * @return array<string, array<string>>
     */
    public function getFinancialHandlers()
    {
        return $this->getAllowedFinancialHandlerTypes();
    }

    /**
     * Copy financial handlers
     * @return bool
     */
    private function copyFinancialHandlers()
    {
        $handlersAll = $this->getFinancialHandlers();

        if (!is_array($handlersAll)) {
            return true;
        }

        foreach ($handlersAll as $type => $handlers) {
            if (!$this->isFinancialHandlerTypeAllowed($type) || empty($handlers)) {
                continue;
            }

            foreach ($handlers as $handler) {
                $handlerSourceDir = static::getBaseDir() . '/handler/' . $handler;
                $handlerDestinationDir = static::getSplynxDir() . 'system/external_handlers/finance/' . $type;
                echo "\nInstalling handler `$handler`...\n";
                if (!is_writable($handlerDestinationDir)) {
                    print "Warning! Can't copy files!\n";
                    print 'You must manually copy content of dir `' . $handlerSourceDir . '` to `' . $handlerDestinationDir . '`' . "\n";
                } else {
                    $handlerDestinationDir .= '/' . $handler;
                    print "Coping handler's files...\n";
                    FileHelper::copyDirectory($handlerSourceDir, $handlerDestinationDir);
                    print "Handler installing done!\n";
                }
            }
        }

        return true;
    }

    /**
     * Get list of simple|extended|add-on modules. Use this method for adding needed for add-on modules
     * For example return array:
     * ```
     * [
     *      [
     *          'module' => 'test_simple_module',
     *          'type' => ConsoleModuleConfig::TYPE_SIMPLE,
     *          'root' => 'controllers\admin\config\NetworkingController',
     *          'icon' => 'fa-cog',
     *          'title' => 'Test simple module',
     *      ]
     * ]
     * ```
     * @return array<array<string, string>>
     */
    public function getModulesList()
    {
        return [];
    }

    /**
     * Create simple|extended|add-on modules
     * @return bool
     */
    public function createModules()
    {
        $modules = $this->getModulesList();

        if (empty($modules)) {
            return true;
        }

        foreach ($modules as $item) {
            // Check if exist
            $module = (new ConsoleModuleConfig())->findOne([
                'module' => $item['module'],
                'type' => $item['type'],
            ]);

            if (!empty($module)) {
                continue;
            }

            $module = new ConsoleModuleConfig($item);
            $module->enableOutputErrorsToConsole();
            if (!$module->save()) {
                return false;
            }
        }

        return true;
    }

    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);
        $this->clearRedisCache();

        return $result;
    }

    /**
     * @return void
     */
    protected function clearRedisCache()
    {
        exec(static::getSplynxDir() . 'system/script/tools reset-redis-cache > /dev/null 2>&1 3>/dev/null');
    }
}
