<?php

namespace splynx\base;

use yii\base\Model;

class BaseApiModel extends Model
{
    /**
     * @param BaseApiModel $model
     * @param array<string, mixed> $data
     * @return void
     */
    public static function populate($model, $data)
    {
        foreach ($data as $name => $value) {
            if (property_exists($model, $name)) {
                $model->$name = $value;
            }
        }
    }
}
