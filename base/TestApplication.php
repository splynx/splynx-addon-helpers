<?php

namespace splynx\base;

use splynx\helpers\ConfigHelper;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\console\Application;

/**
 * Class TestApplication is the base application for Splynx Add-Ons Tests
 * @package splynx\base
 */
class TestApplication extends Application
{
    /** @var array<string, mixed> */
    public $api;

    /**
     * TestApplication constructor.
     * @param string $baseDir Path to add-on directory
     * @param string $configPath Path to config file
     * @throws InvalidConfigException
     */
    public function __construct(string $baseDir, string $configPath)
    {
        $config = ConfigHelper::getTestConfig($baseDir, $configPath);
        register_shutdown_function([ApiHelper::class, 'logoutBeforeDie']);
        parent::__construct($config);
    }
}
