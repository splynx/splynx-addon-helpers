<?php

namespace splynx\base;

use splynx\helpers\ConfigHelper;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\web\Application;

/**
 * Class WebApplication is the base web application for Splynx Add-Ons
 * @package splynx\base
 */
class WebApplication extends Application
{
    /** @var array<string, mixed> */
    public $api;

    /**
     * WebApplication constructor.
     * @param string $baseDir Path to add-on directory
     * @param string $configPath Path to config file
     * @throws InvalidConfigException
     */
    public function __construct($baseDir, $configPath)
    {
        $config = ConfigHelper::getWebConfig($baseDir, $configPath);
        register_shutdown_function([ApiHelper::class, 'logoutBeforeDie']);
        parent::__construct($config);
    }
}
