<?php

namespace splynx\base;

use splynx\helpers\ApiHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;

/**
 * Class BaseActiveApi
 *
 * @property bool $isNewRecord Whether the record is new and should be inserted when calling [[save()]].
 * @package splynx\base
 */
class BaseActiveApi extends BaseApiModel
{
    use CheckResponseTrait;

    /**
     * In some cases after saving model Splynx are change some model properties values like total in invoice.
     * So if you have such model you must set this property to true - to get updated data from Splynx.
     * @var bool
     */
    protected $reloadAttributesAfterSave = false;

    /**
     * @var array<string, string>|null old attribute values indexed by attribute names.
     * This is `null` if the record [[isNewRecord|is new]].
     */
    private $_oldAttributes;

    /** @var string */
    protected static $apiUrl;

    /**
     * @param BaseActiveApi $model
     * @param array<string, mixed> $data
     * @return void
     */
    public static function populate($model, $data)
    {
        parent::populate($model, $data);
        $model->_oldAttributes = $model->attributes;
    }

    /**
     * @param null|int $id
     * @param array<string, mixed> $conditions
     * @return string
     */
    protected function getApiUrl($id = null, $conditions = [])
    {
        $result = static::$apiUrl;

        // Set id
        if ($id !== null) {
            $result .= '/' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return 'id';
    }

    /**
     * @param array<string, mixed> $conditions
     * @param bool $one
     * @return array|mixed|null
     * @throws ApiResponseException
     */
    private function find(array $conditions, $one)
    {
        if ($one) {
            $conditions['limit'] = 1;
        }

        try {
            $result = ApiHelper::getInstance()->get($this->getApiUrl(null, $conditions));
        } catch (InvalidConfigException $e) {
            return null;
        }

        if ($result['result'] == false) {
            $this->checkResponseForError($result['response']);
        }

        /** @var array{'result': bool, 'response': array<string, mixed>} $result */

        if (empty($result['response'])) {
            return $one ? null : [];
        }

        $models = [];

        foreach ($result['response'] as $item) {
            $model = new static();//@phpstan-ignore-line
            static::populate($model, $item);
            $models[] = $model;
        }

        return $one ? reset($models) : $models;
    }

    /**
     * @param int|null $id
     * @return null|static
     * @throws \splynx\base\ApiResponseException
     */
    public function findById(?int $id)
    {
        if ($id === null) {
            return null;
        }

        try {
            $result = ApiHelper::getInstance()->get($this->getApiUrl($id));
        } catch (InvalidConfigException $e) {
            return null;
        }

        if ($result['result'] == false) {
            $this->checkResponseForError($result['response']);
        }

        /** @var array{'result': bool, 'response': array<string, mixed>} $result */

        if (empty($result['response'])) {
            return null;
        }

        $model = new static();//@phpstan-ignore-line
        static::populate($model, $result['response']);

        return $model;
    }

    /**
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $limit
     * @param int $offset
     * @return array<string, mixed>
     */
    private function combineSearchConditions(array $mainAttributes = [], array $additionalAttributes = [], array $order = [], $limit = null, $offset = null)
    {
        $result = [];
        if (!empty($mainAttributes)) {
            $result['main_attributes'] = $mainAttributes;
        }
        if (!empty($additionalAttributes)) {
            $result['additional_attributes'] = $additionalAttributes;
        }
        if (!empty($order)) {
            $result['order'] = $order;
        }
        if ($limit !== null) {
            if (!is_int($limit)) {
                throw new InvalidParamException('limit must be integer');
            }
            $result['limit'] = $limit;
        }

        if ($offset !== null) {
            if (!is_int($offset)) {
                throw new InvalidParamException('offset must be integer');
            }
            $result['offset'] = $offset;
        }
        return $result;
    }

    /**
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @return $this
     * @throws \splynx\base\ApiResponseException
     */
    public function findOne(array $mainAttributes, array $additionalAttributes = [], array $order = [])
    {
        return $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order), true);
    }

    /**
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $limit
     * @param int $offset
     * @return $this[]
     */
    public function findAll(array $mainAttributes, array $additionalAttributes = [], array $order = [], $limit = null, $offset = null)
    {
        return $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order, $limit, $offset), false);
    }

    /**
     * @return $this[]
     */
    public static function getListAll()
    {
        return (new static())->findAll([]);//@phpstan-ignore-line
    }

    /**
     * Returns a value indicating whether the current record is new.
     * @return bool whether the record is new and should be inserted when calling [[save()]].
     */
    public function getIsNewRecord()
    {
        return $this->_oldAttributes === null;
    }

    /**
     * Save model
     *
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        if ($this->isNewRecord) {
            return $this->create($runValidation);
        }
        return $this->update($runValidation);
    }

    /**
     * @param bool $runValidation
     * @return bool
     */
    public function create($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $primaryKey = $this->getPrimaryKey();

        $attributesForSend = $this->getAttributesForSend();
        try {
            $result = ApiHelper::getInstance()->post($this->getApiUrl(), $attributesForSend);
        } catch (InvalidConfigException $e) {
            return false;
        }

        // Set primary key value after create record
        if ($result['result'] === true && $primaryKey !== null && isset($result['response'][$primaryKey])) {
            $this->$primaryKey = $result['response'][$primaryKey];
        }

        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * @param bool $runValidation
     * @return bool
     */
    public function update($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }
        $attributesForSend = $this->getAttributesForSend();
        try {
            $result = ApiHelper::getInstance()->put($this->getApiUrl($this->getPrimaryKeyValue()), null, $attributesForSend);
        } catch (InvalidConfigException $e) {
            return false;
        }

        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * Return list of attributes for save or update.
     *
     * @return array<string>
     */
    public function getAttributesForSendArray()
    {
        return $this->attributes();
    }

    /**
     * @return array<string, mixed>
     */
    public function getAttributesForSend()
    {
        $attributes = $this->getAttributes($this->getAttributesForSendArray());

        $additional_attributes = null;
        if (array_key_exists('additional_attributes', $attributes)) {
            $additional_attributes = $attributes['additional_attributes'];
            unset($attributes['additional_attributes']);
        }
        $attributesForSend = [];
        if ($this->isNewRecord) {
            // Add not empty attributes for send
            foreach ($attributes as $name => $value) {
                if (isset($value)) {
                    $attributesForSend[$name] = $value;
                }
            }
        } else {
            // Add changed attributes for send
            foreach ($attributes as $name => $value) {
                if ($this->_oldAttributes !== null && (array_key_exists($name, $this->_oldAttributes) && $value !== $this->_oldAttributes[$name])) {
                    $attributesForSend[$name] = $value;
                }
            }
        }
        if ($additional_attributes !== null) {
            $attributesForSend['additional_attributes'] = $additional_attributes;
        }
        return $attributesForSend;
    }

    /**
     * @param array{'result': bool, 'response': array<string, mixed>|string|null} $result
     * @return void
     */
    private function checkResult($result)
    {
        if ($result['result'] == true) {
            // Reload attributes
            if ($this->reloadAttributesAfterSave) {
                $this->reloadAttributes();
            } else {
                $this->_oldAttributes = $this->attributes;
            }
        } else {
            if (!empty($result['response']) && is_array($result['response'])) {
                foreach ($result['response'] as $item) {
                    if (!empty($item['field']) && !empty($item['message'])) {
                        $this->addError($item['field'], $item['message']);
                    } elseif (!empty($result['response']['error']) && !empty($result['response']['error']['message'])) {
                        $this->addError('result', 'An internal API error occurred.');
                        Yii::error('API returned an error:' . $result['response']['error']['message']);
                    } else {
                        $this->addError('result', 'Unexpected data returned from API');
                        Yii::error('Unexpected error:' . print_r($result['response'], true));
                    }
                }
            } else {
                $this->addError('result', 'Unexpected data returned from API');
                Yii::error('Unexpected data returned from API' . (!empty($result['response']) ? ':' . print_r($result, true) : ''));
            }
        }
    }

    /**
     * Call this method to reload all models attributes by AP
     *
     * @return void
     */
    public function reloadAttributes()
    {
        if ($this->isNewRecord) {
            throw new InvalidParamException('Can\'t get primary key');
        }
        $primaryKey = $this->getPrimaryKeyValue();
        if ($primaryKey === null) {
            throw new InvalidParamException('Can\'t get primary key');
        }
        $result = ApiHelper::getInstance()->get($this->getApiUrl($primaryKey));
        if ($result['result'] == true && !empty($result['response'])) {
            /** @var array<string, mixed> $result */
            static::populate($this, $result['response']);
        } else {
            throw new InvalidParamException('Cant get data for reload attributes!');
        }
    }

    /**
     * @return null|mixed
     */
    private function getPrimaryKeyValue()
    {
        $attributes = $this->getAttributes();
        $primaryKey = $this->getPrimaryKey();
        if (isset($attributes[$primaryKey]) && $attributes[$primaryKey] !== null) {
            return $attributes[$primaryKey];
        }

        return null;
    }

    /**
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function delete()
    {
        $id = $this->getPrimaryKeyValue();
        if ($id === null) {
            return false;
        }

        $result = ApiHelper::getInstance()->delete($this->getApiUrl(), $id);
        if ($result['result'] === true) {
            $this->_oldAttributes = null;
        }
        return $result['result'];
    }

    /**
     * Get all models batch on $batchSize
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $batchSize default 100
     * @return BaseBatchApiResult<array<static>>
     */
    public function batch(array $mainAttributes, array $additionalAttributes = [], array $order = [], $batchSize = 100)
    {
        return new BaseBatchApiResult([
            'model' => new static(),//@phpstan-ignore-line
            'mainAttributesCondition' => $mainAttributes,
            'additionalAttributesCondition' => $additionalAttributes,
            'order' => $order,
            'batchSize' => $batchSize,
        ]);
    }

    /**
     * Get all models by one and load batch on $batchSize
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $batchSize default 100
     * @return BaseBatchApiResult<static>
     */
    public function each(array $mainAttributes, array $additionalAttributes = [], array $order = [], $batchSize = 100)
    {
        return new BaseBatchApiResult([
            'model' => new static(),//@phpstan-ignore-line
            'mainAttributesCondition' => $mainAttributes,
            'additionalAttributesCondition' => $additionalAttributes,
            'order' => $order,
            'batchSize' => $batchSize,
            'each' => true,
        ]);
    }

    /**
     * @return void
     */
    public static function clearAuthDataCache(): void
    {
    }
}
