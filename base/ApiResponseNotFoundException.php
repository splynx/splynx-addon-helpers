<?php

namespace splynx\base;

/**
 * Class ApiResponseNotFoundException
 * @package splynx\v2\base
 */
class ApiResponseNotFoundException extends ApiResponseException
{
}
