<?php

namespace splynx\base;

use yii\base\InvalidParamException;
use yii\base\Model;

/**
 * Class BaseConsoleModel
 *
 * @property bool $isNewRecord Whether the record is new and should be inserted when calling [[save()]].
 * @package splynx\base
 */
class BaseConsoleModel extends Model
{
    /**
     * Minimum Splynx version needs for this add-on. If empty then version will not check.
     * @var string
     */
    public static $minimumSplynxVersion = '2.2';

    /**
     * Controller name for working with model.
     * @var string
     */
    public static $controllerName;

    /**
     * Show errors in the console
     * @var bool
     */
    protected bool $_outputErrorsToConsole = false;

    /**
     * Old attribute values indexed by attribute names.
     * @var array<string, string>|null
     * This is `null` if the record [[isNewRecord|is new]].
     */
    protected $_oldAttributes;

    /**
     * This is property for saving some error message from splynx.
     *
     * @var string|null
     */
    private $_errorResponseMessage;

    // Default actions
    public const ACTION_INDEX = 'index';
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_VIEW = 'view';
    public const ACTION_DELETE = 'delete';

    /**
     * Set attributes from  array to model properties.
     *
     * @param static $model
     * @param array<string, mixed> $data
     * @return void
     */
    public static function populate($model, $data)
    {
        foreach ($data as $name => $value) {
            if (property_exists($model, $name)) {
                $model->$name = $value;
            }
        }
        $model->_oldAttributes = $model->attributes;
    }

    /**
     * Get splynx global path.
     *
     * @return string
     */
    protected static function getSplynxDir()
    {
        return '/var/www/splynx/';
    }

    /**
     * Create url, set controller action  and encode params
     *
     * @param string $action
     * @param array<string, mixed> $params
     * @return string
     */
    protected function createUrl($action = null, array $params = [])
    {
        return $this->getBaseConsolePath() . ' ' . static::$controllerName . (!empty($action) ? '--' . $action : '') . ' --params=\'' . json_encode($params) . '\'';
    }

    /**
     * Get splynx console api path.
     *
     * @return string
     */
    protected function getBaseConsolePath()
    {
        return static::getSplynxDir() . 'system/script/console_api';
    }

    /**
     * @var string[]
     */
    protected static $primaryKeys = ['id'];

    /**
     * Get model`s primary key
     *
     * @return string[]
     */
    public function getPrimaryKey()
    {
        return static::$primaryKeys;
    }

    /**
     * Returns a value indicating whether the current record is new.
     *
     * @return bool whether the record is new and should be inserted when calling [[save()]].
     */
    public function getIsNewRecord()
    {
        return $this->_oldAttributes === null;
    }

    /**
     * Check result, if result true we set old attributes, else we check if is errors or response message.
     *
     * @param array{'result': bool, 'response': array<string, mixed>|string|null} $result
     * @return void
     */
    protected function checkResult($result)
    {
        if ($result['result'] === true) {
            $this->_oldAttributes = $this->attributes;
        } else {
            if (is_array($result['response'])) {
                foreach ($result['response'] as $item) {
                    $this->addError($item['field'], $item['message']);
                }
            } else {
                $this->_errorResponseMessage = $result['response'];
            }
        }
    }

    /**
     * Get primary keys values.
     *
     * @return array<string, mixed>
     */
    private function getPrimaryKeysValues()
    {
        $attributes = $this->getAttributes();
        $primaryKeys = $this->getPrimaryKey();
        $primaryKeysValues = [];

        foreach ($primaryKeys as $key) {
            if (isset($attributes[$key]) && $attributes[$key] !== null) {
                $primaryKeysValues[$key] = $attributes[$key];
            }
        }

        return $primaryKeysValues;
    }

    /**
     * Method is called before saving model.
     *
     * @return void
     */
    public function beforeSave()
    {
    }

    /**
     * Save model
     *
     * @param bool $runValidation
     * @return bool
     */
    public function save($runValidation = true)
    {
        $this->beforeSave();

        $result = $this->isNewRecord ? $this->create($runValidation) : $this->update($runValidation);
        if (!$result && $this->_outputErrorsToConsole) {
            $this->writeErrorsToConsole();
        }

        return $result;
    }

    /**
     * Create record.
     *
     * @param bool $runValidation
     * @return bool
     */
    public function create($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $command = $this->createUrl(static::ACTION_CREATE, $this->getAttributesForSend());
        $result = $this->exec($command);

        // Set primary key value after create record
        if ($result['result'] === true && !empty($result['response'])) {
            foreach ($this->getPrimaryKey() as $key) {
                if (isset($result['response'][$key])) {
                    $this->$key = $result['response'][$key];
                }
            }
        }

        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * Update record.
     *
     * @param bool $runValidation
     * @return bool
     */
    public function update($runValidation = true)
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        $params = $this->getAttributesForSend();
        // Add primary keys values to attributes if it is not set
        foreach ($this->getPrimaryKeysValues() as $key => $value) {
            if (!isset($params[$key])) {
                $params[$key] = $value;
            }
        }

        $command = $this->createUrl(static::ACTION_UPDATE, $params);
        $result = $this->exec($command);
        $this->checkResult($result);

        return $result['result'];
    }

    /**
     * Get attributes for send.
     * If it is new record we return all attributes else return only changed attributes
     *
     * @return array<string, mixed>
     */
    public function getAttributesForSend()
    {
        $attributes = $this->getAttributes($this->attributes());

        $additional_attributes = null;
        if (array_key_exists('additional_attributes', $attributes)) {
            $additional_attributes = $attributes['additional_attributes'];
            unset($attributes['additional_attributes']);
        }

        $attributesForSend = [];
        if ($this->isNewRecord) {
            // Add not empty attributes for send
            foreach ($attributes as $name => $value) {
                if ($value !== null) {
                    $attributesForSend[$name] = $value;
                }
            }
        } else {
            // Add changed attributes for send
            foreach ($attributes as $name => $value) {
                if (!empty($this->_oldAttributes) && (array_key_exists($name, $this->_oldAttributes) && $value !== $this->_oldAttributes[$name])) {
                    $attributesForSend[$name] = $value;
                }
            }
        }

        if ($additional_attributes !== null) {
            $attributesForSend['additional_attributes'] = $additional_attributes;
        }

        return $attributesForSend;
    }

    /**
     * Delete record.
     *
     * @return bool
     */
    public function delete()
    {
        $primaryKeysValues = $this->getPrimaryKeysValues();
        if (empty($primaryKeysValues)) {
            return false;
        }

        $command = $this->createUrl(static::ACTION_DELETE, $primaryKeysValues);

        $result = $this->exec($command);

        if ($result['result'] === true) {
            $this->_oldAttributes = null;
        } else {
            $this->checkResult($result);
        }

        return $result['result'];
    }

    /**
     * @param string $action
     * @param array<string, mixed> $params
     * @return mixed
     */
    public function customAction($action, $params = [])
    {
        $command = $this->createUrl($action, $params);
        $result = $this->exec($command);
        $this->checkResult($result);

        return $result['response'];
    }

    /**
     * Run command and decode result.
     *
     * @param string $command
     * @return array{'result': bool, 'response': array<string, mixed>|string|null}
     */
    protected function exec($command)
    {
        // Check if console api file is exist
        $this->checkConsoleApi();

        $result = shell_exec($command . ' 2>&1');
        $resultArray = null;
        if ($result !== null) {
            $resultArray = json_decode($result, true);
        }

        if ($resultArray === null) {
            return [
                'result' => false,
                'response' => $result,
            ];
        }

        return $resultArray;
    }

    /**
     * Check if console api file is exist.
     *
     * @return void
     */
    protected function checkConsoleApi()
    {
        if (!file_exists($this->getBaseConsolePath())) {
            exit("Error: Your Splynx version is old for this add-on!\nMinimum required Splynx version: " . static::$minimumSplynxVersion . "!\nPlease upgrade your system\n");
        }
    }

    /**
     * Find record by conditions.
     *
     * @param array<string, mixed> $conditions
     * @param bool $one
     * @return array|mixed|null
     */
    private function find(array $conditions, $one)
    {
        $command = $this->createUrl(static::ACTION_INDEX, $conditions);
        $result = $this->exec($command);
        $this->checkResult($result);

        if ($result['result'] == false || empty($result['response']) || !is_array($result['response'])) {
            return $one ? null : [];
        }

        $models = [];
        foreach ($result['response'] as $item) {
            $model = new static();//@phpstan-ignore-line
            static::populate($model, $item);
            $models[] = $model;
        }

        return $one ? reset($models) : $models;
    }

    /**
     * Find model by primary key, if model has several primary keys need set array with pk values.
     * Example:
     *          HookEvents has 3 pk (id, model, action) then function call must be:
     *          $model->findByPk([
     *              'id' => 5,
     *              'model' => 'models\common\finance\Transactions',
     *              'action' => 'edit'
     *          ]);
     * @param array<string> $pkValues
     * @return null|static
     */
    public function findByPk($pkValues = [])
    {
        $command = $this->createUrl(static::ACTION_VIEW, $pkValues);

        $result = $this->exec($command);
        if ($result['result'] == false || empty($result['response']) || !is_array($result['response'])) {
            $this->checkResult($result);
            return null;
        }

        $model = new static();//@phpstan-ignore-line
        static::populate($model, $result['response']);

        return $model;
    }

    /**
     * Combine conditions for search.
     *
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param integer|null $limit
     * @return array<string, mixed>
     */
    private function combineSearchConditions(array $mainAttributes = [], array $additionalAttributes = [], array $order = [], $limit = null)
    {
        $result = [];
        if (!empty($mainAttributes)) {
            $result['main_attributes'] = $mainAttributes;
        }
        if (!empty($additionalAttributes)) {
            $result['additional_attributes'] = $additionalAttributes;
        }
        if (!empty($order)) {
            $result['search_conditions']['order'] = $order;
        }
        if ($limit !== null) {
            if (!is_int($limit)) {
                throw new InvalidParamException('limit must be integer');
            }
            $result['search_conditions']['limit'] = $limit;
        }

        return $result;
    }

    /**
     * Find one record by conditions.
     *
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @return $this|null
     */
    public function findOne(array $mainAttributes, array $additionalAttributes = [], $order = [])
    {
        return $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order, 1), true);
    }

    /**
     * Find all record by conditions.
     *
     * @param array<string, mixed> $mainAttributes
     * @param array<string, mixed> $additionalAttributes
     * @param array<string, string> $order
     * @param int $limit
     * @return $this[]
     */
    public function findAll(array $mainAttributes, array $additionalAttributes = [], array $order = [], $limit = null)
    {
        return $this->find($this->combineSearchConditions($mainAttributes, $additionalAttributes, $order, $limit), false);
    }

    /**
     * Get response errors.
     *
     * @return string|null
     */
    public function getResponseErrorMesage()
    {
        return $this->_errorResponseMessage;
    }

    /**
     * @return void
     */
    protected function writeErrorsToConsole(): void
    {
        $message = '';

        $errorsArray = $this->getErrors();
        if ($errorsArray) {
            foreach ($errorsArray as $errors) {
                foreach ($errors as $error) {
                    $message .= $error . PHP_EOL;
                }
            }
        }

        $additionalErrors = $this->getResponseErrorMesage();
        if ($additionalErrors) {
            $message .= $additionalErrors . PHP_EOL;
        }

        fwrite(STDERR, $message);
    }

    /**
     * @return void
     */
    public function enableOutputErrorsToConsole(): void
    {
        $this->_outputErrorsToConsole = true;
    }
}
