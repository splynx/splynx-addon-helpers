<?php

namespace splynx\base;

use yii\web\Request;
use yii\web\Response;

class BaseUser extends \yii\web\User
{
    public function loginRequired($checkAjax = true, $checkAcceptHeader = true)
    {
        parent::loginRequired($checkAjax, $checkAcceptHeader);

        /** @var Request $request */
        $request = \Yii::$app->getRequest();
        /** @var Response $response */
        $response = \Yii::$app->getResponse();
        return $response->redirect('/portal/login/?return=' . $request->getUrl());
    }
}
