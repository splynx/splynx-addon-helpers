<?php

namespace splynx\base;

use Yii;

/**
 * Trait CheckResponseTrait
 * @package splynx\base
 */
trait CheckResponseTrait
{
    /**
     * @param string|array<string, mixed> $response
     * @return void
     * @throws ApiResponseException
     */
    public function checkResponseForError($response)
    {
        if (empty($response)) {
            return;
        }

        $responseException = ApiResponseException::class;

        if (is_string($response)) {
            throw new $responseException(
                Yii::t('app', 'System return response with exception. Message: "{message}"', [
                    'message' => $response,
                ])
            );
        }

        if (isset($response['error'])) {
            /** @see https://jira.splynx.com/browse/SAHELPER-164 */
            if ($response['error']['code'] == 401) {
                $response['error']['message'] = 'Please check your system API settings';
                static::clearAuthDataCache();
            }

            if ($response['error']['code'] == 404) {
                $responseException = ApiResponseNotFoundException::class;
            }

            throw new $responseException(
                Yii::t('app', 'System return response with exception. Message: "{message}". Internal code: {internal_code}', [
                    'message' => $response['error']['message'],
                    'internal_code' => $response['error']['internal_code'],
                ]),
                $response['error']['code']
            );
        }
    }

    /**
     * @return void
     */
    abstract public static function clearAuthDataCache(): void;
}
