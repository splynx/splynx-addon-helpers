<?php

namespace splynx\base;

use splynx\helpers\ConfigHelper;
use splynx\models\Admin;
use splynx\v2\models\administration\BaseAdministrator;
use Throwable;
use Yii;
use yii\redis\Session;

/**
 * Class RedisSession provide functionality for working with Splynx session that stored in Redis
 * @package splynx\base
 */
class RedisSession extends Session
{
    public const MAX_SESSION_TTL = 60 * 60 * 24 * 30; // 30 days
    /** @var string Session key prefix */
    public $keyPrefix = 'php_session:';

    /** @var string Splynx prefix for keys in redis */
    public $splynxRedisPrefix;

    /** Default prefix for all splynx keys stored in redis */
    public const DEFAULT_SPLYNX_REDIS_KEY_PREFIX = 'spl:';

    protected int $_timeout;

    /**
     * @inheritdoc
     * @return void
     */
    public function init()
    {
        $redisConfig = ConfigHelper::getRedisConfig();
        $redisConfig = isset($redisConfig['server']) ? $redisConfig['server'] : $redisConfig;
        $this->splynxRedisPrefix = isset($redisConfig['prefix']) ? $redisConfig['prefix'] : self::DEFAULT_SPLYNX_REDIS_KEY_PREFIX;

        ini_set('session.serialize_handler', 'msgpack');
        parent::init();
    }

    /**
     * @inheritdoc
     */
    protected function calculateKey($id)
    {
        return $this->splynxRedisPrefix . $this->keyPrefix . $id;
    }

    /**
     * @inheritdoc
     */
    public function readSession($id)
    {
        $sessionData = parent::readSession($id);
        if (empty($sessionData)) {
            return '';
        }

        $sessionData = json_decode($sessionData, true);
        return \msgpack_pack($sessionData);
    }

    /**
     * @inheritdoc
     */
    public function writeSession($id, $data)
    {
        $data = json_encode($_SESSION);
        if ($data === false) {
            return false;
        }
        return parent::writeSession($id, $data);
    }

    /**
     * @param string $key
     * @return string|null
     */
    public function getPrimaryFromKey($key)
    {
        /** @var array<string>|false $parts */
        $parts = explode(':', $key);
        if (!empty($parts)) {
            return end($parts);
        }

        return null;
    }

    /**
     * @return string[]
     */
    public function getAllOnlineAdmins()
    {
        $pattern = $this->splynxRedisPrefix . 'record:online_admin:data';
        /** @var \yii\redis\Connection $redis */
        $redis = $this->redis;
        $data = $redis->hgetall($pattern);
        $sessions = [];
        $sessionKey = null;
        foreach ($data as $key => $session) {
            if ($key === 0 || ($key % 2) == 0) {
                $sessionKey = $session;
                continue;
            }
            $sessions[$sessionKey] = $session;
        }
        return $sessions;
    }

    /**
     * Get active sessions for online admins by admin ids
     * @param int[] $adminIds Admins ids
     * @return array<string>
     */
    public function getSessionsIdsForOnlineAdmins($adminIds)
    {
        $allSessions = $this->getAllOnlineAdmins();
        $sessions = [];
        foreach ($allSessions as $key => $session) {
            $session = json_decode($session, true);
            if (isset($session['admin_id']) && in_array($session['admin_id'], $adminIds) && $key) {
                $sessions[] = $key;
            }
        }
        return $sessions;
    }

    /**
     * @inheritdoc
     * @throws Throwable
     */
    public function getTimeout()
    {
        if (!isset($this->_timeout)) {
            /** @var WebApplication $app */
            $app = Yii::$app;
            $user = $app->getUser()->getIdentity();
            if ($user instanceof BaseAdministrator || $user instanceof Admin) {
                /** @var BaseAdministrator $user */
                $this->_timeout = $user->timeout;
            } else {
                $this->_timeout = -1; // If authorized not admin set timeout -1
            }
        }

        return $this->_timeout === -1 ?
            parent::getTimeout() :
            ($this->_timeout > 0 ? $this->_timeout : self::MAX_SESSION_TTL);
    }
}
