<?php

namespace splynx\base;

use yii\base\Exception;

/**
 * Class ApiResponseException
 * @package splynx\v2\base
 */
class ApiResponseException extends Exception
{
}
