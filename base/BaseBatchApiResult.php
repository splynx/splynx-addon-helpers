<?php

namespace splynx\base;

use Iterator;
use yii\base\BaseObject;

/**
 * Class BaseBatchApiResult
 * @package splynx\base
 *
 * @template T
 * @implements Iterator<T>
 */
class BaseBatchApiResult extends BaseObject implements Iterator
{
    /** @var bool */
    public $each = false;

    /** @var BaseActiveApi $model */
    public $model;

    /** @var array<string, mixed> */
    public $mainAttributesCondition;

    /** @var array<string, mixed> */
    public $additionalAttributesCondition;

    /** @var array<string, string> */
    public $order;

    /** @var int */
    public $batchSize = 100;

    /** @var int|null */
    private $_eachPosition = 0;

    /** @var int */
    private $_position = 0;

    /** @var mixed */
    private $_data;

    /** @var mixed */
    private $_value;

    /**
     * Need for implement Iterator interface
     */
    public function rewind(): void
    {
        $this->_eachPosition = 0;
        $this->_position = 0;

        $this->_value = null;
        $this->next();
    }

    /**
     * Need for implement Iterator interface
     * @return BaseActiveApi|null
     */
    public function current(): mixed
    {
        return $this->_value;
    }

    /**
     * Need for implement Iterator interface
     * @return int|null
     */
    public function key(): ?int
    {
        if ($this->each) {
            return $this->_eachPosition;
        }

        return $this->_position;
    }

    /**
     * Need for implement Iterator interface
     */
    public function next(): void
    {
        if (!$this->each) {
            $this->_value = $this->getBatch();
            ++$this->_position;
            return;
        }

        if (empty($this->_data) || next($this->_data) === false) {
            $this->_data = $this->getBatch();
            ++$this->_position;
        }

        $this->_value = current($this->_data);
        if (key($this->_data) !== null) {
            $this->_eachPosition++;
        } else {
            $this->_eachPosition = null;
        }
    }

    /**
     * Need for implement Iterator interface
     * @return bool
     */
    public function valid(): bool
    {
        return !empty($this->_value);
    }

    /**
     * Get items by conditions and with limit and offset
     * @return BaseActiveApi[]
     */
    private function getBatch()
    {
        return $this->model->findAll(
            $this->mainAttributesCondition,
            $this->additionalAttributesCondition,
            $this->order,
            $this->batchSize,
            $this->_position * $this->batchSize
        );
    }
}
