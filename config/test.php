<?php

/**
 * Base configuration for Splynx Add-Ons test applications.
 * In your add-on's test.php you can use special classes for rewrite or unset settings.
 * For rewrite settings use yii\helpers\ReplaceArrayValue class instead of setting value.
 * For uset settings use yii\helpers\UnsetArrayValue class instead of setting value.
 */

use splynx\helpers\ConfigHelper;
use yii\helpers\ArrayHelper;

/**
 * @param array $params Settings from config/params.php file
 * @param string $baseDir Path to add-on directory
 * @return array
 */

return function ($params, $baseDir) {
    return ArrayHelper::merge(require 'common.php', [
        // Generate application id by add-on directory name
        'id' => ConfigHelper::getAddOnNameFromBaseDir($baseDir) . '-test',
        'basePath' => $baseDir,
        'vendorPath' => "{$baseDir}/../" . ConfigHelper::ADD_ON_BASE . '/vendor',
        'params' => $params,
    ]);
};
