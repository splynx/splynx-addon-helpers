<?php

/**
 * Base configuration for Splynx Add-Ons web applications.
 * In your add-on's web.php you can use special classes for rewrite or unset settings.
 * For rewrite settings use yii\helpers\ReplaceArrayValue class instead of setting value.
 * For uset settings use yii\helpers\UnsetArrayValue class instead of setting value.
 */

use splynx\helpers\ConfigHelper;
use yii\helpers\ArrayHelper;

/**
 * @param array $params Settings from config/params.php file
 * @param string $baseDir Path to add-on directory
 * @return array
 */

return function ($params, $baseDir) {
    $addonBasePath = "{$baseDir}/../" . ConfigHelper::ADD_ON_BASE;

    $config = [
        // Generate application id by add-on directory name
        'id' => ConfigHelper::getAddOnNameFromBaseDir($baseDir) . '-web',
        'basePath' => $baseDir,
        // Path to add-on base vendor directory
        'vendorPath' => "$addonBasePath/vendor",
        // Settings from config/params.php
        'params' => $params,
        'aliases' => [
            '@bower' => "$addonBasePath/vendor/bower-asset",
            '@npm' => "$addonBasePath/vendor/npm-asset",
        ],
        'components' => [
            'request' => [
                'cookieValidationKey' => $params['cookieValidationKey'],
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'user' => [
                'identityClass' => 'splynx\models\customer\BaseCustomer',
                'idParam' => 'splynx_customer_id',
                'loginUrl' => '/portal/',
                'enableAutoLogin' => true,
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                // Load url rules from config/url_rules.php file
                'rules' => ConfigHelper::getUrlRules($baseDir),
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'yii\i18n\PhpMessageSource',
                        'basePath' => '@app/messages',
                        'sourceLanguage' => 'en-US',
                    ],
                ],
            ],
            'assetManager' => [
                'forceCopy' => YII_ENV_DEV,
            ],
        ],
    ];

    if (YII_ENV_DEV) {//@phpstan-ignore-line
        // Configuration adjustments for 'dev' environment
        $config['components']['view']['renderers']['twig']['options']['debug'] = true;
        $config['components']['view']['renderers']['twig']['options']['auto_reload'] = true;
        $config['components']['view']['renderers']['twig']['extensions'][] = '\Twig\Extension\DebugExtension';

        // Enable yii-debug module
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['*'],
        ];
    }

    return ArrayHelper::merge(require 'common.php', $config);
};
