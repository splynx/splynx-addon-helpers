<?php

use splynx\components\log\SplynxDispatcher;
use splynx\helpers\ConfigHelper;
use yii\log\FileTarget;

return [
    'bootstrap' => ['log'],
    // Set Splynx timeZone
    'timeZone' => ConfigHelper::getSplynxTimeZone(),
    'api' => [
        'version' => SplynxApi::API_VERSION_2,
    ],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'sqlite:@app/data/data.db',
            'charset' => 'utf8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => ConfigHelper::getSessionConfig(),
        'redis' => ConfigHelper::getRedisConfigComponent(),
        'formatter' => [
            'timeZone' => ConfigHelper::getSplynxTimeZone(),
            'defaultTimeZone' => ConfigHelper::getSplynxTimeZone(),
            'dateFormat' => 'php:Y-m-d',
            'timeFormat' => 'php:H:i:s',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
        ],
        'log' => [
            'class' => SplynxDispatcher::class,
            'traceLevel' => YII_DEBUG ? 3 : 0,//@phpstan-ignore-line
            'targets' => [
                [
                    'levels' => ['info'],
                    'logFile' => '@app/runtime/logs/info.log',
                    'class' => FileTarget::class,
                    'maxFileSize' => 1024 * 10,
                    // Do not add $_SESSION, $_SERVER... to logs
                    'logVars' => [],
                    // Remove IP and other information from message prefix
                    'prefix' => static function ($message) {
                        return '';
                    },
                ],
                [
                    'levels' => ['warning'],
                    'logFile' => '@app/runtime/logs/warning.log',
                    'class' => FileTarget::class,
                    'maxFileSize' => 1024 * 10,
                    'logVars' => [],
                    'prefix' => static function ($message) {
                        return '';
                    },
                ],
                [
                    'levels' => ['error'],
                    'logFile' => '@app/runtime/logs/error.log',
                    'class' => FileTarget::class,
                    'maxFileSize' => 1024 * 10,
                    'prefix' => static function ($message) {
                        return '';
                    },
                ],
                [
                    'categories' => ['callback'],
                    'logFile' => '@app/runtime/logs/callback.log',
                    'class' => FileTarget::class,
                    'maxFileSize' => 1024 * 10,
                    'prefix' => static function ($message) {
                        return '';
                    },
                ],
            ],
        ],
        'view' => [
            'class' => 'yii\web\View',
            'defaultExtension' => 'twig',
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'cache' => false,
                    ],
                    'extensions' => [
                        '\splynx\components\twig\TwigActiveForm',
                        '\splynx\components\twig\TwigPjax',
                    ],
                    'globals' => [
                        'Html' => ['class' => '\yii\helpers\Html'],
                        'Url' => ['class' => '\yii\helpers\Url'],
                        'GridView' => ['class' => '\yii\grid\GridView'],
                        'DatePicker' => ['class' => '\yii\jui\DatePicker'],
                        'DateRangePicker' => ['class' => '\splynx\widgets\DateRangePicker'],
                        'MaskedInput' => ['class' => '\yii\widgets\MaskedInput'],
                        'Yii' => ['class' => 'Yii'],
                        'DateHelper' => ['class' => '\splynx\helpers\DateHelper'],
                        'Select2' => ['class' => '\splynx\widgets\Select2'],
                    ],
                    'uses' => [
                        'yii\bootstrap',
                    ],
                ],
            ],
        ],
    ],
];
