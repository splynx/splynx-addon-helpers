<?php

namespace splynx\assets;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = __DIR__;

    /** @var string[] */
    public $js = [
        'js/select2/select2.min.js',
        'js/select2/initSelect2.js',
    ];

    /** @var string[] */
    public $css = [
        'css/select2/select2.min.css',
        'css/select2/customSelect2.css',
    ];

    /** @var string[] */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
