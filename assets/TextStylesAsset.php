<?php

namespace splynx\assets;

use yii\web\AssetBundle;

class TextStylesAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    /** @var string[] */
    public $css = [
        'css/text-styles.css',
    ];
}
