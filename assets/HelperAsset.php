<?php

namespace splynx\assets;

use yii\web\AssetBundle;

/**
 * Class HelperAsset
 * For usage add this code to your AppAsset:
 *      public $depends = [
 *           'splynx\assets\HelperAsset',
 *      ];
 *
 * @package splynx
 */
class HelperAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    /** @var string[] */
    public $js = [
        'js/jquery.noty.packaged.min.js',
        'js/notify.min.js?v=20181001',
        // Files for datatables
        'js/datatables/datatables.min.js',
        'js/datatables/dataTables.buttons.min.js',
        'js/datatables/dataTables.sort.js',
        'js/datatables/buttons.html5.min.js',
        'js/datatables/buttons.print.min.js',
        'js/datatables/jszip.min.js',
        'js/datatables/pdfmake.min.js',
        'js/datatables/vfs_fonts.js',
        'js/datatables/dataTables.rowGroup.js',
        'js/function.min.js',
        'js/iframe-wrap.js',
    ];

    /** @var string[] */
    public $css = [
        'css/notify.css',
        'css/base.css',
        // Files for datatables
        'css/datatables/datatables.min.css',
        'css/datatables/rowGroup.dataTables.css',
        'css/font-awesome.min.css',
        'fonts/FluentSystemIcons-Regular.css',
        'css/redesign4_0.css',
    ];
}
