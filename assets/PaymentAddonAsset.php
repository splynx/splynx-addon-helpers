<?php

namespace splynx\assets;

use yii\web\AssetBundle;

/**
 * Class PaymentAddonAsset.php
 * For usage add this code to your AppAsset:
 *      public $depends = [
 *           'splynx\assets\PaymentAddonAsset',
 *      ];
 *
 * @package splynx
 */
class PaymentAddonAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    /** @var string[] */
    public $css = [
        'css/paymentAddon.css',
    ];

    /** @var string[] */
    public $js = [
        'js/iframe-wrap.js',
        'js/loading.js',
    ];

    /** @var string[] */
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
