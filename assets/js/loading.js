let loadingElement;
if (window.frameElement) {
    window.frameElement.onload = () => {
        loadingElement = window.frameElement.contentDocument.querySelector('body');
        const observer = new MutationObserver((mutations) => {
            mutations.forEach((mutation) => {
                const { target } = mutation;
                if (mutation.attributeName === 'data-content') {
                    return false;
                }
                if (mutation.attributeName === 'class') {
                    loadingElement = target;
                    if (target.classList.contains('loading')) {
                        onClassAdded();
                    } else {
                        onClassRemoved();
                    }
                }
            });
        });
        const config = { attributes: true, childList: true, subtree: true };
        observer.observe(loadingElement, config);
    };
}

const onClassAdded = () => {
    loadingElement?.setAttribute('data-content', 'Please wait while we are connecting to the secure payment portal');
};

const onClassRemoved = () => {
    loadingElement?.setAttribute('data-content', '');
};
