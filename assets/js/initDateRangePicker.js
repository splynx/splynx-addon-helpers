var loadOptions = new Array();

function initDateRangePicker(options) {
    loadOptions[options.id] = options;
    let id = options.id;
    let singleDatePicker = options.pluginOptions.singleDatePicker !== undefined ? options.pluginOptions.singleDatePicker : false;
    let jsDateFormat = options.pluginOptions.locale.format !== undefined ? options.pluginOptions.locale.format : 'options.pluginOptions.locale.format';
    let autoUpdateInput = options.pluginOptions.autoUpdateInput !== undefined ? options.pluginOptions.autoUpdateInput : true;
    let dateRangePickerId = $('#' + id);
    let dateSelectInput = dateRangePickerId;
    let defaultCallback = function (start, end) {
        $('#daterangepicker_' + id).find('#daterangepicker_start_' + id).val(start.format(jsDateFormat));
        $('#daterangepicker_' + id).find('#daterangepicker_end_' + id).val(end.format(jsDateFormat));
    }

    let callbackDateRange = (!options.callback) ? options.callback : defaultCallback;

    let templateForDatapicker = '<div class="daterangepicker" id="daterangepicker_' + id + '">\n' +
        '                    <div class="drp-calendar left">\n' +
        '                        <div class="daterangepicker_input">\n' +
        '                            <input class="input-mini form-control" type="text" name="daterangepicker_start"\n' +
        '                                   id="daterangepicker_start_' + id + '" value=""/>\n' +
        '                            <i class="fa fa-calendar"></i>\n' +
        '                            <div class="calendar-time">\n' +
        '                                <div>\n' +
        '                                </div>\n' +
        '                                <i class="fa fa-clock-o"></i>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="calendar-table"></div>\n' +
        '                    </div>\n' +
        '                    <div class="drp-calendar right">\n' +
        '                        <div class="daterangepicker_input">\n' +
        '                            <input class="input-mini form-control" type="text" name="daterangepicker_end"\n' +
        '                                   id="daterangepicker_end_' + id + '" value=""/>\n' +
        '                            <i class="fa fa-calendar"></i>\n' +
        '                            <div class="calendar-time">\n' +
        '                                <div>\n' +
        '                                </div>\n' +
        '                                <i class="fa fa-clock-o"></i>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                        <div class="calendar-table"></div>\n' +
        '                    </div>\n' +
        '                    <div class="ranges">\n' +
        '                        <div class="range_inputs drp-buttons">\n' +
        '                            <button class="applyBtn" disabled="disabled" type="button"></button>\n' +
        '                            <button class="cancelBtn" type="button"></button>\n' +
        '                        </div>\n' +
        '                    </div>\n' +
        '                </div>';

    let templateForSingleDatapicker = '<div class="daterangepicker" id="daterangepicker_' + id + '">\n' +
        '                    <div class="drp-calendar left">\n' +
        '                        <div class="calendar-table"></div>\n' +
        '                    </div>\n' +
        '                    <div class="drp-calendar right">\n' +
        '                        <div class="calendar-table"></div>\n' +
        '                    </div>\n' +
        '                </div>';

    let ranges = {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    };

    let defaultOptions = {
        autoUpdateInput: autoUpdateInput,
        alwaysShowCalendars: true,
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        ranges: ranges,
        template: templateForDatapicker,
        applyButtonClasses: 'btn-success',
        cancelButtonClasses: 'btn-default',
    };

    let singleCalendarDefaultOptions = {
        autoUpdateInput: autoUpdateInput,
        alwaysShowCalendars: true,
        autoApply: true,
        single: true,
        ranges: false,
        template: templateForSingleDatapicker,
    };

    let dateRangepickerOptions = {};

    if (singleDatePicker) {
        dateRangepickerOptions = $.extend(true, singleCalendarDefaultOptions, options.pluginOptions);
    } else {
        dateRangepickerOptions = $.extend(true, defaultOptions, options.pluginOptions);
    }

    let dateRangePicker = dateRangePickerId.daterangepicker(dateRangepickerOptions, callbackDateRange);
    let dataCalendar = dateRangePickerId.data('daterangepicker');

    if (!singleDatePicker) {
        dateSelectInput.on('show.daterangepicker', function (ev, picker) {
            let dateRangePickerPopup = $('#daterangepicker_' + id);
            let dateRangePickerStart = dateRangePickerPopup.find('#daterangepicker_start_' + id);
            let dateRangePickerEnd = dateRangePickerPopup.find('#daterangepicker_end_' + id);
            dateRangePickerStart.val(picker.startDate.format(jsDateFormat));
            dateRangePickerEnd.val(picker.endDate.format(jsDateFormat));

            let oldStartDate = picker.startDate ? picker.startDate.format(jsDateFormat) : null;
            let oldEndDate = picker.endDate ? picker.endDate.format(jsDateFormat) : null;

            dateRangePickerPopup.on('mouseenter.daterangepicker', 'td.available', function (e) {
                oldStartDate = picker.startDate ? picker.startDate.format(jsDateFormat) : null;
                oldEndDate = picker.endDate ? picker.endDate.format(jsDateFormat) : null;

                if (!$(e.target).hasClass('available')) {
                    return;
                }

                let title = $(e.target).attr('data-title');
                let row = title.substr(1, 1);
                let col = title.substr(3, 1);
                let calendar = $(e.target).parents('.drp-calendar');
                let date = calendar.hasClass('left') ? dataCalendar.leftCalendar.calendar[row][col] : dataCalendar.rightCalendar.calendar[row][col];


                if (dataCalendar.endDate || date.isBefore(dataCalendar.startDate, 'day')) { //picking start
                    dateRangePickerStart.val(date.format(jsDateFormat));
                } else if (!dataCalendar.endDate && date.isBefore(dataCalendar.startDate)) {
                    dateRangePickerEnd.val(date.format(jsDateFormat));
                } else { // picking end
                    dateRangePickerEnd.val(date.format(jsDateFormat));
                }
            }).on('mouseout', 'td.available', function (e) {
                if ($('.start-date').length > 0 && oldStartDate != null) {
                    dateRangePickerStart.val(oldStartDate);
                }
                if ($('.end-date').length > 0 && oldEndDate != null) {
                    dateRangePickerEnd.val(oldEndDate);
                }
            });

            dateRangePickerPopup.on('mouseenter.daterangepicker', '.ranges li', function (e) {
                oldStartDate = picker.startDate ? picker.startDate.format(jsDateFormat) : null;
                oldEndDate = picker.endDate ? picker.endDate.format(jsDateFormat) : null;

                let label = e.target.getAttribute('data-range-key');
                dataCalendar.chosenLabel = label;
                if (label === dataCalendar.locale.customRangeLabel) {
                    dataCalendar.showCalendars();
                } else {
                    let dates = dataCalendar.ranges[label];
                    dateRangePickerStart.val(dates[0].format(jsDateFormat));
                    dateRangePickerEnd.val(dates[1].format(jsDateFormat));
                }
            }).on('mouseout', '.ranges li', function (e) {
                if ($('.start-date').length > 0 && oldStartDate != null) {
                    dateRangePickerStart.val(oldStartDate);
                }
                if ($('.end-date').length > 0 && oldEndDate != null) {
                    dateRangePickerEnd.val(oldEndDate);
                }
            });

            dateRangePickerStart.on('input', function () {
                dataCalendar.setStartDate($(this).val());
                dataCalendar.updateView();
            });

            dateRangePickerEnd.on('input', function () {
                dataCalendar.setEndDate($(this).val());
                dataCalendar.updateView();
            });
        });
    }
    //needs for responsive DateRangePicker
    dataCalendar.constructor.prototype.move = function () {
        var parentOffset = {top: 0, left: 0},
            containerTop,
            drops = this.drops;

        var parentRightEdge = $(window).width();
        if (!this.parentEl.is('body')) {
            parentOffset = {
                top: this.parentEl.offset().top - this.parentEl.scrollTop(),
                left: this.parentEl.offset().left - this.parentEl.scrollLeft()
            };
            parentRightEdge = this.parentEl[0].clientWidth + this.parentEl.offset().left;
        }

        switch (drops) {
            case 'auto':
                containerTop = this.element.offset().top + this.element.outerHeight() - parentOffset.top;
                if (containerTop + this.container.outerHeight() >= this.parentEl[0].scrollHeight) {
                    containerTop = this.element.offset().top - this.container.outerHeight() - parentOffset.top;
                    drops = 'up';
                }
                break;
            case 'up':
                containerTop = this.element.offset().top - this.container.outerHeight() - parentOffset.top;
                break;
            default:
                containerTop = this.element.offset().top + this.element.outerHeight() - parentOffset.top;
                break;
        }

        // Force the container to it's actual width
        this.container.css({
            top: 0,
            left: 0,
            right: 'auto'
        });


        var needingPosition = this.opens;
        var currentWindowScreen = $(window).width();
        var outerWidth = this.element.outerWidth();
        var offsetLeft = this.element.offset().left;
        var offsetCenterLeft = offsetLeft + Math.ceil(outerWidth / 2);
        var offsetCenterRight = currentWindowScreen - offsetCenterLeft;
        var widthForCalculate = this.container.find('.ranges').is(':visible') ? 680 : 500;
        var halfWidth = Math.ceil(widthForCalculate / 2);

        var availableOpeningToLeft = false;
        var availableOpeningToRight = false;
        var availableOpeningToCenter = false;

        if (offsetCenterRight >= halfWidth && offsetCenterRight >= halfWidth) {
            availableOpeningToCenter = true;
            needingPosition = 'center';
        }

        if (offsetLeft + outerWidth >= widthForCalculate) {
            availableOpeningToLeft = true;
            needingPosition = 'left';
        }

        if ($(window).width() - offsetLeft >= widthForCalculate) {
            availableOpeningToRight = true;
            needingPosition = 'right';
        }

        if (
            !(availableOpeningToCenter || availableOpeningToRight || availableOpeningToLeft) ||
            (offsetCenterLeft + offsetCenterRight) < widthForCalculate
        ) {
            this.container.addClass('daterangepicker-is-small');
            // needingPosition = 'center';
        } else {
            this.container.removeClass('daterangepicker-is-small');
        }

        $(this.container).removeClass('openscenter opensright opensleft').addClass('opens' + needingPosition);

        if (needingPosition == 'left') {
            this.container.css({
                top: containerTop,
                right: parentRightEdge - this.element.offset().left - this.element.outerWidth(),
                left: 'auto'
            });
            if (this.container.offset().left < 0) {
                this.container.css({
                    right: 'auto',
                    left: 9
                });
            }
        } else if (needingPosition == 'center') {
            this.container.css({
                top: containerTop,
                left: this.element.offset().left - parentOffset.left + this.element.outerWidth() / 2
                    - this.container.outerWidth() / 2,
                right: 'auto'
            });
            if (this.container.offset().left < 0) {
                this.container.css({
                    right: 'auto',
                    left: 9
                });
            }
        } else {
            this.container.css({
                top: containerTop,
                left: this.element.offset().left - parentOffset.left,
                right: 'auto'
            });
            if (this.container.offset().left + this.container.outerWidth() > $(window).width()) {
                this.container.css({
                    left: 'auto',
                    right: 0
                });
            }
        }
    }

    $('#daterangepicker_start_' + id + ', #daterangepicker_end_' + id).on('input', function () {
        $(this).val($(this).val().replace(/[A-Za-zА-Яа-яЁё]/, ''))
    });


    if (!autoUpdateInput) {
        console.log(autoUpdateInput);
        $('#' + id).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format(jsDateFormat));
            $(this).trigger('change');
        });
    }
}

