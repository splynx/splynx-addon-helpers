<?php

namespace splynx\assets;

use yii\web\AssetBundle;

class DataRangePickerAsset extends AssetBundle
{
    public $sourcePath = __DIR__;

    /** @var string[] */
    public $js = [
        'js/moment.min.js',
        'js/daterangepicker.min.js',
        'js/initDateRangePicker.js',
    ];

    /** @var string[] */
    public $css = [
        'css/daterangepicker.css',
        'css/customDateRangePicker.css',
    ];

    /** @var string[] */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
