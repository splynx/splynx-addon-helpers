<?php

namespace splynx\models;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\helpers\ArrayHelper;

/**
 * Class Partner
 * @package splynx\models
 * @deprecated
 */
class Partner extends BaseApiModel
{
    public $id;
    public $name;
    public $additional_attributes = [];

    public static $apiCall = 'admin/administration/partners';

    private static $_partners;

    /**
     * @return Partner[]|array
     */
    public static function getAll()
    {
        if (self::$_partners === null) {
            $result = ApiHelper::getInstance()->get(self::$apiCall);

            if ($result['result'] == false) {
                return $result['response'];
            }

            foreach ($result['response'] as $row) {
                $model = new static();
                static::populate($model, $row);
                self::$_partners[] = $model;
            }
        }

        return self::$_partners;
    }

    public static function getArray()
    {
        return ArrayHelper::map(self::getAll(), 'id', 'name');
    }
}
