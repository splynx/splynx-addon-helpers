<?php

namespace splynx\models\customer;

use CURLFile;
use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use yii\web\BadRequestHttpException;

class BaseCustomerDocument extends BaseActiveApi
{
    public $id;
    public $added_by = self::ADDED_BY_API;
    public $added_by_id;
    public $customer_id;
    public $type;
    public $title;
    public $created_at;
    public $updated_at;
    public $description;
    public $visible_by_customer;
    public $file;
    public $filename_original;
    public $filename_uploaded;
    public $code;
    public $additional_attributes = [];

    public static $apiUrl = 'admin/customers/customer-documents';

    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_API   = 'api';
    public const ADDED_BY_CUSTOMER = 'customer';
    public const ADDED_BY_SYSTEM   = 'system';

    public const TYPE_UPLOADED = 'uploaded';
    public const TYPE_GENERATED = 'generated';

    public function rules()
    {
        return [
            [['customer_id', 'type', 'title'], 'required'],
            [['title'], 'string', 'max' => 64],
            [['customer_id', 'added_by_id', 'visible_by_customer'], 'integer'],
            [['visible_by_customer'], 'default', 'value' => 0],
            [['visible_by_customer'], 'in', 'range' => [1, 0]],
            [['added_by'], 'in', 'range' => self::getRolesList()],
            [['type'], 'in', 'range' => self::getTypesList()],
        ];
    }

    /**
     * return array of adding types
     * @return array
     */
    public static function getRolesList()
    {
        return [
            self::ADDED_BY_ADMIN,
            self::ADDED_BY_API,
            self::ADDED_BY_CUSTOMER,
            self::ADDED_BY_SYSTEM,
        ];
    }

    /**
     * return array of types
     * @return array
     */
    public static function getTypesList()
    {
        return [
            self::TYPE_UPLOADED,
            self::TYPE_GENERATED,
        ];
    }

    /**
     * upload file
     * @param $filename
     * @return bool
     * @throws BadRequestHttpException
     * @throws InvalidConfigException
     */
    public function upload($filename)
    {
        if (!class_exists('CURLFile')) {
            throw new InvalidCallException('Method CURLFile not found! It exists in PHP 5.5 and higher!');
        }
        if (!$this->save()) {
            throw new BadRequestHttpException('Error while saving customer document by API!');
        }
        $result = ApiHelper::getInstance()->upload(self::$apiUrl, $this->id, [
            'file' => new CURLFile($filename),
        ]);

        return (bool)$result['result'];
    }
}
