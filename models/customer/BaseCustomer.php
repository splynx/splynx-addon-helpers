<?php

namespace splynx\models\customer;

use splynx\base\BaseActiveApi;
use yii\base\NotSupportedException;
use yii\validators\EmailValidator;
use yii\web\IdentityInterface;

/**
 * Class BaseCustomer
 * @package splynx\models\customer
 */
class BaseCustomer extends BaseActiveApi implements IdentityInterface
{
    public $id;
    public $login;
    public $partner_id;
    public $location_id;
    public $category = self::CATEGORY_PERSON;
    public $password;
    public $name;
    public $email;
    public $phone;
    public $street_1;
    public $zip_code;
    public $city;
    public $status;
    public $added_by = self::ADDED_BY_API;
    public $billing_type = self::BILLING_RECURRING;
    public $date_add;
    public $last_online;
    public $last_update;
    public $additional_attributes = [];

    public static $apiUrl = 'admin/customers/customer';

    // Categories
    public const CATEGORY_PERSON = 'person';
    public const CATEGORY_COMPANY = 'company';

    // Statuses
    public const STATUS_NEW = 'new';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_BLOCKED = 'blocked';
    public const STATUS_DOESNT_USE_SERVICES = 'disabled';

    // Billing type
    public const BILLING_RECURRING = 'recurring';
    public const BILLING_PREPAID = 'prepaid';
    public const BILLING_PREPAID_MONTHLY = 'prepaid_monthly';

    // Added by
    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_API = 'api';

    public function rules()
    {
        return [
            [['name', 'partner_id', 'location_id', 'category'], 'required'],
            ['email', 'validateEmail'],
            ['added_by', 'in', 'range' => [self::ADDED_BY_ADMIN, self::ADDED_BY_API]],
            ['billing_type', 'in', 'range' => self::getAllBillingTypes()],
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (strpos($this->$attribute, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->$attribute));
        } else {
            $emails = [$this->$attribute];
        }

        $validator = new EmailValidator();

        foreach ($emails as $email) {
            if (!$validator->validate($email)) {
                $this->addError($attribute, "$email is not a valid email.");
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return (new static())->findById($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }

    /**
     * Get all customers email
     * @return array
     */
    public function getEmailsArray()
    {
        if (empty($this->email)) {
            return [];
        }

        if (strpos($this->email, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->email));
        } else {
            $emails = [$this->email];
        }

        return $emails;
    }

    /**
     * Get all billing types
     * @return array
     */
    public static function getAllBillingTypes()
    {
        return [
            self::BILLING_PREPAID,
            self::BILLING_PREPAID_MONTHLY,
            self::BILLING_RECURRING,
        ];
    }
}
