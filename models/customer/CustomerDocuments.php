<?php

namespace splynx\models\customer;

use CURLFile;
use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\base\InvalidCallException;

/**
 * Class CustomerDocuments
 * @package splynx\models\customer
 * @deprecated
 */
class CustomerDocuments extends BaseApiModel
{
    public $id;
    public $added_by;
    public $added_by_id;
    public $customer_id;
    public $type;
    public $title;
    public $description;
    public $visible_by_customer;
    public $additional_attributes = [];

    public static $apiCall = 'admin/customers/customer-documents';

    public static function getById($id)
    {
        $result = ApiHelper::getInstance()->get(self::$apiCall . '/' . $id);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        $models = [];
        foreach ($result['response'] as $data) {
            $model = new static();
            static::populate($model, $data);
            $models[$model->id] = $data;
        }

        return $models;
    }

    /**
     * Create and upload file
     *
     * @param array $params Array with params to create document
     * @param string $filename Full path to file
     * @return bool
     */
    public static function upload($params, $filename)
    {
        if (!class_exists('CURLFile')) {
            throw new InvalidCallException('Method CURLFile not found! It exists in PHP 5.5 and higher!');
        }

        // Create document
        $result = ApiHelper::getInstance()->post(self::$apiCall, $params);
        if ($result['result'] == false || empty($result['response'])) {
            return false;
        }

        // Upload file
        $result = ApiHelper::getInstance()->upload(self::$apiCall, $result['response']['id'], [
            'file' => new CURLFile($filename),
        ]);

        return $result['result'];
    }
}
