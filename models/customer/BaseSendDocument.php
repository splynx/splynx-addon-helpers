<?php

namespace splynx\models\customer;

use splynx\base\BaseActiveApi;

/**
 * Class BaseSendDocument
 * @package splynx\models\customer
 */
class BaseSendDocument extends BaseActiveApi
{
    /** @var integer Id of document */
    public $id;
    /** @var string Type of InstantMessage model */
    public $type;
    /** @var string Document type like: invoice, request etc. */
    public $document_type;
    /** @var string Subject of message */
    public $subject;
    /** @var integer Template id */
    public $template_id;
    /** @var string Text of message */
    public $message;

    public static $apiUrl = 'admin/customers/send-documents';

    public const TYPE_MAIL = 'mail';
    public const TYPE_SMS = 'sms';
    public const TYPE_CUSTOMER_PORTAL = 'customer-portal';
    public const TYPE_CUSTOMER_PORTAL_AND_MAIL = 'customer-portal_and_mail';

    public const DOCUMENT_TYPE_DOCUMENT = 'document';
    public const DOCUMENT_TYPE_INVOICE = 'invoice';
    public const DOCUMENT_TYPE_REQUEST = 'request';
    public const DOCUMENT_TYPE_PAYMENT = 'payment';

    public const DOCUMENT_TYPE_EMPTY = 'empty';

    public function rules()
    {
        return [
            [['id', 'type', 'document_type', 'subject'], 'required'],
            [['id'], 'integer'],
            [['subject'], 'string', 'max' => 128],
            [['message'], 'string'],
            [['type'], 'in', 'range' => self::getTypesList()],
            [['document_type'], 'in', 'range' => self::getDocumentsTypeList()],
        ];
    }

    /**
     * return array of type list
     * @return array
     */
    public static function getTypesList()
    {
        return [
            self::TYPE_MAIL,
            self::TYPE_SMS,
            self::TYPE_CUSTOMER_PORTAL,
            self::TYPE_CUSTOMER_PORTAL_AND_MAIL,
        ];
    }

    /**
     * return document type list
     * @return array
     */
    public static function getDocumentsTypeList()
    {
        return [
            self::DOCUMENT_TYPE_DOCUMENT,
            self::DOCUMENT_TYPE_INVOICE,
            self::DOCUMENT_TYPE_REQUEST,
            self::DOCUMENT_TYPE_PAYMENT,
            self::DOCUMENT_TYPE_EMPTY,
        ];
    }

    public function getPrimaryKey()
    {
        return null;
    }
}
