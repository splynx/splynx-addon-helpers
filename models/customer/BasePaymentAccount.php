<?php

namespace splynx\models\customer;

use splynx\base\BaseActiveApi;
use yii\base\InvalidCallException;

/**
 * Class BasePaymentAccount
 * @package splynx\models\customer
 */
class BasePaymentAccount extends BaseActiveApi
{
    public $account_id;
    public $customer_id;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;
    public $field_6;
    public $field_7;
    public $field_8;
    public $field_9;
    public $field_10;

    public static $apiUrl = 'admin/customers/customer-payment-accounts';

    protected function getApiUrl($id = null, $conditions = [])
    {
        return static::$apiUrl . '/' . $this->customer_id . '--' . $this->account_id;
    }

    public function create($runValidation = true)
    {
        throw new InvalidCallException('Payment accounts does not has create call!');
    }

    /**
     * Because payment accounts a little bit different than ither API models
     * use this method to get customer payment account data.
     * @param integer $customerId Customer id
     * @param integer $accountId Payment account id
     * @return $this
     */
    public static function get($customerId, $accountId)
    {
        $model = new static();
        $model->customer_id = $customerId;
        $model->account_id = $accountId;
        return $model->findById($customerId);
    }
}
