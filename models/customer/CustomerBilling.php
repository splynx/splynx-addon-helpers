<?php

namespace splynx\models\customer;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;

/**
 * Class CustomerBilling
 * @package splynx\models\customer
 * @deprecated
 */
class CustomerBilling extends BaseApiModel
{
    public $customer_id;
    public $auto_pay_invoices_from_deposit;
    public $billing_city;
    public $billing_date;
    public $billing_due;
    public $billing_person;
    public $billing_street_1;
    public $billing_zip_code;
    public $deposit;
    public $enabled;
    public $grace_period;
    public $make_invoices;
    public $min_balance;
    public $payment_method;
    public $reminder_day_1;
    public $reminder_day_2;
    public $reminder_day_3;
    public $reminder_enable;
    public $reminder_payment;
    public $reminder_payment_comment;
    public $reminder_payment_value;
    public $reminder_type;
    public $request_auto_day;
    public $request_auto_enable;
    public $request_auto_next;
    public $request_auto_period;
    public $request_auto_type;
    public $type;

    public static $apiCall = 'admin/customers/customer-billing';

    public static function getById($id)
    {
        $result = ApiHelper::getInstance()->get(self::$apiCall . '/' . $id);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        $model = new static();
        static::populate($model, $result['response']);
        return $model;
    }
}
