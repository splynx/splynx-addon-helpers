<?php

namespace splynx\models\customer;

use splynx\base\BaseActiveApi;

/**
 * Class BaseCustomerBilling
 * @package splynx\models\customer
 */
class BaseCustomerBilling extends BaseActiveApi
{
    public $customer_id;
    public $blocking_period;
    public $billing_city;
    public $billing_date;
    public $billing_due;
    public $billing_person;
    public $billing_street_1;
    public $billing_zip_code;
    public $deposit;
    public $enabled;
    public $grace_period;
    public $make_invoices;
    public $min_balance;
    public $payment_method;
    public $reminder_day_1;
    public $reminder_day_2;
    public $reminder_day_3;
    public $reminder_enable;
    public $reminder_payment;
    public $reminder_payment_comment;
    public $reminder_payment_value;
    public $reminder_type;
    public $request_auto_day;
    public $request_auto_enable;
    public $request_auto_next;
    public $request_auto_period;
    public $request_auto_type;
    public $type;

    public static $apiUrl = 'admin/customers/customer-billing';

    public function getPrimaryKey()
    {
        return 'customer_id';
    }
}
