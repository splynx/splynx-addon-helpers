<?php

namespace splynx\models\console_api\customers;

use splynx\base\BaseConsoleModel;
use Yii;
use yii\validators\EmailValidator;

/**
 * Class Customer
 * @package splynx\models\console_api\config\customers
 */
class ConsoleCustomer extends BaseConsoleModel
{
    public $id;
    public $login;
    public $partner_id;
    public $location_id;
    public $category = self::CATEGORY_PERSON;
    public $password;
    public $name;
    public $email;
    public $phone;
    public $street_1;
    public $zip_code;
    public $city;
    public $status;
    public $added_by = self::ADDED_BY_API;
    public $billing_type = self::BILLING_RECURRING;
    public $date_add;
    public $last_online;
    public $last_update;
    public $additional_attributes = [];

    public static $controllerName = 'customer';

    // Categories
    public const CATEGORY_PERSON = 'person';
    public const CATEGORY_COMPANY = 'company';

    // Statuses
    public const STATUS_NEW = 'new';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_BLOCKED = 'blocked';
    public const STATUS_DOESNT_USE_SERVICES = 'disabled';

    // Billing type
    public const BILLING_RECURRING = 'recurring';
    public const BILLING_PREPAID = 'prepaid';
    public const BILLING_PREPAID_MONTHLY = 'prepaid_monthly';

    // Added by
    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_API = 'api';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['login', 'category', 'password', 'name', 'phone', 'street_1', 'zip_code', 'city', 'date_add', 'last_update', 'last_online'], 'string'],
            [['name', 'partner_id', 'location_id', 'category'], 'required'],
            ['email', 'validateEmail'],
            ['status', 'in', 'range' => static::getStatusesArray()],
            ['category', 'in', 'range' => static::getCategoriesArray()],
            ['added_by', 'in', 'range' => [static::ADDED_BY_ADMIN, static::ADDED_BY_API]],
            ['billing_type', 'in', 'range' => static::getAllBillingTypes()],
        ];
    }

    /**
     * Email validator
     * @param string $attribute
     * @param $params
     */
    public function validateEmail($attribute, $params)
    {
        if (strpos($this->$attribute, ',') !== false) {
            $emails = array_map('trim', explode(',', $this->$attribute));
        } else {
            $emails = [$this->$attribute];
        }

        $validator = new EmailValidator();
        foreach ($emails as $email) {
            if (!$validator->validate($email)) {
                $this->addError($attribute, Yii::t('app', '{email} is not a valid email.', [
                    'email' => $email,
                ]));
            }
        }
    }

    /**
     * Get all billing types
     * @return array
     */
    public static function getAllBillingTypes()
    {
        return [
            static::BILLING_PREPAID,
            static::BILLING_PREPAID_MONTHLY,
            static::BILLING_RECURRING,
        ];
    }

    /**
     * @return array of customer statuses
     */
    public static function getStatusesArray()
    {
        return [
            static::STATUS_ACTIVE,
            static::STATUS_BLOCKED,
            static::STATUS_DOESNT_USE_SERVICES,
            static::STATUS_NEW,
        ];
    }

    /**
     * @return array of customer categories
     */
    public static function getCategoriesArray()
    {
        return [
            static::CATEGORY_COMPANY,
            static::CATEGORY_PERSON,
        ];
    }
}
