<?php

namespace splynx\models\console_api\administration;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleApiKey
 * @package splynx\models\console_api\administration
 */
class ConsoleApiKey extends BaseConsoleModel
{
    public static $controllerName = 'api-keys';

    public $id;
    public $partner_id;
    public $title;
    public $key;
    public $secret;
    public $enable_logging;
    public $check_nonce;
    public $last_ip;
    public $last_dt;
    public $last_nonce;
    public $white_list = [];

    public function rules()
    {
        return [
            [['id', 'partner_id'], 'integer'],
            [['title', 'key', 'secret', 'last_ip', 'last_dt', 'last_nonce'], 'string'],
            ['title', 'required'],
            [['enable_logging', 'check_nonce'], 'boolean'],
            ['white_list', $this->getIsNewRecord() ? 'string' : 'validateArray'],
        ];
    }

    /**
     * Validate for array
     * @param $attribute
     */
    public function validateArray($attribute)
    {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, 'White list should be array!');
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave()
    {
        if ($this->partner_id !== null || $this->isNewRecord) {
            $this->partner_id = $this->partner_id == null ? 0 : $this->partner_id;
        }
    }
}
