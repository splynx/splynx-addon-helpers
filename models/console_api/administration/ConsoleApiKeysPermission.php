<?php

namespace splynx\models\console_api\administration;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleApiKeysPermission
 * Allowed only update and delete actions. When delete action we send api key id and delete all permissions for key
 * @package splynx\models\console_api\administration
 */
class ConsoleApiKeysPermission extends BaseConsoleModel
{
    public static $controllerName = 'api-keys-permissions';

    public const RULE_ALLOW = 'allow';
    public const RULE_DENY = 'deny';

    public $id;
    public $controller;
    public $module;
    public $action;
    public $rule;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['controller', 'module', 'action', 'rule'], 'string'],
            ['module', 'default', 'value' => ''],
            [['id', 'controller', 'action', 'rule'], 'required'],
            ['rule', 'in', 'range' => [static::RULE_DENY, static::RULE_ALLOW]],
        ];
    }
}
