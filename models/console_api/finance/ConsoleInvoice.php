<?php

namespace splynx\models\console_api\finance;

use Yii;
use splynx\base\BaseConsoleModel;
use splynx\models\console_api\finance\ConsoleInvoiceItemsForm;

/**
 * Class ConsoleInvoice
 * @package splynx\models\console_api\finance
 */
class ConsoleInvoice extends BaseConsoleModel
{
    public $id;
    public $customer_id;
    public $number;
    public $date_created;
    public $real_create_datetime;
    public $date_updated;
    public $date_payment;
    public $date_till;
    public $total;
    public $status = self::STATUS_NOT_PAID;
    public $payment_id;
    public $is_sent;
    public $payd_from_deposit;
    public $use_transactions;
    public $note;
    public $memo;
    public $added_by = self::ADDED_BY_API;
    public $items;

    public static $controllerName = 'invoices';

    // Statuses
    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';
    public const STATUS_DELETED = 'deleted';
    public const STATUS_PENDING = 'pending';
    public const STATUS_PAID_BY_DEPOSIT = 'paid_by_deposit';
    public const STATUS_OVERDUE = 'overdue';

    // Added by
    public const ADDED_BY_ADMIN = 'admin';
    public const ADDED_BY_API = 'api';

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'payment_id', 'is_sent', 'payd_from_deposit', 'use_transactions'], 'integer'],
            [['number', 'date_created', 'real_create_datetime', 'date_updated', 'date_payment', 'date_till', 'status', 'note', 'memo'], 'string'],
            [['total'], 'double'],
            ['items', 'validateItems'],
            ['status', 'in', 'range' => static::getStatusesArray()],
            ['added_by', 'in', 'range' => [static::ADDED_BY_ADMIN, static::ADDED_BY_API]],
        ];
    }

    /**
     * @return array of customer statuses
     */
    public static function getStatusesArray()
    {
        return [
            static::STATUS_PAID,
            static::STATUS_NOT_PAID,
            static::STATUS_DELETED,
            static::STATUS_PENDING,
            static::STATUS_PAID_BY_DEPOSIT,
            static::STATUS_OVERDUE,
        ];
    }

    /**
     * @param $customerId
     * @param null $date
     * @return string|array
     */
    public function payAllFromDeposit($customerId, $date = null)
    {
        $command = $this->getBaseConsolePath() . ' ' . static::$controllerName . '--pay-all-from-deposit --id=' . $customerId . ' --date=\'' . $date . '\'';
        $result = $this->exec($command);

        $this->checkResult($result);

        return $result['response'];
    }

    /**
     * @param $attribute
     * @param $params
     * @return bool
     */
    public function validateItems($attribute, $params)
    {
        $form = new ConsoleInvoiceItemsForm();

        foreach ($this->$attribute as $item) {
            if (!$form->load($item, '') || !$form->validate()) {
                $this->addError($attribute, json_encode($form->errors));
                return false;
            }
        }
        return true;
    }
}
