<?php

namespace splynx\models\console_api\finance;

use yii\base\Model;

class ConsoleInvoiceItemsForm extends Model
{
    public $id;
    public $invoice_id;
    public $pos;
    public $description;
    public $quantity;
    public $unit;
    public $price;
    public $tax;
    public $period_from;
    public $period_to;
    public $transaction_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'price'], 'required'],
            [['description', 'unit', 'period_from', 'period_to'], 'string'],
            [['id', 'invoice_id', 'pos', 'quantity', 'transaction_id'], 'integer'],
            [['price', 'tax'], 'double'],
            ['period_to', 'validatePeriod'],
        ];
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function validatePeriod($attribute)
    {
        if (empty($this->period_from) || empty($this->period_to)) {
            return true;
        }

        if (strtotime($this->period_to) < strtotime($this->period_from)) {
            $this->setError($attribute, 'Invalid period (date from, to)');
        }
    }
}
