<?php

namespace splynx\models\console_api\accounting;

use splynx\helpers\ArrayHelper;

/**
 * Class AccountingCustomers
 * @package splynx\models\console_api\accounting
 */
class ConsoleAccountingInvoices extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-invoices';

    /** @var int */
    public $invoice_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['invoice_id', 'integer'],
        ], parent::rules());
    }
}
