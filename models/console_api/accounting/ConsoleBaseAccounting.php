<?php

namespace splynx\models\console_api\accounting;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleBaseAccounting
 * @package splynx\models\console_api\accounting
 */
abstract class ConsoleBaseAccounting extends BaseConsoleModel
{
    public $id;
    public $modified;
    public $accounting_id;
    public $accounting_status;
    public $create_date;
    public $last_updated;
    public $additional_1;
    public $additional_2;
    public $additional_3;

    // Accounting statuses
    public const ACCOUNTING_STATUS_NEW = 0;
    public const ACCOUNTING_STATUS_PENDING = 1;
    public const ACCOUNTING_STATUS_UNKNOWN = 2;
    public const ACCOUNTING_STATUS_ERROR = 3;
    public const ACCOUNTING_STATUS_OK = 4;

    public const ACCOUNTING_NOT_MODIFIED = '0';
    public const ACCOUNTING_MODIFIED = '1';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['modified', 'in', 'range' => [static::ACCOUNTING_NOT_MODIFIED, static::ACCOUNTING_MODIFIED]],
            ['accounting_id', 'string'],
            ['accounting_status', 'in', 'range' => $this->getAccountingStatuses()],
            [['create_date', 'last_updated'], 'datetime', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            [['additional_1', 'additional_2', 'additional_3'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function getAccountingStatuses()
    {
        return [
            static::ACCOUNTING_STATUS_NEW,
            static::ACCOUNTING_STATUS_PENDING,
            static::ACCOUNTING_STATUS_UNKNOWN,
            static::ACCOUNTING_STATUS_ERROR,
            static::ACCOUNTING_STATUS_OK,
        ];
    }

    /**
     * @param string|null $startDate
     * @return mixed
     */
    public function sync($startDate = null)
    {
        return $this->customAction('sync', ['startDate' => $startDate]);
    }
}
