<?php

namespace splynx\models\console_api\accounting;

use splynx\helpers\ArrayHelper;

/**
 * Class AccountingCustomers
 * @package splynx\models\console_api\accounting
 */
class ConsoleAccountingCustomers extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-customers';

    /** @var int */
    public $customer_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['customer_id', 'integer'],
        ], parent::rules());
    }
}
