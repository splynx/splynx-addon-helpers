<?php

namespace splynx\models\console_api\accounting;

use splynx\helpers\ArrayHelper;

/**
 * Class AccountingCustomers
 * @package splynx\models\console_api\accounting
 */
class ConsoleAccountingPayments extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-payments';

    /** @var int */
    public $payment_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['payment_id', 'integer'],
        ], parent::rules());
    }
}
