<?php

namespace splynx\models\console_api\accounting;

use splynx\helpers\ArrayHelper;

/**
 * Class ConsoleAccountingCreditNotes
 * @package splynx\models\console_api\accounting
 */
class ConsoleAccountingCreditNotes extends ConsoleBaseAccounting
{
    public static $controllerName = 'accounting-credit-notes';

    /** @var int */
    public $credit_note_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['credit_note_id', 'integer'],
        ], parent::rules());
    }
}
