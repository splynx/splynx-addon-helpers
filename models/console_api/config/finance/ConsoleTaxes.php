<?php

namespace splynx\models\console_api\config\finance;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleTaxes
 * @package splynx\models\console_api\config\finance
 * @property string $typeInGroup
 * @property int $positionInGroup
 */
class ConsoleTaxes extends BaseConsoleModel
{
    public const TYPE_SINGLE = 'single';
    public const TYPE_GROUP = 'group';

    public const TAX_TYPE_NET = 'net';
    public const TAX_TYPE_TAX = 'tax';
    public const TAX_TYPE_COMPOUND_WITH_ONE_PREVIOUS = 'compound_with_one_previous';
    public const TAX_TYPE_COMPOUND_WITH_ALL_PREVIOUS = 'compound_with_all_previous';

    public static $controllerName = 'taxes';

    public int $id = 0;
    public ?string $name = null;
    public float $rate = 0;
    public string $type = self::TYPE_SINGLE;
    public bool $archived = false;
    public ?int $accounting_tax_rates_id = null;
    public ?int $einvoicing_tax_rates_id = null;
    public array $items = [];

    private ?string $_type_in_group = null;
    private ?int $_position_in_group = null;

    /**
     * Set attributes from  array to model properties.
     *
     * @param static $model
     * @param array<string, mixed> $data
     * @return void
     */
    public static function populate($model, $data): void
    {
        foreach ($data as $name => $value) {
            if (property_exists($model, $name)) {
                $model->$name = $value;
            }
        }
        $model->_oldAttributes = $model->attributes;

        if ($model->type === self::TYPE_GROUP && isset($data['items'])) {
            $model->items = [];
            foreach ($data['items'] as $item) {
                $singleTax = (new ConsoleTaxes())->findByPk(['id' => $item['id']]);
                if ($singleTax === null) {
                    continue;
                }

                $singleTax->setTypeInGroup($item['type']);
                $singleTax->setPositionInGroup($item['position']);
                $model->items[] = $singleTax;
            }
        }
    }

    /**
     * @return string
     */
    public function getTypeInGroup(): string
    {
        return $this->_type_in_group;
    }

    /**
     * @param string $type
     * @return void
     */
    public function setTypeInGroup(string $type): void
    {
        $this->_type_in_group = $type;
    }

    /**
     * @return int
     */
    public function getPositionInGroup(): int
    {
        return $this->_position_in_group;
    }

    /**
     * @param int $position
     * @return void
     */
    public function setPositionInGroup(int $position): void
    {
        $this->_position_in_group = $position;
    }
}
