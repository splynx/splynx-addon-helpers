<?php

namespace splynx\models\console_api\config\finance;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsolePaymentMethod
 * @package splynx\models\console_api\config\finance
 */
class ConsolePaymentMethod extends BaseConsoleModel
{
    public static $controllerName = 'payment-methods';

    public $id;
    public $name;
    public $is_active;
    public $name_1;
    public $name_2;
    public $name_3;
    public $name_4;
    public $name_5;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name_1', 'name_2', 'name_3', 'name_4', 'name_5'], 'string'],
            ['id', 'integer'],
            ['is_active', 'boolean'],
            [['name', 'is_active'], 'required'],
        ];
    }
}
