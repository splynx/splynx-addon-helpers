<?php

namespace splynx\models\console_api\config\finance;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsolePaymentAccount
 * @package splynx\models\console_api\config\finance
 */
class ConsolePaymentAccount extends BaseConsoleModel
{
    public static $controllerName = 'payment-accounts';

    public $id;
    public $title;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;
    public $field_6;
    public $field_7;
    public $field_8;
    public $field_9;
    public $field_10;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['title', 'string'],
            [['title', 'field_1'], 'required'],
            [
                [
                    'field_1',
                    'field_2',
                    'field_2',
                    'field_4',
                    'field_5',
                    'field_6',
                    'field_7',
                    'field_8',
                    'field_9',
                    'field_10',
                ],
                'string',
                'min' => 1,
                'max' => 128,
            ],
        ];
    }
}
