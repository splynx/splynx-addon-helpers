<?php

namespace splynx\models\console_api\config;

use splynx\base\BaseConsoleModel;
use BadFunctionCallException;

/**
 * ```php
 * examples:
 *  - list all values from 'path' => 'system', 'module' => 'main' (path & system are required)
 * $values = (new ConsoleSplynxConfig())->findAll(['path' => 'system', 'module' => 'main']);
 * var_dump($values);
 *
 *  - get and update value (path , system & key are required)
 * $value = (new ConsoleSplynxConfig())->findOne(['path' => 'system', 'module' => 'main', 'key' => 'date_format']);
 * if(!$value){
 * print "not found\n"; die;
 *   }
 * $value->value = 'Y-m-d';
 * if(!$value->save()){
 * print "Error\n";
 * var_dump($value->getErrors());
 * die;
 *  }
 * ```
 *
 * Class ConsoleSplynxConfig
 * @package splynx\models\console_api\config
 */
class ConsoleSplynxConfig extends BaseConsoleModel
{
    public static $controllerName = 'config';
    protected static $primaryKeys = ['module', 'path', 'key'];

    public $module;
    public $path;
    public $partner_id;
    public $key;
    public $value;
    public $type;

    public const TYPE_BOOL = 'boolean';
    public const TYPE_INT = 'integer';
    public const TYPE_FLOAT = 'float';
    public const TYPE_EMAIL = 'email';
    public const TYPE_SELECT = 'select';
    public const TYPE_MULTIPLE_SELECT = 'array';
    public const TYPE_STRING = 'string';
    public const TYPE_HIDDEN = 'hidden';
    public const TYPE_ENCRYPTED = 'encrypted';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['partner_id', 'integer'],
            [['module', 'path', 'key', 'type'], 'string'],
            [['module', 'path', 'key'], 'required'],
            ['value', 'validateValue'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateValue($attribute, $params)
    {
        if (empty($this->{$attribute}) || empty($this->type)) {
            return;
        }
        switch ($this->type) {
            case self::TYPE_BOOL:
                if (!filter_var($this->{$attribute}, FILTER_VALIDATE_BOOLEAN)) {
                    $this->addError($attribute, 'Invalid boolean value!');
                }
                break;
            case self::TYPE_INT:
                if (!filter_var($this->{$attribute}, FILTER_VALIDATE_INT)) {
                    $this->addError($attribute, 'Invalid integer value!');
                }
                break;
            case self::TYPE_FLOAT:
                if (!filter_var($this->{$attribute}, FILTER_VALIDATE_FLOAT)) {
                    $this->addError($attribute, 'Invalid float value!');
                }
                break;
            case self::TYPE_EMAIL:
                if (strpos($this->{$attribute}, ',') !== false) {
                    $emails = explode(',', $this->{$attribute});
                    foreach ($emails as $email) {
                        if (!filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
                            $this->addError($attribute, 'Invalid email value!');
                            return;
                        }
                    }
                } elseif (!filter_var($this->{$attribute}, FILTER_VALIDATE_EMAIL)) {
                    $this->addError($attribute, 'Invalid email value!');
                }
                break;
            case self::TYPE_MULTIPLE_SELECT:
                if (!is_array($this->{$attribute})) {
                    $this->addError($attribute, 'Invalid value for multiple select!');
                }
                break;
            case self::TYPE_SELECT:
            case self::TYPE_STRING:
            case self::TYPE_HIDDEN:
            case self::TYPE_ENCRYPTED:
            default:
                if (is_array($this->{$attribute}) || is_object($this->{$attribute})) {
                    $this->addError($attribute, "Invalid value for type $this->type!");
                }
        }
    }

    public function delete()
    {
        throw new BadFunctionCallException('Deleting is not allowed');
    }

    public function create($runValidation = true)
    {
        throw new BadFunctionCallException('Creating is not allowed');
    }

    /**
     * @inheritdoc
     */
    public function findOne(array $primaryKeys, array $additionalAttributes = [], $order = [])
    {
        return parent::findByPk($primaryKeys);
    }

    /**
     * @inheritdoc
     */
    public function findAll(array $mainAttributes, array $additionalAttributes = [], array $order = [], $limit = null)
    {
        $command = $this->createUrl(static::ACTION_INDEX, $mainAttributes);
        $result = $this->exec($command);
        $this->checkResult($result);

        if ($result['result'] == false || empty($result['response'])) {
            return [];
        }

        $models = [];
        foreach ($result['response'] as $item) {
            $model = new static();
            static::populate($model, $item);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * @inheritdoc
     */
    public function update($runValidation = true)
    {
        $attributes = $this->getAttributes($this->attributes());
        // Check changed attributes for send
        if (empty($this->_oldAttributes)) {
            $this->addError('main', 'Can\'t check primary keys for changed values');
        }
        foreach ($attributes as $name => $value) {
            if ((array_key_exists($name, $this->_oldAttributes) && $value !== $this->_oldAttributes[$name]) && $name != 'value') {
                $this->addError($name, "{$name} can't be changed!");
                return false;
            }
        }
        return parent::update($runValidation);
    }
}
