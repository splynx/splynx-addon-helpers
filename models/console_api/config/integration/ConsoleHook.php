<?php

namespace splynx\models\console_api\config\integration;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleHook
 * @package splynx\models\console_api\config\integration
 */
class ConsoleHook extends BaseConsoleModel
{
    public static $controllerName = 'hooks';

    public $id;
    public $title;
    public $enabled = true;
    public $type = self::TYPE_CLI;
    public $path;
    public $url;
    public $content_type;
    public $secret;
    public $queue;

    public const TYPE_CLI = 'cli';
    public const TYPE_WEB = 'web';
    public const TYPE_QUEUE = 'queue';

    public const CONTENT_TYPE_JSON = 'application/json';
    public const CONTENT_TYPE_FORM_URLENCODED = 'application/x-www-form-urlencoded';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['type', 'content_type', 'queue'], 'string'],
            ['path', 'string', 'max' => 256],
            ['queue', 'string', 'max' => 256],
            ['title', 'string', 'max' => 32, 'min' => 3],
            ['enabled', 'boolean'],
            [['title', 'type'], 'required'],
            ['url', 'url'],
            [['url', 'content_type', 'secret'], 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_WEB;
            },],
            ['path', 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_CLI;
            },],
            ['path', 'validatePath'],
            ['queue', 'required', 'when' => function ($model) {
                return $model->type == static::TYPE_QUEUE;
            },],
            ['type', 'in', 'allowArray' => true, 'range' => $this->getTypes()],
            ['content_type', 'in', 'allowArray' => true, 'range' => [
                static::CONTENT_TYPE_JSON,
                static::CONTENT_TYPE_FORM_URLENCODED,
            ],
            ],
        ];
    }

    /**
     * @param string $attribute
     * @return bool
     */
    public function validatePath($attribute)
    {
        if ($this->type != static::TYPE_CLI) {
            return true;
        }

        $filename = $this->$attribute;

        // Check if there are some params if path
        if (($pos = strpos($filename, ' ')) !== false) {
            $filename = substr($filename, 0, $pos);
        }

        if ($filename[0] != '/') {
            $this->addError($attribute, 'Path must be absolute!');
            return false;
        }
        if (!file_exists($filename)) {
            $this->addError($attribute, 'File not exists!');
            return false;
        }
        if (!is_file($filename)) {
            $this->addError($attribute, 'Invalid file!');
            return false;
        }
        if (!is_executable($filename)) {
            $this->addError($attribute, 'Invalid file!');
            return false;
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave()
    {
        if ($this->content_type === null) {
            $this->content_type = static::CONTENT_TYPE_FORM_URLENCODED;
        }
    }

    /**
     * @return array
     */
    private function getTypes()
    {
        return [static::TYPE_WEB, static::TYPE_CLI, static::TYPE_QUEUE];
    }
}
