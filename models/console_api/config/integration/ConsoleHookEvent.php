<?php

namespace splynx\models\console_api\config\integration;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleHookEvent
 * @package splynx\models\console_api\config\integration
 */
class ConsoleHookEvent extends BaseConsoleModel
{
    public static $controllerName = 'hook-events';
    protected static $primaryKeys = ['id', 'model', 'action'];

    public $id;
    public $model;
    public $action;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['model', 'action'], 'string'],
            [['id', 'model', 'action'], 'required'],
        ];
    }
}
