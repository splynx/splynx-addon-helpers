<?php

namespace splynx\models\console_api\config\integration;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleModuleEntryPoint
 * @package splynx\models\console_api\config\integration
 */
class ConsoleModuleEntryPoint extends BaseConsoleModel
{
    public static $controllerName = 'module-entry-points';

    // Place constant
    public const PLACE_ADMIN = 'admin';
    public const PLACE_PORTAL = 'portal';

    // Constant for content
    public const LOCATION_AFTER_CONTENT = 'after_content';
    public const LOCATION_BEFORE_CONTENT = 'before_content';

    // Type constant
    public const TYPE_MENU_LINK = 'menu_link';
    public const TYPE_CODE = 'code';
    public const TYPE_ACTION_LINK = 'action_link';
    public const TYPE_TAB = 'tab';

    public $id;
    public $module;
    public $place = self::PLACE_ADMIN;
    public $type = self::TYPE_MENU_LINK;
    public $title;
    public $root;
    public $model;
    public $icon = ConsoleModuleConfig::DEFAULT_ICON;
    public $background;
    public $url;
    public $code;
    public $partners;
    public $name;
    public $location = self::LOCATION_AFTER_CONTENT;
    public $enabled = 1;

    public $location_geo;
    public $payment_type;
    public $payment_account_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [
                [
                    'module',
                    'place',
                    'type',
                    'title',
                    'root',
                    'model',
                    'icon',
                    'background',
                    'url',
                    'code',
                    'name',
                    'location',
                    'payment_account_id',
                ],
                'string',
            ],
            [['module', 'place', 'type', 'name'], 'required'],
            ['type', 'in', 'allowArray' => true, 'range' => $this->getTypes()],
            ['place', 'in', 'allowArray' => true, 'range' => [static::PLACE_ADMIN, static::PLACE_PORTAL]],
            ['location', 'in', 'allowArray' => true, 'range' => [static::LOCATION_AFTER_CONTENT, static::LOCATION_BEFORE_CONTENT]],
            [['partners', 'location_geo', 'payment_type'], 'validateArray'],
            ['enabled', 'boolean'],
            ['root', 'required', 'when' => function ($model) {
                return $model->type !== static::TYPE_ACTION_LINK;
            },],
            ['model', 'required', 'when' => function ($model) {
                return $model->type === static::TYPE_ACTION_LINK;
            },],
            ['url', 'required', 'when' => function ($model) {
                return $model->type !== static::TYPE_CODE;
            },],
            ['code', 'required', 'when' => function ($model) {
                return $model->type === static::TYPE_CODE;
            },],
        ];
    }

    /**
     * @return array
     */
    private function getTypes()
    {
        return [static::TYPE_ACTION_LINK, static::TYPE_CODE, static::TYPE_MENU_LINK, static::TYPE_TAB];
    }

    public function validateArray($attribute)
    {
        if (!is_array($this->$attribute)) {
            $this->addError($attribute, $attribute . ' should be array!');
            return false;
        }
        return true;
    }
}
