<?php

namespace splynx\models\console_api\einvoicing;

use splynx\base\BaseConsoleModel;

/**
 * Class ConsoleBaseEInvoicing
 * @package splynx\models\console_api\einvoicing
 *
 * @property-read array|int[] $eInvoicingStatuses
 */
abstract class ConsoleBaseEInvoicing extends BaseConsoleModel
{
    public $id;
    public $customer_id;
    public $modified;
    public $einvoicing_id;
    public $einvoicing_status;
    public $create_date;
    public $updated_date;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;

    // Einvoicing statuses
    public const EINVOICING_STATUS_NEW = 0;
    public const EINVOICING_STATUS_PENDING = 1;
    public const EINVOICING_STATUS_UNKNOWN = 2;
    public const EINVOICING_STATUS_ERROR = 3;
    public const EINVOICING_STATUS_OK = 4;

    public const EINVOICING_NOT_MODIFIED = '0';
    public const EINVOICING_MODIFIED = '1';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['modified', 'in', 'range' => [static::EINVOICING_NOT_MODIFIED, static::EINVOICING_MODIFIED]],
            ['einvoicing_id', 'string'],
            ['einvoicing_status', 'in', 'range' => $this->getEInvoicingStatuses()],
            [['create_date', 'updated_date'], 'datetime', 'format' => 'yyyy-MM-dd HH:mm:ss'],
            [['field_1', 'field_2', 'field_3', 'field_4', 'field_5'], 'string', 'max' => 256],
        ];
    }

    /**
     * @return array
     */
    public function getEInvoicingStatuses()
    {
        return [
            static::EINVOICING_STATUS_NEW,
            static::EINVOICING_STATUS_PENDING,
            static::EINVOICING_STATUS_UNKNOWN,
            static::EINVOICING_STATUS_ERROR,
            static::EINVOICING_STATUS_OK,
        ];
    }

    /**
     * @param string|null $startDate
     * @return mixed
     */
    public function sync($startDate = null)
    {
        return $this->customAction('sync', ['startDate' => $startDate]);
    }
}
