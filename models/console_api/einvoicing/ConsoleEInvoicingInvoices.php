<?php

namespace splynx\models\console_api\einvoicing;

use splynx\helpers\ArrayHelper;

/**
 * Class ConsoleEInvoicingInvoices
 * @package splynx\models\console_api\einvoicing
 */
class ConsoleEInvoicingInvoices extends ConsoleBaseEInvoicing
{
    public static $controllerName = 'e-invoicing-invoices';

    /** @var int */
    public $invoice_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['invoice_id', 'integer'],
        ], parent::rules());
    }
}
