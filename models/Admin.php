<?php

namespace splynx\models;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\base\NotSupportedException;

/**
 * Class Admin
 * @package splynx\models
 * @deprecated
 */
class Admin extends BaseApiModel implements \yii\web\IdentityInterface
{
    public $id;
    public $login;
    public $password;
    public $partner_id;
    public $role_name;
    public $name;
    public $email;
    public $timeout;
    public $last_ip;
    public $last_dt;
    public $filter_partner_id;
    public $router_access;
    public $otp_secret;
    public $additional_attributes = [];

    public static $apiCall = 'admin/administration/administrators';

    private static $_admins = [];

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $result = ApiHelper::getInstance()->search(self::$apiCall, [
            'main_attributes' => [
                'id' => $id,
            ],
        ]);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        self::$_admins = $result['response'];

        foreach (self::$_admins as $admin) {
            if (strcasecmp($admin['id'], $id) === 0) {
                $model = new static();
                static::populate($model, $admin);
                return $model;
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param  string $login
     * @return static|null
     */
    public static function findByUsername($login)
    {
        $result = ApiHelper::getInstance()->search(self::$apiCall, [
            'main_attributes' => [
                'login' => $login,
            ],
        ]);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        self::$_admins = $result['response'];

        foreach (self::$_admins as $admin) {
            if (strcasecmp($admin['login'], $login) === 0) {
                $model = new static();
                static::populate($model, $admin);
                return $model;
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function checkEnabledCashDesk()
    {
        if (!isset($this->additional_attributes['cashdesk']) || $this->additional_attributes['cashdesk'] == false) {
            return false;
        } else {
            return true;
        }
    }

    public function getDeposit()
    {
        return isset($this->additional_attributes['cashdesk_deposit']) ? $this->additional_attributes['cashdesk_deposit'] : 0;
    }

    public function getCredit()
    {
        return isset($this->additional_attributes['cashdesk_credit']) ? $this->additional_attributes['cashdesk_credit'] : 0;
    }

    public function getBalance()
    {
        return $this->getDeposit() + $this->getCredit();
    }

    public function withdrawMoney($amount)
    {
        $deposit = $this->getDeposit();
        $credit = $this->getCredit();

        if ($amount <= $deposit) {
            $deposit = $deposit - $amount;
        } else {
            $credit = $credit - ($amount - $deposit);
            $deposit = 0;
        }

        $params = [
            'additional_attributes' => [
                'cashdesk_deposit' => $deposit,
                'cashdesk_credit' => $credit,
            ],
        ];

        $result = ApiHelper::getInstance()->put(static::$apiCall, $this->id, $params);

        if ($result['result']) {
            $this->additional_attributes['cashdesk_deposit'] = $deposit;
            $this->additional_attributes['cashdesk_credit'] = $credit;
        }

        return $result['result'];
    }
}
