<?php

namespace splynx\models\administration;

use splynx\base\BaseActiveApi;

/**
 * Class BasePartner
 * @package splynx\models\administration
 */
class BasePartner extends BaseActiveApi
{
    public $id;
    public $name;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/partners';

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
