<?php

namespace splynx\models\administration;

use splynx\base\BaseActiveApi;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Class BaseAdministrator
 * @package splynx\models\administration
 */
class BaseAdministrator extends BaseActiveApi implements IdentityInterface
{
    public $id;
    public $login;
    public $password;
    public $name;
    public $email;
    public $phone;
    public $timeout;
    public $role_name;
    public $router_access;
    public $partner_id;
    public $otp_secret;
    public $last_ip;
    public $last_dt;
    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/administrators';

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return (new static())->findById($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
