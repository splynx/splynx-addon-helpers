<?php

namespace splynx\models\administration;

use splynx\base\BaseActiveApi;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * Class BaseLocation
 * @package splynx\models\administration
 */
class BaseLocation extends BaseActiveApi
{
    public $id;
    public $name;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/administration/locations';

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
