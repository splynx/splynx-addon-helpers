<?php

namespace splynx\models\services;

/**
 * Class BaseCustomService
 * @package splynx\models\services
 */
class BaseCustomService extends BaseService
{
    public function getServiceApiUrl()
    {
        return 'custom-services';
    }
}
