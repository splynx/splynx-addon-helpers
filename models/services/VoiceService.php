<?php

namespace splynx\models\services;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\helpers\Html;

/**
 * Class VoiceService
 * @package splynx\models\services
 * @deprecated
 */
class VoiceService extends BaseApiModel
{
    public $id;
    public $type;
    public $voice_device_id;
    public $phone;
    public $parent_id;
    public $customer_id;
    public $tariff_id;
    public $description;
    public $quantity;
    public $unit;
    public $unit_price;
    public $start_date;
    public $end_date;
    public $discount;
    public $discount_percent;
    public $discount_start_date;
    public $discount_end_date;
    public $discount_text;
    public $status;
    public $status_new;
    public $additional_attributes = [];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_STOPPED = 'stopped';
    public const STATUS_PENDING = 'pending';

    public static $baseApiCall = 'admin/customers/customer';

    public static $serviceApiCall = 'voice-services';

    public static function getApiCall($customer_id, $service_id = null)
    {
        $uri = self::$baseApiCall . '/' . $customer_id . '/' . self::$serviceApiCall;

        if ($service_id !== null) {
            $uri .= '--' . $service_id;
        }

        return $uri;
    }

    public static $services;

    /**
     * @param $customer_id
     * @return VoiceService[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function findByCustomerId($customer_id)
    {
        if (self::$services === null || !isset(self::$services[$customer_id])) {
            $result = ApiHelper::getInstance()->get(self::getApiCall($customer_id));

            if ($result['result'] == false || empty($result['response'])) {
                self::$services[$customer_id] = [];
            } else {
                foreach ($result['response'] as $serviceData) {
                    $service = new self();
                    static::populate($service, $serviceData);
                    self::$services[$customer_id][$service->id] = $service;
                }
            }
        }

        return self::$services[$customer_id];
    }

    public static function findOneByCustomerIdAndId($customer_id, $service_id)
    {
        $services = self::findByCustomerId($customer_id);
        if (empty($services)) {
            return [];
        }

        foreach ($services as $serviceId => $service) {
            if ($serviceId == $service_id) {
                return $service;
            }
        }

        return [];
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE:
                $class = 'primary';
                break;
            case self::STATUS_PENDING:
                $class = 'warning';
                break;
            case self::STATUS_STOPPED:
                $class = 'danger';
                break;
            case self::STATUS_DISABLED:
            default:
                $class = 'default';
                break;
        }
        return Html::tag('span', $this->status, [
            'class' => 'label label-' . $class,
        ]);
    }
}
