<?php

namespace splynx\models\services;

/**
 * Class BaseInternetService
 * @package splynx\models\services
 */
class BaseInternetService extends BaseService
{
    public $router_id;
    public $sector_id;
    public $login;
    public $password;
    public $taking_ipv4;
    public $ipv4;
    public $ipv4_route;
    public $ipv4_pool_id;
    public $mac;
    public $port_id;

    public const STATUS_ONLINE = 'online';

    public function getServiceApiUrl()
    {
        return 'internet-services';
    }
}
