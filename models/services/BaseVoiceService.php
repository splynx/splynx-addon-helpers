<?php

namespace splynx\models\services;

/**
 * Class BaseVoiceService
 * @package splynx\models\services
 */
class BaseVoiceService extends BaseService
{
    public $voice_device_id;
    public $phone;

    public function getServiceApiUrl()
    {
        return 'voice-services';
    }
}
