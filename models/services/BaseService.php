<?php

namespace splynx\models\services;

use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;

/**
 * Class BaseService
 * @package splynx\models\services
 */
abstract class BaseService extends BaseActiveApi
{
    public $id;
    public $parent_id;
    public $customer_id = 0;
    public $tariff_id;
    public $status = self::STATUS_ACTIVE;
    public $status_new;
    public $description;
    public $quantity = 1;
    public $unit;
    public $unit_price;
    public $start_date;
    public $end_date;
    public $discount;
    public $discount_percent;
    public $discount_start_date;
    public $discount_end_date;
    public $discount_text;
    public $bundle_service_id;

    public $additional_attributes = [];

    public const STATUS_ACTIVE = 'active';
    public const STATUS_DISABLED = 'disabled';
    public const STATUS_STOPPED = 'stopped';
    public const STATUS_PENDING = 'pending';

    public static $apiUrl = 'admin/customers/customer';

    protected function getApiUrl($id = null, $conditions = [])
    {
        $result = self::$apiUrl . '/' . $this->customer_id . '/' . $this->getServiceApiUrl();

        // Set id
        if ($id !== null) {
            $result .= '--' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * Method is overwritten because services has own logic (SPL-1117)
     * @param $id
     * @return null|BaseActiveApi
     */
    public function findById($id)
    {
        return parent::findOne([
            'id' => $id,
        ]);
    }

    abstract protected function getServiceApiUrl();

    /**
     * Delete service. Method is overwritten because in services we use custom URLs
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function delete()
    {
        $result = ApiHelper::getInstance()->delete($this->getApiUrl($this->id), null);
        return $result['result'];
    }
}
