<?php

namespace splynx\models\tariffs;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class BaseInternetTariff
 * @package splynx\models\tariffs
 */
class BaseInternetTariff extends BaseTariff
{
    public $speed_download;
    public $speed_upload;
    public $speed_limit_at = 10;
    public $aggregation = 1;
    public $burst_limit = 0;
    public $burst_threshold = 0;
    public $burst_time = 0;
    public $services_online;
    public $priority = self::PRIORITY_NORMAL;

    public const PRIORITY_LOW = 'low';
    public const PRIORITY_NORMAL = 'normal';
    public const PRIORITY_HIGH = 'high';

    public static $apiUrl = 'admin/tariffs/internet';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['speed_limit_at', 'default', 'value' => 10],
            ['aggregation', 'default', 'value' => 1],
            ['burst_limit', 'default', 'value' => 0],
            ['burst_threshold', 'default', 'value' => 0],
            ['burst_time', 'default', 'value' => 0],
            [['priority'], 'string'],
            ['priority', 'in', 'range' => array_keys(static::getPrioritiesArray())],
            [['speed_download', 'speed_upload', 'burst_limit', 'burst_threshold', 'burst_time'], 'integer'],
            [['speed_download', 'speed_upload'], 'required'],
        ], parent::rules());
    }

    /**
     * Return priorities array
     * @return array
     */
    public static function getPrioritiesArray()
    {
        return [
            static::PRIORITY_LOW => Yii::t('app', 'Low'),
            static::PRIORITY_NORMAL => Yii::t('app', 'Normal'),
            static::PRIORITY_HIGH => Yii::t('app', 'High'),
        ];
    }
}
