<?php

namespace splynx\models\tariffs;

/**
 * Class BaseCustomTariff
 * @package splynx\models\tariffs
 */
class BaseCustomTariff extends BaseTariff
{
    public static $apiUrl = 'admin/tariffs/custom';
}
