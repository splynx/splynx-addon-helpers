<?php

namespace splynx\models\tariffs;

use splynx\base\BaseActiveApi;
use splynx\models\customer\BaseCustomer;
use Yii;

/**
 * Class BaseTariff
 * @package splynx\models\tariffs
 */
abstract class BaseTariff extends BaseActiveApi
{
    public $id;
    public $price;
    public $title;
    public $with_vat;
    public $vat_percent;
    public $services;
    public $service_name;
    public $available_for_services;
    public $update_service_price;
    public $update_service_name;
    public $force_update_service_price;
    public $force_update_service_name;
    public $tariffs_for_change = [];
    public $partners_ids = [];
    public $additional_attributes = [];
    public $billing_types = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'service_name'], 'string'],
            [['price'], 'number'],
            ['with_vat', 'boolean'],
            ['with_vat', 'default', 'value' => true],
            [['vat_percent'], 'number', 'min' => 0, 'max' => 100],
            [['available_for_services', 'update_service_price', 'force_update_service_price', 'update_service_name', 'force_update_service_name'], 'boolean'],
            [['title', 'with_vat', 'partners_ids', 'billing_types'], 'required'],
            [['partners_ids'], 'each', 'rule' => ['required']],
            ['billing_types', 'validateBillingTypes'],
        ];
    }

    /**
     * @param string $attribute
     */
    public function validateBillingTypes($attribute)
    {
        foreach ($this->{$attribute} as $type) {
            if (!in_array($type, BaseCustomer::getAllBillingTypes())) {
                $this->addError($attribute, Yii::t('customers', 'Invalid billing type {type}!', ['type' => $type]));
                break;
            }
        }
    }
}
