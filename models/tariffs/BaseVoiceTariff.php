<?php

namespace splynx\models\tariffs;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class BaseVoiceTariffs
 * @package splynx\models\tariffs
 */
class BaseVoiceTariff extends BaseTariff
{
    public $type = self::TYPE_VOIP;

    public const TYPE_VOIP = 'voip';
    public const TYPE_MOBILE = 'Mobile';
    public const TYPE_FIX = 'fix';

    public static $apiUrl = 'admin/tariffs/voice';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            ['type', 'default', 'value' => self::TYPE_VOIP],
            ['type', 'in', 'range' => array_keys(static::getTypesArray())],
            [['type'], 'required'],
        ], parent::rules());
    }

    /**
     * Return array with types
     * @return array
     */
    public static function getTypesArray()
    {
        return [
            static::TYPE_VOIP => Yii::t('app', 'VoIP'),
            static::TYPE_FIX => Yii::t('app', 'Fix'),
            static::TYPE_MOBILE => Yii::t('app', 'Mobile'),
        ];
    }
}
