<?php

namespace splynx\models\tariffs;

use splynx\base\BaseActiveApi;
use splynx\models\customer\BaseCustomer;
use Yii;

/**
 * @see \models\admin\tariffs\Bundle Splynx model
 *
 * @property integer $id
 * @property string $title
 * @property string $service_description
 * @property double $price
 * @property integer $customers
 * @property integer $services
 * @property boolean $with_vat
 * @property boolean $available_for_services
 * @property double $vat_percent
 * @property array $partner_ids
 * @property array $billing_types
 * @property double $activation_fee
 * @property string $get_activation_fee_when
 * @property boolean $issue_invoice_while_service_creation
 * @property integer $contract_duration
 * @property boolean $automatic_renewal
 * @property boolean $auto_reactivate
 * @property double $cancellation_fee
 * @property double $prior_cancellation_fee
 * @property double $change_to_other_bundle_fee
 * @property integer $discount_period
 * @property double $discount_percent
 * @property array $internet_tariffs
 * @property array $voice_tariffs
 * @property array $custom_tariffs
 * @property array $additional_attributes
 * @property string $apiUrl
 */
class BaseBundleTariff extends BaseActiveApi
{
    public $id;
    public $title;
    public $service_description;
    public $price;
    public $customers;
    public $services;
    public $with_vat;
    public $available_for_services;
    public $vat_percent;
    public $partner_ids = [];
    public $billing_types = [];
    public $activation_fee;
    public $get_activation_fee_when;
    public $issue_invoice_while_service_creation;
    public $contract_duration;
    public $automatic_renewal;
    public $auto_reactivate;
    public $cancellation_fee;
    public $prior_cancellation_fee;
    public $change_to_other_bundle_fee;
    public $discount_period;
    public $discount_percent;

    public $internet_tariffs = [];
    public $voice_tariffs = [];
    public $custom_tariffs = [];
    public $additional_attributes = [];

    public const ACTIVATION_FEE_WHEN_FIRST_SERVICE_BILLING = 'first_service_billing';
    public const ACTIVATION_FEE_WHEN_CREATE_SERVICE = 'create_service';

    public static $apiUrl = 'admin/tariffs/bundle';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'partner_ids', 'billing_types', 'billing_types'], 'required'],
            [['title', 'service_description'], 'string'],
            [['price', 'activation_fee', 'cancellation_fee', 'prior_cancellation_fee', 'change_to_other_bundle_fee'], 'number'],
            [['with_vat', 'available_for_services', 'issue_invoice_while_service_creation', 'automatic_renewal', 'auto_reactivate'], 'boolean'],
            [['vat_percent', 'discount_percent'], 'number', 'min' => 0],
            ['billing_types', 'in', 'range' => BaseCustomer::getAllBillingTypes(), 'allowArray' => true],
            ['get_activation_fee_when', 'in', 'range' => array_keys(self::getActivationFeeWhenArray())],
            [['contract_duration', 'discount_period', 'customers', 'services'], 'integer', 'min' => 0],
            [['partner_ids', 'internet_tariffs', 'voice_tariffs', 'custom_tariffs'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * Get all activation fee conditions
     * @return array
     */
    public static function getActivationFeeWhenArray()
    {
        return [
            self::ACTIVATION_FEE_WHEN_FIRST_SERVICE_BILLING => Yii::t('app', 'first service billing'),
            self::ACTIVATION_FEE_WHEN_CREATE_SERVICE => Yii::t('app', 'create service'),
        ];
    }
}
