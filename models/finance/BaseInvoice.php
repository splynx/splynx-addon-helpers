<?php

namespace splynx\models\finance;

/**
 * Class BaseInvoice
 * @package splynx\models\finance
 */
class BaseInvoice extends BaseFinanceClass
{
    public $id;
    public $customer_id;
    public $date_created;
    public $date_payment;
    public $date_till;
    public $date_updated;
    public $disable_cache;
    public $memo;
    public $note;
    public $number;
    public $payment_id;
    public $status;
    public $total;
    public $due;
    public $use_transactions;
    public $payd_from_deposit;
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/invoices';

    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';
    public const STATUS_DELETED = 'deleted';
    public const STATUS_PENDING = 'pending';
}
