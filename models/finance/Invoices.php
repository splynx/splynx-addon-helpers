<?php

namespace splynx\models\finance;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\base\UserException;
use Yii;

/**
 * Class Invoices
 * @package splynx\models\finance
 * @deprecated
 */
class Invoices extends BaseApiModel
{
    public $id;
    public $customer_id;
    public $date_created;
    public $date_payment;
    public $date_till;
    public $date_updated;
    public $disable_cache;
    public $memo;
    public $note;
    public $number;
    public $payment_id;
    public $status;
    public $total;
    public $use_transactions;
    public $payd_from_deposit;
    public $items;
    public $additional_attributes = [];

    public static $apiCall = 'admin/finance/invoices';

    private static $_invoices;

    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';
    public const STATUS_DELETED = 'deleted';

    /**
     * @param $customer_id
     * @return Invoices[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function findByCustomerId($customer_id)
    {
        if (self::$_invoices === null || !isset(self::$_invoices[$customer_id])) {
            $params = [
                'customer_id' => $customer_id,
            ];
            $result = ApiHelper::getInstance()->search(self::$apiCall, [
                'main_attributes' => $params,
            ]);

            if ($result['result'] == false || empty($result['response'])) {
                self::$_invoices[$customer_id] = [];
            } else {
                foreach ($result['response'] as $invoiceData) {
                    $invoice = new static();
                    static::populate($invoice, $invoiceData);
                    self::$_invoices[$customer_id][$invoice->id] = $invoice;
                }
            }
        }

        return self::$_invoices[$customer_id];
    }

    public static function findByNumber($number)
    {
        $params = [
            'number' => $number,
        ];

        $result = ApiHelper::getInstance()->search(self::$apiCall, [
            'main_attributes' => $params,
        ]);

        if ($result['result'] == false || empty($result['response']) || !isset($result['response'][0])) {
            return null;
        }

        $invoice = new static();
        static::populate($invoice, $result['response'][0]);

        return $invoice;
    }

    /**
     * @param $id
     * @return static
     */
    public static function findById($id)
    {
        $result = ApiHelper::getInstance()->get(self::$apiCall, $id);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        $invoice = new static();
        static::populate($invoice, $result['response']);

        return $invoice;
    }

    /**
     * @param $condition
     * @return Invoices[]|array
     */
    public static function findByCondition($condition)
    {
        $result = ApiHelper::getInstance()->search(self::$apiCall, $condition);

        if ($result['result'] == false || empty($result['response'])) {
            return [];
        }

        $results = [];
        foreach ($result['response'] as $invoiceData) {
            $invoice = new static();
            static::populate($invoice, $invoiceData);
            $results[$invoice->id] = $invoice;
        }

        return $results;
    }

    public function update($params)
    {
        // Fix for items
        if (!isset($params['items'])) {
            $params['items'] = $this->items;
        }

        $result = ApiHelper::getInstance()->put(static::$apiCall, $this->id, $params);

        if ($result['result'] == false) {
            $errors = '';
            foreach ($result['response'] as $error) {
                $errors .= $error['field'] . ': ' . $error['message'] . PHP_EOL;
            }
            throw new UserException(Yii::t('app', 'Error while updating invoice: {errors}', ['errors' => $errors]));
        }

        return true;
    }
}
