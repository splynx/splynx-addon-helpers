<?php

namespace splynx\models\finance;

use splynx\helpers\ApiHelper;
use yii\base\BaseObject;

/**
 * Class BaseToBill
 * @package splynx\models\finance
 */
class BaseToBill extends BaseObject
{
    public $customer_id;
    public $action;

    public $toBillDate;
    public $transactionDate;
    public $period;

    public $result;
    public $invoice;

    public const ACTION_PREVIEW = 'preview';
    public const ACTION_GENERATE = 'generate';

    public static $apiUrl = 'admin/finance/to-bill';

    protected function getApiUrl()
    {
        return static::$apiUrl . '/' . $this->customer_id . '--' . $this->action;
    }

    public function call()
    {
        $attributes = [
            'toBillDate' => $this->toBillDate,
            'transactionDate' => $this->transactionDate,
            'period' => $this->period,
        ];

        $result = ApiHelper::getInstance()->post($this->getApiUrl(), $attributes);

        if ($result['result'] == true) {
            $this->result = $result['response']['result'];
            $this->invoice = $result['response']['invoice'];
        }

        return $result['result'];
    }
}
