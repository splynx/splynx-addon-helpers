<?php

namespace splynx\models\finance;

use splynx\base\ApiResponseException;
use splynx\base\BaseActiveApi;
use splynx\models\finance\item\BaseItem;
use splynx\models\finance\item\ItemsObject;
use yii\helpers\ArrayHelper;

/**
 * Class BaseFinanceClass
 *
 * adding property and methods for use Items in the Finance classes.
 *
 * usage:
 *
 * create item
 *
 * ```php
 * $newItem = ['id'=>1,'description'=>'Desc'...]
 * ```
 *
 * or
 *
 * ```php
 * $newItem = new Item(['id'=>1,'description'=>'Desc'...]);
 * ```
 * or
 *
 * ```php
 * $newItem = new Item();
 * $newItem->id = 1;
 * $newItem->description = 'Desc';
 * $newItem->...
 * ```
 *
 * Add Item
 *
 * ```php
 * $model->items[] = $newItem;
 * ```
 *
 * Change Item
 *
 * ```php
 * $item = $model->items[0];
 * $item->description = 'New description'
 * ```
 *
 * @property BaseItem[]|ItemsObject $items
 * */
class BaseFinanceClass extends BaseActiveApi
{
    /**
     * @var ItemsObject|BaseItem[]
     */
    protected $_items;

    /**
     * Set [[BaseFinanceClass::_items]]  as ItemsObject
     */
    public function init()
    {
        $this->clearItems();

        parent::init();
    }

    /**
     * @return BaseItem[]
     */
    public function getItems()
    {
        return $this->_items;
    }

    /**
     * @param array|BaseItem[] $items
     */
    public function setItems(array $items = null)
    {
        // Fix for version 3.1 api response for empty items
        if (is_array($items)) {
            foreach ($items as $item) {
                $this->items[] = $item;
            }
        }
    }

    /**
     * @return void
     */
    public function clearItems()
    {
        $this->_items = null;
        if ($this instanceof BaseInvoice) {
            $this->_items = new ItemsObject();
            $this->_items->model = BaseInvoice::className();
        } else {
            $this->_items = new ItemsObject();
        }
    }

    /**
     * @param BaseFinanceClass $model
     * @inheritdoc
     */
    public static function populate($model, $data)
    {
        // Populate Items to model
        $items = null;
        if (array_key_exists('items', $data)) {
            $items = $data['items'];
            unset($data['items']);
        }
        parent::populate($model, $data);
        // Deleting old items
        $model->clearItems();
        $model->items = $items;
    }

    /**
     * @inheritdoc
     * and validate items before save
     */
    public function save($runValidation = true)
    {
        if ($runValidation) {
            // Validate Items Before save
            foreach ($this->items as $item) {
                if (!$item->validate()) {
                    $this->addError('items', json_encode($item->getErrors()));
                }
            }
            if (!empty($this->errors)) {
                return false;
            }
        }
        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     *
     * Add Items to attributesForSend
     *
     * @see BaseActiveApi::getAttributesForSend()
     */
    public function getAttributesForSend()
    {
        // Add Items to attributesForSend
        $attributesForSend = parent::getAttributesForSend();
        $attributesForSend['items'] = [];
        foreach ($this->items as $item) {
            $attributesForSend['items'][] = ArrayHelper::toArray($item);
        }
        return $attributesForSend;
    }

    /**
     * @param int|string $number
     * @return BaseFinanceClass|null
     * @throws ApiResponseException
     */
    public static function findByNumber(int | string $number): ?BaseFinanceClass
    {
        return (new static())->findOne(['number' => $number]);
    }

    /**
     * Update Invoice and reload attributes.
     * @inheritdoc
     */
    public function update($runValidation = true)
    {
        $this->reloadAttributesAfterSave = true;
        return parent::update($runValidation);
    }
}
