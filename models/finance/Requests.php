<?php

namespace splynx\models\finance;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\base\UserException;
use Yii;

/**
 * Class Requests
 * @package splynx\models\finance
 * @deprecated
 */
class Requests extends BaseApiModel
{
    public $id;
    public $customer_id;
    public $date_created;
    public $date_payment;
    public $date_till;
    public $date_updated;
    public $memo;
    public $note;
    public $number;
    public $payment_id;
    public $status;
    public $total;
    public $items;
    public $additional_attributes = [];

    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';

    public static $apiCall = 'admin/finance/requests';

    private static $_requests;

    /**
     * @param $customer_id
     * @return Requests[]
     * @throws \yii\base\InvalidConfigException
     */
    public static function findByCustomerId($customer_id)
    {
        if (self::$_requests === null || !isset(self::$_requests[$customer_id])) {
            $params = [
                'customer_id' => $customer_id,
            ];
            $result = ApiHelper::getInstance()->search(self::$apiCall, [
                'main_attributes' => $params,
            ]);

            if ($result['result'] == false || empty($result['response'])) {
                self::$_requests[$customer_id] = [];
            } else {
                foreach ($result['response'] as $requestData) {
                    $request = new self();
                    static::populate($request, $requestData);
                    self::$_requests[$customer_id][$request->id] = $request;
                }
            }
        }

        return self::$_requests[$customer_id];
    }

    public static function findByNumber($number)
    {
        $params = [
            'number' => $number,
        ];

        $result = ApiHelper::getInstance()->search(self::$apiCall, [
            'main_attributes' => $params,
        ]);

        if ($result['result'] == false || empty($result['response']) || !isset($result['response'][0])) {
            return null;
        }

        $request = new self();
        static::populate($request, $result['response'][0]);

        return $request;
    }

    /**
     * @param $id
     * @return static
     */
    public static function findById($id)
    {
        $result = ApiHelper::getInstance()->get(self::$apiCall, $id);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        $request = new static();
        static::populate($request, $result['response']);

        return $request;
    }

    /**
     * @param $condition
     * @return Requests[]|array
     */
    public static function findByCondition($condition)
    {
        $result = ApiHelper::getInstance()->search(self::$apiCall, $condition);

        if (!$result['result'] || empty($result['response'])) {
            return [];
        }

        foreach ($result['response'] as $requestData) {
            $request = new static();

            static::populate($request, $requestData);
            $data[$request->id] = $request;
        }

        return isset($data) ? $data : [];
    }

    public function update($params)
    {
        // Fix for items
        if (!isset($params['items'])) {
            $params['items'] = $this->items;
        }

        $result = ApiHelper::getInstance()->put(static::$apiCall, $this->id, $params);

        if ($result['result'] == false) {
            $errors = '';
            foreach ($result['response'] as $error) {
                $errors .= $error['field'] . ': ' . $error['message'] . PHP_EOL;
            }
            throw new UserException(Yii::t('app', 'Error while updating request: {errors}', ['errors' => $errors]));
        }

        return true;
    }
}
