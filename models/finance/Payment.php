<?php

namespace splynx\models\finance;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use yii\base\UserException;
use Yii;

/**
 * Class Payment
 * @package splynx\models\finance
 * @deprecated
 */
class Payment extends BaseApiModel
{
    public $id;
    public $customer_id;
    public $invoice_id;
    public $request_id;
    public $transaction_id;
    public $payment_type;
    public $receipt_number;
    public $date;
    public $amount;
    public $comment;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;
    public $additional_attributes = [];

    public static $apiCall = 'admin/finance/payments';

    public function create()
    {
        $params = [
            'customer_id' => $this->customer_id,
            'invoice_id' => $this->invoice_id,
            'request_id' => $this->request_id,
            'transaction_id' => $this->transaction_id,
            'payment_type' => $this->payment_type,
            'receipt_number' => $this->receipt_number,
            'date' => $this->date,
            'amount' => $this->amount,
            'comment' => $this->comment,
            'field_1' => $this->field_1,
            'field_2' => $this->field_3,
            'field_3' => $this->field_3,
            'field_4' => $this->field_4,
            'field_5' => $this->field_5,
        ];

        if ($this->additional_attributes != []) {
            $params['additional_attributes'] = $this->additional_attributes;
        }

        $result = ApiHelper::getInstance()->post(self::$apiCall, $params);

        if ($result['result'] == false) {
            foreach ($result['response'] as $item) {
                $this->addError($item['field'], $item['message']);
            }

            return false;
        }

        $this->id = $result['response']['id'];

        return $this->id;
    }

    /**
     * @param $id
     * @return static
     */
    public static function findById($id)
    {
        $result = ApiHelper::getInstance()->get(self::$apiCall, $id);

        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }

        $model = new static();
        static::populate($model, $result['response']);

        return $model;
    }

    /**
     * @param $condition
     * @return Payment[]|array
     */
    public static function findByCondition($condition)
    {
        $result = ApiHelper::getInstance()->search(self::$apiCall, $condition);

        if ($result['result'] == false || empty($result['response'])) {
            return [];
        }

        $results = [];
        foreach ($result['response'] as $row) {
            $model = new static();
            static::populate($model, $row);
            $results[$model->id] = $model;
        }

        return $results;
    }

    public function update($params)
    {
        $result = ApiHelper::getInstance()->put(static::$apiCall, $this->id, $params);

        if ($result['result'] == false) {
            $errors = '';
            foreach ($result['response'] as $error) {
                $errors .= $error['field'] . ': ' . $error['message'] . PHP_EOL;
            }
            throw new UserException(Yii::t('app', 'Error while updating payment: {errors}', ['errors' => $errors]));
        }

        return true;
    }
}
