<?php

namespace splynx\models\finance;

use splynx\base\BaseActiveApi;

/**
 * Class BasePayment
 * @package splynx\models\finance
 */
class BasePayment extends BaseActiveApi
{
    public $id;
    public $customer_id;
    public $invoice_id;
    public $request_id;
    public $transaction_id;
    public $payment_type;
    public $receipt_number;
    public $date;
    public $amount;
    public $comment;
    public $field_1;
    public $field_2;
    public $field_3;
    public $field_4;
    public $field_5;
    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/payments';

    public function getProformaInvoice()
    {
        return (new BaseProformaInvoice())->findById($this->request_id);
    }
}
