<?php

namespace splynx\models\finance\payments;

use splynx\models\finance\BaseInvoice;
use yii\helpers\ArrayHelper;

/**
 * Class BasePaymentInvoice
 * @package splynx\models\finance\payments
 * @property BaseInvoice $invoice
 */
abstract class BasePaymentInvoice extends BasePaymentModel
{
    protected $_invoice;

    public function rules()
    {
        return ArrayHelper::merge([
            [['invoice'], 'required'],
        ], parent::rules());
    }

    /**
     * @return BaseInvoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * Set Invoice, amount and customer ID
     * @param BaseInvoice $invoice
     */
    public function setInvoice($invoice)
    {
        $this->_invoice = $invoice;
        $this->amount = $invoice->total;
        $this->customer_id = $invoice->customer_id;
    }
}
