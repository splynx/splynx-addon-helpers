<?php

namespace splynx\models\finance\payments;

use splynx\models\finance\BaseProformaInvoice;
use yii\helpers\ArrayHelper;

/**
 * Class BasePaymentProformaInvoice
 * @package splynx\models\finance\payments
 * @property BaseProformaInvoice $invoice
 */
abstract class BasePaymentProformaInvoice extends BasePaymentModel
{
    protected $_invoice;

    public function rules()
    {
        return ArrayHelper::merge([
            [['invoice'], 'required'],
        ], parent::rules());
    }

    /**
     * @return BaseProformaInvoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * Set Proforma Invoice, amount and customer ID
     * @param BaseProformaInvoice $request
     */
    public function setInvoice($request)
    {
        $this->_invoice = $request;
        $this->amount = $request->total;
        $this->customer_id = $request->customer_id;
    }
}
