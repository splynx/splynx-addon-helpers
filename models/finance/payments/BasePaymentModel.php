<?php

namespace splynx\models\finance\payments;

use Exception;
use InvalidArgumentException;
use RuntimeException;
use splynx\helpers\ConfigHelper;
use splynx\models\customer\BaseCustomer;
use splynx\models\finance\BankStatement;
use splynx\models\finance\BaseInvoice;
use splynx\models\finance\BasePayment;
use splynx\models\finance\BaseProformaInvoice;
use splynx\models\finance\BaseTransactions;
use splynx\models\finance\item\BaseInvoiceItem;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class BasePaymentModel
 * @package splynx\models\finance\payments
 * @property float $amount;
 * @property string $addonTitle;
 * @property BankStatement $bankStatement;
 * @property BasePayment $basePayment;
 * @property int|null $partnerId;
 */
abstract class BasePaymentModel extends Model
{
    public const ADD_FEE = 'add_fee';
    public const FEE_VAT = 'fee_vat';
    public const FEE_MESSAGE = 'fee_message';
    public const PAYMENT_METHOD = 'payment_method';
    public const FEE_TRANSACTION_CATEGORY = 'fee_transaction_category';
    public const ADD_FEE_TO_REQUEST = 'add_fee_to_request';

    public const AMOUNT_FROM_BANK_STATEMENT = 'amount_from_bank_statement';
    public const AMOUNT_FROM_MODEL = 'amount_from_model';
    public const AMOUNT_FROM_DEFAULT = 'amount_from_default';

    /** @var string */
    public $amountSource = self::AMOUNT_FROM_DEFAULT;

    /** @var integer */
    public $customer_id;

    /** @var integer */
    private $_partner_id;

    /**
     * Default config names.
     *
     * @see [[getCustomConfigNames()]]
     * @see [[getConfigName($name)]]
     *
     * @var array associative array where key is config name, value is your default config name.
     */
    private $_defaultConfigNames = [
        self::ADD_FEE => 'serviceFee',
        self::FEE_VAT => 'fee_VAT',
        self::FEE_MESSAGE => 'fee_message',
        self::PAYMENT_METHOD => 'payment_method_id',
        self::FEE_TRANSACTION_CATEGORY => 'transaction_fee_category',
        self::ADD_FEE_TO_REQUEST => 'add_fee_request',
    ];

    /** @var float */
    private $_amount;

    /** @var BankStatement */
    private $_bank_statement;

    /** @var string */
    protected $_paymentComment;

    /** @var BasePayment */
    private $_basePayment;

    public function init()
    {
        $this->basePayment = new BasePayment();
        $this->setPaymentComment();
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'amount', 'addonTitle'], 'required'],
            [['customer_id'], 'integer'],
            [['amount'], 'number', 'min' => 0.01],
        ];
    }

    /**
     * Get Addon title for use in BankStatement.
     * @return string
     */
    abstract public function getAddonTitle();

    /**
     * Get BankStatement.
     *
     * @return BankStatement
     */
    public function getBankStatement()
    {
        return $this->_bank_statement;
    }

    /**
     * @param BasePayment $basePayment
     */
    public function setBasePayment(BasePayment $basePayment)
    {
        $this->_basePayment = $basePayment;
    }

    /**
     * @return BasePayment
     */
    public function getBasePayment()
    {
        return $this->_basePayment;
    }

    /**
     * @return int|null
     */
    public function getPartnerId()
    {
        if ($this->_partner_id) {
            return $this->_partner_id;
        }
        if (!$this->customer_id) {
            return null;
        }

        $customer = BaseCustomer::findIdentity($this->customer_id);

        if (!$customer) {
            return null;
        }

        $this->_partner_id = $customer->partner_id;
        return $this->_partner_id;
    }

    /**
     * @param int $partnerId
     * @return void
     */
    public function setPartnerId($partnerId)
    {
        $this->_partner_id = $partnerId;
    }

    /**
     * Create BankStatement.
     *
     * @param array $params
     * @return $this|null|BankStatement
     */
    public function createBankStatement($params = [])
    {
        if ($this->validate(['customer_id', 'amount']) == false) {
            $message = implode(
                ' ',
                array_map(function ($errors) {
                    return implode(' ', $errors);
                }, $this->getErrors())
            );
            throw new InvalidParamException($message);
        }

        $attr = [
            'customer_id' => $this->customer_id,
            'amount' => $this->getTotalAmount(),
            'addonTitle' => $this->addonTitle,
            'payment_date' => date('Y-m-d'),
        ];

        if ($this instanceof BasePaymentInvoice && $this->invoice instanceof BaseInvoice) {
            $attr['invoice_id'] = $this->invoice->id;
            if ($this->invoice->due !== null) {
                $attr['additional_3'] = Yii::t('app', 'From Due');
            }
        } elseif ($this instanceof BasePaymentProformaInvoice && $this->invoice instanceof BaseProformaInvoice) {
            $attr['request_id'] = $this->invoice->id;
        }

        if (is_array($params)) {
            $attr = ArrayHelper::merge($attr, $params);
        }
        $bankStatement = new BankStatement($attr);
        $this->_bank_statement = $bankStatement->createBankStatement();
        if ($this->_bank_statement == null) {
            $this->addError('bankStatement', Yii::t('app', 'Error adding bank statement!'));
            $this->addErrors($bankStatement->errors);
        }
        return $this->_bank_statement;
    }

    /**
     * Find and set BankStatement By id or BankStatement model
     * @param BankStatement|integer $bankStatement
     */
    public function setBankStatement($bankStatement)
    {
        if ($bankStatement instanceof BankStatement) {
            $this->_bank_statement = $bankStatement;
        } elseif (is_integer($bankStatement)) {
            $this->_bank_statement = (new BankStatement())->findById($bankStatement);
        }
    }

    /**
     * @param string $status
     * @return bool
     */
    public function setBankStatementStatus($status)
    {
        if (!$this->_bank_statement) {
            return false;
        }
        $this->_bank_statement->status = $status;
        return $this->_bank_statement->save();
    }

    /**
     * @param bool $due
     * @return float
     */
    public function getAmount($due = true)
    {
        if ($this->amountSource === self::AMOUNT_FROM_BANK_STATEMENT) {
            return $this->bankStatement->amount;
        }
        if ($this->amountSource === self::AMOUNT_FROM_MODEL) {
            return $this->_amount;
        }
        if ($due && $this instanceof BasePaymentInvoice && $this->invoice->due !== null) {
            return round($this->invoice->due, 2);
        }
        if ($this instanceof BasePaymentInvoice || $this instanceof BasePaymentProformaInvoice) {
            return round($this->invoice->total, 2);
        }
        return $this->_amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->_amount = $amount;
    }

    /**
     * @return null|mixed
     */
    public function getFee()
    {
        return ConfigHelper::get($this->getConfigName(self::ADD_FEE), $this->partnerId);
    }

    /**
     * @return bool
     */
    public function isFee()
    {
        return is_numeric($this->getFee()) && $this->getFee() != 0;
    }

    /**
     * @param bool $due
     * @return float
     */
    public function getTotalAmount($due = true)
    {
        return round($this->getAmount($due) + $this->getFeeAmount(), 2);
    }

    /**
     * @param float|int|null $amount
     * @return float|int
     */
    public function getFeeAmount($amount = null)
    {
        if ($this->isFee()) {
            return round(($amount !== null ? $amount : $this->amount) / 100 * $this->getFee(), 2);
        } else {
            return 0;
        }
    }

    /**
     * @return null|float
     */
    public function getFeeVat()
    {
        return ConfigHelper::get($this->getConfigName(self::FEE_VAT), $this->partnerId);
    }

    /**
     * @return bool
     */
    public function isFeeVAT()
    {
        return is_numeric($this->getFeeVat()) && $this->getFeeVat() != 0;
    }

    /**
     * @return float|int
     */
    public function getFeeWithoutVAT()
    {
        return $this->getFeeAmount() / (100 + $this->getFeeVat()) * 100;
    }

    /**
     * @param float|null $amount
     * @return float|int
     */
    public function getAmountWithoutFee($amount = null)
    {
        return ($amount !== null ? $amount : $this->amount) / (100 + $this->getFee()) * 100;
    }

    /**
     * Rename config options.
     *
     * You can change default names of config options.
     * Available names to change, see [[_defaultConfigNames]].
     *
     * ```php
     *
     * return [
     *      BasePaymentModel::ADD_FEE => 'yourNewConfigNameForFee',
     *      BasePaymentModel::FEE_VAT => 'yourNewConfigNameForFeeVat',
     * ];
     *
     * ```
     *
     * @return array associative array where key is config name which you want to redefine, value is your config name.
     */
    public function getCustomConfigNames()
    {
        return [];
    }

    /**
     * Get config name.
     * If config option name is not defined in [[getCustomConfigNames()]] method, default value will be used. See [[defaultConfigNames]].
     *
     * @param string $name config name.
     * @return mixed
     *
     * @throws InvalidArgumentException if name is invalid.
     */
    public function getConfigName($name)
    {
        if (isset($this->getCustomConfigNames()[$name])) {
            return $this->getCustomConfigNames()[$name];
        } elseif (isset($this->_defaultConfigNames[$name])) {
            return $this->_defaultConfigNames[$name];
        } else {
            throw new InvalidArgumentException(Yii::t('app', 'Invalid config name: {name}', ['name' => $name]));
        }
    }

    /**
     *Set Amount without Fee
     * @param float|null $amount
     */
    public function setAmountWithOutFee($amount = null)
    {
        if ($this->isFee()) {
            $amountWithFee = $amount ? $amount : $this->amount;
            // Set amounts
            $this->amount = $amountWithFee / (100 + $this->getFee()) * 100;
        } else {
            $this->amount = $amount;
        }
    }

    /**
     * @param null|string $apiPaymentId
     * @param integer|null $bankStatementId
     * @return bool
     */
    public function processPayment($apiPaymentId = null, $bankStatementId = null)
    {
        if ($bankStatementId) {
            $bankStatement = (new BankStatement())->getBankStatementById($bankStatementId);
            $this->bankStatement = $bankStatement;
        } else {
            $bankStatement = $this->bankStatement;
        }
        if (
            !$bankStatement
            || !in_array(
                $bankStatement->status,
                [BankStatement::STATUS_NEW, BankStatement::STATUS_PENDING],
                true
            )
        ) {
            return false;
        }

        $this->amountSource = self::AMOUNT_FROM_BANK_STATEMENT;

        if ($this->validate() == false) {
            return false;
        }

        if ($this->isFee()) {
            // Invoices and proforma
            if (
                $this instanceof BasePaymentInvoice ||
                (
                    $this instanceof BasePaymentProformaInvoice &&
                    ConfigHelper::get($this->getConfigName(self::ADD_FEE_TO_REQUEST), $this->partnerId)
                )
            ) {
                $this->amountSource = self::AMOUNT_FROM_MODEL;
                $this->amount = $this->getAmountWithoutFee($this->bankStatement->amount);

                // Add item with fee to invoice
                $fee_message = ConfigHelper::get($this->getConfigName(self::FEE_MESSAGE), $this->partnerId);
                $fee_item = [
                    'price' => ($this->isFeeVAT()) ? $this->getFeeWithoutVAT() : $this->getFeeAmount(),
                    'description' => $fee_message ?: 'Commission',
                    'quantity' => 1,
                    'tax' => ($this->isFeeVAT()) ? $this->getFeeVat() : 0,
                ];
                $this->invoice->items[] = $fee_item;
                if (!$this->invoice->update()) {
                    throw new RuntimeException(
                        'Update invoice failed, errors: ' . var_export($this->invoice->getErrors(), true)
                    );
                }
            } else {
                // Top-ups
                // Increase amount with fee
                $this->amount += $this->getFeeAmount();
            }
        }
        $serviceFeeInvoiceId = null;
        if (
            $this->isFee()
            && !($this instanceof BasePaymentInvoice)
            && ($this instanceof BasePaymentProformaInvoice || !isset($this->invoice))
        ) {
            if (!($serviceFeeInvoiceId = $this->createInvoiceForFee($apiPaymentId, false))) {
                throw new Exception(
                    'Create invoice for service fee failed, errors:' . var_export($this->getErrors(), true)
                );
            }
        }
        $createdPaymentId = $this->createPayment($apiPaymentId, $serviceFeeInvoiceId);
        if ($createdPaymentId === false) {
            throw new Exception('Create payment failed, errors:' . var_export($this->getErrors(), true));
        }

        $bankStatement->status = BankStatement::STATUS_PROCESSED;
        $bankStatement->payment_id = $createdPaymentId;

        // Update bank statement
        $bankStatement->update();
        return true;
    }

    /**
     * @param int|null $apiPaymentId
     * @param int|null $apiPaymentId
     * @return bool|integer
     */
    public function createPayment($apiPaymentId = null, $serviceFeeInvoiceId = null)
    {
        // Create Splynx payment
        $payment = $this->_basePayment;
        $payment->customer_id = $this->customer_id;
        $payment->amount = $this->bankStatement->amount; // Amount with fee (total amount)
        $payment->payment_type = ConfigHelper::get($this->getConfigName(self::PAYMENT_METHOD), $this->partnerId);
        if ($this instanceof BasePaymentInvoice) {
            $payment->invoice_id = $this->invoice->id;
        } elseif ($this instanceof BasePaymentProformaInvoice) {
            $payment->request_id = $this->invoice->id;
        }
        if ($serviceFeeInvoiceId) {
            $payment->invoice_id = $serviceFeeInvoiceId;
        }
        $payment->comment = $this->_paymentComment;
        if ($apiPaymentId) {
            $payment->field_4 = 'Payment: ' . $apiPaymentId;
        }
        if ($this instanceof BasePaymentInvoice) {
            $payment->field_5 = 'Invoice: ' . $this->invoice->id;
        } elseif ($this instanceof BaseProformaInvoice) {
            $payment->field_5 = 'Request: ' . $this->invoice->id;
        } else {
            $payment->field_5 = 'Bank Statement: ' . $this->bankStatement->id;
        }

        if ($payment->create()) {
            return $payment->id;
        }
        if (count($payment->errors) > 0) {
            $this->addErrors($payment->errors);
        } else {
            $this->addError('payment', Yii::t('app', 'Payment create failed'));
        }
        return false;
    }

    /**
     * Set comment for payment
     * @param string $comment
     */
    public function setPaymentComment(string $comment = '')
    {
        $this->_paymentComment = !empty($comment) ? $comment : 'Pay by ' . $this->addonTitle;
    }

    /**
     * @param integer $createdPaymentId
     * @return bool|integer
     */
    public function createTransaction($createdPaymentId)
    {
        $feeAmount = $this->getFeeAmount(
            $this->getAmountWithoutFee($this->bankStatement->amount)
        ); // Get fee amount from total amount

        $transaction = new BaseTransactions();

        if ($this->isFeeVAT()) {
            $feeAmount = $feeAmount / (100 + $this->getFeeVat()) * 100;
            $transaction->tax_percent = $this->getFeeVat();
        }

        $transaction->customer_id = $this->customer_id;
        $transaction->type = BaseTransactions::TYPE_DEBIT;
        $transaction->price = $feeAmount;
        $transaction->category = ConfigHelper::get(
            $this->getConfigName(self::FEE_TRANSACTION_CATEGORY),
            $this->partnerId
        );
        $transaction->description = $this->addonTitle . ' fee (for payment #' . $createdPaymentId . ')';
        $transaction->source = BaseTransactions::SOURCE_MANUAL;

        if ($transaction->create()) {
            return $transaction->id;
        }
        if (count($transaction->errors) > 0) {
            $this->addErrors($transaction->errors);
        } else {
            $this->addError('transaction', Yii::t('app', 'Transaction create failed'));
        }
        return false;
    }

    /**
     * @param int $createdPaymentId
     * @param bool $payInvoice
     * @return int|false
     */
    public function createInvoiceForFee($createdPaymentId, $payInvoice = true)
    {
        $feeAmount = $this->getFeeAmount(
            $this->getAmountWithoutFee($this->bankStatement->amount)
        ); // Get fee amount from total amount

        $invoiceItem = new BaseInvoiceItem();

        if ($this->isFeeVAT()) {
            $feeAmount = $feeAmount / (100 + $this->getFeeVat()) * 100;
            $invoiceItem->tax = $this->getFeeVat();
        }
        $invoice = new BaseInvoice();
        $invoice->customer_id = $this->customer_id;
        $invoiceItem->price = $feeAmount;
        $invoiceItem->quantity = 1;
        $invoiceItem->description = $this->addonTitle . ' fee (for payment #' . $createdPaymentId . ')';
        $invoice->items[] = $invoiceItem;

        if (!$invoice->create()) {
            if (count($invoice->errors) > 0) {
                $this->addErrors($invoice->errors);
            } else {
                $this->addError('invoice', Yii::t('app', 'Invoice for fee create failed'));
            }
            return false;
        }

        if (!$payInvoice) {
            return $invoice->id;
        }

        $payment = new BasePayment();
        $payment->customer_id = $this->customer_id;
        $payment->invoice_id = $invoice->id;
        $payment->amount = $feeAmount;
        $payment->payment_type = ConfigHelper::get($this->getConfigName(self::PAYMENT_METHOD), $this->partnerId);
        $payment->comment = 'Pay by ' . $this->addonTitle;
        $payment->field_5 = 'Invoice: ' . $invoice->id;
        if ($payment->create()) {
            return $payment->id;
        }
        if (count($payment->errors) > 0) {
            $this->addErrors($payment->errors);
        } else {
            $this->addError('payment', Yii::t('app', 'Payment for fee create failed'));
        }
        return false;
    }

    /**
     * @param null|int $bankStatementId
     */
    public function cancel($bankStatementId = null)
    {
        if ($bankStatementId) {
            $bankStatement = (new BankStatement())->findById($bankStatementId);
        } else {
            $bankStatement = $this->bankStatement;
        }
        if (
            $bankStatement !== null
            && in_array(
                $bankStatement->status,
                [BankStatement::STATUS_PENDING, BankStatement::STATUS_NEW],
                true
            )
        ) {
            $bankStatement->status = BankStatement::STATUS_CANCELED;
            $bankStatement->update();
        }
    }

    /**
     * @param null $bankStatementId
     */
    public function error($bankStatementId = null)
    {
        if ($bankStatementId) {
            $bankStatement = (new BankStatement())->findById($bankStatementId);
        } else {
            $bankStatement = $this->bankStatement;
        }
        if (
            $bankStatement !== null
            && in_array(
                $bankStatement->status,
                [BankStatement::STATUS_PENDING, BankStatement::STATUS_NEW],
                true
            )
        ) {
            $bankStatement->status = BankStatement::STATUS_ERROR;
            $bankStatement->update();
        }
    }
}
