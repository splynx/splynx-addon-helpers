<?php

namespace splynx\models\finance\item;

/**
 * Class BaseInvoiceItem
 * @package splynx\models\finance\item
 */
class BaseInvoiceItem extends BaseItem
{
    /** @var  int */
    public $transaction_id;
    /** @var int */
    public $categoryIdForTransaction;
}
