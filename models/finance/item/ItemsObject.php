<?php

namespace splynx\models\finance\item;

use splynx\models\finance\BaseInvoice;

/**
 * Class ItemsObject
 *
 * Convert array item to Item object
 * @package splynx\models\finance\item
 */
class ItemsObject extends \ArrayObject
{
    public $model;

    /**
     * Check and write new Item
     *
     * @param $index
     * @param array|BaseItem $newItem
     */
    public function offsetSet($index, $newItem)
    {
        $item = null;
        if (is_array($newItem)) {
            if ($this->model == BaseInvoice::className()) {
                $item = new BaseInvoiceItem($newItem);
            } else {
                $item = new BaseItem($newItem);
            }
        } elseif ($newItem instanceof BaseItem) {
            $item = $newItem;
        } else {
            return;
        }
        parent::offsetSet($index, $item);
    }
}
