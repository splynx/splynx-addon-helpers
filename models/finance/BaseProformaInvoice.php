<?php

namespace splynx\models\finance;

/**
 * Class BaseProformaInvoice
 * @package splynx\models\finance
 */
class BaseProformaInvoice extends BaseFinanceClass
{
    public $id;
    public $customer_id;
    public $date_created;
    public $real_create_datetime;
    public $date_updated;
    public $date_payment;
    public $date_till;
    public $number;
    public $total;
    public $payment_id;
    public $status;
    public $is_sent;
    public $note;
    public $memo;

    public $additional_attributes = [];

    public static $apiUrl = 'admin/finance/proforma-invoices';

    public const STATUS_PAID = 'paid';
    public const STATUS_NOT_PAID = 'not_paid';
    public const STATUS_PENDING = 'pending';
}
