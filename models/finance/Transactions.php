<?php

namespace splynx\models\finance;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;

/**
 * Class Transactions
 * @package splynx\models\finance
 * @deprecated
 */
class Transactions extends BaseApiModel
{
    public $id;
    public $customer_id;
    public $type;
    public $quantity = 1;
    public $unit;
    public $price;
    public $tax_percent;
    public $total;
    public $date;
    public $category;
    public $description;
    public $period_from;
    public $period_to;
    public $service_id;
    public $payment_id;
    public $invoice_id;
    public $invoiced_by_id;
    public $comment;
    public $to_invoice;
    public $service_type;
    public $source;

    public $additional_attributes = [];

    public const SOURCE_MANUAL = 'manual';
    public const SOURCE_AUTO = 'auto';
    public const SOURCE_CDR = 'cdr';

    public const TYPE_DEBIT = 'debit';
    public const TYPE_CREDIT = 'credit';

    public static $apiCall = 'admin/finance/transactions';

    public function create()
    {
        $params = [
            'customer_id' => $this->customer_id,
            'type' => $this->type,
            'quantity' => $this->quantity,
            'unit' => $this->unit,
            'price' => $this->price,
            'tax_percent' => $this->tax_percent,
            'date' => $this->date,
            'category' => $this->category,
            'description' => $this->description,
            'period_from' => $this->period_from,
            'period_to' => $this->period_to,
            'service_id' => $this->service_id,
            'payment_id' => $this->payment_id,
            'invoice_id' => $this->invoice_id,
            'invoiced_by_id' => $this->invoiced_by_id,
            'comment' => $this->comment,
            'to_invoice' => $this->to_invoice,
            'service_type' => $this->service_type,
            'source' => $this->source,
        ];

        $result = ApiHelper::getInstance()->post(self::$apiCall, $params);

        if ($result['result'] == false) {
            foreach ($result['response'] as $item) {
                $this->addError($item['field'], $item['message']);
            }

            return false;
        }

        $this->id = $result['response']['id'];

        return $this->id;
    }

    /**
     * @param $id
     * @return null|Transactions
     */
    public static function findById($id)
    {
        $result = ApiHelper::getInstance()->get(self::$apiCall, $id);
        if ($result['result'] == false || empty($result['response'])) {
            return null;
        }
        $model = new self();
        static::populate($model, $result['response']);
        return $model;
    }
}
