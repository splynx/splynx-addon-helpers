<?php

namespace splynx\models\finance;

use splynx\base\BaseActiveApi;

/**
 * Class BaseTransactions
 * @package splynx\models\finance
 */
class BaseTransactions extends BaseActiveApi
{
    public $id;
    public $customer_id;
    public $type;
    public $quantity = 1;
    public $unit;
    public $price;
    public $tax_percent;
    public $total;
    public $date;
    public $category;
    public $description;
    public $period_from;
    public $period_to;
    public $service_id;
    public $payment_id;
    public $invoice_id;
    public $invoiced_by_id;
    public $comment;
    public $to_invoice;
    public $service_type;
    public $source;
    public $additional_attributes = [];

    public const SOURCE_MANUAL = 'manual';
    public const SOURCE_AUTO = 'auto';
    public const SOURCE_CDR = 'cdr';

    public const TYPE_DEBIT = 'debit';
    public const TYPE_CREDIT = 'credit';

    public const SERVICE_TYPE_INTERNET = 'internet';
    public const SERVICE_TYPE_VOICE = 'voice';
    public const SERVICE_TYPE_CUSTOM = 'custom';

    public static $apiUrl = 'admin/finance/transactions';
}
