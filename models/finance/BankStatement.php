<?php

namespace splynx\models\finance;

use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;
use yii\base\InvalidCallException;
use yii\base\InvalidConfigException;
use Yii;

/**
 * Class BankStatement
 * @package splynx\models\finance
 */
class BankStatement extends BaseActiveApi
{
    public $id;
    public $bank_statements_process_id;
    public $customer_id;
    public $payment_date;
    public $amount;
    public $status = self::STATUS_NEW;
    public $payment_id;
    public $invoice_id;
    public $request_id;
    public $transaction_id;
    public $comment;
    public $additional_1;
    public $additional_2;
    public $additional_3;

    public $addonTitle;

    public const STATUS_PENDING = 'pending';
    public const STATUS_NEW = 'new';
    public const STATUS_PROCESSED = 'processed';
    public const STATUS_ERROR = 'error';
    public const STATUS_CANCELED = 'canceled';

    protected static $statementsApiCall = 'admin/finance/bank-statements';

    protected static $apiUrl = 'admin/finance/bank-statements-records';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'amount'], 'required'],
            [['id', 'customer_id', 'payment_id', 'bank_statements_process_id', 'invoice_id', 'request_id', 'transaction_id'], 'integer'],
            [['comment'], 'string'],
            ['status', 'in', 'range' => [self::STATUS_PENDING,self::STATUS_NEW, self::STATUS_PROCESSED, self::STATUS_CANCELED, self::STATUS_ERROR]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function save($runValidation = true)
    {
        if (is_null($this->bank_statements_process_id)) {
            $this->bank_statements_process_id = $this->getStatementProcessId();
        }
        if (is_null($this->comment)) {
            $this->comment = $this->addonTitle;
        }
        return parent::save($runValidation);
    }

    /**
     * @inheritdoc
     */
    public function getAttributesForSendArray()
    {
        return [
            'bank_statements_process_id',
            'customer_id',
            'payment_date',
            'amount',
            'status',
            'comment',
            'payment_id',
            'invoice_id',
            'request_id',
            'transaction_id',
            'additional_1',
            'additional_2',
            'additional_3',
        ];
    }

    /**
     * @return $this|null
     */
    public function createBankStatement()
    {
        if ($this->save()) {
            return $this;
        }
        return null;
    }

    /**
     * @return string
     */
    protected function getStatementProcessTitle()
    {
        $period = \Yii::$app->params['bank_statements_group'];

        if ($period == 'day') {
            $comment = date('d/m/Y');
        } else {
            $comment = date('m/Y');
        }

        return $this->addonTitle . ' ' . $comment;
    }

    /**
     * @return mixed
     * @throws InvalidConfigException
     */
    protected function getStatementProcessId()
    {
        $title = $this->getStatementProcessTitle();

        $params = [
            'main_attributes' => [
                'title' => $title,
            ],
        ];

        $result = ApiHelper::getInstance()->search(self::$statementsApiCall, $params);

        if ($result['result'] == false) {
            throw new InvalidCallException(Yii::t('app', 'Error in API Call!'));
        }

        if (!empty($result['response'])) {
            $item = reset($result['response']);
            return $item['id'];
        }

        // Create new
        $params = [
            'title' => $title,
            'status' => 'success',
        ];

        $result = ApiHelper::getInstance()->post(self::$statementsApiCall, $params);
        if ($result['result'] == false) {
            throw new InvalidCallException(Yii::t('app', 'Error in API Call!'));
        }

        return $result['response']['id'];
    }

    /**
     * Find BankStatement by id.
     * @param integer $id
     * @return BankStatement
     */
    public function getBankStatementById($id)
    {
        return $this->findOne(['id' => $id]);
    }
}
