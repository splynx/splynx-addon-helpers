<?php

namespace splynx\models\transport;

use splynx\helpers\ArrayHelper;
use yii\validators\EmailValidator;

/**
 * Class BaseMail
 * @package splynx\models\transport
 */
class BaseMail extends BaseMessage
{
    public $bcc;
    public $cc;
    public $subject;

    public const TYPE_TICKET_NOTIFICATION = 'ticket';

    protected static $apiUrl = 'admin/config/mail';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['recipient', 'cc', 'bcc'], 'validateEmail'],
            [['subject'], 'required'],
            [['subject'], 'string', 'max' => 128],
        ], parent::rules());
    }

    /**
     * @param string $attribute
     */
    public function validateEmail($attribute)
    {
        $validator = new EmailValidator();

        $emails = array_filter(
            array_map('trim', explode(',', $this->$attribute)),
            function ($val) use ($validator) {
                return !$validator->validate($val);
            }
        );
        if (count($emails)) {
            array_walk($emails, function ($email) use ($attribute) {
                $this->addError($attribute, "$email is not a valid email.");
            });
        }
    }

    /**
     * @inheritdoc
     */
    public static function getTypeList()
    {
        return ArrayHelper::merge([static::TYPE_TICKET_NOTIFICATION], parent::getTypeList());
    }
}
