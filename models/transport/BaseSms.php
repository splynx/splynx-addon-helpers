<?php

namespace splynx\models\transport;

use splynx\helpers\ArrayHelper;

/**
 * Class BaseSms
 * @package splynx\models\transport
 */
class BaseSms extends BaseMessage
{
    protected static $apiUrl = 'admin/config/sms';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge([
            [['recipient'], 'string', 'max' => 64],
        ], parent::rules());
    }
}
