<?php

namespace splynx\models\transport;

use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;
use yii\base\InvalidConfigException;
use yii\base\UserException;

/**
 * Class BaseMessage
 * @package splynx\models\transport
 */
abstract class BaseMessage extends BaseActiveApi
{
    public $id;
    public $type;
    public $status;
    public $message_id;
    public $recipient;
    public $message;
    public $datetime_added;
    public $datetime_start_sending;
    public $datetime_sent;

    public const STATUS_NEW = 'new';
    public const STATUS_SENDING = 'sending';
    public const STATUS_SENT = 'sent';
    public const STATUS_ERROR = 'error';
    public const STATUS_EXPIRED = 'expired';

    public const TYPE_MESSAGE = 'message';
    public const TYPE_TEST = 'test';
    public const TYPE_MONITORING = 'monitoring';
    public const TYPE_ADDON = 'add-on';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['recipient', 'message', 'type'], 'required'],
            [['type'], 'string', 'max' => 32],
            [['message'], 'string'],
            [['message_id'], 'integer'],
            [['status'], 'in', 'range' => static::getStatusList()],
            [['type'], 'in', 'range' => static::getTypeList()],
        ];
    }

    /**
     * Puts a message in the queue
     * @param bool $send send a message if true
     * @return bool
     * @throws UserException
     */
    public function send($send = true)
    {
        if (!$this->validate()) {
            return false;
        }
        if (!$this->save(false)) {
            throw new UserException('Error while adding message pool by API!');
        }
        if ($send) {
            return $this->sendMessage();
        }
        return true;
    }

    /**
     * Send a message
     * @return bool true if sending is successful
     * @throws InvalidConfigException
     */
    protected function sendMessage()
    {
        $result = ApiHelper::get(static::$apiUrl . '/' . $this->id . '--send');

        return (bool)$result['result'];
    }

    /**
     * Return array with statuses
     * @return array
     **/
    public static function getStatusList()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_SENDING,
            self::STATUS_SENT,
            self::STATUS_ERROR,
            self::STATUS_EXPIRED,
        ];
    }

    /**
     * Return array with types
     * @return array
     **/
    public static function getTypeList()
    {
        return [
            static::TYPE_MESSAGE,
            static::TYPE_TEST,
            static::TYPE_MONITORING,
            static::TYPE_ADDON,
        ];
    }
}
