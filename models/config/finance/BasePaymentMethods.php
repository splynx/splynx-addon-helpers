<?php

namespace splynx\models\config\finance;

use splynx\base\BaseActiveApi;

/**
 * Class BasePaymentMethods
 * @package splynx\models\config\finance
 */
class BasePaymentMethods extends BaseActiveApi
{
    public $id;
    public $name;
    public $is_active;
    public $name_1;
    public $name_2;
    public $name_3;
    public $name_4;
    public $name_5;

    public static $apiUrl = 'admin/finance/payment-methods';

    public function rules()
    {
        return [
            [['name', 'is_active'], 'required'],
            [['is_active'], 'boolean'],
            [['name', 'name_1', 'name_2', 'name_3', 'name_4', 'name_5'], 'string'],
        ];
    }
}
