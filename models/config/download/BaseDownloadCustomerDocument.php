<?php

namespace splynx\models\config\download;

class BaseDownloadCustomerDocument extends BaseDownload
{
    public $module = 'customer_documents';
}
