<?php

namespace splynx\models\config\download;

use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;
use yii\base\InvalidCallException;
use yii\base\InvalidValueException;

abstract class BaseDownload extends BaseActiveApi
{
    public $module;
    public $document_id;

    public $filename;
    public $content_type;
    public $content;

    public const TYPE_MODULE_CUSTOMER_DOCUMENT = 'customer_documents';
    public const TYPE_MODULE_CUSTOMER_INVOICE = 'invoices';
    public const TYPE_MODULE_CUSTOMER_REQUEST = 'requests';
    public const TYPE_MODULE_CUSTOMER_PAYMENT = 'payments';

    protected static $apiUrl = 'admin/config/download';

    public function __construct($document_id, array $config = [])
    {
        $this->document_id = $document_id;
        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['module', 'document_id'], 'required'],
            [['document_id'], 'integer'],
            [['module'], 'in', 'range' => self::getDocumentTypes()],
        ];
    }

    /**
     * Download document.
     * @return string
     * @throws \yii\base\InvalidConfigException|InvalidCallException
     */
    public function download()
    {
        if ($this->validate()) {
            $result = ApiHelper::getInstance()->get('admin/config/download', $this->module . '--' . $this->document_id);
            if ($result['result'] && !empty($result['response'])) {
                $this->content = $this->decodeContent($result['response']['content']);
                $this->filename = $result['response']['name'];
                $this->content_type = $result['response']['content_type'];
                return $this->content;
            }
            throw new InvalidCallException('Error in Api call');
        }
        throw new InvalidValueException('Parameters module or document_id not set correctly ' . PHP_EOL . implode(PHP_EOL, array_values($this->getFirstErrors())));
    }

    /**
     * get document type
     * @return array
     */
    public function getDocumentTypes()
    {
        return [
            self::TYPE_MODULE_CUSTOMER_DOCUMENT,
            self::TYPE_MODULE_CUSTOMER_INVOICE,
            self::TYPE_MODULE_CUSTOMER_REQUEST,
            self::TYPE_MODULE_CUSTOMER_PAYMENT,
        ];
    }

    /**
     * @param $content
     * @return bool|string
     */
    protected function decodeContent($content)
    {
         return base64_decode($content);
    }
}
