<?php

namespace splynx\models\config;

use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;
use yii\base\InvalidParamException;
use yii\base\UserException;

class BaseAdditionalField extends BaseActiveApi
{
    public const TYPE_STRING = 'string';
    public const TYPE_INTEGER = 'integer';
    public const TYPE_DECIMAL = 'decimal';
    public const TYPE_NUMERIC = 'numeric';
    public const TYPE_DATE = 'date';
    public const TYPE_DATETIME = 'datetime';
    public const TYPE_BOOLEAN = 'boolean';
    public const TYPE_SELECT = 'select';
    public const TYPE_SELECT_MULTIPLE = 'select_multiple';
    public const TYPE_RELATION = 'relation';
    public const TYPE_RELATION_MULTIPLE = 'relation_multiple';
    public const TYPE_PASSWORD = 'password';
    public const TYPE_FILE = 'file';
    public const TYPE_ADDON = 'add-on';
    public const TYPE_IP = 'ip';
    public const TYPE_TEXTAREA = 'textarea';

    protected static $apiUrl = 'admin/config/additional-fields';

    public $module;

    public $name;
    public $title;
    public $type;
    public $position;
    public $default_value;
    public $min_length;
    public $max_length;
    public $select_values;
    public $decimals;
    public $is_required;
    public $is_unique;
    public $is_add;
    public $show_in_list;
    public $searchable;
    public $readonly;
    public $disabled;
    public $hidden;
    public $relation_module;
    public $addon;
    public $addon_uri;
    public $addon_input_type;
    public $set_default_value = false;

    public function rules()
    {
        return [
            [['name', 'module', 'title', 'type', 'is_required', 'is_unique', 'is_add', 'show_in_list', 'searchable', 'readonly', 'disabled'], 'required'],
            [['name', 'module', 'title', 'type', 'default_value', 'select_values', 'relation_module', 'addon', 'addon_uri', 'addon_input_type'], 'string'],
            [['select_values'], 'required', 'when' => function ($model) {
                return ($model->type == static::TYPE_SELECT || $model->type == static::TYPE_SELECT_MULTIPLE);
            },],
            [['addon', 'addon_uri', 'addon_input_type'], 'required', 'when' => function ($model) {
                return ($model->type == static::TYPE_ADDON);
            },],
            [['decimals'], 'required', 'when' => function ($model) {
                return ($model->type == static::TYPE_DECIMAL);
            },],
            [['relation_module'], 'required', 'when' => function ($model) {
                return ($model->type == static::TYPE_RELATION || $model->type == static::TYPE_RELATION_MULTIPLE);
            },],
            [['is_required', 'is_unique', 'is_add', 'show_in_list', 'searchable', 'readonly', 'set_default_value', 'disabled', 'hidden'], 'boolean'],
            [['min_length', 'max_length'], 'number', 'min' => 0],
            [['type'], 'in', 'range' => self::getTypeList()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getPrimaryKey()
    {
        return 'name';
    }

    /**
     * @inheritdoc
     */
    protected function getApiUrl($id = null, $conditions = [])
    {
        $result = static::$apiUrl . '/' . $this->module;

        if ($id !== null) {
            $result .= '--' . $id;
        }
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }
        return $result;
    }

    /**
     * Create new additional field
     * @param string $module
     * @param array $additionalParams
     * @return BaseAdditionalField
     * @throws UserException
     */
    public static function createField($module, $additionalParams)
    {

        if (!is_array($additionalParams) || !$module) {
            throw new InvalidParamException('invalid params');
        }
        $obj = new static(array_merge(['module' => $module], $additionalParams));

        if (!$obj->save()) {
            throw new UserException(implode(PHP_EOL, $obj->getFirstErrors()));
        };
        return $obj;
    }

    /**
     * Find all fields by module name
     * @param string $module
     * @return array|bool
     * @throws \yii\base\InvalidConfigException
     */
    public static function findByModule($module)
    {
        if (!$module || !is_string($module)) {
            throw new InvalidParamException('invalid params');
        }
        $result = ApiHelper::getInstance()->get(static::$apiUrl . '/' . $module);

        if ($result['result'] && !isset($result['response']['error'])) {
            $models = [];
            foreach ($result['response'] as $field) {
                $model = new static();
                static::populate($model, array_merge(['module' => $module], $field));
                $models[] = $model;
            }
            return $models;
        }
        return false;
    }

    /**
     * Find field by module and field name
     * @param string $module
     * @param string $field
     * @return null|BaseAdditionalField
     */
    public static function findByModuleAndName($module, $field)
    {
        if (!$module || !$field) {
            throw new InvalidParamException('invalid params');
        }
        $obj = new static(['module' => $module]);
        $obj = $obj->findById($field);
        if (!$obj) {
            return null;
        }
        $obj->module = $module;
        return $obj;
    }

    /**
     * @return array
     */
    public static function getTypeList()
    {
        return [
            static::TYPE_STRING,
            static::TYPE_INTEGER,
            static::TYPE_DECIMAL,
            static::TYPE_NUMERIC,
            static::TYPE_DATE,
            static::TYPE_DATETIME,
            static::TYPE_BOOLEAN,
            static::TYPE_SELECT,
            static::TYPE_SELECT_MULTIPLE,
            static::TYPE_RELATION,
            static::TYPE_RELATION_MULTIPLE,
            static::TYPE_PASSWORD,
            static::TYPE_FILE,
            static::TYPE_ADDON,
            static::TYPE_IP,
            static::TYPE_TEXTAREA,
        ];
    }
}
