<?php

namespace splynx\models\config;

use splynx\base\BaseActiveApi;
use splynx\helpers\ApiHelper;
use yii\base\InvalidParamException;

class BaseTemplate extends BaseActiveApi
{
    public $id;
    public $type;
    public $title;
    public $description;

    protected static $apiUrl = 'admin/config/templates';

    public const TYPE_CUSTOMER_PORTAL = 'customer-portal';
    public const TYPE_INVOICE_PDF = 'invoice-pdf';
    public const TYPE_MAIL = 'mail';
    public const TYPE_SMS = 'sms';
    public const TYPE_DOCUMENTS = 'documents';
    public const TYPE_CARDS = 'cards';
    public const TYPE_PAYMENT_CALENDAR = 'payment-calendar';
    public const TYPE_PAYMENT_RECEIPT = 'payment-receipt';
    public const TYPE_REQUEST_PDF = 'request-pdf';
    public const TYPE_REMINDER_MAIL = 'reminder-mail';
    public const TYPE_REMINDER_SMS = 'reminder-sms';
    public const TYPE_MONITORING_NOTIFICATION_MAIL = 'monitoring-notification-mail';
    public const TYPE_MONITORING_NOTIFICATION_SMS = 'monitoring-notification-sms';
    public const TYPE_EXPORTS = 'exports';
    public const TYPE_TICKETS = 'tickets';
    public const TYPE_MAIL_CAP = 'mail_cap';
    public const TYPE_SMS_CAP = 'sms_cap';
    public const TYPE_INTERNAL = 'internal';
    public const TYPE_MAIL_FUP = 'mail_fup';
    public const TYPE_SMS_FUP = 'sms_fup';
    public const TYPE_REPORT_STATEMENTS = 'report_statements';

    public function rules()
    {
        return [
            [['type', 'title'], 'required'],
            [['title'], 'string', 'min' => 3, 'max' => 128],
            [['description'], 'string', 'max' => 512],
            [['type'], 'in', 'range' => self::getTemplateTypes()],
        ];
    }

    /**
     * get template array
     * @return array
     */
    public function getTemplateTypes()
    {
        return [
            self::TYPE_CUSTOMER_PORTAL,
            self::TYPE_INVOICE_PDF,
            self::TYPE_MAIL,
            self::TYPE_SMS,
            self::TYPE_DOCUMENTS,
            self::TYPE_CARDS,
            self::TYPE_PAYMENT_CALENDAR,
            self::TYPE_PAYMENT_RECEIPT,
            self::TYPE_REQUEST_PDF,
            self::TYPE_REMINDER_MAIL,
            self::TYPE_REMINDER_SMS,
            self::TYPE_MONITORING_NOTIFICATION_MAIL,
            self::TYPE_MONITORING_NOTIFICATION_SMS,
            self::TYPE_EXPORTS,
            self::TYPE_TICKETS,
            self::TYPE_MAIL_CAP,
            self::TYPE_SMS_CAP,
            self::TYPE_INTERNAL,
            self::TYPE_MAIL_FUP,
            self::TYPE_SMS_FUP,
            self::TYPE_REPORT_STATEMENTS,
        ];
    }

    /**
     * Render Template including Customer data.
     * @param $customer_id
     * @param $template_id
     * @return null|string
     * @throws \yii\base\InvalidParamException
     */
    public function renderTemplate($customer_id, $template_id)
    {
        if (!$customer_id || !$template_id) {
            throw new InvalidParamException('customer_id an render_id must be set');
        }
        $result = ApiHelper::getInstance()->get(static::$apiUrl, "{$template_id}-render-{$customer_id}");
        if ($result['result'] && !empty($result['response'])) {
            return isset($result['response']['result']) ? $result['response']['result'] : null;
        }
        return null;
    }
}
