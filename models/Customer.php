<?php

namespace splynx\models;

use splynx\base\BaseApiModel;
use splynx\helpers\ApiHelper;
use splynx\models\customer\CustomerBilling;
use splynx\models\finance\Invoices;
use splynx\models\finance\Requests;
use yii\base\NotSupportedException;

/**
 * Class Customer
 * @package splynx\models
 * @deprecated
 */
class Customer extends BaseApiModel implements \yii\web\IdentityInterface
{
    public $id;
    public $login;
    public $partner_id;
    public $location_id;
    public $category;
    public $password;
    public $name;
    public $email;
    public $phone;
    public $street_1;
    public $zip_code;
    public $city;
    public $status;
    public $date_add;
    public $last_online;
    public $last_update;
    public $additional_attributes = [];

    public static $apiCall = 'admin/customers/customer';

    private static $_customers = [];

    /**
     * Find customer by id
     *
     * @param integer $id
     * @return static|null
     * @throws \yii\base\InvalidConfigException
     */
    public static function findById($id)
    {
        if (!isset(self::$_customers[$id])) {
            $result = ApiHelper::getInstance()->get(self::$apiCall . '/' . $id);

            if ($result['result'] == false || empty($result['response'])) {
                return null;
            }

            $model = new static();
            static::populate($model, $result['response']);
            self::$_customers[$id] = $model;
        }

        return self::$_customers[$id];
    }

    public function getFullTitle()
    {
        return $this->name . ' (login: ' . $this->login . ', id: ' . $this->id . ')';
    }

    private $_billing;

    public function getBilling()
    {
        if ($this->_billing === null) {
            $this->_billing = CustomerBilling::getById($this->id);
        }

        return $this->_billing;
    }

    private $_invoices;

    /**
     * Get customer`s invoices
     *
     * @return Invoices[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getInvoices()
    {
        if ($this->_invoices === null) {
            $this->_invoices = Invoices::findByCustomerId($this->id);
        }

        return $this->_invoices;
    }

    private $_requests;

    /**
     * Get customer`s requests
     * @return Requests[]
     * @throws \yii\base\InvalidConfigException
     */
    public function getRequests()
    {
        if ($this->_requests === null) {
            $this->_requests = Requests::findByCustomerId($this->id);
        }

        return $this->_requests;
    }

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public static function findIdentity($id)
    {
        return static::findById($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->login . $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->login . $this->password === $authKey;
    }
}
